<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>
    <main class="main">
        <div class="page-404">
            <div class="page-404__content">
                <div class="page-404__img">
                    <span class="page-404__num">4</span>
                    <img class="page-404__pic" src="/upload/images/p404-pizza.jpg" width="292" alt="">
                    <span class="page-404__num">4</span>
                </div>
                <h4 class="h4 title title_middle page-404__title">Страница не найдена</h4>
                <a href="<?=SITE_DIR?>" class="btn  btn_default btn_yellow">
                    Вернуться на главную страницу
                </a>
            </div>
        </div>
    </main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>