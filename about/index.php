<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(" ");
?><div class="about section_t-offset">
	<div class="about__item">
		<div class="about__bg">
<svg viewBox="0 0 154 16" id="icon-wave"><path d="M139.56 5.69543C136.49 8.14917 133.841 10.2667 128.323 10.2667C122.805 10.2667 120.153 8.14917 117.086 5.69543C113.747 3.0261 109.964 0 102.644 0C95.3286 0 91.5479 3.0261 88.2086 5.69543C85.144 8.14917 82.4978 10.2667 76.982 10.2667C71.4663 10.2667 68.8175 8.14917 65.7503 5.69543C62.4136 3.0261 58.6304 0 51.3128 0C43.9952 0 40.2145 3.0261 36.8779 5.698C33.8133 8.14917 31.167 10.2667 25.6538 10.2667C20.1406 10.2667 17.4944 8.14917 14.4298 5.69543C11.0931 3.0261 7.315 0 0 0V5.13333C5.51063 5.13333 8.15687 7.25083 11.2215 9.702C14.5581 12.3739 18.3388 15.4 25.6538 15.4C32.9688 15.4 36.7495 12.3739 40.0888 9.70457C43.1508 7.25083 45.797 5.13333 51.3128 5.13333C56.8286 5.13333 59.4774 7.25083 62.5445 9.70457C65.8812 12.3739 69.6645 15.4 76.982 15.4C84.2996 15.4 88.0803 12.3739 91.417 9.702C94.4816 7.25083 97.1278 5.13333 102.644 5.13333C108.162 5.13333 110.813 7.25083 113.88 9.70457C117.217 12.3739 121.003 15.4 128.323 15.4C135.643 15.4 139.426 12.3739 142.766 9.70457C145.833 7.25083 148.482 5.13333 154 5.13333V0C146.68 0 142.897 3.0261 139.56 5.69543Z" fill-rule="evenodd"></path></svg>
		</div>
		<div class="row">
			<div class="col col--lg-6">
				<div class="about__pic">
					<div class="about__img-wrap">
 <img alt="about1.jpg" src="/upload/medialibrary/e79/e79457ade8232ce91eb6e80b0e8c4442.jpg" title="about1.jpg">
                        <a href="/menu/" class="about__img-badge">
						<div class="icon">
 <img width="42" src="/upload/images/i-pizza.svg">
						</div>
						 Посмотреть меню </a>
					</div>
				</div>
			</div>
			<div class="col col--lg-6">
				<div class="about__text">
					<h1 class="section__title">У нас вкусно</h1>
					<h3 class="section__title_sec">Синьор Помидор радует Вас с&nbsp;2003&nbsp;г.</h3>
					<p>
						 Синьор Помидор - это сочетание традиционной итальянской кухни с современной европейской и экзотической японской кухней.
					</p>
					<p>
						 В меню Синьор Помидор - пицца, салаты, роллы, паста, супы, бургеры, закуски и десерты.
					</p>
					<p>
						 В нашем кафе каждый найдет свое любимое блюдо.
					</p>
					<div class="logos">
 <img width="214" alt="logo_sp.png" src="/upload/medialibrary/4c9/4c95b3bf0c70c5b7613efe7685b1cb54.png" height="auto" title="logo_sp.png"> <img width="76" alt="logo_jp.png" src="/upload/medialibrary/31f/31f8164a9576c7482a2bf0ce72777d15.png" height="auto" title="logo_jp.png">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="about__item">
		<div class="about__bg">
<svg viewBox="0 0 154 16" id="icon-wave"><path d="M139.56 5.69543C136.49 8.14917 133.841 10.2667 128.323 10.2667C122.805 10.2667 120.153 8.14917 117.086 5.69543C113.747 3.0261 109.964 0 102.644 0C95.3286 0 91.5479 3.0261 88.2086 5.69543C85.144 8.14917 82.4978 10.2667 76.982 10.2667C71.4663 10.2667 68.8175 8.14917 65.7503 5.69543C62.4136 3.0261 58.6304 0 51.3128 0C43.9952 0 40.2145 3.0261 36.8779 5.698C33.8133 8.14917 31.167 10.2667 25.6538 10.2667C20.1406 10.2667 17.4944 8.14917 14.4298 5.69543C11.0931 3.0261 7.315 0 0 0V5.13333C5.51063 5.13333 8.15687 7.25083 11.2215 9.702C14.5581 12.3739 18.3388 15.4 25.6538 15.4C32.9688 15.4 36.7495 12.3739 40.0888 9.70457C43.1508 7.25083 45.797 5.13333 51.3128 5.13333C56.8286 5.13333 59.4774 7.25083 62.5445 9.70457C65.8812 12.3739 69.6645 15.4 76.982 15.4C84.2996 15.4 88.0803 12.3739 91.417 9.702C94.4816 7.25083 97.1278 5.13333 102.644 5.13333C108.162 5.13333 110.813 7.25083 113.88 9.70457C117.217 12.3739 121.003 15.4 128.323 15.4C135.643 15.4 139.426 12.3739 142.766 9.70457C145.833 7.25083 148.482 5.13333 154 5.13333V0C146.68 0 142.897 3.0261 139.56 5.69543Z" fill-rule="evenodd"></path></svg>
		</div>
		<div class="row">
			<div class="col col--lg-6">
				<div class="about__pic">
					<div class="about__img-wrap">
 <img alt="about2.jpg" src="/upload/medialibrary/4dc/4dc808e0cc7b000babecf21cd1c2b65b.jpg" title="about2.jpg">
                        <a href="/restaurants/" class="about__img-badge">
						<div class="icon">
 <img width="42" src="/upload/images/i-map.svg">
						</div>
						 Наши кафе </a>
					</div>
				</div>
			</div>
			<div class="col col--lg-6">
				<div class="about__text">
					<h1 class="section__title">У нас уютно</h1>
					<h3 class="section__title_sec">Вкусные блюда итальянской и японской кухни,комфортный интерьер и&nbsp;быстрое обслуживание!</h3>
					<p>
						 Синьор Помидор - для друзей<br>
						 Синьор Помидор - для семьи<br>
						 Синьор Помидор - для любимых<br>
						 Синьор Помидор - для событий<br>
						 Синьор Помидор - для вас
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="about__item">
		<div class="about__bg">
<svg viewBox="0 0 154 16" id="icon-wave"><path d="M139.56 5.69543C136.49 8.14917 133.841 10.2667 128.323 10.2667C122.805 10.2667 120.153 8.14917 117.086 5.69543C113.747 3.0261 109.964 0 102.644 0C95.3286 0 91.5479 3.0261 88.2086 5.69543C85.144 8.14917 82.4978 10.2667 76.982 10.2667C71.4663 10.2667 68.8175 8.14917 65.7503 5.69543C62.4136 3.0261 58.6304 0 51.3128 0C43.9952 0 40.2145 3.0261 36.8779 5.698C33.8133 8.14917 31.167 10.2667 25.6538 10.2667C20.1406 10.2667 17.4944 8.14917 14.4298 5.69543C11.0931 3.0261 7.315 0 0 0V5.13333C5.51063 5.13333 8.15687 7.25083 11.2215 9.702C14.5581 12.3739 18.3388 15.4 25.6538 15.4C32.9688 15.4 36.7495 12.3739 40.0888 9.70457C43.1508 7.25083 45.797 5.13333 51.3128 5.13333C56.8286 5.13333 59.4774 7.25083 62.5445 9.70457C65.8812 12.3739 69.6645 15.4 76.982 15.4C84.2996 15.4 88.0803 12.3739 91.417 9.702C94.4816 7.25083 97.1278 5.13333 102.644 5.13333C108.162 5.13333 110.813 7.25083 113.88 9.70457C117.217 12.3739 121.003 15.4 128.323 15.4C135.643 15.4 139.426 12.3739 142.766 9.70457C145.833 7.25083 148.482 5.13333 154 5.13333V0C146.68 0 142.897 3.0261 139.56 5.69543Z" fill-rule="evenodd"></path></svg>
		</div>
		<div class="row">
			<div class="col col--lg-6">
				<div class="about__pic">
					<div class="about__img-wrap">
 <img alt="about3.jpg" src="/upload/medialibrary/dfe/dfe0afe2a75dfb166180775360ccf7a1.jpg" title="about3.jpg">
                        <a href="/dostavka-i-oplata/samovyvoz.php" class="about__img-badge">
						<div class="icon">
 <img width="42" src="/upload/images/i-map.svg">
						</div>
						 Мы на карте </a>
					</div>
				</div>
			</div>
			<div class="col col--lg-6">
				<div class="about__text">
					<h1 class="section__title">Мы рядом</h1>
					<h3 class="section__title_sec">Синьор Помидор радует Вас с&nbsp;2003&nbsp;г.</h3>
					<p>
						 Кафе расположены во многих районах районах города.
					</p>
					<p>
						 Ближайшее к вам вы можете найти на карте.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="about__item">
		<div class="about__bg">
<svg viewBox="0 0 154 16" id="icon-wave"><path d="M139.56 5.69543C136.49 8.14917 133.841 10.2667 128.323 10.2667C122.805 10.2667 120.153 8.14917 117.086 5.69543C113.747 3.0261 109.964 0 102.644 0C95.3286 0 91.5479 3.0261 88.2086 5.69543C85.144 8.14917 82.4978 10.2667 76.982 10.2667C71.4663 10.2667 68.8175 8.14917 65.7503 5.69543C62.4136 3.0261 58.6304 0 51.3128 0C43.9952 0 40.2145 3.0261 36.8779 5.698C33.8133 8.14917 31.167 10.2667 25.6538 10.2667C20.1406 10.2667 17.4944 8.14917 14.4298 5.69543C11.0931 3.0261 7.315 0 0 0V5.13333C5.51063 5.13333 8.15687 7.25083 11.2215 9.702C14.5581 12.3739 18.3388 15.4 25.6538 15.4C32.9688 15.4 36.7495 12.3739 40.0888 9.70457C43.1508 7.25083 45.797 5.13333 51.3128 5.13333C56.8286 5.13333 59.4774 7.25083 62.5445 9.70457C65.8812 12.3739 69.6645 15.4 76.982 15.4C84.2996 15.4 88.0803 12.3739 91.417 9.702C94.4816 7.25083 97.1278 5.13333 102.644 5.13333C108.162 5.13333 110.813 7.25083 113.88 9.70457C117.217 12.3739 121.003 15.4 128.323 15.4C135.643 15.4 139.426 12.3739 142.766 9.70457C145.833 7.25083 148.482 5.13333 154 5.13333V0C146.68 0 142.897 3.0261 139.56 5.69543Z" fill-rule="evenodd"></path></svg>
		</div>
		<div class="row">
			<div class="col col--lg-6">
				<div class="about__pic">
					<div class="about__img-wrap">
 <img alt="about4.jpg" src="/upload/medialibrary/97d/97da5926c734264a598a6be6bb217893.jpg" title="about4.jpg">
                        <a href="/menu-devilery/" class="about__img-badge">
						<div class="icon">
 <img width="42" src="/upload/images/i-pizza1.svg">
						</div>
						 Меню доставки </a>
					</div>
				</div>
			</div>
			<div class="col col--lg-6">
				<div class="about__text">
					<h1 class="section__title">Мы доставляем</h1>
					<h3 class="section__title_sec">Синьор Помидор радует Вас с&nbsp;2003&nbsp;г.</h3>
					<p>
						 А если, вы хотите порадовать себя и своих близких, закажите доставку блюд домой.
					</p>
					<p>
						 Мы с удовольствием их доставим.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="about window window_bordered window_shadow window_s-offset">
	<div class="row">
		<div class="col--md-6 col--lg-7">
			<p class="black">
				 Наше первое кафе с настоящей итальянской пиццей открылось в Хабаровске в ноябре 2003 года. Сейчас "Синьор Помидор" - это десять кафе (в Хабаровске и Комсомольске-на-Амуре) и отдельное подразделение службы доставки.
			</p>
			<p>
				 Кроме того, в 2012 году было открыто новое направление "Японский помидор" с блюдами традиционной японской кухни.
			</p>
			<div class="row about__info">
				<div class="col--md-6 col--lg-4 about__info-item">
					<div class="section__title">
						 2003
					</div>
					<p class="mb-0">
						 Открытие<br>
						 первого кафе
					</p>
				</div>
				<div class="col--md-6 col--lg-4 about__info-item">
					<div class="section__title">
						 10
					</div>
					<p class="mb-0">
						 Кафе Итальянской<br>
						 и Японской кухни
					</p>
				</div>
			</div>
		</div>
		<div class="col--md-6 col--lg-5 text-center">
			<div class="logos">
 <img width="350" alt="logo_sp.png" src="/upload/medialibrary/4c9/4c95b3bf0c70c5b7613efe7685b1cb54.png" title="logo_sp.png"><img width="142" alt="logo_jp.png" src="/upload/medialibrary/31f/31f8164a9576c7482a2bf0ce72777d15.png" title="logo_jp.png">
			</div>
		</div>
	</div>
</div>
<div class="section section_t-offset text-center">
	<h2 class="title">История</h2>
    <div class="history history-slider section_t-offset col--md-offset-1 col--md-10">
        <div class="history__item">
            <div class="history__item-wrap match-height">
                <div class="title mb-1">2003</div>
                <p>Открытие первого кафе в&nbsp;Хабаровске</p>
            </div>
        </div>
        <div class="history__item">
            <div class="history__item-wrap match-height">
                <div class="title mb-1">2004</div>
                <p>Зарегистрирован товарный знак "Синьор Помидор"</p>
            </div>
        </div>
        <div class="history__item">
            <div class="history__item-wrap match-height">
                <div class="title mb-1">2005</div>
                <p>Открытие третьего кафе в&nbsp;Хабаровске.</p>
            </div>
        </div>
        <div class="history__item">
            <div class="history__item-wrap match-height">
                <div class="title mb-1">2006</div>
                <p>Открытие четвертого кафе в&nbsp;Хабаровске.</p>
            </div>
        </div>
    </div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>