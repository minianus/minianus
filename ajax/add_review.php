<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];


function result_error($error){
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;

}

$restoran = $_REQUEST['restoran'];
$name = $_REQUEST['name'];
$phone = $_REQUEST['phone'];
$email = $_REQUEST['email'];
$feedback = $_REQUEST['feedback'];
$agree = $_REQUEST['agree'];

if($name == ''){
    result_error('Заполните имя.');
}
if($feedback == ''){
    result_error('Заполните отзыв.');
}
if($agree == ''){
    result_error('Поставьте отметку согласия с обработкой персональных данных.');
}

if(!$result['error']){

    $arLoadArray = array(
        "IBLOCK_ID" => get_iblock_id_by_code('reviews'),
        "PROPERTY_VALUES" => [],
        "NAME" => "Заявка от ". $name . ' '. date("d.m.Y H:i:s"),
        "PREVIEW_TEXT" =>   'ресторан: ' . $restoran . "\n" . 
                            'имя: ' . $name . "\n" . 
                            'телефон:' . $phone . "\n" . 
                            'email: ' . $email ,
        "DETAIL_TEXT" => $feedback,
        "ACTIVE" => "N",
    );
    CModule::IncludeModule('iblock');
    $el = new CIBlockElement;
    if ($element_id = $el->Add($arLoadArray)) {
        $result['id'] = $element_id;

        $mail_data = [
            "CAFE" => $restoran,
            "NAME" => $name,
            "PHONE" => $phone,
            "EMAIL" => $EMAIL,
            "FEEDBACK" => $feedback,
            "ID" => $element_id,
        ];
        CEvent::Send('NEW_FEEDBACK', SITE_ID, $mail_data);
    }

}

echo json_encode($result);
