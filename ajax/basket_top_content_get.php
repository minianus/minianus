<?require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

require $_SERVER['DOCUMENT_ROOT'] . '/api/api.php';

$api = new order_api();

$action = $_GET['action'];
$item_id = $_GET['item_id'];

switch($action){
    case 'remove_item': {
        $api->remove_item($item_id);
        break;
    }
    case 'inc_item': {
        $api->inc_item_count($item_id);
        break;
    }
    case 'dec_item': {
        $api->dec_item_count($item_id);
        break;
    }
    case 'clean_basket': {
        $api->clean_basket();
        break;
    }
    case 'add_item': {
        $api->add_to_basket($_GET);
        break;
    }
}
$basket_data = $api->get_basket_data();
$recomendations = $api->get_recomendations();

$link = '/personal/cart';
    if (count($basket_data['ITEMS']) > 0) {
        $class = "popup-call";
        $link = "#popup-cart";
    }

ob_start();
?>

<a class="btn header__btn btn__cart <?=$class?>" href="<?=$link?>">
    <svg class="icon icon-cart-h">
        <svg viewBox="0 0 69.65 73.8" id="icon-cart-h">
            <g id="Слой_2" data-name="Слой 2">
                <g id="Layer_1" data-name="Layer 1">
                    <path d="M34.77 3.5c-21.2 0-21.7 25.3-21.7 25.3h43.4S56 3.5 34.77 3.5z" fill="none"
                        stroke-miterlimit="10" stroke-width="7"></path>
                    <path
                        d="M68.67 30a4.84 4.84 0 00-4.8-4H5.77A4.84 4.84 0 001 30C.2 34.5-.7 42.4 1 50.2c.1 0 .1 0 .2.1s10.7 3.3 29 4.8c1.7.1 2.9 2.6 2.8 4.2s-1.4 3.8-3 3.8h-.2C17.87 62.2 8.87 61 3.67 59 8 67.7 17 73.8 34.87 73.8c38.2 0 35.9-32.7 33.8-43.8z">
                    </path>
                </g>
            </g>
        </svg>
    </svg>

    <span class="cart__count">
        <?echo count($basket_data['ITEMS']) ?>
    </span>


</a>
<?
$delivery_price = get_delivery_price();
$basket_icon = ob_get_contents();
ob_end_clean();

$basket_mob_items_count = count($basket_data['ITEMS']);
ob_start();
?>
<?if($basket_data['TOTAL']['DISCOUNT_PRICE'] > 0){?>
<div class="window__header window__header_sb">
    <h4 class="h4 window__title text-center">
        Корзина
    </h4>

    <a href="#" class="link link_decorated" onclick="do_basket_action(event, {action: 'clean_basket'})">Очистить корзину</a>
</div>
<?}else{?>
    Ваша корзина пуста. Чтобы сделать заказ, выберите блюдо в <a href="/menu-devilery/" class="header__link">меню доставки</a>
    <?}?>
<div class="popup-cart">
    <hr class="w-total__hr">
    <div class="comporison-list">
        <?foreach($basket_data['ITEMS'] as $item){?>
        <div class="comporison">
            <div class="product-lite">
                <a href="#" class="product-lite__img">
                    <img width="152" src="<?=$item['PICTURE']?>" alt="">
                </a>
                <div class="product-lite__content">
                    <a href="#" class="basket-item__remove"
                        onclick="do_basket_action(event, {action: 'remove_item', item_id: <?=$item['ID']?>})"></a>
                    <a href="#" class="product-lite__title"><?=$item['NAME']?></a>
                    <span class="product-lite__weight">
                        <?=$item['DESC']?>
                    </span>
                    <div class="order-edited">
                        <?if($item['ADD_ING']){?>
                        <span class="order-edited__item order-edited__item_offset">
                            <i class="order-edited__ico order-edited__ico_plus"></i>
                            <span class="order-edited__title">
                                <?=$item['ADD_ING']?>
                            </span>
                        </span>
                        <?}?>
                        <?if($item['REMOVE_ING']){?>
                        <span class="order-edited__item order-edited__item_offset">
                            <i class="order-edited__ico order-edited__ico_minus"></i>
                            <span class="order-edited__title">
                                <?=$item['REMOVE_ING']?>
                            </span>
                        </span>
                        <?}?>
                    </div>
                    <div class="product-lite__price">
                        <div class="counter">
                            <button class="counter__btn counter__btn_minus"
                                onclick="do_basket_action(event, {action: 'dec_item', item_id: <?=$item['ID']?>})"></button>
                            <input type="text" class="counter__input" value="<?=$item['QUANTITY']?>">
                            <button class="counter__btn counter__btn_plus "
                                onclick="do_basket_action(event, {action: 'inc_item', item_id: <?=$item['ID']?>})"></button>
                        </div>
                        <span class="price comporison__price medium green">
                            <?=$item['FULL_PRICE'] * $item['QUANTITY']?> <span class="rub">i</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <?}?>
    </div>
    <div class="product-lite__title">Рекомендуем</div>
    <div class="cart-slider">
        <?foreach($recomendations as $item){?>
        <div class="cart-slider__item">
            <img src="<?=$item['picture']?>" alt="">
            <div class="product-lite__title"><?=$item['name']?></div>
            <span><?=$item['price']?> <span class="rub">i</span></span>
            <a href="#" class="btn btn_default btn_yellow_bordered"
                onclick="do_basket_action(event, {action: 'add_item', item_id: <?=$item['id']?> })">В корзину</a>
        </div>
        <?}?>

    </div>
    <?if($basket_data['TOTAL']['DISCOUNT_PRICE'] > 0){?>
    <div class="grey-bg">
        <span class="notif-title">
            <?
            if($delivery_price['STEP_TO_FREE'] > 0){?>
            До бесплатной доставки осталось <b><?=$delivery_price['STEP_TO_FREE']?>&nbsp;<span class="rub">i</span></b>
            <?}else{?>
            Бесплатная доставка!
            <?}?>
        </span>
        <div class="w-total__content">
            <div class="key-val w-total__line">
                <span class="key w-total__key">
                    Товары
                </span>
                <span class="val w-total__val green">
                    <?=$basket_data['TOTAL']['ITEMS_PRICE']?> <span class="rub">i</span>
                </span>
            </div>
            <div class="key-val w-total__line">
                <span class="key w-total__key">
                    С учетом скидки
                </span>
                <span class="val w-total__val green">
                    <?=$basket_data['TOTAL']['DISCOUNT_PRICE']?> <span class="rub">i</span>
                </span>
            </div>
            <div class="key-val w-total__line">
                <span class="key w-total__key">
                    Доставка
                </span>
                <span class="val w-total__val green">
                    <?=$basket_data['TOTAL']['DELIVERY_PRICE']?> <span class="rub">i</span>
                </span>
            </div>

        </div>
        <div class="w-result w-result_s">
            <span class="w-result__title">
                Итог
            </span>
            <span class="val w-total__val">
                <?=$basket_data['TOTAL']['TOTAL_PRICE']?> <span class="rub">i</span>
            </span>
        </div>
        <a href="/personal/cart" class="btn w-result__btn btn_default btn_yellow">
            Далее
        </a>
    </div>
    <?}?>
</div>


<?
$popup_content = ob_get_contents();
ob_end_clean();

if($delivery_price['STEP_TO_FREE'] > 0){
    $delivery_summ_message = '
    <span class="menu__count-number">' . $delivery_price['STEP_TO_FREE'] . ' руб</span><span class="menu__count-text">до бесплатной доставки</span>
    ';
}else{
    $delivery_summ_message = '
    <span class="menu__count-number">Бесплатная доставка!</span>
    ';
}


$header_price = $basket_data['TOTAL']['ITEMS_PRICE'] . ' <span class="rub">i</span>';

$result = [
    'basket_icon' => $basket_icon,
    'popup_content' => $popup_content,
    'delivery_summ_message' => $delivery_summ_message,
    'header_price' => $header_price,
    'mob_items_count' => $basket_mob_items_count
];

echo json_encode($result);
?>