<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];

CModule::IncludeModule('iblock');

function result_error($error){
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;

}


$company_name = $_REQUEST['company_name'];
$phone = $_REQUEST['phone'];
$agree = $_REQUEST['agree'];

if($company_name == ''){
    result_error('Заполните номер телефона.');
}

if($phone == ''){
    result_error('Заполните название компании.');
}

if($agree != 'Y'){
    result_error('Поставьте отметку согласия на обработку персональных данных.');
}

if(!$result['error']){

    $commerce_presentation = $_FILES['commerce_presentation'];

    $arLoadArray = array(
        "IBLOCK_ID" => get_iblock_id_by_code('cooperation_form'),
        "PROPERTY_VALUES" => [
            'PHONE' => $phone,
            'COMPANY_NAME' => $company_name,
            'COMMERCE_PRESENTATION' => $commerce_presentation,
        ],
        "NAME" => "Заявка на сотрудничество от ". $name . ' '. date("d.m.Y H:i:s"),
        "ACTIVE" => "N",
    );
    
    $el = new CIBlockElement;
    if ($element_id = $el->Add($arLoadArray)) {
        $result['id'] = $element_id;

        $mail_data = [
            'PHONE' => $phone,
            'COMPANY_NAME' => $company_name,
            "ID" => $element_id,
        ];
        CEvent::Send('COOPERATION_FEEDBACK', SITE_ID, $mail_data);
    }

}

echo json_encode($result);
