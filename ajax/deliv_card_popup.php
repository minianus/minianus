<?require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$result = [
    'errors' => [],
    'error' => false,
    'html' => '',
];

$el_id = $_REQUEST['id'];

$filter = [
    'IBLOCK_ID' => get_iblock_id_by_code('menu-devilery'),
    'ID' => $el_id,
    'ACTIVE' => 'Y',
];
$select = [
    'ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'PROPERTY_SHOT_ABOUT',
    'PROPERTY_COOKING_CAFE', 'PROPERTY_INGRIDIENTS_FOR_REMOVING', 'IBLOCK_SECTION_ID',
];

$res = CIBlockElement::GetList(array(), $filter, false, false, $select);

$element = null;

while ($ob = $res->GetNext()) {
    $element = $ob;
}

if ($element == null) {
    $result['error'] = true;
    $result['errors'][] = 'Элемент с id=' . $el_id . ' не существует';
} else {

    $price = CCatalogProduct::GetOptimalPrice($el_id);

    $element['price'] = $price;
    $img = CFile::getPath($element['DETAIL_PICTURE']);

//выбираем готовящие рестораны
    $restoran_section = get_section_id_by_code(get_city(), get_iblock_id_by_code('restaurants'));
    $restorans = [];
    if (count($element['PROPERTY_COOKING_CAFE_VALUE']) > 0) {
        $filter = [
            'IBLOCK_ID' => get_iblock_id_by_code('restaurants'),
            'ID' => $element['PROPERTY_COOKING_CAFE_VALUE'],
            'SECTION_ID' => $restoran_section,
            'ACTIVE' => 'Y',
        ];
        $select = [
            'ID', 'IBLOCK_ID', 'NAME',
        ];

        $res = CIBlockElement::GetList(array(), $filter, false, false, $select);

        while ($item = $res->getNext()) {
            $restorans[] = $item['NAME'];
        }
    }

    $element['restorans'] = $restorans;

//выбираем добавки которыми можно дополнить заказ
    $add_ing_ids = [];
    $filter = [
        'IBLOCK_ID' => get_iblock_id_by_code('menu-devilery'),
        'ID' => $element['IBLOCK_SECTION_ID'],
    ];
    $select = ['UF_ADD_ING'];

    $res = CIBlockSection::GetList([], $filter, true, $select);
    while ($item = $res->getNext()) {
        $add_ing_ids = $item['UF_ADD_ING'];
    }

    $add_ing = [];

    if (count($add_ing_ids) > 0) {
        $filter = [
            'IBLOCK_ID' => get_iblock_id_by_code('add_ingridients'),
            'ID' => $add_ing_ids,
            'ACTIVE' => 'Y',
        ];
        $select = [
            'ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE',
        ];

        $res = CIBlockElement::GetList(array(), $filter, false, false, $select);

        while ($ob = $res->GetNext()) {
            $ob['price'] = CCatalogProduct::GetOptimalPrice($ob['ID']);
            $add_ing[] = $ob;
        }
    }

    $element['add_ing'] = $add_ing;

//выбираем ингридиенты которые можно удалить из заказа
    $ing_for_removing = [];
    if (count($element['PROPERTY_INGRIDIENTS_FOR_REMOVING_VALUE']) > 0) {
        $filter = [
            'IBLOCK_ID' => get_iblock_id_by_code('ingridients_for_removing'),
            'ID' => $element['PROPERTY_INGRIDIENTS_FOR_REMOVING_VALUE'],
            'ACTIVE' => 'Y',
        ];
        $select = [
            'ID', 'IBLOCK_ID', 'NAME',
        ];

        $res = CIBlockElement::GetList(array(), $filter, false, false, $select);

        while ($item = $res->getNext()) {
            $ing_for_removing[] = $item;
        }
    }

    $element['ing_for_removing'] = $ing_for_removing;

    $delivery_price = get_delivery_price()['STEP_TO_FREE'];

    $delivery_desc = '';
    $filter = ['IBLOCK_ID' => get_iblock_id_by_code('delivery_description'), 'CODE' => get_city()];
    $select = ['PREVIEW_TEXT'];
    $res = CIBlockElement::GetList(array(), $filter, false, false, $select);

        while ($item = $res->getNext()) {
            $delivery_desc = $item['PREVIEW_TEXT'];
        }

    ob_start();
/*
echo '<pre>';
echo print_r($element);
echo '</pre>';
 */
    ?>

<div class="row card__row" id="deliv_item_card" data-id="<?=$el_id?>">
<meta name="item_price" value="<?=$element['price']['RESULT_PRICE']['DISCOUNT_PRICE']?>">
    <div class="col col--lg-6">
        <a href="#!" class="card__img">
            <img class="card__pic mb-3" src="<?=$img?>" alt="">
        </a>
        <div class="card__delivery">
            <div class="card__delivery__item card__delivery-info">
                <div class="delivery-notif">
                    <div class="delivery-notif__ico">
                        <img class="delivery-notif__pic" src="/upload/images/p-bike.svg" alt="">
                    </div>

					<?=$delivery_desc?>

                </div>
                <b>Доступна доставка</b>
            </div>
            <div class="card__delivery__item card__delivery-info">
                <div class="delivery-notif">
                    <div class="delivery-notif__ico delivery-notif__ico_green">
                        <img class="delivery-notif__pic" src="/upload/images/take-away-ico.svg" alt="">
                    </div>

                    <span class="delivery-notif__title">Этот товар можно забрать по адресу:</span>

                    <div class="delivery-notif__links">
					<?foreach ($element['restorans'] as $restoran) {?>
                        <a href="#!" class="delivery-notif__link">
                            <?=$restoran?>
                        </a>
                    <?}?>
                    </div>

                </div>
                <b>Самовывоз из <?=count($element['restorans'])?> кафе</b>
            </div>
        </div>

    </div>
    <div class="col col--lg-6">
        <div class="card__content">
            <a href="#!" class="about__img-badge">
                <div class="icon"><button class="counter__btn counter__btn_plus "></button></div>
                <div class="text" id="non_free_delivery" <?=($delivery_price == 0)?' style="display: none;" ':''?>>
                    <b id="step_for_free_delivery"><?=$delivery_price?></b> <b>рублей</b><br>
                    до бесплатной доставки
                </div>
                <div class="text" <?=($delivery_price > 0)?' style="display: none;" ':''?> id="delivery_is_free">Бесплатная доставка!</div>
            </a>
            <div class="card__header">
                <h1 class="card__title"><?=$element['NAME']?></h1>
                <span class="card__desc">
					<?=$element['PROPERTY_SHOT_ABOUT_VALUE']?>
                </span>
            </div>
            <div class="box box_b-large">
                <div class="card__desc">
                    <div class="info">
                        <p><?=$element['DETAIL_TEXT']?></p>
                    </div>
                </div>
            </div>
            <div class="box box_b-large">
                <div class="add-w" id="deliv_menu_item_add_ing">
                    <h3 class="h3 h3_b-middle add-w__title">Добавить в пиццу</h3>
                    <div class="add-w__btns">
					<?foreach ($element['add_ing'] as $add_ing) {
        $pic = resize($add_ing['PREVIEW_PICTURE'], 205, 163);
        ?>
                        <a href="#" class="add-w__pull">
                            <img class="add-w__pic" src="<?=$pic?>" alt="">
                            <div class="add-w__title"><?=$add_ing['NAME']?></div>
                            <div class="add-w__pull-add">
                                <span class="add-w__price"><?=$add_ing['price']['RESULT_PRICE']['DISCOUNT_PRICE']?> р.</span>
                                <button
                                class="counter__btn counter__btn_plus "
                                data-id="<?=$add_ing['ID']?>"
                                data-price="<?=$add_ing['price']['RESULT_PRICE']['DISCOUNT_PRICE']?>"
                                onclick="add_ing_plus(event)"
                                >
                                </button>
                                <button
                                class="counter__btn counter__btn_minus no-active"
                                data-id="<?=$add_ing['ID']?>"
                                data-price="<?=$add_ing['price']['RESULT_PRICE']['DISCOUNT_PRICE']?>"
                                onclick="add_ing_minus(event)"
                                >
                                </button>
                            </div>
                        </a>
                    <?}?>
                    </div>
                </div>
            </div>
            <div class="box box_b-large">
                <div class="add-w">
                    <h3 class="h3 h3_b-middle add-w__title">Убрать из блюда</h3>
                    <div class="add-w__btns add-w__btns_remove">
					<?foreach ($element['ing_for_removing'] as $ing) {?>
                        <a 
                        href="#" class="add-w__btn btn btn_medium btn_yellow btn_dark"
                        data-id="<?=$ing['ID']?>"
                        onclick="remove_ing(event)"
                        >
                        <?=$ing['NAME']?>
                        </a>
					<?}?>


                    </div>
                </div>
            </div>

            <div class="card-nav">
                <div class="counter">
                    <button class="counter__btn counter__btn_minus" onclick="dec_item_count(event)"></button>
                    <input type="text" class="counter__input" name="item_count" value="1">
                    <button class="counter__btn counter__btn_plus " onclick="inc_item_count(event)"></button>
                </div>
                <span class="h3 green price medium" id="total_price">
                    <?=$element['price']['RESULT_PRICE']['DISCOUNT_PRICE']?> р.
                </span>
                <a href="#" class="btn card__btn btn_submit btn_green" onclick="addToBasket(event)">В корзину</a>
            </div>
        </div>
    </div>
</div>
<script>
function calc_delivery_price(){

    var total_price = calc_total_price();
    var data = {};
    data.action = 'get_delivery_price';
    data.virtual_price = total_price;

    $.ajax({
        url: '/api/handler.php',
        method: 'get',
        dataType: 'json',
        data,
        success: function(data) {
            
            if(data.STEP_TO_FREE == 0){
                $('#non_free_delivery').hide();
                $('#delivery_is_free').show();
            }else{
                $('#non_free_delivery').show();
                $('#delivery_is_free').hide();
                $('#step_for_free_delivery').html(data.STEP_TO_FREE);
            }
        },
        error: function(data) {
           
        }
    });
}

function dec_item_count(event){
    event.preventDefault();
    var item_count_input = $('#deliv_item_card').find('input[name="item_count"]');
    var count_val = item_count_input.val();
    if(count_val > 1){
        count_val--;
    }
    item_count_input.val(count_val);
    calc_delivery_price();
}

function inc_item_count(event){
    event.preventDefault();
    var item_count_input = $('#deliv_item_card').find('input[name="item_count"]');
    var count_val = item_count_input.val();
        count_val++;
    item_count_input.val(count_val);
    calc_delivery_price();
    
}

var basket_item_data = [];

function clear_item_data(){
    data = {
                add_ing:{},
                remove_ing:{}
        };
    localStorage.setItem("basket_item_data", JSON.stringify(data));
};

clear_item_data();

function calc_total_price(){
    var item_price = parseFloat($('#deliv_item_card meta[name=item_price]').attr('value'));
    var item_count = $('#deliv_item_card').find('input[name="item_count"]').val();
    var total_price_field = $('#total_price');
    var total_price = item_price;
    var basket_item_data = load_data();
    for(add_ing in basket_item_data.add_ing){
        if(basket_item_data.add_ing[add_ing].set == "1"){
            total_price += parseFloat(basket_item_data.add_ing[add_ing].price);
        }
        
    }
    total_price_field.html((total_price * item_count)+ ' р.');
    return total_price * item_count;

}

function addToBasket(event){
    event.preventDefault();
    var data = load_data();
    var item_id = $(event.currentTarget).closest('#deliv_item_card').attr('data-id');
    var item_count = $('#deliv_item_card').find('input[name="item_count"]').val();
    var free_delivery = $('#delivery_is_free');
    var non_free_delivery = $('#non_free_delivery');
    var step_to_free_delivery = $('#step_for_free_delivery');
    $.ajax({
        url: '/ajax/add_to_basket.php',
        method: 'post',
        dataType: 'json',
        data: { 
            data,
            item_id,
            item_count,
        },
        success: function(data) {
            if(data.step_to_free_delivery > 0){
                step_to_free_delivery.html(data.step_to_free_delivery);
            }else{
                non_free_delivery.hide();
                free_delivery.show();
            }
            Swal.fire({
                toast: true,
                position: 'center',
                showConfirmButton: false,
                timer: 2000,
                icon: 'success',
                title: 'Добавлено!'
                })
            calc_delivery_price();
            window.eventBus.fire('basket_changed');
            if (data.error == false) {
                
            } else {


            }
        },
        error: function(data) {
           
        }
    });
}

function load_data(){
    var res = localStorage.getItem("basket_item_data");

    if(res == '' || res == null){
        res = {
                add_ing:{},
                remove_ing:{}
        };
    }else{
        res = JSON.parse(res);
    }

    return res;
}

function save_data(data){

    data = JSON.stringify(data);
    localStorage.setItem("basket_item_data", data);
}

function add_ing_plus(event){

    event.preventDefault();
    var el_id = $('#deliv_item_card').attr('data-id');
    var ing_id = $(event.currentTarget).attr('data-id');
    var ing_price = $(event.currentTarget).attr('data-price');
    var minus_button = $(event.currentTarget).siblings('.counter__btn_minus');
    $(event.currentTarget).addClass('no-active');
    minus_button.removeClass('no-active');
    

    basket_item_data = load_data();
    basket_item_data.add_ing[ing_id] = {
        "set": 1,
        "price": ing_price
    };

    save_data( basket_item_data);
    calc_delivery_price();

}

function add_ing_minus(event){
    event.preventDefault();
    var el_id = $('#deliv_item_card').attr('data-id');
    var ing_id = $(event.currentTarget).attr('data-id');
    var ing_price = $(event.currentTarget).attr('data-price');
    var plus_button = $(event.currentTarget).siblings('.counter__btn_plus');
    $(event.currentTarget).addClass('no-active');
    plus_button.removeClass('no-active');

    basket_item_data = load_data();
    basket_item_data.add_ing[ing_id] = {
        "set": 0,
        "price": ing_price
    };

    save_data(basket_item_data);
    calc_delivery_price();
}

function remove_ing(event){
    event.preventDefault();
    var el_id = $('#deliv_item_card').attr('data-id');
    var ing_id = $(event.currentTarget).attr('data-id');
    $(event.currentTarget).toggleClass('no-active');

    basket_item_data = load_data();
    basket_item_data.remove_ing[ing_id] = {
        "set": $(event.currentTarget).hasClass('no-active')?1:0,
    };

    save_data(basket_item_data);

}

</script>
<?
    $result['html'] = ob_get_contents();
    ob_end_clean();
}

echo json_encode($result);
?>
