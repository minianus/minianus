<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];




function result_error($error){
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;

}



$action = $_REQUEST['action'];

switch($action){
    case "need_restore" : {
        need_restore();
        break;
    }
    case "set_new_passwd" : {
        set_new_passwd();
        break;
    }
    default : {

        result_error('Неопределенное действие. Попробуйте перезагрузить страницу и заполнить форму еще раз.');
    }
}

echo json_encode($result);

function need_restore(){

    $email = $_REQUEST['email'];
    $filter = ['EMAIL' => $email, 'ACTIVE' => 'Y'];
    $rsUsers = CUser::GetList([], [], $filter);
    $user = null;
    while($item = $rsUsers->getNext()){
        $user = $item;
    }
    if($user != null){
        $send_result = CUser::SendPassword($user['LOGIN'], $user['EMAIL']);
        if($send_result['TYPE'] != 'OK'){
            result_error('Ошибка отправки письма для восстановления пароля. Попробуйте позже.');
        }
    }else{
        result_error('Пользователь с указанной почтой не найден.');
    }


}

function set_new_passwd(){

    $passwd = $_REQUEST['passwd'];
    $passwd_confirm = $_REQUEST['passwd_confirm'];
    $checkword = $_REQUEST['checkword'];
    $login = $_REQUEST['login'];

    if($passwd != $passwd_confirm){
        result_error('Пароль и его подтверждение не совпадают.');
    }

    global $result;
    if($result['error'] == false){
        $change_result = CUser::ChangePassword($login, $checkword, $passwd, $passwd_confirm);
        if($change_result['TYPE'] != 'OK'){
            result_error('Ошибка смены пароля. Попробуйте получить новую ссылку.');
        }
    }

}
