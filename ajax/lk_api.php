<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];


CModule::IncludeModule('iblock');

function result_error($error){
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;

}

global $USER;
if(!$USER->IsAuthorized()){
    result_error('Вы не прошли аутентификацию.');
    $result['need_auth'] = true;

}
if(!$result['error']){
    $action = $_REQUEST['action'];
    switch ($action) {
    case 'change_name': {
        $result['name'] = change_name();
        break;
    }
    case 'change_phone': {
        $result['phone'] = change_phone();
        break;
    }
    case 'change_email': {
        $result['email'] = change_email();
        break;
    }
    case 'change_password': {
        change_password();
        break;
    }
    case 'change_birthday': {
        change_birthday();
        break;
    }
    default: {

        result_error('Неверный action');
    }
}
}
echo json_encode($result);

function change_name(){

    global $USER;
    $id = $USER->getID();

    $user = new CUser;
    $fields = [
        'NAME' => $_REQUEST['name'],
    ];
    $result = $user->update($id, $fields);
    if(!$result){
        result_error($user->LAST_ERROR);
    }else{
        $result = $_REQUEST['name'];
    }

    return $result;
}

function change_phone(){

    global $USER;
    $id = $USER->getID();

    $user = new CUser;
    $fields = [
        'PERSONAL_PHONE' => $_REQUEST['phone'],
    ];
    $result = $user->update($id, $fields);
    if(!$result){
        result_error($user->LAST_ERROR);
    }else{
        $result = $_REQUEST['phone'];
    }

    return $result;
}

function change_email(){

    global $USER;
    $id = $USER->getID();

    $user = new CUser;
    $fields = [
        'EMAIL' => $_REQUEST['email'],
    ];
    $result = $user->update($id, $fields);
    if(!$result){
        result_error($user->LAST_ERROR);
    }else{
        $result = $_REQUEST['email'];
    }

    return $result;
}

function change_password(){

    global $USER;
    $id = $USER->getID();

    $user = new CUser;
    $fields = [
        'PASSWORD' => $_REQUEST['password'],
        'CONFIRM_PASSWORD' => $_REQUEST['confirm_password']
    ];
    $result = $user->update($id, $fields);
    if (!$result) {
        result_error($user->LAST_ERROR);
    }
}

function change_birthday(){

    global $USER;
    $id = $USER->getID();

    $user = new CUser;
    $fields = [
        'PERSONAL_BIRTHDAY' => $_REQUEST['birthday'],
    ];
    $result = $user->update($id, $fields);
    if (!$result) {
        result_error($user->LAST_ERROR);
    }
}
