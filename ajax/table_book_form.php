<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];


function result_error($error){
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;

}

$restoran = $_REQUEST['restoran'];
$date = $_REQUEST['date'];
$time = $_REQUEST['time'];
$people_count = $_REQUEST['people_count'];
$name = $_REQUEST['name'];
$phone = $_REQUEST['phone'];
$cook_for_me = $_REQUEST['cook_for_me'];
$comment = $_REQUEST['comment'];
$agree = $_REQUEST['agree'];

if($name == ''){
    result_error('Заполните имя.');
}

if($agree == ''){
    result_error('Поставьте отметку согласия на обработку персональных данных.');
}

if($phone == ''){
    result_error('Заполните номер телефона.');
}

if($date == ''){
    result_error('Заполните дату.');
}
if($time == ''){
    result_error('Заполните время.');
}

if(!$result['error']){

    $arLoadArray = array(
        "IBLOCK_ID" => get_iblock_id_by_code('table_book'),
        "PROPERTY_VALUES" => [],
        "NAME" => "Заявка от ". $name . ' '. date("d.m.Y H:i:s"),
        "PREVIEW_TEXT" =>   'ресторан: ' . $restoran . "\n" . 
                            'дата: ' . $date . "\n" . 
                            'время:' . $time . "\n" . 
                            'кол-во гостей: ' . $people_count . "\n" . 
                            'имя: ' . $name . "\n" . 
                            'телефон:' . $phone . "\n" . 
                            'приготовить к приходу: ' . $cook_for_me . "\n" . 
                            'комментарий: ' . $comment,
        "ACTIVE" => "N",
    );
    CModule::IncludeModule('iblock');
    $el = new CIBlockElement;
    if ($element_id = $el->Add($arLoadArray)) {
        $result['id'] = $element_id;

        $mail_data = [
            "CAFE" => $restoran,
            "DATE" => $date,
            "TIME" => $time,
            "PEOPLE_COUNT" => $people_count,
            "NAME" => $name,
            "PHONE" => $phone,
            "COOK_FOR_ME" => $cook_for_me,
            "COMMENT" => $comment,
            "ID" => $element_id,
        ];
        CEvent::Send('TABLE_BOOK', SITE_ID, $mail_data);
    }

}

echo json_encode($result);
