<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];

CModule::includeModule('iblock');

function result_error($error)
{
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;
}

function getVacancyListByCafe($cafe_id)
{
    $filter = ['IBLOCK_ID' => get_iblock_id_by_code('vacancy'), 'PROPERTY_CAFE' => $cafe_id];
    $select = ['ID', 'NAME'];
    $res = CIBlockElement::getList([], $filter, false, false, $select);
    $list = [];

    while ($vacancy = $res->getNext()) {
        $list[] = $vacancy;
    }

    return $list;
}

function getCafeVacancyListByCity($city)
{
    
    $section_id = get_section_id_by_code($city, get_iblock_id_by_code('restaurants'));
    if ($section_id == 0) {
        return [
            'cafelist' => [],
            'vacancylist' => []
        ];
    }

    $filter = ['IBLOCK_ID' => get_iblock_id_by_code('restaurants'), 'SECTION_ID' => $section_id];
    $select = ['ID', 'NAME'];
    $res = CIBlockElement::getList([], $filter, false, false, $select);
    $cafelist = [];
    $cafe_ids = [];

    while ($cafe = $res->getNext()) {
        $cafelist[] = $cafe;
        $cafe_ids[$cafe['ID']] = $cafe['ID'];
    }

    $vacancylist = [];

    $cafe_with_vacancies = [];

    if (count($cafelist) > 0) {
        $filter = ['IBLOCK_ID' => get_iblock_id_by_code('vacancy'), 'PROPERTY_CAFE' => $cafe_ids];
        $select = ['ID', 'NAME', 'PROPERTY_CAFE'];
        $res = CIBlockElement::getList([], $filter, false, false, $select);
        while($vacancy = $res->getNext()){
            $vacancylist[] = [
                'ID' => $vacancy['ID'],
                'NAME' => $vacancy['NAME'],
            ];
            foreach($vacancy['PROPERTY_CAFE_VALUE'] as $cafe_id){
                $cafe_with_vacancies[$cafe_id] = $cafe_id;
            }
        }

        $result['vacancylist'] = $vacancylist;

    } 
    $temp = [];
    foreach($cafelist as $cafe){
        if(in_array($cafe['ID'], $cafe_with_vacancies)){
            $temp[] = [
                'ID' => $cafe['ID'],
                'NAME' => $cafe['NAME'],
            ];
        }
    }

    $result['cafelist'] = $temp;

    return $result;
}

$city_value = $_REQUEST['vacancy_city'];
$cafe = (integer)$_REQUEST['cafe'];
$changed = $_REQUEST['changed'];

switch ($changed) {
    case 'vacancy_city': {
        $data = getCafeVacancyListByCity($city_value);
        $result['cafelist'] = $data['cafelist'];
        $result['vacancylist'] = $data['vacancylist'];
        break;
    }
    case 'cafe': {
        $result['vacancylist'] = getVacancyListByCafe($cafe);
        break;
    }
    default: {

    }

}

echo json_encode($result);
