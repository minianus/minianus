<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

$result = [];
$result['error'] = false;
$result['errors'] = [];


CModule::IncludeModule('iblock');

function result_error($error){
    global $result;
    $result['error'] = true;
    $result['errors'][] = $error;

}

function validate_vacancy($city, $cafe, $vacancy){

    $filter = ['IBLOCK_ID' => get_iblock_id_by_code('vacancy'), 'ID' => $vacancy, 'PROPERTY_CAFE' => $cafe];
    $select = ['ID'];
    $res = CIBlockElement::getList([], $filter, false, false, $select);
    while($item = $res->getNext()){
        return true;
    }
    return false;
    
}

$vacancy_city = $_REQUEST['vacancy_city'];
$cafe = (integer)$_REQUEST['cafe'];
$vacancy = (integer)$_REQUEST['vacancy'];
$name = $_REQUEST['name'];
$surname = $_REQUEST['surname'];
$birthday = $_REQUEST['birthday'];
$phone = $_REQUEST['phone'];
$agree = $_REQUEST['agree'];

if($vacancy_city == ''){
    result_error('Укажите город.');
}

if($cafe == '' || $cafe == 0){
    result_error('Укажите кафе.');
}

if($vacancy == '' || $vacancy == 0){
    result_error('Укажите вакансию.');
}

if($name == '' || strlen($name) < 2){
    result_error('Заполните имя.');
}

if($surname == '' || strlen($surname) < 2){
    result_error('Заполните фамилию.');
}

if($phone == '' || strlen($phone) < 11){
    result_error('Заполните номер телефона.');
}

if($agree != 'Y'){
    result_error('Поставьте отметку согласия на обработку персональных данных.');
}



if(!$result['error']){

    $arLoadArray = array(
        "IBLOCK_ID" => get_iblock_id_by_code('vacancy_form'),
        "PROPERTY_VALUES" => [
            'CITY' => $vacancy_city,
            'CAFE' => $cafe,
            'VACANCY' => $vacancy,
            'NAME' => $name,
            'SURNAME' => $surname,
            'BIRTHDAY' => $birthday,
            'PHONE' => $phone,
        ],
        "NAME" => "Отклик на вакансию от ". $name . ' '. date("d.m.Y H:i:s"),
        "ACTIVE" => "N",
    );
    
    $el = new CIBlockElement;
    if ($element_id = $el->Add($arLoadArray)) {
        $result['id'] = $element_id;

        $mail_data = [
            'CITY' => $vacancy_city,
            'CAFE' => $cafe,
            'VACANCY' => $vacancy,
            'NAME' => $name,
            'SURNAME' => $surname,
            'BIRTHDAY' => $birthday,
            'PHONE' => $phone,
            "ID" => $element_id,
        ];
        CEvent::Send('VACANCY_FEEDBACK', SITE_ID, $mail_data);
    }

}

echo json_encode($result);
