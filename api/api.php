<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

define('DEFAULT_DELIVERY_TYPE', 'delivery_man');
define('DEFAULT_PAY_SYSTEM', 10);

use Bitrix\Sale;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Fuser;
use Bitrix\Sale\PriceMaths;
use Bitrix\Main\Context;
use Bitrix\Sale\Order;

class order_api
{
    public $errors = [];
    public $log = [];

    public function response_error($error, $field = '', $level = 'mess')
    {
        $this->errors[] = [
            'ERROR' => $error,
            'FIELD' => $field,
            'LEVEL' => $level,
        ];
    }

    public function response_log($mess, $data)
    {
        $this->log[$mess . '_' . rand(0, 1000)] = $data;
    }

    public function response($data = [])
    {
        $data['LOG'] = $this->log;
        $data['ERRORS'] = $this->errors;
        echo json_encode($data);
    }

    public function make_new_order_action($request_data)
    {
        $result = $this->make_new_order($request_data);
        $response = $result;
        $this->response($response);
    }

    public function getOrderPropertiesMapCodeToId($order)
    {
        $propertyCollection = $order->getPropertyCollection();
        $ar = $propertyCollection->getArray();
        $result = [];
        foreach ($ar['properties'] as $property) {
            $result[$property['CODE']] = $property['ID'];
        }
        return $result;
    }

    public function get_saved_user_address($address_id)
    {
        $UserAddress_hlb = get_hlb_by_name('UserAddress');
        global $USER;
        $user_id = $USER->getID();

        if ($user_id) {
            $filter = ['UF_USER_ID' => $user_id, 'ID' => $address_id];
            $select = ['*'];

            $res = $UserAddress_hlb::getList([
            'filter' => $filter,
            'select' => $select,
        ]);

            $address = '';

            while ($address = $res->fetch()) {
                $address = build_user_address_name($address);
            }

            return $address;
        }

        return false;
    }

    public function get_cafe_by_id($cafe){

        $filter = ['IBLOCK_ID' => get_iblock_id_by_code('restaurants'), 'ID' => $cafe];
        $select = ['ID', 'NAME'];
        $res = CIBlockElement::getList([], $filter, false, false, $select);

        $result = '';
        while($cafe = $res->getNext()){
            $result = $cafe['NAME'];
        }

        return $result;
    }

    public function make_new_order($request_data)
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $user_id = $USER->getID();
        } else {
            $user_id = $this->register_or_get_exists_user($request_data['phone']);
            if (intval($user_id) > 0) {
            } else {
                $this->response_error($user_id);
            }
        }

        $phone = $request_data['phone'];
        $fio = $request_data['fio'];
        $comment = $request_data['comment'];
        $delivery_time_type = ($request_data['delivery_time_type'] == 'time_moment')?'Точное время':'Как можно быстрее';
        $delivery_time = $request_data['delivery_time'];
        $delivery_type = $request_data['delivery_type'];

        CModule::includeModule('sale');
        $siteId = Context::getCurrent()
            ->getSite();
        $basketItems = Basket::loadItemsForFUser(Fuser::getId(), $siteId)
            ->getOrderableItems();
        if (count($basketItems) === 0) {
            $this->response_error('Корзина пуста.');
            return;
        }
        $order = Order::create($siteId, $user_id);
        $order->setPersonTypeId(1);
        $order->setField('USER_DESCRIPTION', $comment);

        $order->setBasket($basketItems);
        

        $delivery_id = 1;
        switch ($delivery_type) {
            case 'delivery_man': {
                $delivery_id = 1;
                break;
            }
            case 'delivery_self': {
                $delivery_id = 2;
                break;
            }

        }
        $this->createOrderShipment($delivery_id, $order);
        $this->createOrderPayment($request_data['paying_method'], $order);

        $save_result = $order->save();

        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection[0];
        $service = Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $initResult = $service->initiatePay($payment, null, \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);

        $propertyCollection = $order->getPropertyCollection();
        $phoneProp = $propertyCollection->getPhone();
        $phoneProp->setValue($phone);
        $nameProp = $propertyCollection->getPayerName();
        $nameProp->setValue($fio);
        $addrProp = $propertyCollection->getAddress();
        switch($delivery_type){
            case 'delivery_man' : {
                if($USER->IsAuthorized()){
                    $delivery_address = $this->get_saved_user_address($request_data['selected_delivery']);
                }else{
                    $delivery_address = build_address_name($request_data['address']);
                }
                break;
            }
            case 'delivery_self' : {
                $delivery_address = $this->get_cafe_by_id($request_data['selected_delivery']);
                break;
            }

        }
        $addrProp->setValue($delivery_address);

        $prop_code_to_id = $this->getOrderPropertiesMapCodeToId($order);

        $delivery_time_type_prop = $propertyCollection->getItemByOrderPropertyId($prop_code_to_id['DELIVERY_TIME_TYPE']);
        if ($delivery_time_type_prop) {
            $delivery_time_type_prop->setValue($delivery_time_type);
        }

        $delivery_time_prop = $propertyCollection->getItemByOrderPropertyId($prop_code_to_id['DELIVERY_TIME']);
        if ($delivery_time_prop) {
            $delivery_time_prop->setValue($delivery_time);
        }

        $result['order_result'] = $save_result->isSuccess();
        //$result['order_errors'] = $save_result->getErrors();
        $result['order_id'] = $save_result->getID();

        $result['pay_template'] = $initResult->getTemplate();
        $this->clear_promocode();
        return $result;
    }

    public function register_or_get_exists_user($phone)
    {
        $phone = clear_phone($phone);
        $user_res = CUser::GetByLogin($phone);
        if ($user = $user_res->fetch()) {
            return $user['ID'];
        }
        $user = new CUser;
        $pass = get_random_string(30);

        $fields = [
            'LOGIN' => $phone,
            'NAME' => $phone,
            'EMAIL' => $phone . '@' . SITE_SERVER_NAME,
            'PASSWORD' => $pass,
            'CONFIRM_PASSWORD' => $pass,
            'PERSONAL_PHONE' => $phone

        ];

        $id = $user->Add($fields);
        if (intval($id) > 0) {
            return $id;
        } else {
            return $user->LAST_ERROR;
        }
    }

    public function createOrderShipment(int $deliveryId = 0, $order)
    {
        $order_basket = $order->getBasket();
        $basket_price = $order_basket->getPrice();
        /* @var $shipmentCollection ShipmentCollection */
        $shipmentCollection = $order->getShipmentCollection();

        if ($deliveryId > 0) {
            $shipment = $shipmentCollection->createItem(
                Bitrix\Sale\Delivery\Services\Manager::getObjectById($deliveryId)
            );
        } else {
            $shipment = $shipmentCollection->createItem();
        }

        /** @var $shipmentItemCollection ShipmentItemCollection */
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $order->getCurrency());
        $shipment->setField('CUSTOM_PRICE_DELIVERY', true);
        $shipment->setField('BASE_PRICE_DELIVERY', get_delivery_price($basket_price));

        foreach ($order->getBasket()->getOrderableItems() as $basketItem) {
            /**
             * @var $basketItem BasketItem
             * @var $shipmentItem ShipmentItem
             */
            $shipmentItem = $shipmentItemCollection->createItem($basketItem);
            $shipmentItem->setQuantity($basketItem->getQuantity());
        }

        return $shipment;
    }

    /**
     * @param int $paySystemId
     * @return Payment
     * @throws Exception
     */
    public function createOrderPayment(int $paySystemId, $order)
    {
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(
            Bitrix\Sale\PaySystem\Manager::getObjectById($paySystemId)
        );
        $payment->setField('SUM', $order->getPrice());
        $payment->setField('CURRENCY', $order->getCurrency());

        return $payment;
    }

    /**
     * отдает инфо о заказе.
     *
     * @return void
     */
    public function get_order_data_action($data)
    {
        $response['basket_data'] = $this->get_basket_data();
        $response['source_data'] = $this->get_source_data($data);
        $response['user'] = $this->get_user_info();

        $this->response($response);
    }

    public function get_user_info()
    {
        global $USER;

        $result = [
            'is_auth' => $USER->IsAuthorized(),
        ];

        return $result;
    }

    /**
     * получает PROPS элемента корзины и выдает id добавляемых ингридиентов
     *
     * @param [type] $props
     * @return void
     */
    public function get_props_item_ids($props)
    {
        $result = [];
        foreach ($props as $prop) {
            $id = get_add_ing_id_from_basket_item_prop($prop);
            if ($id) {
                $result[] = $id;
            }
        }
        return $result;
    }

    /**
     * удаляет полностью элемент из корзины
     *
     * @param [type] $id
     * @return void
     */
    public function remove_item($item_id)
    {
        CModule::includeModule('sale');

        $basketItems = [];

        $registry = Sale\Registry::getInstance(Sale\Registry::REGISTRY_TYPE_ORDER);

        /** @var Sale\Basket $basketClass */
        $basketClass = $registry->getBasketClassName();

        $basketItemsResult = $basketClass::getList([
            'filter' => [
                'FUSER_ID' => Fuser::getId(),
                '=LID' => SITE_ID,
                'ORDER_ID' => null,
            ],
            'order' => [
                'SORT' => 'ASC',
                'ID' => 'ASC',
            ],
        ]);

        $product_id_to_item_id_map = [];

        while ($basketItem = $basketItemsResult->fetch()) {
            $basketItems[$basketItem['ID']] = $basketItem;
            if ($basketItem['ID'] == $item_id) {
                $item_data = $basketItem;
            }
            $product_id_to_item_id_map[$basketItem['PRODUCT_ID']] = $basketItem['ID'];
        }

        if ($item_data) {
            //получаем пропсы чтобы определить связанные продукты и тоже удалить их
            $propertyResult = Sale\BasketPropertiesCollection::getList(
                [
                    'filter' => [
                        '=BASKET_ID' => $item_id,
                        ['!CODE' => 'CATALOG.XML_ID'],
                        ['!CODE' => 'PRODUCT.XML_ID'],
                    ],
                    'order' => [
                        'ID' => 'ASC',
                        'SORT' => 'ASC',
                    ],
                ]
            );
            while ($property = $propertyResult->fetch()) {
                $item_data['PROPS'][] = $property;
            }
            $add_ing_ids = $this->get_props_item_ids($item_data['PROPS']);
            //составляем массив элементов которые надо уменьшить в кол-ве
            $items_for_dec = [];
            foreach ($add_ing_ids as $id) {
                $items_for_dec[] = $product_id_to_item_id_map[$id];
            }
            foreach ($items_for_dec as $item) {
                $quantity = $basketItems[$item]['QUANTITY'];
                if ($quantity > 1) {
                    CSaleBasket::Update($item, [
                        'QUANTITY' => $quantity - 1,
                    ]);
                } else {
                    CSaleBasket::Delete($item);
                }
            }
            CSaleBasket::Delete($item_data['ID']);
        } else {
        }
    }

    public function get_basket_data_action()
    {
        $this->response($this->get_basket_data());
    }

    public function remove_item_action($item_id)
    {
        $this->remove_item($item_id);
        $this->response($this->get_basket_data());
    }

    public function inc_item_count($item_id)
    {
        CModule::includeModule('sale');

        $basketItems = [];

        $registry = Sale\Registry::getInstance(Sale\Registry::REGISTRY_TYPE_ORDER);

        /** @var Sale\Basket $basketClass */
        $basketClass = $registry->getBasketClassName();

        $basketItemsResult = $basketClass::getList([
            'filter' => [
                'FUSER_ID' => Fuser::getId(),
                '=LID' => SITE_ID,
                'ORDER_ID' => null,
            ],
            'order' => [
                'SORT' => 'ASC',
                'ID' => 'ASC',
            ],
        ]);

        $product_id_to_item_id_map = [];

        while ($basketItem = $basketItemsResult->fetch()) {
            $basketItems[$basketItem['ID']] = $basketItem;
            if ($basketItem['ID'] == $item_id) {
                $item_data = $basketItem;
            }
            $product_id_to_item_id_map[$basketItem['PRODUCT_ID']] = $basketItem['ID'];
        }

        if ($item_data) {
            //получаем пропсы чтобы определить связанные продукты и тоже удалить их
            $propertyResult = Sale\BasketPropertiesCollection::getList(
                [
                    'filter' => [
                        '=BASKET_ID' => $item_id,
                        ['!CODE' => 'CATALOG.XML_ID'],
                        ['!CODE' => 'PRODUCT.XML_ID'],
                    ],
                    'order' => [
                        'ID' => 'ASC',
                        'SORT' => 'ASC',
                    ],
                ]
            );
            while ($property = $propertyResult->fetch()) {
                $item_data['PROPS'][] = $property;
            }
            $add_ing_ids = $this->get_props_item_ids($item_data['PROPS']);
            //составляем массив элементов которые надо уменьшить в кол-ве
            $items_for_inc = [];
            foreach ($add_ing_ids as $id) {
                $items_for_inc[] = $product_id_to_item_id_map[$id];
            }
            $this->response_log('', $items_for_inc);
            foreach ($items_for_inc as $item) {
                $quantity = $basketItems[$item]['QUANTITY'];

                $update_result = CSaleBasket::Update($item, [
                    'QUANTITY' => $quantity + 1,
                ]);
                $this->response_log('update qty for item ' . $item . ' to ' . $quantity . ' with result ' . $update_result, '');
            }
            $quantity = $basketItems[$item_data['ID']]['QUANTITY'];
            CSaleBasket::Update($item_data['ID'], [
                'QUANTITY' => $quantity + 1,
            ]);
        } else {
        }
    }

    public function inc_item_count_action($item_id)
    {
        $this->inc_item_count($item_id);
        $this->response($this->get_basket_data());
    }

    public function dec_item_count($item_id)
    {
        CModule::includeModule('sale');

        $basketItems = [];

        $registry = Sale\Registry::getInstance(Sale\Registry::REGISTRY_TYPE_ORDER);

        /** @var Sale\Basket $basketClass */
        $basketClass = $registry->getBasketClassName();

        $basketItemsResult = $basketClass::getList([
            'filter' => [
                'FUSER_ID' => Fuser::getId(),
                '=LID' => SITE_ID,
                'ORDER_ID' => null,
            ],
            'order' => [
                'SORT' => 'ASC',
                'ID' => 'ASC',
            ],
        ]);

        $product_id_to_item_id_map = [];

        while ($basketItem = $basketItemsResult->fetch()) {
            $basketItems[$basketItem['ID']] = $basketItem;
            if ($basketItem['ID'] == $item_id) {
                $item_data = $basketItem;
            }
            $product_id_to_item_id_map[$basketItem['PRODUCT_ID']] = $basketItem['ID'];
        }

        if ($item_data) {
            //получаем пропсы чтобы определить связанные продукты и тоже удалить их
            $propertyResult = Sale\BasketPropertiesCollection::getList(
                [
                    'filter' => [
                        '=BASKET_ID' => $item_id,
                        ['!CODE' => 'CATALOG.XML_ID'],
                        ['!CODE' => 'PRODUCT.XML_ID'],
                    ],
                    'order' => [
                        'ID' => 'ASC',
                        'SORT' => 'ASC',
                    ],
                ]
            );
            while ($property = $propertyResult->fetch()) {
                $item_data['PROPS'][] = $property;
            }
            $add_ing_ids = $this->get_props_item_ids($item_data['PROPS']);
            //составляем массив элементов которые надо уменьшить в кол-ве
            $items_for_dec = [];
            foreach ($add_ing_ids as $id) {
                $items_for_dec[] = $product_id_to_item_id_map[$id];
            }
            foreach ($items_for_dec as $item) {
                $quantity = $basketItems[$item]['QUANTITY'];
                if ($quantity > 1) {
                    CSaleBasket::Update($item, [
                        'QUANTITY' => $quantity - 1,
                    ]);
                } else {
                    $this->remove_item($item);
                }
            }
            $quantity = $basketItems[$item_data['ID']]['QUANTITY'];
            if ($quantity > 1) {
                CSaleBasket::Update($item_data['ID'], [
                    'QUANTITY' => $quantity - 1,
                ]);
            } else {
                $this->remove_item($item_data['ID']);
            }
        } else {
        }
    }

    public function dec_item_count_action($item_id)
    {
        $this->dec_item_count($item_id);
        $this->response($this->get_basket_data());
    }

    public function get_recomendations_action()
    {
        $response['ITEMS'] = $this->get_recomendations();
        $this->response($response);
    }

    public function add_to_basket_action($data)
    {
        $this->add_to_basket($data);
        $this->response($this->get_basket_data());
    }

    public function add_to_basket($data)
    {
        CModule::IncludeModule("sale");
        CModule::IncludeModule("iblock");
        //получаем данные об основном товаре
        $error = false;
        $item_id = $data['item_id'];
        $item_count = ($data['count'])?$data['count']:1;
        if(!($item_count > 0)){
            $item_count = 1;
        }
        $item_data = CIBlockElement::GetByID($item_id);
        if ($res = $item_data->GetNext()) {
            $item_data = $res;
        } else {
            $error = true;
            $this->response_error('Несуществующий основной товар.');
        }
        if (!$error) {
            $item_price = CCatalogProduct::GetOptimalPrice($item_id, 1)['RESULT_PRICE']['DISCOUNT_PRICE'];

            //получаем данные о добавляемых и удаляемых ингридиентах
            $add_ing_id = [];
            $add_ing_for_basket = [];
            foreach ($data['add_ing'] as $id => $add_ing_data) {
                if ($add_ing_data['set'] == 1) {
                    $add_ing_ids[] = $id;
                }
            }

            if (count($add_ing_ids) > 0) {
                $filter = ['IBLOCK_ID' => get_iblock_id_by_code('add_ingridients'), 'ID' => $add_ing_ids];
                $select = ['ID', 'NAME'];

                $res = CIBlockElement::GetList([], $filter, false, [], $select);
                while ($i = $res->getNext()) {
                    $price = CCatalogProduct::GetOptimalPrice($i['ID'], 1);
                    $ingridient = [
                        'ID' => $i['ID'],
                        'NAME' => $i['NAME'],
                        'PRICE' => $price['RESULT_PRICE']['DISCOUNT_PRICE'],
                    ];
                    $add_ing_for_basket[] = $ingridient;
                }
            }

            //$result['add_ings'] = $add_ing_for_basket;

            $remove_ing_ids = [];
            foreach ($data['remove_ing'] as $id => $remove_ing_data) {
                if ($remove_ing_data['set'] == 1) {
                    $remove_ing_ids[] = $id;
                }
            }
            $remove_ing_for_basket = [];
            if (count($remove_ing_ids) > 0) {
                $filter = ['IBLOCK_ID' => get_iblock_id_by_code('ingridients_for_removing'), 'ID' => $remove_ing_ids];
                $select = ['ID', 'NAME'];

                $res = CIBlockElement::GetList([], $filter, false, [], $select);
                while ($i = $res->getNext()) {
                    $ingridient = [
                        'ID' => $i['ID'],
                        'NAME' => $i['NAME'],
                    ];
                    $remove_ing_for_basket[] = $ingridient;
                }
            }

            //$result['remove_ings'] = $remove_ing_for_basket;

            //добавляем товар в корзину

            $props = [];
            foreach ($add_ing_for_basket as $add_ing) {
                $props[] = [
                    'NAME' => '+',
                    'CODE' => '+|' . $add_ing['ID'],
                    'VALUE' => $add_ing['NAME'],
                ];
            }
            foreach ($remove_ing_for_basket as $remove_ing) {
                $props[] = [
                    'NAME' => '-',
                    'CODE' => '-|' . $remove_ing['ID'],
                    'VALUE' => $remove_ing['NAME'],
                ];
            }

            $fields = [
                'PRODUCT_ID' => $item_id,
                'PRICE' => $item_price,
                'CURRENCY' => GetDefaultCurrency(),
                'QUANTITY' => $item_count,
                "LID" => LANG,
                'NAME' => $item_data['NAME'],
                'PROPS' => $props,
                "WEIGHT" => CCatalogProduct::GetByID($item_id)['WEIGHT'],
            ];

            CSaleBasket::Add($fields);

            //добавляем в корзину дополнительные ингридиенты
            foreach ($add_ing_for_basket as $add_ing) {
                $fields = [
                    'PRODUCT_ID' => $add_ing['ID'],
                    'PRICE' => $add_ing['PRICE'],
                    'CURRENCY' => GetDefaultCurrency(),
                    'QUANTITY' => $item_count,
                    "LID" => LANG,
                    'NAME' => $add_ing['NAME'],
                    "WEIGHT" => 1,
                ];
                CSaleBasket::Add($fields);
            }
        }
    }

    public function get_empty_response()
    {
        $this->response(['empty' => true, 'input_error' => true, 'server' => $_SERVER]);
    }

    public function get_basket_data()
    {
        CModule::includeModule('sale');

        $basketItems = [];

        $discount_prices = calculateDiscount();

        //print_r($discount_prices);

        $registry = Sale\Registry::getInstance(Sale\Registry::REGISTRY_TYPE_ORDER);

        /** @var Sale\Basket $basketClass */
        $basketClass = $registry->getBasketClassName();

        $basketItemsResult = $basketClass::getList([
            'filter' => [
                'FUSER_ID' => Fuser::getId(),
                '=LID' => SITE_ID,
                'ORDER_ID' => null,
            ],
            'order' => [
                'SORT' => 'ASC',
                'ID' => 'ASC',
            ],
        ]);

        $product_id_to_price_map = [];
        $product_id_to_item_id_map = [];

        while ($basketItem = $basketItemsResult->fetch()) {
            $product_id_to_item_id_map[$basketItem['PRODUCT_ID']] = $basketItem['ID'];
            $product_ids[] = $basketItem['PRODUCT_ID'];
            $basketItem['PROPS'] = [];
            $basketItem['QUANTITY'] = (float) $basketItem['QUANTITY'];

            $basketItem['WEIGHT'] = (float) $basketItem['WEIGHT'];

            /*$basketItem['FULL_PRICE'] = PriceMaths::roundPrecision((float) $basketItem['BASE_PRICE']);
            $basketItem['FULL_PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($basketItem['FULL_PRICE'], $basketItem['CURRENCY'], true);*/

            $basketItem['FULL_PRICE'] = ($discount_prices[$basketItem['ID']])?
                $discount_prices[$basketItem['ID']]['PRICE']
                :$basketItem['PRICE'];

            $product_id_to_price_map[$basketItem['PRODUCT_ID']] = $basketItem['FULL_PRICE'];

            $basketItem['SUM_FULL_PRICE'] = $basketItem['FULL_PRICE'] * $basketItem['QUANTITY'];
            $basketItem['SUM_FULL_PRICE_FORMATED'] = CCurrencyLang::CurrencyFormat($basketItem['SUM_FULL_PRICE'], $basketItem['CURRENCY'], true);

            $basketItem['SUM_DISCOUNT_PRICE'] = $basketItem['DISCOUNT_PRICE'] * $basketItem['QUANTITY'];
            $basketItem['SUM_DISCOUNT_PRICE_FORMATED'] = 
            CCurrencyLang::CurrencyFormat($basketItem['SUM_DISCOUNT_PRICE'], $basketItem['CURRENCY'], true);

            $basketItem['DISCOUNT_PRICE_PERCENT'] = 0;
            if ($basketItem['CUSTOM_PRICE'] !== 'Y' && $basketItem['FULL_PRICE'] > 0 && $basketItem['DISCOUNT_PRICE'] > 0) {
                $basketItem['DISCOUNT_PRICE_PERCENT'] = Sale\Discount::calculateDiscountPercent(
                    $basketItem['FULL_PRICE'],
                    $basketItem['DISCOUNT_PRICE']
                );
                if ($basketItem['DISCOUNT_PRICE_PERCENT'] === null) {
                    $basketItem['DISCOUNT_PRICE_PERCENT'] = 0;
                }
            }

            $basketItem['DISCOUNT_PRICE_PERCENT_FORMATED'] = Sale\BasketItem::formatQuantity($basketItem['DISCOUNT_PRICE_PERCENT']) . '%';


            if ($basketItem['CAN_BUY'] !== 'Y' && $basketItem['DELAY'] !== 'Y') {
                $basketItem['NOT_AVAILABLE'] = true;
                $notAvailableItemsCount++;
            } elseif ($basketItem['DELAY'] !== 'Y') {
                $orderableItemsCount++;
            }

            $basketItems[$basketItem['ID']] = $basketItem;
        }

        //получаем картинки для товаров
        $product_ids = [];
        foreach ($basketItems as $item) {
            $product_ids[] = $item['PRODUCT_ID'];
        }

        $catalog_items_data = get_items_data($product_ids, get_iblock_id_by_code('menu-devilery'));

        foreach ($basketItems as $item) {
            $basketItems[$item['ID']]['PICTURE'] = CFile::getPath($catalog_items_data[$item['PRODUCT_ID']]['PREVIEW_PICTURE']);
            $basketItems[$item['ID']]['DESC'] = $catalog_items_data[$item['PRODUCT_ID']]['PROPERTY_SHOT_ABOUT_VALUE'];
            $basketItems[$item['ID']]['CAFE_COUNT'] = count($catalog_items_data[$item['PRODUCT_ID']]['PROPERTY_COOKING_CAFE_VALUE']);
            $basketItems[$item['ID']]['ITEM_DATA'] = $catalog_items_data[$item['PRODUCT_ID']];
        }

        $propertyResult = Sale\BasketPropertiesCollection::getList(
            [
                'filter' => [
                    '=BASKET_ID' => array_keys($basketItems),
                    ['!CODE' => 'CATALOG.XML_ID'],
                    ['!CODE' => 'PRODUCT.XML_ID'],
                ],
                'order' => [
                    'ID' => 'ASC',
                    'SORT' => 'ASC',
                ],
            ]
        );
        while ($property = $propertyResult->fetch()) {
            $basketItems[$property['BASKET_ID']]['PROPS'][] = $property;
        }

        //основным товарам добавляем к цене цену дополнительных ингридиентов

        foreach ($basketItems as $item) {
            $ing_price = 0;
            if ($item['PROPS']) {
                $add_ing = [];
                $remove_ing = [];

                foreach ($item['PROPS'] as $prop) {
                    if ($prop['NAME'] == '+') {
                        $add_ing[] = $prop['VALUE'];
                    };
                    if ($prop['NAME'] == '-') {
                        $remove_ing[] = $prop['VALUE'];
                    };
                    $ing_price += $product_id_to_price_map[get_add_ing_id_from_basket_item_prop($prop)];
                }
                $basketItems[$item['ID']]['ADD_ING'] = implode(', ', $add_ing);
                $basketItems[$item['ID']]['REMOVE_ING'] = implode(', ', $remove_ing);

                $basketItems[$item['ID']]['FULL_PRICE'] = $basketItems[$item['ID']]['FULL_PRICE'] + $ing_price;
                $basketItems[$item['ID']]['FULL_PRICE_FORMATED'] =
                CCurrencyLang::CurrencyFormat($basketItems[$item['ID']]['FULL_PRICE'], $basketItems[$item['ID']]['CURRENCY'], true);
                $basketItems[$item['ID']]['SUM_FULL_PRICE'] = $basketItems[$item['ID']]['FULL_PRICE'] * $basketItems[$item['ID']]['QUANTITY'];
                $basketItems[$item['ID']]['SUM_FULL_PRICE_FORMATED'] =
                CCurrencyLang::CurrencyFormat($basketItems[$item['ID']]['SUM_FULL_PRICE'], $basketItems[$item['ID']]['CURRENCY'], true);
            }
        }

        $result = [];
        $total_data = [
            'ITEMS_PRICE' => 0,
            'DISCOUNT_PRICE' => 0,
            'DELIVERY_PRICE' => 0,
            'BONUSES' => 0,
            'TOTAL_PRICE' => 0,

        ];

        $catalog_ids = filter_items_id_by_iblock_id($product_ids, get_iblock_id_by_code('menu-devilery'));
        $result['ITEMS'] = [];
        foreach ($basketItems as $item) {
            if (in_array($item['PRODUCT_ID'], $catalog_ids)) {
                $result['ITEMS'][$item['ID']] = $item;
                $total_data['ITEMS_PRICE'] += $item['SUM_FULL_PRICE'];
                $total_data['DISCOUNT_PRICE'] += $item['SUM_FULL_PRICE'];
            }
        }
        $delivery_price = get_delivery_price();
        $total_data['DELIVERY_PRICE'] = $delivery_price['DELIVERY_PRICE'];
        $total_data['BONUSES'] = get_bonuses($total_data['DISCOUNT_PRICE']);
        $total_data['TOTAL_PRICE'] = $total_data['DISCOUNT_PRICE'] + $total_data['DELIVERY_PRICE'];

        $result['TOTAL'] = $total_data;
        $result['DELIVERY_STEP_TO_FREE'] = $delivery_price['STEP_TO_FREE'];
        return $result;
    }

    public function get_recomendations()
    {
        CModule::IncludeModule("sale");
        CModule::IncludeModule("iblock");

        $filter = ['IBLOCK_ID' => get_iblock_id_by_code('menu-devilery'), 'ACTIVE' => 'Y'];
        $select = ['ID', 'NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT'];
        $res = CIBlockElement::GetList([], $filter, false, ["nTopCount" => 10], $select);

        $result = [];

        while ($item = $res->getNext()) {
            $price = CCatalogProduct::GetOptimalPrice($item['ID'], 1);
            $data = [
                'id' => $item['ID'],
                'name' => $item['NAME'],
                'picture' => CFile::getPath($item['PREVIEW_PICTURE']),
                'over_picture' => CFile::getPath($item['PREVIEW_PICTURE']),
                'description' => $item['PREVIEW_TEXT'],
                'price' => $price['RESULT_PRICE']['DISCOUNT_PRICE'],
            ];
            $result[] = $data;
        }

        return $result;
    }

    public function clean_basket_action()
    {
        $this->clean_basket();
        $this->response($this->get_basket_data());
    }

    public function clean_basket()
    {
        CModule::includeModule('sale');
        CSaleBasket::DeleteAll(Fuser::getId());
    }

    public function save_user_address_action($data)
    {
        $response = $this->save_user_address($data);
        $this->response($response);
    }

    public function save_user_address($data)
    {
        global $USER;

        $result = [];

        $street = $data['street'];
        $section = $data['section'];
        $flat_call = $data['flat_call'];
        $floor = $data['floor'];
        $flat = $data['flat'];

        if(strlen($street) < 3){
            $this->response_error('Не указана улица', 'street');
            $error = true;
        }
        

        if ($USER->IsAuthorized() && !$error) {
            $user_id = $USER->getID();
            

            $UserAddress_hlb = get_hlb_by_name('UserAddress');

            $address_id = 0;

            if($data['address_id']){
                $address_id = $data['address_id'];
            }

            $fields = [
                'UF_USER_ID' => $user_id,
                'UF_STREET' => $street,
                'UF_SECTION' => $section,
                'UF_FLAT_CALL' => $flat_call,
                'UF_FLOOR' => $floor,
                'UF_FLAT' => $flat,
            ];

            $UserAddress_hlb::add($fields);

            
            $result['user']['is_auth'] = true;
        } else {
            $result['user']['is_auth'] = false;
        }

        //$result['source_data'] = $this->get_source_data($data);

        return $result;
    }

    public function get_user_saved_address_list()
    {
        $UserAddress_hlb = get_hlb_by_name('UserAddress');
        global $USER;
        $user_id = $USER->getID();

        $filter = ['UF_USER_ID' => $user_id];
        $select = ['*'];

        $res = $UserAddress_hlb::getList([
            'filter' => $filter,
            'select' => $select,
        ]);

        $address_list = [];

        while ($address = $res->fetch()) {
            $address_list[] = [
                'street' => $address['UF_STREET'],
                'section' => $address['UF_SECTION'],
                'flat_call' => $address['UF_FLAT_CALL'],
                'floor' => $address['UF_FLOOR'],
                'flat' => $address['UF_FLAT'],
                'name' => build_user_address_name($address),
                'id' => $address['ID'],

            ];
        }

        return $address_list;
    }

    public function get_source_data($data)
    {
        $result = [
            'cafe_list' => get_cafe_list(),
            'user_saved_address_list' => $this->get_user_saved_address_list(),
            'pay_method_list' => $this->get_pay_system_list(DEFAULT_DELIVERY_TYPE),
            'promocodes' => $this->get_applied_promocode_list(),
        ];

        return $result;
    }

    public function get_pay_system_list($delivery_type)
    {
        CModule::includeModule('sale');
        $list = [];
        switch ($delivery_type) {
            case 'delivery_man': {
                $res = CSalePaySystem::GetList([], array("LID"=>SITE_ID, "CURRENCY"=>"RUB", "ACTIVE"=>"Y"));
                while ($ptype = $res->Fetch()) {
                    $list[] = [
                        'id' => $ptype['ID'],
                        'name' => $ptype['NAME'],
                        'default' => $ptype['ID'] == DEFAULT_PAY_SYSTEM
                    ];
                }
                break;
            }
            case 'delivery_self': {
                $res = CSalePaySystem::GetList([], array("LID"=>SITE_ID, "CURRENCY"=>"RUB", "ACTIVE"=>"Y"));
                while ($ptype = $res->Fetch()) {
                    $list[] = [
                        'id' => $ptype['ID'],
                        'name' => $ptype['NAME'],
                        'default' => $ptype['ID'] == DEFAULT_PAY_SYSTEM
                    ];
                }
                break;
            }
        }
        //print_r($list);
        return $list;
    }

    public function apply_promocode_action($data)
    {
        $this->apply_promocode($data);
        $response['basket_data'] = $this->get_basket_data();
        $this->response($response);
    }

    public function apply_promocode($data)
    {
        $promocode = $data['promocode'];
        if (trim($promocode) == '') {
            return;
        }
        $_SESSION['order_promocode'] = $promocode;
    }

    public function clear_promocode()
    {
        $_SESSION['order_promocode'] = false;
        \Bitrix\Sale\DiscountCouponsManager::clear();
    }

    public function get_applied_promocode_list()
    {
        $promocode = $_SESSION['order_promocode'];
        if ($promocode) {
            return $promocode;
        } else {
            return '';
        }
    }

    public function change_delivery_type_action($data)
    {
        $response['pay_systems'] = [];
        $delivery_type = $data['delivery_type'];
        switch ($delivery_type) {
            case 'delivery_man': {
                $response['pay_systems'] = $this->get_pay_system_list($delivery_type);
                break;
            }
            case 'delivery_self': {
                $response['pay_systems'] = $this->get_pay_system_list($delivery_type);
                break;
            }
            default: {

                $this->response_error('Несуществующий тип доставки.');
            }
        }

        $this->response($response);
    }

    public function get_delivery_price_action($data){

        $response = get_delivery_price($data['virtual_price']);
        $this->response($response);
    }

    public function remove_user_address_action($data){
        $response = $this->remove_user_address($data['id']);
        $this->response($response);
    }

    public function remove_user_address($id){

        $addr_hlb = get_hlb_by_name('UserAddress');
        global $USER;
        if(!$USER->IsAuthorized()){
            return [];
        }
        $user_id = $USER->getID();

        $filter = ['UF_USER_ID' => $user_id, 'ID' => $id];
        $select = ['ID'];

        $res = $addr_hlb::getList([
            'filter' => $filter,
            'select' => $select,
        ]);
        $addr_id = 0;
        while($addr = $res->fetch()){
            $addr_id = $addr['ID'];
        }

        if($addr_id != 0){
            $addr_hlb::delete($addr_id);
        }

        return [];
    }


}
