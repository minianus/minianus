<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
require __DIR__ . '/api.php';

$method = $_SERVER['REQUEST_METHOD'];
$request_data = [];

switch ($method) {
    case 'GET':{
            $action = $_REQUEST['action'];
            $item_id = $_REQUEST['item_id'];
            $request_data = $_REQUEST;
            break;
        }
    case 'POST':{
            $postData = file_get_contents('php://input');
            $data = json_decode($postData, true);

            $action = $data['action'];
            $item_id = $data['item_id'];
            $request_data = $data;
            break;
        }
    default:{
            $api->get_empty_response();
            die();
        }
}

$api = new order_api();

switch ($action) {

    case 'apply_promocode':{
            $api->apply_promocode_action($request_data);
            break;
        }
    case 'make_new_order':{
            $api->make_new_order_action($request_data);
            break;
        }
    case 'get_order_data':{
            $api->get_order_data_action($request_data);
            break;
        }
    case 'get_basket_data':{
            $api->get_basket_data_action();
            break;
        }
    case 'remove_item':{
            $api->remove_item_action($item_id);
            break;
        }
    case 'inc_item_count':{
            $api->inc_item_count_action($item_id);
            break;
        }
    case 'dec_item_count':{
            $api->dec_item_count_action($item_id);
            break;
        }
    case 'get_recomendations':{
            $api->get_recomendations_action();
            break;
        }
    case 'add_to_basket':{
            $api->add_to_basket_action($request_data);
            break;
        }
    case 'save_user_address_action':{
            $api->save_user_address_action($request_data);
            break;
        }
    case 'change_delivery_type':{
            $api->change_delivery_type_action($request_data);
            break;
        }
    case 'get_delivery_price':{
            $api->get_delivery_price_action($request_data);
            break;
        }
    case 'remove_user_address':{
            $api->remove_user_address_action($request_data);
            break;
        }
    default:{
            $api->get_empty_response();
        }
}
die();
