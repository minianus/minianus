<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(" ");
?>

    <div class="wrap pomidor_kurer">
        <div class="page__title-bg sublinks" style="background-image:url('/upload/images/cap5.jpg')">
            <h1 class="page__title">Территория доставки</h1>
            <div class="sublinks__content">
                <span>Курьером</span>
                <a href="/dostavka-i-oplata/samovyvoz.php">Самовывоз</a>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent('bitrix:news.list','dostavka_pay_methods',Array(
            'AJAX_MODE' => 'N',
            'IBLOCK_TYPE' => 'content',
            'IBLOCK_ID' => get_iblock_id_by_code('pay_methods'),
            'NEWS_COUNT' => '20',
            'SORT_BY1' => 'SORT',
            'SORT_ORDER1' => 'DESC',
            'SORT_BY2' => 'ID',
            'SORT_ORDER2' => 'ASC',
            'FILTER_NAME' => '',
            'FIELD_CODE' => Array('ID'),
            'PROPERTY_CODE' => ['ICON'],
            'CHECK_DATES' => 'Y',
            'DETAIL_URL' => '',
            'PREVIEW_TRUNCATE_LEN' => '',
            'ACTIVE_DATE_FORMAT' => 'd.m.Y',
            'SET_TITLE' => 'N',
            'SET_BROWSER_TITLE' => 'N',
            'SET_META_KEYWORDS' => 'N',
            'SET_META_DESCRIPTION' => 'N',
            'SET_LAST_MODIFIED' => 'Y',
            'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
            'ADD_SECTIONS_CHAIN' => 'N',
            'HIDE_LINK_WHEN_NO_DETAIL' => 'Y',
            'PARENT_SECTION' => '',
            'PARENT_SECTION_CODE' => 'kurerom',
            'INCLUDE_SUBSECTIONS' => 'Y',
            'CACHE_TYPE' => 'A',
            'CACHE_TIME' => '3600',
            'CACHE_FILTER' => 'Y',
            'CACHE_GROUPS' => 'Y',
            'DISPLAY_TOP_PAGER' => 'N',
            'DISPLAY_BOTTOM_PAGER' => 'N',
            'SET_STATUS_404' => 'N',
            'SHOW_404' => 'N',
            'MESSAGE_404' => '',
            'AJAX_OPTION_JUMP' => 'N',
            'AJAX_OPTION_STYLE' => 'Y',
            'AJAX_OPTION_HISTORY' => 'N',
            'AJAX_OPTION_ADDITIONAL' => ''
        )
        );?> 
        <div class="section section_t-offset loyalty__about">
            <h2 class="section__title big text-center">Доставка в вашем городе</h2>
            <div class="row row_tablet-small">
                <div class="col col--lg-6">
                    <div class="loyalty__about__item window window_shadow window_bordered window_s-offset window_b-offset match-height" style="height: 247px;">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => '/include/dostavka/inc2.php',
                    "EDIT_TEMPLATE" => "standard.php"
                    )
                    );?>
                    </div>
                </div>
                <div class="col col--lg-6">
                    <div class="loyalty__about__item window window_shadow window_bordered window_s-offset window_b-offset match-height" style="height: 247px;">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => '/include/dostavka/inc3.php',
                    "EDIT_TEMPLATE" => "standard.php"
                    )
                    );?>
                    </div>
                </div>
            </div>
            <div class="line-w window_b-offset mt-2">
                <div href="#" class="line-w__pic" style="background-image: url('/upload/images/car-bg.png'); width:100%;">
                        <span class="line-w__slogan">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => '/include/dostavka/inc4.php',
                    "EDIT_TEMPLATE" => "standard.php"
                    )
                    );?>
                        </span>
                </div>
            </div>
        </div>
        <div class="section section_t-offset">
            <h2 class="section__title big">Территория доставки</h2>
            <?if(get_city() == 'habarovsk'){?>
            <div class="box box_b-large" style="margin-top: -2rem">
                <div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps/76/khabarovsk/?utm_medium=mapframe&amp;utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Хабаровск</a><a href="https://yandex.ru/maps/76/khabarovsk/?ll=135.155051%2C48.452490&amp;mode=usermaps&amp;source=constructorLink&amp;um=constructor%3A16a7af615cdd1cd70051bd49034ed64385164c74ce4e88641cb3b348a06cde00&amp;utm_medium=mapframe&amp;utm_source=maps&amp;z=11" style="color:#eee;font-size:12px;position:absolute;top:14px;">Яндекс.Карты — поиск мест и адресов, городской транспорт</a><iframe src="https://yandex.ru/map-widget/v1/-/CCQ~fQuMoC" width="100%" height="500" frameborder="0" allowfullscreen="true" style="position:relative;"></iframe></div>
            </div>
            <?}else if(get_city() == 'komsomolsk'){?>
            <div class="box box_b-large">
                <div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps/11453/komsomolsk-at-amur/?utm_medium=mapframe&amp;utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Комсомольск‑на‑Амуре</a><a href="https://yandex.ru/maps/11453/komsomolsk-at-amur/?ll=137.100832%2C50.577995&amp;mode=usermaps&amp;source=constructorLink&amp;um=constructor%3A988eb57d919e4f18d8892d22e2aaf9b358b0a98b9522964159b84320221f5ad2&amp;utm_medium=mapframe&amp;utm_source=maps&amp;z=12" style="color:#eee;font-size:12px;position:absolute;top:14px;">Яндекс.Карты — поиск мест и адресов, городской транспорт</a><iframe src="https://yandex.ru/map-widget/v1/-/CCUA74uUKB" width="100%" height="500" frameborder="0" allowfullscreen="true" style="position:relative;"></iframe></div>
            </div>
            <?}?>
        </div>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>