<div class="window__header">
    <h4 class="h4 window__title text-center">
        Оставить отзыв
    </h4>
</div>
<p class="block mb-2 text-center">Поделитесь с другими вашими впечатлениями</p>
<form action="#" class="form form-reset" id="feedback_form" onsubmit="feedback_form_send(event)">
    <div class="form__item form__item_s-offset">
        <input type="text" placeholder="Имя" class="input input_default input_yellow" name="name"  required>
    </div>
    <div class="row">
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="E-mail" class="input input_default input_yellow" name="email">
            </div>
        </div>
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Телефон" class="input input_default input_yellow" name="phone">
            </div>
        </div>
    </div>
    <p class="block mb-2 small">Ваш E-mail и телефон не будут видны другим пользователям</p>
    <div class="form__item form__item_s-offset -custom-select">
        <select class="input input_default input_yellow" name="restoran">
            <option>Выберите кафе</option>
            <?$APPLICATION->IncludeComponent('bitrix:news.list','restoran_list_for_select',Array(
                    'AJAX_MODE' => 'N',
                    'IBLOCK_TYPE' => 'Restorans',
                    'IBLOCK_ID' => get_iblock_id_by_code('restaurants'),
                    'NEWS_COUNT' => '20',
                    'SORT_BY1' => 'SORT',
                    'SORT_ORDER1' => 'DESC',
                    'SORT_BY2' => 'ID',
                    'SORT_ORDER2' => 'ASC',
                    'FILTER_NAME' => '',
                    'FIELD_CODE' => Array('ID'),
                    'PROPERTY_CODE' => [],
                    'CHECK_DATES' => 'Y',
                    'DETAIL_URL' => '',
                    'PREVIEW_TRUNCATE_LEN' => '',
                    'ACTIVE_DATE_FORMAT' => 'd.m.Y',
                    'SET_TITLE' => 'N',
                    'SET_BROWSER_TITLE' => 'N',
                    'SET_META_KEYWORDS' => 'N',
                    'SET_META_DESCRIPTION' => 'N',
                    'SET_LAST_MODIFIED' => 'Y',
                    'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
                    'ADD_SECTIONS_CHAIN' => 'N',
                    'HIDE_LINK_WHEN_NO_DETAIL' => 'Y',
                    'PARENT_SECTION' => '',
                    'PARENT_SECTION_CODE' => get_city(),
                    'INCLUDE_SUBSECTIONS' => 'Y',
                    'CACHE_TYPE' => 'A',
                    'CACHE_TIME' => '3600',
                    'CACHE_FILTER' => 'Y',
                    'CACHE_GROUPS' => 'Y',
                    'DISPLAY_TOP_PAGER' => 'N',
                    'DISPLAY_BOTTOM_PAGER' => 'N',
                    'SET_STATUS_404' => 'N',
                    'SHOW_404' => 'N',
                    'MESSAGE_404' => '',
                    'AJAX_OPTION_JUMP' => 'N',
                    'AJAX_OPTION_STYLE' => 'Y',
                    'AJAX_OPTION_HISTORY' => 'N',
                    'AJAX_OPTION_ADDITIONAL' => ''
                )
                );?>
        </select>
    </div>
    <div class="form__item form__item_s-offset">
        <textarea placeholder="Ваш отзыв" class="input input_default input_yellow" name="feedback"  required></textarea>
    </div>
    <div class="form__item form__item_s-offset mt-1">
        <label class="checkbox checkbox_yellow">
            <input type="checkbox" class="checkbox__input" name="agree" value="Y" >
            <span class="checkbox__label">
                Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
            </span>
        </label>
    </div>
    <div class="form__item form__item_s-offset">
        <button  class="btn btn_yellow btn_middle">
            Оставить отзыв
        </button>
    </div>
    <span id="result_success2" style="color: green;"></span>
    <span id="result_errors2" style="color: red;"></span>
</form>
<script>
function clear_message2() {
    $('#result_success2').html('');
    $('#result_errors2').html('');
}

function show_message2(data) {
    clear_message2();
    var s = '';
    if (typeof(data) == 'string') {
        s = data;
    } else if (typeof(data) == 'object') {
        data.forEach(function(item, i, arr) {
            s += item + '<br>';
        });
    }

    $('#result_success2').html(s);
}

function show_error2(data) {
    clear_message2();
    var s = '';
    if (typeof(data) == 'string') {
        s = data;
    } else if (typeof(data) == 'object') {
        data.forEach(function(item, i, arr) {
            s += item + '<br>';
        });
    }
    $('#result_errors2').html(s);
}

function feedback_form_send(event) {
    event.preventDefault();

    var form = $('#feedback_form');

    var data = form.serialize();

    event.preventDefault();
    $.ajax({
        url: '/ajax/add_review.php',
        method: 'get',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.error == false) {
                $.magnificPopup.close();
                $.magnificPopup.open({
                    items: {
                        src: $('#feedback_tnx')
                    },
                    type: 'inline'
                });
            } else {
                show_error2(data.errors);

            }
        },
        error: function(data) {
            show_error2('Произошла ошибка. Попробуйте еще раз позднее.');
        }
    });
}
</script>

<div class="popup middle-popup tnx-popup mfp-hide" id="feedback_tnx">
    <img class="tnx-popup__img" src="images/pizza-popup.svg" alt="">
    <div class="">
        <h3 class="tnx-popup__title">Спасибо, ваш отзыв принят!</h3>
        <a href="#" class="btn btn_green tnx-popup__btn" onclick="$.magnificPopup.close();">
            ok
        </a>
    </div>
</div>