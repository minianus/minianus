<div id="cooperation" class="mfp-hide popup-block">
    <div class="window__header">
        <h4 class="h4 window__title window__title_b-offset text-center">
            Заявка на сотрудничество
        </h4>
        <p class="block mb-2 text-center">Заполните, пожалуйста, форму заказа </p>
        <form action="#" class="form form-reset" id="cooperation_form" onsubmit="cooperation_form_send(event)" enctype="multipart/form-data">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Номер телефона" class="input input_default input_yellow time-w__input phone-mask" name="phone">
            </div>
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Название компании" class="input input_default input_yellow time-w__input" name="company_name">
            </div>
            <div class="form__item form__item_s-offset">
                <label class="file-w file-w_offset" data-title="Прикрепить коммерческое предложение">
                    <input type="file" class="file-w__input" name="commerce_presentation">
                    <span class="file-w__label">
                        <img class="file-w__ico" src="/local/templates/pomidor/images/file-w-ico.svg" alt="">
                        <span class="file-w__title">
                            Прикрепить коммерческое предложение
                        </span>
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <label class="checkbox checkbox_yellow">
                    <input type="checkbox" class="checkbox__input" name="agree" value="Y">
                    <span class="checkbox__label">
                        Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <button class="btn btn_rounded btn_yellow btn_middle">
                    Откликнуться
                </button>
            </div>
            <span style="color: red;" id="error_message"></span>
            <span style="color: green;" id="success_message"></span>
        </form>
        
    </div>
</div>
<script>
    function cooperation_form_send(event){
        event.preventDefault();
        var form = $(event.currentTarget);
        var formData = new FormData(form.get(0));

        $.ajax({
        url: '/ajax/cooperation_form.php',
        contentType: false, 
        processData: false, 
        method: 'POST',
        dataType: 'json',
        data: formData,
        success: function(result) {
            if (result.error == false) {
                form_show_success(form, 'Ваша заявка успешно принята!');
            } else {
                
                form_show_error(form, result.errors);

            }
        },
        error: function(result) {
            form_show_error(form, 'Неопределенная ошибка. Администрация сайта уведомлена. Попробуйте пожалуйста позже.');
        }
    });

    }
</script>