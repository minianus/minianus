<div class="window__header">
    <h4 class="h4 window__title text-center">
        Забронировать стол
    </h4>
</div>
<p class="block mb-2 text-center">Заполните, пожалуйста, форму заказа </p>
<form action="#" class="form form-reset" id="table_book" onsubmit="table_book_form_send(event)">
    <div class="form__item form__item_s-offset -custom-select">
        <select class="input input_default input_yellow" name="restoran">
            <option>Выберите кафе</option>
            <?$APPLICATION->IncludeComponent('bitrix:news.list','restoran_list_for_select',Array(
                    'AJAX_MODE' => 'N',
                    'IBLOCK_TYPE' => 'Restorans',
                    'IBLOCK_ID' => get_iblock_id_by_code('restaurants'),
                    'NEWS_COUNT' => '20',
                    'SORT_BY1' => 'SORT',
                    'SORT_ORDER1' => 'DESC',
                    'SORT_BY2' => 'ID',
                    'SORT_ORDER2' => 'ASC',
                    'FILTER_NAME' => '',
                    'FIELD_CODE' => Array('ID'),
                    'PROPERTY_CODE' => [],
                    'CHECK_DATES' => 'Y',
                    'DETAIL_URL' => '',
                    'PREVIEW_TRUNCATE_LEN' => '',
                    'ACTIVE_DATE_FORMAT' => 'd.m.Y',
                    'SET_TITLE' => 'N',
                    'SET_BROWSER_TITLE' => 'N',
                    'SET_META_KEYWORDS' => 'N',
                    'SET_META_DESCRIPTION' => 'N',
                    'SET_LAST_MODIFIED' => 'Y',
                    'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
                    'ADD_SECTIONS_CHAIN' => 'N',
                    'HIDE_LINK_WHEN_NO_DETAIL' => 'Y',
                    'PARENT_SECTION' => '',
                    'PARENT_SECTION_CODE' => get_city(),
                    'INCLUDE_SUBSECTIONS' => 'Y',
                    'CACHE_TYPE' => 'A',
                    'CACHE_TIME' => '3600',
                    'CACHE_FILTER' => 'Y',
                    'CACHE_GROUPS' => 'Y',
                    'DISPLAY_TOP_PAGER' => 'N',
                    'DISPLAY_BOTTOM_PAGER' => 'N',
                    'SET_STATUS_404' => 'N',
                    'SHOW_404' => 'N',
                    'MESSAGE_404' => '',
                    'AJAX_OPTION_JUMP' => 'N',
                    'AJAX_OPTION_STYLE' => 'Y',
                    'AJAX_OPTION_HISTORY' => 'N',
                    'AJAX_OPTION_ADDITIONAL' => ''
                )
                );?>
        </select>
    </div>
    <div class="row">
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <div class="time-w">
                    <img class="time-w__ico" src="/upload/images/i-calendar.svg" alt="">
                    <input readonly type="text" placeholder="дд.мм.гггг"
                        class="input input_default input_yellow time-w__input datepicker-here" name="date">
                </div>
            </div>
        </div>
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <div class="time-w">
                    <img class="time-w__ico" src="/upload/images/i-clock.png" alt="">
                    <input type="time" class="input input_default input_yellow time-w__input time-w__input_s"
                        name="time">
                </div>
            </div>
        </div>
    </div>
    <p class="block mb-2 small">Для бронирования стола в выходные звоните по номеру желаемого кафе</p>
    <div class="form__item form__item_s-offset">
        <p class="block mb small">Количество человек</p>
        <div class="counter">
            <span class="counter__btn counter__btn_minus"></span>
            <input type="text" class="counter__input" value="2" name="people_count">
            <span class="counter__btn counter__btn_plus "></span>
        </div>
    </div>
    <div class="row">
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Имя" class="input input_default input_yellow" name="name">
            </div>
        </div>
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Телефон" class="input input_default input_yellow phone-mask"
                    name="phone">
            </div>
        </div>
    </div>
    <div class="form__item form__item_s-offset">
        <p class="block mb small">Приготовить блюда к приходу</p>
        <input type="text" placeholder="Пицца 4 сыра" class="input input_default input_yellow" name="cook_for_me">
    </div>
    <div class="form__item form__item_s-offset">
        <textarea placeholder="Комментарии" class="input input_default input_yellow" name="comment"></textarea>
    </div>
    <div class="form__item form__item_s-offset mt-1">
        <label class="checkbox checkbox_yellow">
            <input type="checkbox" class="checkbox__input" name="agree">
            <span class="checkbox__label">
                Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
            </span>
        </label>
    </div>
    <div class="form__item form__item_s-offset">
        <button class="btn btn_yellow btn_middle">
            Забронировать
        </button>
    </div>
    <span style="color: red;" id="error_message"></span>
    <span style="color: green;" id="success_message"></span>
</form>

<script>
function table_book_form_send(event) {
    event.preventDefault();

    var form = $('#table_book');

    var data = form.serialize();

    event.preventDefault();
    $.ajax({
        url: '/ajax/table_book_form.php',
        method: 'get',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.error == false) {
                $.magnificPopup.close();
                $.magnificPopup.open({
                    items: {
                        src: $('#tnx')
                    },
                    type: 'inline'
                });
            } else {
                form_show_error(form, data.errors);

            }
        },
        error: function(data) {
            form_show_error(form, 'Произошла ошибка. Попробуйте еще раз позднее.');
        }
    });
}
</script>