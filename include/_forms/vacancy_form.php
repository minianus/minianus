<div class="col col--lg-6">
    <div class="window window_shadow window_bordered window_s-offset job__form">
        <div class="window__header">
            <h4 class="h4 window__title">
                Присоединяйтесь к нашей команде
            </h4>
        </div>
        <p class="gray block mb-2">Заполните анкету и мы рассмотрим вашу заявку</p>
        <form action="#" class="form form-reset" id="vacancy_form" onsubmit="vacancy_form_send(event)">
            <div class="form__item form__item_s-offset">
                <select class="input input_default input_yellow" name="vacancy_city" onchange="renew_vacancy_data(event)">
                    <option value="0" selected>Город</option>
                    <option value="khabarovsk" >Хабаровск</option>
                    <option value="komsomolsk-na-amure">Комсомольск-на-Амуре</option>
                </select>
            </div>
            
            <div class="form__item form__item_s-offset">
                <select class="input input_default input_yellow" name="cafe" onchange="renew_vacancy_data(event)">
                    <option value="0">Адрес кафе</option>
                </select>
            </div>
            <div class="form__item form__item_s-offset">
                <select class="input input_default input_yellow" name="vacancy" onchange="renew_vacancy_data(event)">
                    <option value="0">Вакансия</option>
                </select>
            </div>
            <hr class="w-total__hr">
            <div class="row">
                <div class="col--md-6">
                    <div class="form__item form__item_s-offset">
                        <input type="text" placeholder="Имя" class="input input_default input_yellow" name="name">
                    </div>
                </div>
                <div class="col--md-6">
                    <div class="form__item form__item_s-offset">
                        <input type="text" placeholder="Фамилия" class="input input_default input_yellow" name="surname">
                    </div>
                </div>
                <div class="col--md-6">
                    <div class="form__item form__item_s-offset">
                        <input type="date" placeholder="дд.мм.гггг" class="input input_default input_yellow" name="birthday">
                    </div>
                </div>
                <div class="col--md-6">
                    <div class="form__item form__item_s-offset">
                        <input type="text" placeholder="Телефон" class="input input_default input_yellow phone-mask" name="phone">
                    </div>
                </div>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <label class="checkbox checkbox_yellow">
                    <input type="checkbox" class="checkbox__input" name="agree" value="Y">
                    <span class="checkbox__label">
                        Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset">
                <button class="btn btn_yellow btn_middle">
                    Откликнуться
                </button>
            </div>
            <span style="color: red;" id="error_message"></span>
            <span style="color: green;" id="success_message"></span>
        </form>
    </div>
</div>
<script>
    function renew_vacancy_data(event){
        var form = $('#vacancy_form');
        
        var vacancy_city = form.find('select[name="vacancy_city"]');
        var cafe = form.find('select[name="cafe"]');
        var vacancy = form.find('select[name="vacancy"]');
        var changed = $(event.currentTarget);

        
        
        var data = [];
        data.push({
            'name' : 'vacancy_city',
            'value' : vacancy_city.val()
        });
        data.push({
            'name' : 'cafe',
            'value' : cafe.val()
        });
        data.push({
            'name' : 'changed',
            'value' : changed.attr('name')
        });
        
        $.ajax({
        url: '/ajax/vacancy_data_source.php',
        method: 'get',
        dataType: 'json',
        data,
        success: function(result) {
            if (result.error == false) {
               if(result.cafelist){
                   cafe.html('');
                   cafe.append('<option value="0" selected>Адрес кафе</option>')
                for(option of result.cafelist){
                    cafe.append('<option value="' + option.ID + '">' + option.NAME + '</option>');
                }
               }
               if(result.vacancylist){
                vacancy.html('');
                vacancy.append('<option value="0">Вакансия</option>');
                for(option of result.vacancylist){
                    vacancy.append('<option value="' + option.ID + '">' + option.NAME + '</option>');
                }
               }
            } else {
                form_show_error(form, 'Неопределенная ошибка. Администрация сайта уведомлена. Попробуйте пожалуйста позже.');

            }
        },
        error: function(result) {
            form_show_error(form, 'Неопределенная ошибка. Администрация сайта уведомлена. Попробуйте пожалуйста позже.');
        }
    });
    }

    function vacancy_form_send(event){
        event.preventDefault();
        var form = $('#vacancy_form');
        var data = form.serialize();

        $.ajax({
        url: '/ajax/vacancy_form.php',
        method: 'get',
        dataType: 'json',
        data,
        success: function(result) {
            if (result.error == false) {
                form_show_success(form, 'Ваш отклик принят!');
            } else {
                form_show_error(form, result.errors);

            }
        },
        error: function(result) {
            form_show_error(form, 'Неопределенная ошибка. Администрация сайта уведомлена. Попробуйте пожалуйста позже.');
        }
    });

    }
</script>
