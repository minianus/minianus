<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<div id="basket_top">
    <span id="basket_icon">
    </span>
    <div id="popup-cart" class="mfp-hide popup-block popup-block_small">
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        <div id="popup_content">
        </div>
    </div>

</div>

<script>

$(document).ready(function() {

    redraw_basket_top({});
})
</script>