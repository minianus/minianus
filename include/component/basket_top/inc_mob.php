<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<a class="btn btn__cart mobile__btn popup-call" href="#popup-cart">
    <svg class="icon icon-cart-h mobile__icon">
        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#icon-cart-h"></use>
    </svg>
    <span class="cart__count" id="mob_items_count">...</span>
</a>


<script>
$(document).ready(function() {

    redraw_basket_top({});
})
</script>