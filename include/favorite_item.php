<div class="favorite__container">
	<div class="favorite__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
		<div class="favorite__image">
 <img alt="image-pasta.png" src="/upload/medialibrary/0d3/0d328fe540a015c90c65a786e45941a6.png" title="image-pasta.png"><br>
 <br>
		</div>
		<div class="favorite__link-wrap">
 <a class="btn btn_fav" href="/menu/">Кухня</a>
		</div>
	</div>
	<div class="favorite__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="800">
		<div class="favorite__image">
 <img alt="image-bar.png" src="/upload/medialibrary/110/1101f76f1f6e01a05a5d4c265f1b90c5.png" title="image-bar.png"><br>
 <br>
		</div>
		<div class="favorite__link-wrap">
 <a class="btn btn_fav" href="/upload/files/bannaya-karta.pdf">Барная карта</a>
		</div>
	</div>
	<div class="favorite__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1000">
        <div class="fav-window">
                            <span class="fav-window__title">
                                Только на Ленина, 53
                            </span>
        </div>
		<div class="favorite__image">
 <img alt="image-vine.png" src="/upload/medialibrary/699/69964b7b24eab8b8efeaf7ca953ccbcf.png" title="image-vine.png">
		</div>
		<div class="favorite__link-wrap">
 <a class="btn btn_fav" href="/upload/files/vinnaya-karta.pdf" title="Винная карта имеется только в меню кафе на ул. Ленина, 53">Винная карта</a>
		</div>
	</div>
</div>
 <br>