<?php

/**
 * Сохраняем в сессию части заказа
 */

include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$field = $_REQUEST['field'];
$value = $_REQUEST['value'];

if (!empty($field) && !empty($value)) {
    if ($field == 'LOCATION') {
        $value = json_decode($value);
    }

    $_SESSION['CURRENT_ORDER'][$field] = $value;
}
