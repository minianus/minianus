<?php

namespace Pomidor\General;

/**
 * Класс для работы с настройками
 */
class Settings
{

    /**
     * Получаем конфиг из модуля grain.customsettings
     *
     * @param string $settingName
     * @return string
     */
    public static function getSetting(string $settingName): string
    {
        $value = \Bitrix\Main\Config\Option::get("grain.customsettings", $settingName);
        return  $value ? $value : '';
    }
}
