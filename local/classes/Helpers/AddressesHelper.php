<?php

namespace Pomidor\Helpers;

use Bitrix\Main\Loader;

Loader::includeModule('highloadblock');

/**
 * работа с адресами пользователя
 */
class AddressesHelper
{
    /**
     * Ключи в базе свзанные с адресом
     */
    public const ADDRESS_KEYS = [
        'UF_STREET',
        'UF_SECTION',
        'UF_FLAT_CALL',
        'UF_FLOOR',
        'UF_FLAT'
    ];

    /**
     * Highload блок с адресами
     *
     * @var HighloadBlockTable
     */
    private  $hl_data_class;
    /**
     * ID юзера
     *
     * @var int
     */
    private  $userId;

    /**
     * @param int $userId
     */
    public function __construct( $userId)
    {
        $this->hl_data_class = get_hlb_by_name('UserAddress');
        $this->userId = $userId;
    }

    /**
     * Добавляем запись в highload
     *
     * @param array $fields
     */
    public function addItem( $fields)
    {
        $this->hl_data_class::add($fields);
    }

    /**
     * Список текущих адресов.
     *
     * @param array $extraFilter
     * @return array
     */
    public function getList( $extraFilter = [])
    {
        //TODO CACHE
        $result = [];
        $filter = [
            'UF_USER_ID' => $this->userId,
        ];
        if ($extraFilter) {
            $filter = array_merge($filter, $extraFilter);
        }
        $res = $this->hl_data_class::getList([
            'filter' => $filter,
        ]);
        while ($row = $res->fetch()) {
            $result[$row['ID']] = $row;
        }

        return $result;
    }

    /**
     * Удаление адреса.
     *
     * @param int $id
     *
     * @return void
     */
    public function deleteItem( $id)
    {
        $this->hl_data_class::delete($id);
    }
}
