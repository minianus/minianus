<?php

namespace Pomidor\Helpers;

use Pomidor\General\Settings;
use Pomidor\Helpers\CityHelper;
use Bitrix\Main\Loader;

/**
 * Хелпер для работы с бонусами
 */
class BonusHelper
{
    /**
     * Получаем личный счет юзера
     *
     * @param int $userId
     * @return float
     */
    public static function getUserBonuses(int $userId): float
    {
        Loader::includeModule('sale');
        if ($ar = \CSaleUserAccount::GetByUserID($userId, "RUB")) {
            return $ar['CURRENT_BUDGET'];
        }
        return 0;
    }

    /**
     * Максимальный процент бонусов что может быть использован для заказа
     *
     * @return float
     */
    public static function getBonusMaxPercent(): float
    {
        if ($max_bonus_percent = Settings::getSetting("max_bonus_percent")) {
            return $max_bonus_percent;
        }
        return 0;
    }

    /**
     * Активна ли бонусная система
     *
     * @return bool
     */
    public static function isBonusSystemEnabled(): bool
    {
        return Settings::getSetting("bonus_system_" . CityHelper::getCurCity()) == 'Y' ? true : false;
    }
}
