<?php

namespace Pomidor\Helpers;

/**
 * Хелпер для работы с городов
 */
class CityHelper
{
    /**
     * Код города из куки
     *
     * @return string
     */
    public static function getCurCity(): string
    {
        $cityCode = $_COOKIE[CITY_COOKIE_NAME];

        return $cityCode ? $cityCode : '';
    }
}
