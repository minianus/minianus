<?php

namespace Pomidor\Helpers;

use Pomidor\General\Settings;
use Bitrix\Sale;

/**
 * Хелпер для оформления заказа
 */
class OrderHelper
{
    /**
     * поучаем сумму от которой бесплатная доставка
     *
     * @param string $city
     * @return float
     */
    public static function getFreeDeliverySum(string $city = ''): float
    {
        $freeDeliveryPrice = 0;
        if (empty($city)) {
            $city = get_city();
            if (empty($city)) {
                $city = DEFAULT_CITY;
            }
        }
        $freeDeliveryPrice = (float)Settings::getSetting("free_del_" . $city);
        if (!$freeDeliveryPrice) {
            $freeDeliveryPrice = DEFAULT_FREE_DEL;
        }

        return $freeDeliveryPrice;
    }

    /**
     * Получаем стоимость доставки
     *
     * @param string $city
     * @return float
     */
    public static function getDeliveryPrice(string $city = ''): float
    {
        $deliveryPrice = 0;
        if (empty($city)) {
            $city = get_city();
            if (empty($city)) {
                $city = DEFAULT_CITY;
            }
        }
        $deliveryPrice = (float)Settings::getSetting("del_price_" . $city);
        if (!$deliveryPrice) {
            $deliveryPrice = 0;
        }

        return $deliveryPrice;
    }

    /**
     * Проверка бесплатная доставка или нет
     *
     * @return bool
     */
    public static function checkIfFreeDelivery(): bool
    {
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        return $basket->getPrice() >= self::getFreeDeliverySum();
    }
}
