<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context,
    //\Local\Util\BonusesController,
    Bitrix\Main\Application,
    Bitrix\Main\SystemException;
    use Pomidor\General\Settings;
    use Pomidor\Helpers\OrderHelper;
    use Pomidor\Helpers\AddressesHelper;
    use Pomidor\Helpers\CityHelper;
    use Pomidor\Helpers\BonusHelper;
    
class WFOrderComponent extends \CBitrixComponent
{
    protected $siteId;
    protected $currencyCode;
    protected $userType = [
        'phisic' => 1,
    ];
    protected $newUserId;
    protected $newLocation;
    protected $order;
    protected $shipment;
    protected $newDeliveryId;
    protected $newPayId;
    protected $currentCoupon;
    private  $iblockId;
    private  $bonusPercent = 0;
    private  $free_del;
    private  $userId;
    private  $isBonusSystemEnabled = false;

    protected $requiredModules = ["iblock","sale"];


    protected function isAjax()
    {
        return isset($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'y';
    }

    protected function checkModules()
    {
        foreach ($this->requiredModules as $moduleName) {
            if (!Loader::includeModule($moduleName)) {
                throw new SystemException(Loc::getMessage('COMPONENT_CHANGE_NO_MODULE', ['#MODULE#',
                                                                                         $moduleName]));
            }
        }

        return $this;
    }

    /**
     * Event called from includeComponent before component execution.
     * Takes component parameters as argument and should return it formatted as needed.
     *
     * @param  array [string]mixed $arParams
     *
     * @return array[string]mixed
     */
    public function onPrepareComponentParams($params)
    {
        global $USER;

        $this->iblockId = get_iblock_id_by_code('menu-devilery');
       
        $this->free_del = OrderHelper::getFreeDeliverySum();
        if ($USER->IsAuthorized()) {
            $this->userId = $USER->GetID();
            $this->isBonusSystemEnabled = BonusHelper::isBonusSystemEnabled();
            if ($this->isBonusSystemEnabled) {
                $this->bonusPercent = (float)Settings::getSetting("bonus_percent");
            }
        } else {
            $this->userId = 0;
        }
        return $params;
    }

    /**
     * Event called from includeComponent before component execution.
     * Includes component.php from within lang directory of the component.
     *
     * @return void
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    protected function prepareResult()
    {

        if (
            check_bitrix_sessid() &&
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            $_REQUEST['action'] == 'getAvaliableDelivery' //Проверка доставки по ID
 &&        !empty($_REQUEST['location_id']['CODE']) &&
            is_numeric($_REQUEST['location_id']['CODE'])
        ) {
            $this->newLocation = $_REQUEST['location_id']['CODE'];
            $this->getDeliveryByLocation();
            ob_end_clean();
            echo json_encode($this->arResult['DELIVERY_ARRAY']);
            exit;
        } elseif (
            check_bitrix_sessid() &&
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            $_REQUEST['action'] == 'getTotalPrice' //Проверка доставки по ID
 &&        !empty($_REQUEST['data_delivery_id'])
        ) {
            $this->newLocation = $_REQUEST['location_id']['CODE'];
            $this->getTotalPrice();
            ob_end_clean();
            $data = [
                "PRICE_WITHOUT_DISCOUNT_VALUE" => CCurrencyLang::CurrencyFormat($this->arResult['PRICE_WITHOUT_DISCOUNT_VALUE'], "RUB"),
                "DELIVERY_PRICE" => CCurrencyLang::CurrencyFormat($this->arResult['DELIVERY_PRICE'], "RUB"),
                "ORDER_PRICE" => CCurrencyLang::CurrencyFormat($this->arResult['ORDER_PRICE'], "RUB"),
                "ORDER_TOTAL_PRICE" => CCurrencyLang::CurrencyFormat($this->arResult['ORDER_TOTAL_PRICE'], "RUB"),
                'FREE_DEL_NOTIFY' => $this->arResult['FREE_DEL_NOTIFY'],
                'minusBonus' => $this->arResult['minusBonus'],
                'NEW_BALLS' => $this->arResult['NEW_BALLS']
            ];
            echo json_encode($data);
            exit;
        } elseif (
            check_bitrix_sessid() &&
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            !empty($_REQUEST['coupon']) &&
            $_REQUEST['action'] == 'checkCoupon' //Проверка оплаты после выбора доставки
        ) {
            $this->currentCoupon = $_REQUEST['coupon'];
            $this->checkCoupon();
        } elseif (
            check_bitrix_sessid() &&
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            $_REQUEST['action'] == 'getTotal' //Проверка оплаты после выбора доставки
 &&        !empty($_REQUEST['location_id']['CODE']) &&
            is_numeric($_REQUEST['location_id']['CODE']) &&
            !empty($_REQUEST['data_delivery_id']) &&
            is_numeric($_REQUEST['data_delivery_id'])
        ) {
            $this->newDeliveryId = $_REQUEST['data_delivery_id'];
            $this->newLocation = $_REQUEST['location_id']['CODE'];
            $this->getDeliveryByLocation();
            ob_end_clean();
            echo json_encode($this->arResult);
            exit;
        } elseif (
            check_bitrix_sessid() &&
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            $_REQUEST['action'] == 'disableCoupon' //Отмена купона
 &&        !empty($_REQUEST['coupon'])
        ) {
            $this->currentCoupon = $_REQUEST['coupon'];
            $this->disableCoupon();
            echo '1';
            return true;
        } else {
            $this->getResult();
        }
    }

    protected function getResult()
    {
        global $APPLICATION, $USER;
        $this->siteId = Bitrix\Main\Context::getCurrent()->getSite();
        $this->currencyCode = Option::get('sale', 'default_currency', 'RUB');
        //НЕ ТРОГАТЬ
        //Без этого метода не корректно работает D7
        //Скидка применяется по желанию Битрикса
        Sale\Compatible\DiscountCompatibility::stopUsageCompatible();
        $this->arResult['DATA'] = $this->getData();
        if (check_bitrix_sessid() && $_SERVER['REQUEST_METHOD'] == 'POST' && !$USER->isAuthorized() && !$this->getErrors() && $_REQUEST['final'] == 'yes') {
            $this->autoRegiser();
        }

        //список кафе
        $this->arResult['CAFE_LIST'] = $this->getCafeList();


        $this->arResult['USER_ID'] = $this->getUserId();
        $this->arResult['PERSON_TYPE_ID'] = $this->getPersonTypeId();
        if (!$_REQUEST['ORDER_ID']) {
            DiscountCouponsManager::init();
            $this->initOrder();
            if (is_null($this->order)) {
                $this->initOrder();
            }
            $this->applyCoupon();
            $propertyCollection = $this->order->getPropertyCollection();
            $this->arResult['PROPERTY_COLLECTION'] = $propertyCollection->getArray();
            $locationValue = $propertyCollection->getDeliveryLocation();

            if (!empty($this->newLocation)) :
                $locationValue->setValue($this->newLocation);
            endif;

            $this->order->setPersonTypeId($this->arResult['PERSON_TYPE_ID']);
            $this->order->setField('CURRENCY', $this->currencyCode);


            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite())->getOrderableItems();


            if (!$basket->getBasketItems()) {
                LocalRedirect('/personal/cart/');
            }


            $this->order->setBasket($basket);
            $this->initShipment();
            $this->initDelivery();
            $this->initPayment($this->arResult['DATA']['PAYMENT_ID']);
            $this->arResult['ORDER_TOTAL_PRICE'] = $this->order->getPrice();
            $minusBonus = 0;
            
            if ($this->userId > 0 && $this->isBonusSystemEnabled) { //работа с бонусами для авторизованного
                $this->arResult['BONUSES'] = $this->getUserBonuses();
                $this->arResult['CAN_BONUSES'] = $this->getUserBonusesCanSpend();
                $minusBonus = intval($_REQUEST['minusBalls']);
                if ($minusBonus > $this->arResult['CAN_BONUSES']) {
                    $minusBonus = $this->arResult['CAN_BONUSES'];
                }
            }
            $this->arResult['minusBonus'] = $minusBonus;
            
            /*result*/
            $collection = $this->order->getPropertyCollection();
            $this->arResult['ORDER_DATA']['PAYMENT_ID'] = $collection->getArray();
            $this->arResult['ORDER'] = $this->order;

            $couponList = $this->order->getDiscount()->getApplyResult();
            $this->arResult['COUPONS'] = $couponList['COUPON_LIST'];

            $this->getBasket();
            $this->arResult['ORDER_PRICE'] -= $minusBonus;
            $this->arResult['ORDER_TOTAL_PRICE']  -= $minusBonus;
            //корзина для отображения
            $this->arResult['DISPLAY_BASKET'] = $this->prepareDisplayBasket();
            //сколько бонусов начислим
            if ($this->bonusPercent > 0) {
                $this->arResult['NEW_BALLS'] = intval($this->arResult['ORDER_PRICE'] * $this->bonusPercent);
            }
            //сумма бесплатной доставки
            $this->arResult['FREE_DELIVERY_SUM'] = $this->free_del;
            if ($this->arResult['ORDER_PRICE'] >= $this->arResult['FREE_DELIVERY_SUM']) {
                $this->arResult['FREE_DEL_NOTIFY'] = 'Бесплатная доставка!';
            } else {
                $sum2Free = $this->arResult['FREE_DELIVERY_SUM'] - $this->arResult['ORDER_PRICE'];
                $this->arResult['FREE_DEL_NOTIFY'] = 'Для бесплатной доставки добавьте блюда еще на&nbsp;<b>' . $sum2Free . '</b> Р';
            }
            //адреса юзвера
            $this->arResult['ADDRESSES'] = $this->getUserAddresses();

            if ($_REQUEST['final'] == 'yes') {
                
                $this->arResult['DATA']['TIME'] = $this->arResult['DATA']['TIME_RADIO'] == 'soon' ? 'Как можно скорее' : $this->arResult['DATA']['TIME'];
                $propertyCollection = $this->order->getPropertyCollection();
                if (!empty($this->arResult['DATA']["UF_STREET"])) {
                    $this->arResult['DATA']["ADDRESS"] = $this->prepareOrderAddress();
                    //тогда добавляем новый адрес
                } elseif (!empty($this->arResult['DATA']["addr_selected"])) {
                    //тогда уже выбранный
                    $this->arResult['DATA']["ADDRESS"] = $this->prepareOrderAddress(false);
                }            
                if ($_REQUEST['DELIVERY_ID'] == CURIER_DELIVERY_ID) {
                    if (empty($this->arResult['DATA']["addr_selected"]) && empty($this->arResult['DATA']["UF_STREET"])) {
                        $this->arResult['ERRORS'][] = "Заполните адрес доставки";
                    }
                } else {
                    //установим кафе самовывоза
                    $this->arResult['DATA']['CAFE'] = $this->arResult['DATA']['cafe'];
                }
                //записываем свойства в заказ
                foreach ($this->arResult['DATA'] as $key => $item) {
                    $property = $this->getPropertyByCode($propertyCollection, $key);
                    if ($property) {
                        $property->setValue($item);
                    }
                }

                if ($_REQUEST['USER_DESCRIPTION']) {
                    $this->order->setField('USER_DESCRIPTION', $_REQUEST['USER_DESCRIPTION']);
                }
               
                if (!$this->getErrors()) {
                  
                    $discounts = $this->order->getDiscount();
                    $discounts->calculate();
                    $this->order->setField('STATUS_ID', DEFAULT_ORDER_STATUS_ID);
                    $this->order->doFinalAction(true);
                    $r = $this->order->save();
                // $this->saveProfile();
                    $id = $this->order->getId();
                    \Bitrix\Sale\DiscountCouponsManager::init();
                    \Bitrix\Sale\DiscountCouponsManager::clear(true);
                    \Bitrix\Sale\DiscountCouponsManager::clearApply(true);
                    if ($id > 0) {
                        $needSave = true;
                        $paymentCollection = $this->order->getPaymentCollection();
                        foreach ($paymentCollection as $payment) {
                            if ($payment->isInner() || $payment->getPaymentSystemId() == BONUS_PAYMENT_ID) {
                                $payment->setPaid("Y"); // оплата
                                if ($payment->getPaymentSystemId() == BONUS_PAYMENT_ID) {
                                    //BonusesController::changeBonusesByUserId($this->order->getUserId(),$payment->getSum(),"minus");
                                    //пишем транзакции бонусов
                                    //BonusesController::addBonusesTransactionByUserId($this->order->getUserId(),0,$payment->getSum(),$this->order->getPrice(),"Списание баллов","Списание баллов при заказе",$id);
                                } elseif ($payment->isInner()) {
                                    $paymentCollection = $this->order->getPaymentCollection();
                                    if ($paymentCollection->isPaid()) {
                                        $basketPrice = $this->order->getBasket()->getPrice();
                                        if ($this->bonusPercent > 0) {
                                            $bonus = intval($basketPrice * $this->bonusPercent);
                                            //пишем бонусы
                                            //BonusesController::changeBonusesByUserId($this->order->getUserId(),$bonus,"plus");
                                            //пишем транзакции бонусов
                                            //BonusesController::addBonusesTransactionByUserId($this->order->getUserId(),$bonus,0,$basketPrice,"Начисление баллов","Начисление баллов за оплату заказа",$id);
                                        }
                                    }
                                }

                                $needSave = true;
                            }
                        }
                        if ($needSave) {
                            $this->order->save();
                            if ($_REQUEST['DELIVERY_ID'] == CURIER_DELIVERY_ID) {
                                if (!empty($this->arResult['DATA']["UF_STREET"])) {//сохраним в базу новый адрес доставки
                                    $this->saveNewAddress();
                                }
                            }
                        }
                        LocalRedirect($APPLICATION->GetCurPageParam('ORDER_ID=' . $id, ['ORDER_ID']));
                    } else {
                        if ($ex = $APPLICATION->GetException()) {
                            echo $aaaa = $ex->GetString();
                        }
                        print_r($r->getErrors());
                        // либо только сообщения
                        print_r($r->getErrorMessages());

                        // так же в заказе могут быть предупреждения, которые не являются причиной остановки процесса сохранения заказа, но мы их сохраняем в маркировки
                        print_r($r->getWarnings());
                        print_r($r->getWarningMessages());
                    }
                }
            }
        } else {
            $order = Sale\Order::loadByAccountNumber($_REQUEST['ORDER_ID']);
            $paymentCollection = $order->getPaymentCollection();
            foreach ($paymentCollection as $payment) {
                if ($payment->getPaymentSystemId() == ONLINE_PAYMENT_ID) {
                    $this->arResult['onePayment'] = $payment;
                    break;
                }
            }
        }

        return $this;
    }


    /**
     * Данные корзины для отображения
     *
     * @return array
     */
    private function prepareDisplayBasket(): array
    {
        $product_id_to_price_map = [];
        $basketItems = [];
        $basket = $this->order->getBasket();
        foreach ($basket as $item) {
            $basketItem['CURRENCY'] = $item->getField("CURRENCY");
            $basketItem['NAME'] = $item->getField("NAME");
            $basketItem['ID'] = $item->getId();
            $basketItem['PROPS'] = [];
            $basketItem['QUANTITY'] = (float) $item->getQuantity();
            $basketItem['PRODUCT_ID'] = (float) $item->getProductId();
            $basketItem['FULL_PRICE'] = $item->getPrice();
            $basketItem['WEIGHT'] = $item->getWeight();

            $product_id_to_price_map[$basketItem['PRODUCT_ID']] = $basketItem['FULL_PRICE'];
            $product_ids[] = $basketItem['PRODUCT_ID'];

            $basketItems[$basketItem['ID']] = $basketItem;
        }
        $propertyResult = Sale\BasketPropertiesCollection::getList(
            [
                'filter' => [
                    '=BASKET_ID' => array_keys($basketItems),
                    ['!CODE' => 'CATALOG.XML_ID'],
                    ['!CODE' => 'PRODUCT.XML_ID'],
                ],
                'order' => [
                    'ID' => 'ASC',
                    'SORT' => 'ASC',
                ],
            ]
        );
        while ($property = $propertyResult->fetch()) {
            $basketItems[$property['BASKET_ID']]['PROPS'][] = $property;
        }
         //основным товарам добавляем к цене цену дополнительных ингридиентов

        foreach ($basketItems as $item) {
            $ing_price = 0;
            if ($item['PROPS']) {
                $add_ing = [];
                $remove_ing = [];

                foreach ($item['PROPS'] as $prop) {
                    if ($prop['NAME'] == '+') {
                        $add_ing[] = $prop['VALUE'];
                    };
                    if ($prop['NAME'] == '-') {
                        $remove_ing[] = $prop['VALUE'];
                    };
                    $ing_price += $product_id_to_price_map[get_add_ing_id_from_basket_item_prop($prop)];
                }
                $basketItems[$item['ID']]['ADD_ING'] = $add_ing;
                $basketItems[$item['ID']]['REMOVE_ING'] = $remove_ing;

                $basketItems[$item['ID']]['FULL_PRICE'] = $basketItems[$item['ID']]['FULL_PRICE'] + $ing_price;
                $basketItems[$item['ID']]['FULL_PRICE_FORMATED'] =
                CCurrencyLang::CurrencyFormat($basketItems[$item['ID']]['FULL_PRICE'], $basketItems[$item['ID']]['CURRENCY'], true);
                $basketItems[$item['ID']]['SUM_FULL_PRICE'] = $basketItems[$item['ID']]['FULL_PRICE'] * $basketItems[$item['ID']]['QUANTITY'];
                $basketItems[$item['ID']]['SUM_FULL_PRICE_FORMATED'] =
                CCurrencyLang::CurrencyFormat($basketItems[$item['ID']]['SUM_FULL_PRICE'], $basketItems[$item['ID']]['CURRENCY'], true);
            }
        }

        $catalog_ids = filter_items_id_by_iblock_id($product_ids, $this->iblockId);
        //получим теперь картинки
        $catalog_items_data = get_items_data($catalog_ids, $this->iblockId, ['PREVIEW_PICTURE']);

        foreach ($basketItems as $item) {
            if ($catalog_items_data[$item['PRODUCT_ID']]['PREVIEW_PICTURE']) {
                $basketItems[$item['ID']]['PICTURE'] = resize($catalog_items_data[$item['PRODUCT_ID']]['PREVIEW_PICTURE'], 150, 150);
            }
        }

        $result['ITEMS'] = [];
        foreach ($basketItems as $item) {
            if (in_array($item['PRODUCT_ID'], $catalog_ids)) {
                $result['ITEMS'][$item['ID']] = $item;
            }
        }

        return $result;
    }



    protected function getPropertyByCode($propertyCollection, $code)
    {
        $tmpAr = $propertyCollection->getArray();
        $codes = [];
        foreach ($tmpAr['properties'] as $property) {
            $codes[] = $property['CODE'];
        }

        if (!in_array($code, $codes)) {
            return;
        }

        foreach ($propertyCollection as $property) {
            if ($property->getField('CODE') == $code) {
                return $property;
            }
        }
    }

    private function saveProfile()
    {
        \CSaleOrderUserProps::DoSaveUserProfile(
            $this->order->getUserId(),
            $this->profileId,
            'Профиль пользователя [ID: ' . $this->order->getUserId() . ']',
            $this->order->getPersonTypeId(),
            $this->order->getId(),
            $errors
        );
    }
    public function checkCoupon()
    {
        try {
            $this->checkModules();//Подключение подуля Sale
            $arCoupons = DiscountCouponsManager::getData($this->currentCoupon);
            echo json_encode(array('result' => $arCoupons['STATUS'], 'coupon' => $arCoupons['COUPON']));
            die();
            //return true;
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    public function disableCoupon()
    {
        try {
            $this->checkModules();//Подключение подуля Sale
            DiscountCouponsManager::delete($this->currentCoupon);
            return true;
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }
    private function applyCoupon()
    {
        if (!empty($_REQUEST['promo_code'])) :
            $this->currentCoupon = $_REQUEST['promo_code'];
            \Bitrix\Sale\DiscountCouponsManager::clear(true);
            \Bitrix\Sale\DiscountCouponsManager::clearApply(true);
            DiscountCouponsManager::add($this->currentCoupon);
        endif;
    }

    private function getErrors()
    {
        return $this->arResult['ERRORS'];
    }
    public function getTotalPrice()
    {
        try {
            $this->checkModules();//Подключение подуля Sale
            $this->getResult();
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    public function getDeliveryByLocation()
    {
        try {
            $this->checkModules();//Подключение подуля Sale
            $this->getResult();
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    /**
     * ЗАКАЗЫ
     *
     * @return void
     */

    private function getBasket()
    {
        $basket = $this->order->getBasket();

        $this->arResult["ORDER_PRICE"] = $basket->getPrice();
        $this->arResult["ORDER_PRICE_FORMATED"] = SaleFormatCurrency($basket->getPrice(), $this->order->getCurrency());

        $this->arResult["ORDER_WEIGHT"] = $basket->getWeight();
        $this->arResult["ORDER_WEIGHT_FORMATED"] = roundEx(doubleval($basket->getWeight() / $this->arResult["WEIGHT_KOEF"]), SALE_WEIGHT_PRECISION) . " " . $this->arResult["WEIGHT_UNIT"];

        $this->arResult["PRICE_WITHOUT_DISCOUNT_VALUE"] = $basket->getBasePrice();
        $this->arResult["PRICE_WITHOUT_DISCOUNT"] = SaleFormatCurrency($basket->getBasePrice(), $this->order->getCurrency());

        $this->arResult['DISCOUNT_PRICE'] = $this->order->getDiscountPrice();
        $this->arResult['DISCOUNT_PRICE_FORMATED'] = SaleFormatCurrency($this->order->getDiscountPrice(), $this->order->getCurrency());

        $this->arResult['DELIVERY_PRICE'] = $this->order->getDeliveryPrice();
        $this->arResult['DELIVERY_PRICE_FORMATED'] = SaleFormatCurrency($this->order->getDeliveryPrice(), $this->order->getCurrency());

       /*  $this->arResult["ORDER_TOTAL_PRICE"] = $this->order->getPrice();
        $this->arResult["ORDER_TOTAL_PRICE_FORMATED"] = SaleFormatCurrency($this->order->getPrice(), $this->order->getCurrency()); */
    }

    private function initPayment($paySystemId)
    {
        global $USER;

        $arPaySystemServiceAll = [];

        if (!empty($_REQUEST['data_pay_id'])) {
            $paySystemId = $_REQUEST['data_pay_id'];
        }

        //Если уже выбран тип доставки, то устанавливаем его
        if (!empty($this->newDeliveryId)) {
            $this->shipment->setField('DELIVERY_ID', $this->newDeliveryId);
        }
        $paymentCollection = $this->order->getPaymentCollection();
        if ($_REQUEST['minusBalls']) {
            $extPayment1 = $paymentCollection->createItem();
            $extPayment1->setField('SUM', intval($_REQUEST['minusBalls']));
            $extPayment1->setFields(array(
                'PAY_SYSTEM_ID' => BONUS_PAYMENT_ID,
                'PAY_SYSTEM_NAME' => "Оплата баллами",

            ));
        }

        $remainingSum = $this->order->getPrice() - $paymentCollection->getSum();
        if ($remainingSum > 0 || $this->order->getPrice() == 0) {
            $extPayment = $paymentCollection->createItem();
            $extPayment->setField('SUM', $remainingSum);
            $arPaySystemServices = PaySystem\Manager::getListWithRestrictions($extPayment);
            foreach ($arPaySystemServices as $key => $service) {
                if ($service['CODE'] == 'card' && Site\User::isAnonymous() && !$USER->isAuthorized()) {
                    unset($arPaySystemServices[$key]);
                }
            }

            $arPaySystemServiceAll += $arPaySystemServices;

            if (array_key_exists($paySystemId, $arPaySystemServiceAll)) {
                $arPaySystem = $arPaySystemServiceAll[$paySystemId];
            } else {
                reset($arPaySystemServiceAll);

                $arPaySystem = current($arPaySystemServiceAll);
            }

            if (!empty($arPaySystem)) {
                $extPayment->setFields(array(
                    'PAY_SYSTEM_ID' => $arPaySystem["ID"],
                    'PAY_SYSTEM_NAME' => $arPaySystem["NAME"]
                ));
            } else {
                $extPayment->delete();
            }
        }
        $this->arResult['arPaySystem'] = $arPaySystem;
        $this->arResult['PAY_SYSTEMS'] = $arPaySystemServiceAll;
      
    }

    private function initDelivery()
    {
        //Сюда попадают ВСЕ активные типы доставки
        $arDeliveryServiceAll = Delivery\Services\Manager::getRestrictedObjectsList($this->shipment);
        //dump($arDeliveryServiceAll);
        $this->shipmentCollection = $this->shipment->getCollection();


        if (!empty($_REQUEST['data_delivery_id'])) {
            $selectedService = Delivery\Services\Manager::getObjectById($_REQUEST['data_delivery_id']);

            $this->shipment->setFields(array(
                'DELIVERY_ID' => $selectedService->getId(),
                'DELIVERY_NAME' => $selectedService->getName(),
                'CURRENCY' => $this->order->getCurrency()
            ));

            //Если расскоментировать, то Боксберри ставит стоимость доставки
            //Несмотря на все скидки в корзине
            //Тестировалось до установки Sale\Compatible\DiscountCompatibility::stopUsageCompatible();
            //С методом выше не тестировалось
            //Есть мнение, что теперь будет работать правильно.
            $this->shipmentCollection->calculateDelivery();
        } else {
            // Ниже похоже на расчёт доставки
            //Первый раз он выдаёт сумму для первой доставки
            if (!empty($arDeliveryServiceAll)) {
                reset($arDeliveryServiceAll);
                $deliveryObj = current($arDeliveryServiceAll);

                if ($deliveryObj->isProfile()) {
                    $name = $deliveryObj->getNameWithParent();
                } else {
                    $name = $deliveryObj->getName();
                }

                $this->shipment->setFields(array(
                    'DELIVERY_ID' => $deliveryObj->getId(),
                    'DELIVERY_NAME' => $name,
                    'CURRENCY' => $this->order->getCurrency()
                ));

                //Если расскоментировать, то Боксберри ставит стоимость доставки
                //Несмотря на все скидки в корзине
                //Тестировалось до установки Sale\Compatible\DiscountCompatibility::stopUsageCompatible();
                //С методом выше не тестировалось
                //Есть мнение, что теперь будет работать правильно.
                $this->shipmentCollection->calculateDelivery();
            }
        }

        //Массив, который парсится в шаблоне
        $this->arResult['DELIVERY_ARRAY'] = Delivery\Services\Manager::getRestrictedList($this->shipment, 1); // WHY

        $this->arResult['DELIVERY'] = $arDeliveryServiceAll;

        foreach ($arDeliveryServiceAll as $delivery) :
            $calcResult = $delivery->calculate($this->shipment);

            if ($calcResult->isSuccess()) {
                if ($this->free_del) {
                    if ($this->order->getPrice() <= $this->free_del) {
                        $this->arResult['DELIVERY_ARRAY'][$delivery->getId()]['PRICE'] = $calcResult->getPrice();
                    } else {
                        $this->arResult['DELIVERY_ARRAY'][$delivery->getId()]['PRICE'] = 0;
                    }
                } else {
                    $this->arResult['DELIVERY_ARRAY'][$delivery->getId()]['PRICE'] = $calcResult->getPrice();
                }

                $this->arResult['DELIVERY_ARRAY'][$delivery->getId()]['PERIOD_DESCRIPTION'] = $calcResult->getPeriodDescription();
            }
        endforeach;
    }


    private function initShipment()
    {
        $this->shipmentCollection = $this->order->getShipmentCollection();
        $this->shipment = $this->shipmentCollection->createItem();
        $this->shipmentItemCollection = $this->shipment->getShipmentItemCollection();
        $this->shipment->setField('CURRENCY', $this->order->getCurrency());

        foreach ($this->order->getBasket() as $item) {
            $this->shipmentItem = $this->shipmentItemCollection->createItem($item);
            $this->shipmentItem->setQuantity($item->getQuantity());
        }
    }

    private function initOrder()
    {
        if (isset($_REQUEST['ORDER_ID'])) {
            $orderId = intval($_REQUEST['ORDER_ID']);
            $this->order = Sale\Order::loadByAccountNumber($orderId);
        } else {
            $userId = $this->arResult['USER_ID'];
            if (!$userId) {
                $userId = \CSaleUser::GetAnonymousUserID();
            }
            $this->order = Order::create($this->siteId, $userId);
        }
    }

    /**
     *  ДАННЫЕ ПОЛЬЗОВАТЕЛЯ
     */
    protected function getData()
    {
        global $USER;

        $userId = $USER->getId();

        $return = [];

        if ($userId) {
            $filter = ["ID" => $userId];
            $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $filter, ["SELECT" => ["UF_*"]]);
            if ($user = $rsUsers->fetch()) {
                
                $return = [
                    'EMAIL' => $user['EMAIL'],
                    'NAME' => $user['NAME'],
                    'FIO' => $user['NAME'],
                    'LAST_NAME' => $user['LAST_NAME'],
                    'SECOND_NAME' => $user['SECOND_NAME'],
                    'PHONE' => $user['PERSONAL_PHONE'] ? $user['PERSONAL_PHONE'] : $user['LOGIN'],
                    'COMPANY' => $user['WORK_COMPANY'],
                    'COMPANY_ADRESS' => $user['WORK_ZIP'] . ' ' . $user['WORK_STREET'],
                    //'ADDRESS' => $user['PERSONAL_STREET'],
                ];
            }

            /**/
            //$profileData = $this->getProfile();
            if ($profileData) {
                $return = $profileData + $return;
            }
            /**/
        }


        if (!empty($_SESSION['CURRENT_ORDER'])) {
            $return = array_merge($return, $_SESSION['CURRENT_ORDER']);
        }

        if (check_bitrix_sessid() && $_SERVER['REQUEST_METHOD'] == 'POST') {
            $return = $_REQUEST + $return;
        }
        
        return $return;
    }

    protected function getProfile()
    {
        $userId = $this->getUserId();
        if (!$userId) {
            return [];
        }

        $profileData = [];

        $dbUserProfiles = \CSaleOrderUserProps::GetList(
            ["DATE_UPDATE" => "DESC"],
            [
                "PERSON_TYPE_ID" => $this->getPersonTypeId(),
                "USER_ID" => $userId
            ]
        );

        if ($arUserProfiles = $dbUserProfiles->fetch()) {
            $this->profileId = intval($arUserProfiles["ID"]);
        }

        if (!$this->profileId) {
            return;
        }

        $res = \CSaleOrderUserPropsValue::GetList(
            [],
            [
                "PERSON_TYPE_ID" => $this->getPersonTypeId(),
                "USER_PROPS_ID" => $this->profileId,
            ],
            false,
            false,
            []
        );

        while ($profileItem = $res->fetch()) {
            $profileData[$profileItem['PROP_CODE']] = $profileItem['VALUE'];
        }

        return $profileData;
    }

    private function getUserId()
    {
        global $USER;
        if ($this->userId > 0) {
            return $this->userId;
        }

        if ($USER->getId()) {
            $this->userId = $USER->getId();
        } elseif (check_bitrix_sessid()) {
            $this->userId = \CSaleUser::GetAnonymousUserID();
        }

        return $this->userId;
    }
    private function getPersonTypeId()
    {
        return $this->userType['phisic'];
    }

    private function autoRegiser()
    {
        global $USER;
        global $APPLICATION;

        $userPhone = clear_phone($this->arResult['DATA']['PHONE']);
      
        $newLogin = $userPhone;
        if ($this->arResult['DATA']['EMAIL']) {
            $newEmail = $this->arResult['DATA']['EMAIL'];
        } else {
            $newEmail = time() . "@test.ru";
        }
        

        $payerName = $this->arResult['DATA']['FIO'];

        $newName = "";
        $newLastName = "";

        if (strlen($payerName) > 0) {
            $arNames = explode(" ", $payerName);
            if (count($arNames) > 1) {
                $newName = $arNames[1];
                $newLastName = $arNames[0];
            } else {
                $newName = $arNames[0];
            }
        }

        $def_group = Option::get("main", "new_user_registration_def_group", "");
        if ($def_group != "") {
            $groupID = explode(",", $def_group);
            $arPolicy = $USER->GetGroupPolicy($groupID);
        } else {
            $arPolicy = $USER->GetGroupPolicy(array());
        }

        $password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
        if ($password_min_length <= 0) {
            $password_min_length = 6;
        }
        $password_chars = array(
            "abcdefghijklnmopqrstuvwxyz",
            "ABCDEFGHIJKLNMOPQRSTUVWXYZ",
            "0123456789",
        );
        if ($arPolicy["PASSWORD_PUNCTUATION"] === "Y") {
            $password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
        }
        $newPassword = $newPasswordConfirm = randString($password_min_length + 2, $password_chars);

        $registerInfo = array(
            'NEW_EMAIL' => $newEmail,
            'NEW_LOGIN' => $newLogin,
            'NEW_NAME' => $newName,
            'NEW_LAST_NAME' => $newLastName,
            'NEW_PASSWORD' => $newPassword,
            'NEW_PASSWORD_CONFIRM' => $newPasswordConfirm,
            'GROUP_ID' => $groupID
        );

        if (!$this->getErrors()) {
            $arAuthResult = $USER->Register(
                $registerInfo["NEW_LOGIN"],
                $registerInfo["NEW_NAME"],
                $registerInfo["NEW_LAST_NAME"],
                $registerInfo["NEW_PASSWORD"],
                $registerInfo["NEW_PASSWORD_CONFIRM"],
                $registerInfo["NEW_EMAIL"]
            );
        }

        if ($arAuthResult != false && $arAuthResult["TYPE"] == "ERROR") {
            $this->arResult['ERRORS'][] = "Пользователь с таким телефоном уже зарегистрирован.";
        } else {
            $fieldsSend = [
                "EMAIL" => $registerInfo["NEW_EMAIL"],
                "PASS" => $registerInfo["NEW_PASSWORD"],
            ];
            CEvent::Send("AUTOREG", "s1", $fieldsSend);

            $this->newUserId = $arAuthResult['ID'];
            $this->userId = $arAuthResult['ID'];

            $fields = [
                'PERSONAL_PHONE' => $userPhone,
            ];

            $USER->update($this->userId, $fields);
        }
    }

    public function executeComponent()
    {

        global $APPLICATION;

        try {
            $this->checkModules()->prepareResult();
            $this->includeComponentTemplate();
        } catch (SystemException $e) {
            self::__showError($e->getMessage());
        }
    }

    /**
     * Сохраняем адрес доставки в highload блок
     */
    private function saveNewAddress(): void
    {
        if ($this->userId > 0) {
            $AddressesHelper = new AddressesHelper($this->userId);
            $fields = [
                'UF_USER_ID' => $this->userId
            ];
            foreach (AddressesHelper::ADDRESS_KEYS as $key) {
                if ($this->arResult['DATA'][$key]) {
                    $fields[$key] = $this->arResult['DATA'][$key];
                }
            }
            $AddressesHelper->addItem($fields);
        }
    }

    /**
     * Список сохраненных адресов пользователя
     *
     * @return array
     */
    private function getUserAddresses(): array
    {
        if ($this->userId > 0) {
            $AddressesHelper = new AddressesHelper($this->userId);
            return $AddressesHelper->getList();
        }

        return [];
    }

    /**
     * ФОрмируем строку адреса
     *
     * @param bool $fromPost
     * @return string
     */
    private function prepareOrderAddress(bool $fromPost = true): string
    {
        $address = '';
        $addressArray = [];

        if ($fromPost) {
            //тогда из пост запроса
            foreach (AddressesHelper::ADDRESS_KEYS as $key) {
                if ($this->arResult['DATA'][$key]) {
                    if ($key == 'UF_FLAT') {
                        $addressArray[] = 'кв.' . $this->arResult['DATA'][$key];
                    } else {
                        $addressArray[] = $this->arResult['DATA'][$key];
                    }
                }
            }
        } else {
            //тогда уже готовый адрес
            $addressArray = $this->arResult['ADDRESSES'][$this->arResult['DATA']["addr_selected"]];
        }

        return implode(",", $addressArray);
    }

    /**
     * Список кафе
     *
     * @return array
     */
    private function getCafeList(): array
    {
        $result = [];
        $arSelect = ["ID", "NAME"];
        $arFilter = [
            "IBLOCK_ID" => get_iblock_id_by_code('restaurants'),
            "ACTIVE" => "Y",
            "SECTION_CODE" => CityHelper::getCurCity()
        ];
        $res = CIBlockElement::GetList(['SORT' => "ASC", "NAME" => "ASC"], $arFilter, false, false, $arSelect);
        while ($ob = $res->Fetch()) {
            $result[$ob['ID']] = $ob['NAME'];
        }
        return $result;
    }
    /**
     * Бонусы юзера
     *
     * @return float
     */
    private function getUserBonuses(): float
    {
        if ($this->userId > 0) {
            return BonusHelper::getUserBonuses($this->userId);
        }
        return 0;
    }
    /**
     * Сколько может потратить
     *
     * @return float
     */
    private function getUserBonusesCanSpend(): float
    {
        $max_bonus_percent = BonusHelper::getBonusMaxPercent();
        $percentsMax = intval($this->arResult['ORDER_TOTAL_PRICE'] * $max_bonus_percent / 100);
        if ($percentsMax >= $this->arResult['BONUSES']) {
            $canSpend = $this->arResult['BONUSES'];
        } else {
            $canSpend = $percentsMax;
        }
        return $canSpend;
    }   
}
