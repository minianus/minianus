///const { trim } = require("jquery");


var currentLocation;
var changeLocation = function () {
    currentLocation = this.getNodeByLocationId(this.getValue());
    saveToSess('LOCATION', JSON.stringify(currentLocation));
    $('input[name=LOCATION]').val(currentLocation.CODE);
    $('.active').removeClass('active');
    $('.preloader').show();
    $.ajax({
        method: 'POST',
        url: '/personal/order/make/ajax.php',
        data: {
            action: 'getAvaliableDelivery',
            location_id: currentLocation,
            sessid: BX.bitrix_sessid()
        },
        success: function (json) {

            $answer = $.parseJSON(json);
            $('.delivery_element').addClass('non_active');
            $('input[name="DELIVERY_ID"]').addClass('non_active_check');
            $.each($answer, function (index, value) {

                //Если Boxberry не присылает код кнопки «Выбрать местоположение», то не показываем доставку
                //Т.к. Boxberrry не знает некоторых городов (Например, Шатура)
                //В Битриксе ограничения на Боксберри по Местоположению не стоят
                if (value.ID == 28 && typeof value.PERIOD_DESCRIPTION == 'undefined') {
                    return;
                }
                if (currentLocation.DISPLAY=="Краснодар") {
                    if(value.ID==39||value.ID==40) {
                        return;
                    }
                }


                $('.delivery_element[data-delivery-id=' + value.ID + ']').removeClass('non_active');
                $('.delivery_element[data-delivery-id=' + value.ID + ']').removeClass('non_active').find('input[name="DELIVERY_ID"]').removeClass("non_active_check");
                $('.delivery_element[data-delivery-id=' + value.ID + '] .delivery_element_period_description').html(value.PERIOD_DESCRIPTION);
                $('.delivery_element[data-delivery-id=' + value.ID + '] .price_del_block').html(value.PRICE);
                var curDeliv = $('#current-delivery').val();
                if (curDeliv == value.ID)
                    $('.delivery_element[data-delivery-id=' + value.ID + ']').click();
            });

            if($('input[name="DELIVERY_ID"]:checked').closest(".delivery_element").hasClass("non_active")) {
                $('input[name="DELIVERY_ID"]').not(".non_active_check").first().trigger("click").trigger("change");
            }


            $('.preloader').hide();
        }
    })
};



function disableCoupon() {
    var couponField = $('input[name=coupon_name]'),
        current_coupon = couponField.val(),
        couponBlock = $('.coupon_block');

    $.ajax({
        method: 'POST',
        url: '/personal/order/make/ajax.php',
        data: {
            action: 'disableCoupon',
            coupon: current_coupon,
            sessid: BX.bitrix_sessid()
        },
        beforeSend: function () {
            couponBlock.addClass('load');
        },
        success: function (json) {
            updateOrderGoodsList();
            couponField.val('');
            $('input[name=promo_code]').val('');
            $('input[name=coupon_name]').prop('disabled', false);
            $('.cupon_status').addClass('success').text('');
            $('.cancel-coupon').fadeOut();
            $('.use-coupon').fadeIn();
            couponBlock.removeClass('load');
            totalPrice();
        }
    })
}


function checkCoupon() {
    var couponField = $('input[name=coupon_name]'),
        current_coupon = couponField.val(),
        couponBlock = $('.coupon_block');

    if (!(current_coupon.length)) {
        return false;
    }

    $('.cupon_status').removeClass('error');

    $.ajax({
        method: 'POST',
        url: '/personal/order/make/ajax.php',
        data: {
            action: 'checkCoupon',
            coupon: current_coupon,
            sessid: BX.bitrix_sessid()
        },
        beforeSend: function () {
            couponBlock.addClass('load');
        },
        success: function (json) {

            var $answer = $.parseJSON(json);
            updateOrderGoodsList();
            if ($answer.result) {
                if ($answer.result === 2) {
                    $('input[name=coupon_name]').prop('disabled', true);
                    $('.cancel-coupon').fadeIn();
                    $('.use-coupon').fadeOut();
                    $('.cupon_status').addClass('success').removeClass('error').text('Ваш промо-код применен к заказу').show();
                    $('input[name=promo_code]').val($answer.coupon);
                    totalPrice();//Выводим итоговую сумму
                } else {
                    $('.cupon_status').show().removeClass('success').addClass('error').text('Этот промо-код не активен. Проверьте корректность промо-кода');
                }
            }
            couponBlock.removeClass('load');
        },
        done: function () {
            //couponBlock.removeClass('load');
        }
    })

}

function updateOrderGoodsList()
{
    $('.comporison-list').load(location.href+ " .comporison-list >*");
}

/**
 *
 * ОФОРМЛЕНИЕ ЗАКАЗА
 */
$(document).on("change","input[name='DELIVERY_ID']",function(){
    var parent = $(this).closest('.tab-widget');
    parent.find('.tab').removeClass('active');
    parent.find('.tab[data-delivery="'+$(this).val()+'"]').addClass('active');
    $('#data_delivery_id').val($(this).val());
    totalPrice();
    if ($(this).val() == orderConfig.SAMO_DELIVERY_ID) {
        $('.hide_on_samo').hide();
    } else {
        $('.hide_on_samo').show();
    }

})
/**
 * Смена платежного способа
 */
$(document).on("change","input[name='data_pay_id']",function(){
    saveToSess('data_pay_id',$(this).val());
    var id = $(this).val();
    if (id == orderConfig.CASH_PAYMENT_ID) {
        // покажем блок про сдачу
        $('#sdacha-payment-block').val('').show();
    } else {
        //спрячем блок про сдачу
        $('#sdacha-payment-block').val('').hide();
    }
})
$(document).on("change",".save2Sess",function(){
    saveToSess($(this).attr("name"),$(this).val());
})

function totalPrice() {
    $('.preloader').show();
    $.ajax({
        method: 'POST',
        url: '/personal/order/make/ajax.php',
        data: {
            data_delivery_id: $('input[name=DELIVERY_ID]:checked').val(),
            data_pay_id: $('input[name=data_pay_id]:checked').val(),
            promo_code: $('input[name=coupon_name]').val(),
            action: 'getTotalPrice',
            location_id: currentLocation,
            minusBalls:$('input[name=minusBalls]').val(),
            sessid: BX.bitrix_sessid()
        },
        success: function (json) {
            $answer = $.parseJSON(json);
            console.log($answer);
            $('.total_price').html($answer.ORDER_TOTAL_PRICE);
            $('.ORDER_PRICE').html($answer.ORDER_PRICE);
            $('.PRICE_WITHOUT_DISCOUNT_VALUE').html($answer.PRICE_WITHOUT_DISCOUNT_VALUE);
            $('.DELIVERY_PRICE').html($answer.DELIVERY_PRICE);
            $('.free_del_notify').html($answer.FREE_DEL_NOTIFY);
            $('.minusBonus').html($answer.minusBonus);
            $('.NEW_BALLS').html($answer.NEW_BALLS);
            $('.preloader').hide();

            if ($answer.PRICE_WITHOUT_DISCOUNT_VALUE == $answer.ORDER_PRICE) {
                $('.discount_line').hide();
            } else {
                $('.discount_line').show();
            }

        }
    })
}
$(document).on("change","input[name='minusBalls']",function(){
    if(parseInt($(this).val())>parseInt($(this).data("max"))) {
        $(this).val(parseInt($(this).data("max")));
    }
    totalPrice();
});

/**
 * Сохраняем в сессию
 */
function saveToSess(field, value) {
    $.ajax({
        url: '/local/ajax/order2sess.php',
        data: {
            field: field,
            value: value
        }
    })
}
/**
 * смена радио Когда доставить
 */
$(document).on("change","input[name='TIME_RADIO']", function(e){
    if ($(this).val() == 'time') {
        $('#order-delivery-time').val('').show();
    } else {
        $('#order-delivery-time').val('').hide();
    }
})
/**
 * при нажатии оформления заказа надо на js валидировать
 */
$(document).on('click',"#final_order",function(e){
    var error = false;

    if ($('input[name="PHONE"]').val().trim().length < 17) {
        $('input[name="PHONE"]').addClass('invalid_field');
        error = true;
        showWarning('Пожалуйста, укажите номер телефона для доставки');
    } else {
        $('input[name="PHONE"]').removeClass('invalid_field');
    }

    if($("input[name='TIME_RADIO']:checked").val() == 'time') {
        //тогда время должно быть заполнено
        if ($('input[name="TIME"]').val().trim() == '') {
            $('input[name="TIME"]').addClass('invalid_field');
            error = true;
            showWarning('Пожалуйста, укажите время доставки');
        } else {
            $('input[name="TIME"]').removeClass('invalid_field');
        }
    }
    if (!error) {
        //проверим политику
        if (!$('#order_agree_check').is(":checked")) {
            error = true;
            showWarning('Пожалуйста, подтвердите согласие с политикой обработки персональных данных');
        }
    }






    //проверяем что заполнен адрес если курьер
    if($("input[name='DELIVERY_ID']:checked").val() == orderConfig.CURIER_DELIVERY_ID) {

        if ($('input[name="UF_STREET"]').val().trim() == '' && $('input[name="addr_selected"]:checked').length == 0) {
            $('input[name="UF_STREET"]').addClass('invalid_field');
            error = true;
            showWarning('Пожалуйста, укажите адрес доставки');
        } else {
            $('input[name="UF_STREET"]').removeClass('invalid_field');
        }
    }

    if (error) {
        return false;
    }
})

$(document).ready(function(){
    if ($('input[name="minusBalls"]').length) {
        $('input[name="minusBalls"]').trigger("change");
    }
})

function showWarning(text)
{
    Swal.fire({
        toast: true,
        position: 'center',
        showConfirmButton: false,
        timer: 2000,
        icon: 'warning',
        title: text
    })
}