<?php
CModule::includeModule('sale');
use Bitrix\Main;
use Bitrix\Sale\PaySystem\Manager;

Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    'onOrderSavedHandler'
);

function onOrderSavedHandler(Main\Event $event)
{
    $isNew = $event->getParameter("IS_NEW");

    if ($isNew) {
        $order = $event->getParameter("ENTITY");

        $paymentCollection = $order->getPaymentCollection();
        $payment_id = 0;
        foreach ($paymentCollection as $key => $pay_el) {
            if ($pay_el->getPaymentSystemId() == ONLINE_PAYMENT_ID) {
                $payment_id = $key;
                break;
            }
        }

        $payment = $paymentCollection[$payment_id];
        $service = Manager::getObjectById($payment->getPaymentSystemId());
        //addMessage2Log(print_r($service, true));

        $initResult = $service->initiatePay($payment, null, \Bitrix\Sale\PaySystem\BaseServiceHandler::STRING);
        $pay_code = '';
        if ($initResult->isSuccess()) {
            $pay_code = $initResult->getTemplate();
        } else {
            addMessage2Log(implode('\\n', $initResult->getErrorMessages()));
        }

        $order_id = $order->getID();
        $prop_collection = $order->getPropertyCollection();
        $order_email = $prop_collection->getUserEmail()->getValue();

        $order_date = $order->getDateInsert();

        $mail_data = [
'ORDER_ID' => $order_id,
'EMAIL' => $order_email,
'ORDER_DATE' => $order_date,
'PAY_BLOCK' => $pay_code,
];

        CEvent::send('SALE_STATUS_CHANGED_AW', 's1', $mail_data);
    }
}
