<?php

use Bitrix\Main\Type\DateTime as bdt;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Sale;
use Bitrix\Sale\Fuser;
use Pomidor\Helpers\OrderHelper;
use Pomidor\General\Settings;

function get_random_string($length)
{
    $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $strlength = strlen($characters);

    $random = '';

    for ($i = 0; $i < $length; $i++) {
        $random .= $characters[rand(0, $strlength - 1)];
    }
    return $random;
}

function get_cafe_list($city = '')
{
    if ($city == '') {
        $city = get_city();
    }

    if ($city) {
        $cafe_list = [];
        CModule::includeModule('iblock');
        $filter = ['SECTION_CODE' => $city, 'IBLOCK_ID' => get_iblock_id_by_code('restaurants')];
        $select = ['ID', 'NAME'];
        $res = CIblockElement::GetList([], $filter, false, [], $select);
        $result = [];

        while ($item = $res->getNext()) {
            $cafe_list[] = $item;
        }
        return $cafe_list;
    } else {
        return [];
    }
}

/**
 * считает цены в корзине с учетом промокодов. возвращает собственно массив соотносящий id элемента корзины и его цены
 */
function calculateDiscount()
{
    CModule::IncludeModule("sale");
    $promocode = $_SESSION['order_promocode'];
    \Bitrix\Sale\DiscountCouponsManager::clear();
    if ($promocode) {
        Sale\DiscountCouponsManager::add($promocode);
    }

    $oBasket = Sale\Basket::loadItemsForFUser(
        Sale\Fuser::getId(),
        \Bitrix\Main\Context::getCurrent()->getSite()
    );



    // получаем объект скидок для корзины
    $oDiscounts = Sale\Discount::loadByBasket($oBasket);

    if (!$oDiscounts) {
        return [];
    }
    // обновляем поля в корзине
    $oBasket->refreshData(['PRICE','COUPONS']);

    // пересчёт скидок для корзины
    $oDiscounts->calculate();

    $result = $oDiscounts->getApplyResult();

    return $result['PRICES']['BASKET'];
}

/**
 * принимает сумму потенциальной покупки(опционально) и возвраает цену доставки и сколько осталось до суммы при которой будет бесплатная доставка
 *
 * @param [type] $basket_price
 * @return void
 */
function get_delivery_price($virtual_price = 0)
{
    CModule::includeModule('sale');

    $discount_prices = calculateDiscount();

    $registry = Sale\Registry::getInstance(Sale\Registry::REGISTRY_TYPE_ORDER);

    $basketClass = $registry->getBasketClassName();

    $basketItemsResult = $basketClass::getList([
            'filter' => [
                'FUSER_ID' => Fuser::getId(),
                '=LID' => SITE_ID,
                'ORDER_ID' => null,
            ],
        ]);

    $basket_price = 0;


    while ($basket_item = $basketItemsResult->fetch()) {
        $basket_price += ($discount_prices[$basket_item['ID']]['PRICE']) ?
        $discount_prices[$basket_item['ID']]['PRICE'] * (integer)$basket_item['QUANTITY']
        : $basket_item['PRICE'] * (integer)$basket_item['QUANTITY'];
    }


    $basket_price += $virtual_price;

    $city = get_city();
    if (empty($city)) {
        $city = DEFAULT_CITY;
    }
    //сумма бесплатной доставки
    $freeDeliveryPrice = OrderHelper::getFreeDeliverySum($city);
    //цена доставки
    $deliveryPrice = OrderHelper::getDeliveryPrice($city);

    if ($basket_price == 0) {
        return [
            'DELIVERY_PRICE' => $deliveryPrice,
            'STEP_TO_FREE' => $freeDeliveryPrice,
        ];
    }


    if ($basket_price > $freeDeliveryPrice) {
        return [
            'DELIVERY_PRICE' => 0,
            'STEP_TO_FREE' => 0,
        ];
    } else {
        return [
            'DELIVERY_PRICE' => $deliveryPrice,
            'STEP_TO_FREE' => $freeDeliveryPrice - $basket_price,
        ];
    }
     
}


/**
 * возвращает кол-во начисляемых бонусов за данную сумму корзины
 *
 * @param [type] $basket_price
 * @return void
 */
function get_bonuses($basket_price)
{
    //процент начисления из настроек модуля
    $percent = (float)Settings::getSetting("bonus_percent");
    return floor($basket_price * $percent);
}

function get_add_ing_id_from_basket_item_prop($prop)
{
    $code = $prop['CODE'];
    $parts = explode("|", $code);
    if ($parts[0] == '+') {
        return (int)$parts[1];
    } else {
        return false;
    }
}

function get_remove_ing_id_from_basket_item_prop($prop)
{
    $code = $prop['CODE'];
    $parts = explode("|", $code);
    if ($parts[0] == '-') {
        return (int)$parts[1];
    } else {
        return false;
    }
}

function parse_basket_item_props($props)
{
    $result = [
        'add' => [],
        'remove' => [],
    ];

    foreach ($props as $prop) {
        switch ($prop['NAME']) {
            case '+': {
                $result['add'][explode("|", $prop['CODE'])[1]] = $prop['VALUE'];
                break;
            }
            case '-': {
                $result['remove'][explode("|", $prop['CODE'])[1]] = $prop['VALUE'];
                break;
            }
        }
    }
    return $result;
}

function get_items_id_map_to_iblock_id($items_id)
{
    CModule::includeModule('iblock');
    $filter = ['ID' => $items_id];
    $select = ['IBLOCK_ID', 'ID'];
    $res = CIblockElement::GetList([], $filter, false, [], $select);
    $result = [];

    while ($item = $res->getNext()) {
        $result[$item['ID']] = $item['IBLOCK_ID'];
    }

    return $result;
}

function filter_items_id_by_iblock_id($items_id, $iblock_id)
{
    CModule::includeModule('iblock');
    $filter = ['ID' => $items_id, 'IBLOCK_ID' => $iblock_id];
    $select = [ 'ID'];
    $res = CIblockElement::GetList([], $filter, false, [], $select);
    $result = [];

    while ($item = $res->getNext()) {
        $result[] = $item['ID'];
    }

    return $result;
}

function get_items_data($items_id, $iblock_id = 0, $data = ['PREVIEW_PICTURE', 'PREVIEW_TEXT', 'PROPERTY_COOKING_CAFE', 'PROPERTY_SHOT_ABOUT'])
{
    CModule::includeModule('iblock');
    $filter = ['ID' => $items_id];
    if ($iblock_id != 0) {
        $filter['IBLOCK_ID'] = $iblock_id;
    }
    $select = array_merge(['ID'], $data);
    $res = CIblockElement::GetList([], $filter, false, [], $select);
    $result = [];

    while ($item = $res->getNext()) {
        $result[$item['ID']] = $item;
    }

    return $result;
}


function GetDefaultCurrency()
{
    if (!CModule::IncludeModule("sale")) {
        return false;
    }

    $obCache = new CPHPCache();
    $life_time = 36000;
    $cache_params = array('option' => 'getOption');
    $cache_params['module'] = 'sale';
    $cache_params['option'] = 'default_currency';

    $cache_id = md5(serialize($cache_params));
    if ($obCache->InitCache($life_time, $cache_id, "/")) :
        $arResult = $obCache->GetVars(); else :
            $arResult['CURRENCY'] = COption::GetOptionString("sale", "default_currency", "RUB");
    endif;

    if ($obCache->StartDataCache()) :
        $obCache->EndDataCache($arResult);
    endif;
    return $arResult['CURRENCY'];
}

function get_map_center($coord_list)
{
    $points = [];
    $i = 0;
    foreach ($coord_list as $item) {
        $x_y = explode(',', $item);
        if ($i == 0) {
            $max_x = (float) $x_y[0];
            $max_y = (float) $x_y[1];
            $min_x = (float) $x_y[0];
            $min_y = (float) $x_y[1];
        } else {
            if ((float) $x_y[0] > $max_x) {
                $max_x = (float) $x_y[0];
            }
            if ((float) $x_y[0] < $min_x) {
                $min_x = (float) $x_y[0];
            }

            if ((float) $x_y[1] > $max_y) {
                $max_y = (float) $x_y[1];
            }
            if ((float) $x_y[1] < $max_y) {
                $max_y = (float) $x_y[1];
            }
        }

        $i++;
    }

    $x_center = $min_x + ($max_x - $min_x) / 2;
    $y_center = $min_y + ($max_y - $min_y) / 2;

    return $x_center . ',' . $y_center;
}

if (!function_exists('dump')) {
    function dump($data, $show = true)
    {
        if (!$show) {
            return;
        }
        echo "<pre>";
        echo print_r($data, true);
        echo "</pre>";
    }
}

if (!function_exists('split_by_words')) {
    function split_by_words($s)
    {
        $result = [];
        $words = explode(' ', $s);
        $result[0] = $words[0];
        $result[1] = implode(' ', array_slice($words, 1));
        return $result;
    }
}

if (!function_exists('format_element_datetime')) {
    function format_element_datetime($element_timestamp, $format = 'd.m.Y')
    {
        $bdt = new bdt($element_timestamp);
        return $bdt->format($format);
    }
}
if (!function_exists("clear_phone")) {
    /**
     * Функция пиводит телефон к формату +79995552255
     *
     * @param string $phone
     * @return string
     */
    function clear_phone($phone)
    {
        return str_replace(['(', ')', '-', ' '], "", $phone);
    }
}

if (!function_exists("resize")) {
    /**
     * resize($id,$width,$height,$watermark = false, $quality = 7-) - обертка над ResizeImageGet
     *
     * @param $id - айди картинки
     * @param $width - нужная ширина
     * @param $height - нужная высота
     * @param $method - вид обрезки, по умолчанию BX_RESIZE_IMAGE_EXACT
     * @param boolean $watermark - накладывать ли водяной знак
     * @param $quality - качеств новой фотки, по умолчанию 70
     * @return string (file src)
     **/
    function resize($id, $width, $height, $method = BX_RESIZE_IMAGE_EXACT, $watermark = false, $quality = 65)
    {
        if ($watermark) {
            $watermark = [
                [
                    "name" => "watermark",
                    "position" => "bottomright", // Положение
                    "type" => "image",
                    "size" => "real",
                    "file" => $_SERVER["DOCUMENT_ROOT"] . '/watermark.png', // Путь к картинке
                    "fill" => "exact",
                ],
            ];
        }
        $img = CFile::ResizeImageGet(
            $id,
            ['width' => $width, 'height' => $height],
            $method,
            true,
            $watermark,
            false,
            $quality
        );
        return $img['src'];
    }
}

/**
 * Функции для работы с инфоблоками
 */

if (!function_exists("get_iblock_id_by_code")) {
    /**
     * Возвращает ID инфоблока по его символьному коду, либо false в случае неудачи
     *
     * @param string $code - символьный код инфоблока
     * @return integer|false
     */
    function get_iblock_id_by_code($code)
    {
        CModule::IncludeModule('iblock');

        $res = CIBlock::GetList(
            [],
            [
                //'SITE_ID' => SITE_ID,
                "CODE" => $code,
            ]
        );

        if ($arBlock = $res->Fetch()) {
            return $arBlock['ID'];
        }

        return false;
    }
}

if (!function_exists('get_section_by_code')) {
    /**
     * Функция возвращает информацию о секции по её коду
     *
     * @param string $code
     * @param integer $iblockId
     * @return mixed
     */
    function get_section_by_code($code, $iblockId)
    {
        \CModule::IncludeModule('iblock');

        $arFilter = [
            "IBLOCK_ID" => $iblockId,
            "CODE" => $code,
        ];

        $resSect = \CIBlockSection::GetList(
            [],
            $arFilter,
            false,
            [
                'ID',
                'NAME',
                'CODE',
                'IBLOCK_SECTION_ID',
                'SECTION_PAGE_URL',
                'DEPTH_LEVEL',
                'PICTURE',
                'UF_SVG',
                'UF_IGNORE_PROP',
                'DESCRIPTION',
                'SORT',
                'ACTIVE',
                'UF_ANONS',
            ]
        );

        if (($arSect = $resSect->Fetch()) && $code) {
            if ($arSect['UF_ANONS']) {
                $arSect['UF_ANONS'] = "<p>" . nl2br($arSect['UF_ANONS']) . "</p>";
                $arSect['UF_ANONS'] = str_replace('<br>', '</p><p>', $arSect['UF_ANONS']);
            }

            return $arSect;
        }

        return null;
    }
}

if (!function_exists('get_section_by_id')) {
    /**
     * Функция возвращает информацию о секции по её id
     *
     * @param integer $id
     * @param integer $iblockId
     * @return mixed
     */
    function get_section_by_id($id, $iblockId)
    {
        \CModule::IncludeModule('iblock');

        $arFilter = [
            "IBLOCK_ID" => $iblockId,
            "ID" => $id,
        ];

        $resSect = \CIBlockSection::GetList(
            [],
            $arFilter,
            false,
            [
                'ID',
                'NAME',
                'CODE',
                'IBLOCK_SECTION_ID',
                'SECTION_PAGE_URL',
                'DEPTH_LEVEL',
                'PICTURE',
                'UF_SVG',
                'UF_IGNORE_PROP',
                'DESCRIPTION',
                'SORT',
                'ACTIVE',
                'UF_ANONS',
            ]
        );

        if (($arSect = $resSect->GetNext()) && $id) {
            if ($arSect['UF_ANONS']) {
                $arSect['UF_ANONS'] = "<p>" . nl2br($arSect['UF_ANONS']) . "</p>";
                $arSect['UF_ANONS'] = str_replace('<br>', '</p><p>', $arSect['UF_ANONS']);
            }

            return $arSect;
        }

        return null;
    }
}

if (!function_exists('get_prop_values_by_code')) {
    /**
     * Возвращает список вариантов для свойства по его коду
     *
     * @param string $iblockCode
     * @param string $propertyCode
     * @return array
     */
    function get_prop_values_by_code($iblockCode, $propertyCode)
    {
        $values = [];

        $resPropValues = \CIBlockPropertyEnum::GetList(
            ["SORT" => "ASC", "ID" => 'ASC'],
            ['IBLOCK_ID' => get_iblock_id_by_code($iblockCode), 'PROPERTY_ID' => $propertyCode]
        );

        while ($arValue = $resPropValues->Fetch()) {
            $values[$arValue['ID']] = $arValue['VALUE'];
        }

        return $values;
    }
}

if (!function_exists('get_hl_iblock_id_by_name')) {
    /**
     * Функция возвращает ID Highload-инфоблока по его названию сущности
     *
     * @param $name
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    function get_hl_iblock_id_by_name($name)
    {
        Loader::includeModule("highloadblock");

        $hlblock = HL\HighloadBlockTable::getList([
            'filter' => ['=NAME' => trim($name)],
        ])->fetch();

        if ($hlblock) {
            return $hlblock['ID'];
        }

        return false;
    }
}

function get_hlb_by_name($name)
{
    Loader::includeModule("highloadblock");

    $hlblock = HL\HighloadBlockTable::getById(get_hl_iblock_id_by_name($name))->fetch();

    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    return $entity->getDataClass();
}

function is_city_defined()
{
    return isset($_COOKIE['city']);
}

function get_city()
{
    return $_COOKIE['city'];
}

function set_city($city)
{
    $_COOKIE['city'] = $city;
}

function build_user_address_name($address)
{
    $result = '';
    if ($address['UF_STREET']) {
        $result .= $address['UF_STREET'];
    }
    if ($address['UF_SECTION']) {
        $result .= ' п. ' . $address['UF_SECTION'];
    }
    if ($address['UF_FLOOR']) {
        $result .= ' эт. ' . $address['UF_FLOOR'];
    }
    if ($address['UF_FLAT']) {
        $result .= ' кв. ' . $address['UF_FLAT'];
    }
    if ($address['UF_FLAT_CALL']) {
        $result .= ' домофон ' . $address['UF_FLAT_CALL'];
    }
   

    return $result;
}

function build_address_name($address)
{
    $result = '';
    if ($address['street']) {
        $result .= $address['street'];
    }
    if ($address['section']) {
        $result .= ' п. ' . $address['section'];
    }
    if ($address['floor']) {
        $result .= ' эт. ' . $address['floor'];
    }
    if ($address['flat']) {
        $result .= ' кв. ' . $address['flat'];
    }
    if ($address['flat_call']) {
        $result .= ' домофон ' . $address['flat_call'];
    }
    return $result;
}

function get_section_id_by_code($code, $iblock_id = 0)
{
    $filter = ['CODE' => $code];
    if ($iblock_id != 0) {
        $filter['IBLOCK_ID'] = $iblock_id;
    }
    $select = ['ID'];

    $result = 0;

    $res = CIBlockSection::getList([], $filter, false, $select);
    while ($sec = $res->getNext()) {
        $result = $sec['ID'];
    }

    return $result;
}
if (!function_exists("plural_form")) {

    /**
     * Склонение числительных
     * @param int $number
     * @param array $after
     * @return string
     */
    function plural_form(int $number, array $after): string
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $after[ ($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)] ];
    }
}

function get_saved_user_address_list($user_id = 0)
{
    global $USER;
    if ($user_id == 0) {
        $user_id = $USER->getID();
    }

    $addr_hlb = get_hlb_by_name('UserAddress');

    $res = $addr_hlb::getList([
        'filter' => ['UF_USER_ID' => $user_id],
        'select' => ['*'],
    ]);

    $result = [];
    while ($address = $res->fetch()) {
        $result[$address['ID']] = build_user_address_name($address);
    }

    return $result;
}

/**
 * принимает код статуса заказа и состояние оплаченности. возвращает строку статуса для вывода в лк и разрешение на отмену заказа
 */
function get_order_status_for_lk($status_id, $payment_state){

    switch($status_id){
        case 'HA' : {
            return [
                'STATUS' => 'В обработке',
                'CAN_CANCELED' => true
            ];
            break;
        }
        case 'A' : {
            return [
                'STATUS' => 'Подтвержден',
                'CAN_CANCELED' => false
            ];
            break;
        }
        case 'AW' : {
            return [
                'STATUS' => 'Подтвержден, Ожидает оплаты',
                'CAN_CANCELED' => false
            ];
            break;
        }
        case 'AP' : {
            return [
                'STATUS' => 'Подтвержден, Оплачен',
                'CAN_CANCELED' => false
            ];
            break;
        }
        case 'DE' : {
            return [
                'STATUS' => 'Передан на доставку',
                'CAN_CANCELED' => false
            ];
            break;
        }
        case 'DC' : {
            return [
                'STATUS' => 'Готов к выдаче',
                'CAN_CANCELED' => false
            ];
            break;
        }
        case 'RE' : {
            return [
                'STATUS' => 'Получен',
                'CAN_CANCELED' => false
            ];
            break;
        }
        default : {

            return [
                'STATUS' => 'В обработке',
                'CAN_CANCELED' => true
            ];
        }
    }

}
