<?php

namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;
use Pomidor\Helpers\OrderHelper;

class OwnDeliveryHandler extends Base
{


    public static function getClassTitle()
    {
        return 'Собстенная доставка Помидора';
    }

    public static function getClassDescription()
    {
        return 'Доставка по городу';
    }

    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
    {
        $result = new CalculationResult();

        if (OrderHelper::checkIfFreeDelivery()) {
            $deliveryPrice = 0;
        } else {
            $deliveryPrice = OrderHelper::getDeliveryPrice();
        }

        $result->setDeliveryPrice(roundEx($deliveryPrice, 2));
        return $result;
    }

    protected function getConfigStructure()
    {
        return [

            ];
    }

    public function isCalculatePriceImmediately()
    {
        return true;
    }

    public static function whetherAdminExtraServicesShow()
    {
        return true;
    }
}
