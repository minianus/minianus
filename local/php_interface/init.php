<?php

define("cardNoPicLarge", '/local/templates/pomidor/images/wt.png');
//для заказа
define('DEFAULT_CITY', 'khabarovsk');
define('DEFAULT_FREE_DEL', 800);
define('CURIER_DELIVERY_ID', 5); //ID доставки курьером
define('SAMO_DELIVERY_ID', 2); //ID доставки самовывоз
define('CITY_COOKIE_NAME', 'city'); //название cookie с городом
define('CASH_PAYMENT_ID', 1); //ID оплаты наличкой
define('ONLINE_PAYMENT_ID', 10); //ID оплаты онлайн
define('BONUS_PAYMENT_ID', 11); //ID оплаты бонусами
define('DEFAULT_ORDER_STATUS_ID', 'HA');

define('LOG_FILENAME', $_SERVER["DOCUMENT_ROOT"] . "/local/log/" . date('D-m-Y') . '.log');

define('DADATA_API_KEY', '14d2c63e390fc1774447bff3da06ffe9abef0131');

require $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/func.php';
require $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/events.php';
include $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
