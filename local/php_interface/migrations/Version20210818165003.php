<?php

namespace Sprint\Migration;


class Version20210818165003 extends Version
{
    protected $description = "рестораны разделы";

    protected $moduleVersion = "3.25.1";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'restaurants',
            'Restorans'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Хабаровск',
    'CODE' => 'habarovsk',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '31',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  1 => 
  array (
    'NAME' => 'Комсомольск-на-Амуре',
    'CODE' => 'komsomolsk-na-amure',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '32',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
