<?php

namespace Sprint\Migration;


class Version20210826180039 extends Version
{
    protected $description = "настройки кастомные";

    protected $moduleVersion = "3.25.1";

    public function up()
    {
        $helper = $this->getHelperManager();
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'GROUP_DEFAULT_RIGHT',
  'VALUE' => 'D',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
    }

    public function down()
    {
        //your code ...
    }
}
