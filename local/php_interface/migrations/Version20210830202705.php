<?php

namespace Sprint\Migration;


class Version20210830202705 extends Version
{
    protected $description = "кастомные настройки";

    protected $moduleVersion = "3.25.1";

    public function up()
    {
        $helper = $this->getHelperManager();
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'bonus_percent',
  'VALUE' => '0.03',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'bonus_system_habarovsk',
  'VALUE' => 'Y',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'bonus_system_komsomolsk',
  'VALUE' => NULL,
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'del_price_habarovsk',
  'VALUE' => '149',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'del_price_komsomolsk',
  'VALUE' => '180',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'free_del_habarovsk',
  'VALUE' => '800',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'free_del_komsomolsk',
  'VALUE' => '800',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'GROUP_DEFAULT_RIGHT',
  'VALUE' => 'D',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
        $helper->Option()->saveOption(array (
  'MODULE_ID' => 'grain.customsettings',
  'NAME' => 'max_bonus_percent',
  'VALUE' => '50',
  'DESCRIPTION' => NULL,
  'SITE_ID' => NULL,
));
    }

    public function down()
    {
        //your code ...
    }
}
