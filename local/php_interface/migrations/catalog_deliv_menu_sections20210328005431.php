<?php

namespace Sprint\Migration;


class catalog_deliv_menu_sections20210328005431 extends Version
{
    protected $description = "";

    protected $moduleVersion = "3.25.1";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'menu-devilery',
            'catalog'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Пицца',
    'CODE' => 'pizza',
    'SORT' => '100',
    'ACTIVE' => 'Y',
    'XML_ID' => '47',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  1 => 
  array (
    'NAME' => 'Салаты',
    'CODE' => 'salads',
    'SORT' => '200',
    'ACTIVE' => 'Y',
    'XML_ID' => '48',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  2 => 
  array (
    'NAME' => 'Роллы',
    'CODE' => 'rolls',
    'SORT' => '300',
    'ACTIVE' => 'Y',
    'XML_ID' => '49',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  3 => 
  array (
    'NAME' => 'Супы и рамен',
    'CODE' => 'soups-ramen',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '51',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  4 => 
  array (
    'NAME' => 'Закуски',
    'CODE' => 'hot-meals',
    'SORT' => '600',
    'ACTIVE' => 'Y',
    'XML_ID' => '52',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  5 => 
  array (
    'NAME' => 'Десерты',
    'CODE' => 'desserts',
    'SORT' => '700',
    'ACTIVE' => 'Y',
    'XML_ID' => '53',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  6 => 
  array (
    'NAME' => 'Паста',
    'CODE' => 'past',
    'SORT' => '800',
    'ACTIVE' => 'Y',
    'XML_ID' => '54',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  7 => 
  array (
    'NAME' => 'Комбо',
    'CODE' => '',
    'SORT' => '900',
    'ACTIVE' => 'Y',
    'XML_ID' => NULL,
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
