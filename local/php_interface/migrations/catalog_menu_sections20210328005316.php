<?php

namespace Sprint\Migration;


class catalog_menu_sections20210328005316 extends Version
{
    protected $description = "";

    protected $moduleVersion = "3.25.1";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'menu',
            'catalog'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Кухня',
    'CODE' => 'kitchen',
    'SORT' => '100',
    'ACTIVE' => 'Y',
    'XML_ID' => '40',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_PDF' => NULL,
    'CHILDS' => 
    array (
      0 => 
      array (
        'NAME' => 'Горячие блюда',
        'CODE' => 'hot-meals',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '37',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      1 => 
      array (
        'NAME' => 'Салаты',
        'CODE' => 'salads',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '44',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      2 => 
      array (
        'NAME' => 'Десерты',
        'CODE' => 'desserts',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '45',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      3 => 
      array (
        'NAME' => 'Закуски',
        'CODE' => 'drinks',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '46',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      4 => 
      array (
        'NAME' => 'Пицца',
        'CODE' => 'pizza',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '33',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      5 => 
      array (
        'NAME' => 'Роллы',
        'CODE' => 'rolls',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '34',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      6 => 
      array (
        'NAME' => 'Паста',
        'CODE' => 'paste',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '35',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
      7 => 
      array (
        'NAME' => 'Супы',
        'CODE' => 'soups-ramen',
        'SORT' => '500',
        'ACTIVE' => 'Y',
        'XML_ID' => '36',
        'DESCRIPTION' => '',
        'DESCRIPTION_TYPE' => 'text',
        'UF_PDF' => NULL,
      ),
    ),
  ),
  1 => 
  array (
    'NAME' => 'Барная карта',
    'CODE' => '',
    'SORT' => '200',
    'ACTIVE' => 'Y',
    'XML_ID' => '41',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_PDF' => '1004',
  ),
  2 => 
  array (
    'NAME' => 'Винная карта',
    'CODE' => '',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '42',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_PDF' => '1005',
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
