<?php

namespace Sprint\Migration;


class deliv_menu_sec20210420025811 extends Version
{
    protected $description = "";

    protected $moduleVersion = "3.25.1";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'menu-devilery',
            'catalog'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Пицца',
    'CODE' => 'pizza',
    'SORT' => '100',
    'ACTIVE' => 'Y',
    'XML_ID' => '47',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => 
    array (
      0 => 646,
      1 => 669,
      2 => 671,
    ),
  ),
  1 => 
  array (
    'NAME' => 'Салаты',
    'CODE' => 'salads',
    'SORT' => '200',
    'ACTIVE' => 'Y',
    'XML_ID' => '48',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
  2 => 
  array (
    'NAME' => 'Роллы',
    'CODE' => 'rolls',
    'SORT' => '300',
    'ACTIVE' => 'Y',
    'XML_ID' => '49',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
  3 => 
  array (
    'NAME' => 'Супы и рамен',
    'CODE' => 'soups-ramen',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '51',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
  4 => 
  array (
    'NAME' => 'Закуски',
    'CODE' => 'hot-meals',
    'SORT' => '600',
    'ACTIVE' => 'Y',
    'XML_ID' => '52',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
  5 => 
  array (
    'NAME' => 'Десерты',
    'CODE' => 'desserts',
    'SORT' => '700',
    'ACTIVE' => 'Y',
    'XML_ID' => '53',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
  6 => 
  array (
    'NAME' => 'Паста',
    'CODE' => 'past',
    'SORT' => '800',
    'ACTIVE' => 'Y',
    'XML_ID' => '54',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
  7 => 
  array (
    'NAME' => 'Комбо',
    'CODE' => '',
    'SORT' => '900',
    'ACTIVE' => 'Y',
    'XML_ID' => NULL,
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
    'UF_ADD_ING' => false,
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
