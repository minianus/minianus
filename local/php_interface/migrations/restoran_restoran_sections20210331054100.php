<?php

namespace Sprint\Migration;


class restoran_restoran_sections20210331054100 extends Version
{
    protected $description = "";

    protected $moduleVersion = "3.25.1";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'restaurants',
            'Restorans'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Хабаровск',
    'CODE' => 'habarovsk',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '31',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  1 => 
  array (
    'NAME' => 'Комсомольск Амур',
    'CODE' => 'komsomolsk',
    'SORT' => '500',
    'ACTIVE' => 'Y',
    'XML_ID' => '32',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
