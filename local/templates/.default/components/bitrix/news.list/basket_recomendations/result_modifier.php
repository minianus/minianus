<?

$item_ids = [];
foreach($arResult['ITEMS'] as $item){
    $item_ids[] = $item['ID'];
}

$db_res = CPrice::GetList(
        array(),
        array(
                "PRODUCT_ID" => $item_ids,
            )
    );

$product_id_to_price_map = [];
while($price = $db_res->fetch()){
$product_id_to_price_map[$price['PRODUCT_ID']] = $price['PRICE'];
}
$arResult['product_id_to_price_map'] = $product_id_to_price_map;