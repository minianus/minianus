<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="box box_b-large">
    <div class="full-slider-widget">
        <div class="full-nav ">
            <a href="#" class="full-nav__btn full-nav__btn_left">
                <svg class="slider-icon__arrow">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-left"></use>
                </svg>
            </a>
            <a href="#" class="full-nav__btn full-nav__btn_right">
                <svg class="slider-icon__arrow">
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-right"></use>
                </svg>
            </a>
        </div>
        <div class="relative-slider dots-control">

            <?foreach($arResult['ITEMS'] as $item){?>
            <div>
                <div class="menu__item relative menu-item">
                    <a href="#" class="menu-item__top"><img class="menu-item__image" src="<?=$item['PREVIEW_PICTURE']['SRC']?>"
                            alt=""><img class="menu-item__image_second" src="<?=$item['DETAIL_PICTURE']['SRC']?>" alt="">
                    </a>
                    <div class="menu-item__content">
                        <h4 class="menu-item__title"><?=$item['NAME']?></h4>
                        <span class="menu-item__desc">
                            <?=$item['PROPERTIES']['SHOT_ABOUT']['VALUE']?>
                        </span>
                        <span class="price relative__price green">
                            <?=(integer)$arResult['product_id_to_price_map'][$item['ID']]?> Р
                        </span>
                    </div>
                    <div class="menu-item__bottom">
                        <a class="btn btn_add" href="#" onclick="do_basket_action(event, {action: 'add_item', item_id: <?=$item['ID']?> }); location.reload()">В корзину</a>
                    </div>
                </div>
            </div>
            <?}?>
        </div>
    </div>
</div>