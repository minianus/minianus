<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_t-offset loyalty__pluses">
    <h2 class="section__title big text-center">Способы оплаты</h2>
    <div class="row">
    <?foreach($arResult['ITEMS'] as $item){?>
        <div class="col col--md-6 col--xl-3">
            <div class="loyalty__pluses__item window window_dashed window_s-offset window_b-offset match-height"
                style="height: 278px;">
                <div class="icon">
                <?if($item['PROPERTIES']['ICON']['VALUE']){?>
                <img src="<?=CFile::getPath($item['PROPERTIES']['ICON']['VALUE'])?>" width="42">
                <?}else{?>
                    <?=$item['DETAIL_TEXT']?>
                <?}?>
                </div>
                <h5><?=$item['NAME']?></h5>
                <p class="grey"><?=$item['PREVIEW_TEXT']?></p>
            </div>
        </div>
    <?}?>
       
    </div>
</div>