<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_t-offset restoran-list">
    <h2 class="section__title big">Всегда рады вас видеть</h2>
    <?foreach($arResult['ITEMS'] as $section){?>
    <div class="row">
        <div class="col col--md-6">
            <h2><?=$section['NAME']?></h2>
            <?foreach($section['ELEMENTS'] as $item){?>
            <p><?=$item['NAME']?></p>
            <?}?>
        </div>
        <div class="col col--md-6 mb-3">
            <div class="ymap-container restoran-map-wrapper window window_bordered window_b-offset"
                style="background-image: url('/upload/images/map-bg.jpg')">
                <div class="ymap-container">
                    <div id="map_<?=$section['CODE']?>" class="restoran-map"></div>
                </div>
            </div>
        </div>
    </div>
        <?}?>
</div>

<script>
//Переменная для включения/отключения индикатора загрузки
var spinner = $('.ymap-container').children('.loader');
//Переменная для определения была ли хоть раз загружена Яндекс.Карта (чтобы избежать повторной загрузки при наведении)
var check_if_load = false;
//Необходимые переменные для того, чтобы задать координаты на Яндекс.Карте
var myMapTemp, myPlacemarkTemp;

//Функция создания карты сайта и затем вставки ее в блок с идентификатором &#34;map-yandex&#34;
function init() {

    <?
      
    foreach($arResult['ITEMS'] as $section){
        $map_center = ($section['ELEMENTS'][0]['PROPERTIES']['MAP_POINT']['VALUE'])?$section['ELEMENTS'][0]['PROPERTIES']['MAP_POINT']['VALUE']:'48.536087573878255, 135.03679249999996';    
        $points = [];
        foreach ($section['ELEMENTS'] as $item) {
            if($item['PROPERTIES']['MAP_POINT']['VALUE']){
                $points[] = $item['PROPERTIES']['MAP_POINT']['VALUE'];
            }
            
        }
        $map_center = get_map_center($points);

    ?>
    var myMapTemp_<?=$section['CODE']?> = new ymaps.Map("map_<?=$section['CODE']?>", {
        center: [<?=$map_center?>],
        zoom: 17,
        controls: ['zoomControl', 'fullscreenControl']
    });
        <?foreach($section['ELEMENTS'] as $item){?>
            
            var myPlacemarkTemp = new ymaps.Placemark([<?=$item['PROPERTIES']['MAP_POINT']['VALUE']?>], {
                balloonContent: "<?=$item['NAME']?>",
            });

            myMapTemp_<?=$section['CODE']?>.geoObjects.add(myPlacemarkTemp);
        <?}?>



    var layer = myMapTemp_<?=$section['CODE']?>.layers.get(0).get(0);

    // Решение по callback-у для определения полной загрузки карты
    waitForTilesLoad(layer).then(function() {
        // Скрываем индикатор загрузки после полной загрузки карты
        spinner.removeClass('is-active');
    });
    <?}?>

    /****/

}

// Функция для определения полной загрузки карты (на самом деле проверяется загрузка тайлов) 
function waitForTilesLoad(layer) {
    return new ymaps.vow.Promise(function(resolve, reject) {
        var tc = getTileContainer(layer),
            readyAll = true;
        tc.tiles.each(function(tile, number) {
            if (!tile.isReady()) {
                readyAll = false;
            }
        });
        if (readyAll) {
            resolve();
        } else {
            tc.events.once("ready", function() {
                resolve();
            });
        }
    });
}

function getTileContainer(layer) {
    for (var k in layer) {
        if (layer.hasOwnProperty(k)) {
            if (
                layer[k] instanceof ymaps.layer.tileContainer.CanvasContainer ||
                layer[k] instanceof ymaps.layer.tileContainer.DomContainer
            ) {
                return layer[k];
            }
        }
    }
    return null;
}

// Функция загрузки API Яндекс.Карт по требованию (в нашем случае при наведении)
function loadScript(url, callback) {
    var script = document.createElement("script");

    if (script.readyState) { // IE
        script.onreadystatechange = function() {
            if (script.readyState == "loaded" ||
                script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { // Другие браузеры
        script.onload = function() {
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

// Основная функция, которая проверяет когда мы навели на блок с классом &#34;ymap-container&#34;
var ymap = function() {
    $('.ymap-container').mouseenter(function() {
        if (!check_if_load) { // проверяем первый ли раз загружается Яндекс.Карта, если да, то загружаем

            // Чтобы не было повторной загрузки карты, мы изменяем значение переменной
            check_if_load = true;

            // Показываем индикатор загрузки до тех пор, пока карта не загрузится
            spinner.addClass('is-active');

            // Загружаем API Яндекс.Карт
            loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1", function() {
                // Как только API Яндекс.Карт загрузились, сразу формируем карту и помещаем в блок с идентификатором &#34;map-yandex&#34;
                ymaps.load(init);
            });
        }
    });
}

$(function() {
    ymap();
});
</script>