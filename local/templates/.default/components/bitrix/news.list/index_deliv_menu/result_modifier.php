<?
$all_items = $arResult['ITEMS'];
$ids = [];
foreach($arResult['ITEMS'] as $item){
  $ids[] = $item['ID'];
}

CModule::includeModule('sale');

$res = CPrice::GetList(
  array(),
  array(
          "PRODUCT_ID" => $ids,
      )
);

$prices = [];
while($price = $res->getNext()){
  $prices[$price['PRODUCT_ID']] = $price;
}

$arResult['prices'] = $prices;

// получаем разделы
$dbResSect = CIBlockSection::GetList(
    Array("SORT"=>"ASC"),
    Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'])
 );
 //Получаем разделы и собираем в массив
 while($sectRes = $dbResSect->GetNext())
 {
  $arSections[] = $sectRes;
 }
 //Собираем  массив из Разделов и элементов
 foreach($arSections as $arSection){   
  foreach($arResult["ITEMS"] as $key=>$arItem){  
    if($arItem['IBLOCK_SECTION_ID'] == $arSection['ID']){
    $arSection['ELEMENTS'][] =  $arItem;
    }
  } 
  $arElementGroups[] = $arSection; 
 }
 $arResult["ITEMS"] = $arElementGroups;
 $arResult['ALL_ITEMS'] = $all_items;