<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="section menu">
    <div class="wrap">
        <div class="title-wrap" data-aos="slide-up" data-aos-duration="2500">
<!--            <span class="subtitle">лучшая пицца</span>-->
            <a href="#" class="h2 block title title_link">Популярное</a>
            <svg class="icon title-wrap__line">
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#orange-line"></use>
            </svg>
        </div>
        <div class="menu__container">
            <div class="menu__tabs" data-aos="fade" data-aos-duration="1000" data-aos-delay="1000">
                <a class="menu__tabs-item active" href="#!">
                    <span>Все</span>
                </a>
                <?foreach($arResult['ITEMS'] as $section){?>
                <a class="menu__tabs-item" href="#!">
                    <span><?=$section['NAME']?></span>
                </a>
                <?}?>
            </div>
            <div class="menu__content menu__content_active">
                <div class="menu__list">
                    <?foreach($arResult['ALL_ITEMS'] as $item){?>
                    <?
                    $pic = resize($item['PREVIEW_PICTURE'], 305, 305);    
                    $active_pic = resize($item['DETAIL_PICTURE'], 305, 305);
                    ?>
                    <div>
                        <div class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000"
                            data-aos-delay="1500">
                            <a href="#!" class="deliv_popup-call menu-item__top" data-item_id="<?=$item["ID"]?>"
                                data-catalog_id="<?=$arParams['IBLOCK_ID']?>">
                                <img class="menu-item__image" src="<?=$pic?>" alt="">
                                <img class="menu-item__image_second" src="<?=$active_pic?>" alt="">
                                <div class="menu-item__icons">
                                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                                        <svg class="icon coffee__icon">
                                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <path
                                                        d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z"
                                                        id="Capa_1" data-name="Capa 1"></path>
                                                </g>
                                            </svg>
                                        </svg>
                                    </div>
                                    <svg class="icon icon-del icon-del_menu">
                                        <svg viewBox="0 0 361.3 240.8" id="icon-del">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <g id="Capa_1" data-name="Capa 1">
                                                    <path
                                                        d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z">
                                                    </path>
                                                    <path
                                                        d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z">
                                                    </path>
                                                </g>
                                            </g>
                                        </svg>
                                    </svg>
                                </div>
                            </a>
                            <div class="menu-item__content">
                                <h4 class="menu-item__title"><?=$item['NAME']?></h4>
                                <p class="menu-item__desc">
                                    <?=$item['PREVIEW_TEXT']?>
                                </p>
                            </div>
                            <div class="menu-item__bottom">

                                <span class="menu-item__price"><?=round($arResult['prices'][$item['ID']]['PRICE'])?>
                                    <span class="rub">c</span></span>


                                &nbsp;
                                <a  href="#!" onclick="do_basket_action(event, {action: 'add_item', item_id: <?=$item['ID']?> })" class="btn btn_add" rel="nofollow">В корзину</a>
                                &nbsp;


                            </div>
                        </div>

                    </div>
                    <?}?>
                </div>
            </div>
            <?foreach($arResult['ITEMS'] as $section){?>
            <div class="menu__content">
                <div class="menu__list">
                    <?foreach($section['ELEMENTS'] as $item){?>
                    <?
                        $pic = resize($item['PREVIEW_PICTURE'], 305, 305);    
                        $active_pic = resize($item['DETAIL_PICTURE'], 305, 305);
                        ?>
                    <div>
                        <div class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000"
                            data-aos-delay="1500">
                            <a href="#!" class="deliv_popup-call menu-item__top" data-item_id="<?=$item["ID"]?>"
                                data-catalog_id="<?=$arParams['IBLOCK_ID']?>">
                                <img class="menu-item__image" src="<?=$pic?>" alt="">
                                <img class="menu-item__image_second" src="<?=$active_pic?>" alt="">
                                <div class="menu-item__icons">
                                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                                        <svg class="icon coffee__icon">
                                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <path
                                                        d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z"
                                                        id="Capa_1" data-name="Capa 1"></path>
                                                </g>
                                            </svg>
                                        </svg>
                                    </div>
                                    <svg class="icon icon-del icon-del_menu">
                                        <svg viewBox="0 0 361.3 240.8" id="icon-del">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <g id="Capa_1" data-name="Capa 1">
                                                    <path
                                                        d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z">
                                                    </path>
                                                    <path
                                                        d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z">
                                                    </path>
                                                </g>
                                            </g>
                                        </svg>
                                    </svg>
                                </div>
                            </a>
                            <div class="menu-item__content">
                                <h4 class="menu-item__title"><?=$item['NAME']?></h4>
                                <p class="menu-item__desc">
                                    <?=$item['PREVIEW_TEXT']?>
                                </p>
                            </div>
                            <div class="menu-item__bottom">

                                <span class="menu-item__price"><?=round($arResult['prices'][$item['ID']]['PRICE'])?>
                                    <span class="rub">c</span></span>


                                &nbsp;
                                <a href="#!" onclick="do_basket_action(event, {action: 'add_item', item_id: <?=$item['ID']?> })" class="btn btn_add" rel="nofollow">В корзину</a>
                                &nbsp;


                            </div>
                        </div>

                    </div>
                    <?}?>
                </div>
            </div>
            <?}?>
        </div>
<!--        <div class="menu__count">-->
<!--            <svg class="menu__count-bg">-->
<!--                <use xlink:href="--><?//=SITE_TEMPLATE_PATH?><!--/images/icons.svg#menu-count"></use>-->
<!--            </svg>-->
<!--            <div class="menu__count-content">-->
<!--                <span class="menu__count-number">800 руб</span>-->
<!--                <span class="menu__count-text">до бесплатной доставки</span>-->
<!--            </div>-->
<!--        </div>-->
        <div class="slider-arrow menu-prev">
            <svg class="slider-icon__arrow">
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-left"></use>
            </svg>
        </div>
        <div class="slider-arrow menu-next">
            <svg class="slider-icon__arrow">
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-right"></use>
            </svg>
        </div>
</section>