<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="loyalty__faq_list">
<?foreach($arResult['ITEMS'] as $item){?>
    <div class="loyalty__faq__item window window_shadow window_bordered window_b-offset">
        <div class="loyalty__faq__header"
            onclick="$(this).parent().toggleClass('active').find('.loyalty__faq__content').slideToggle();">
            <b><?=$item['NAME']?></b>
            <span class="header__translate-arrow"></span>
        </div>
        <div class="loyalty__faq__content">
            <?=$item['PREVIEW_TEXT']?>
        </div>
    </div>
<?}?>
</div>