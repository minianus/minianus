<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="row">
    <?foreach($arResult['ITEMS'] as $item){
    $file = CFile::getPath($item['PROPERTIES']['ICON']['VALUE']);

    ?>
    <div class="col col--md-6 col--xl-3">
        <div class="loyalty__pluses__item window window_dashed window_s-offset window_b-offset match-height">
            <div class="icon">
            <?if($file != null){?>
                <img src="<?=$file?>" width="42">
            <?}else{?>
                <?=$item['DETAIL_TEXT']?>
            <?}?>
            </div>
            <h5><?=$item['NAME']?></h5>
            <p class="grey"><?=$item['PREVIEW_TEXT']?></p>
        </div>
    </div>
    <?}?>
    
</div>