<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?dump($arResult, false)?>
<div class="section section_t-offset">
    <div class="row">
        <div class="col col--lg-6 tabs">
            <div class="tabs__content tabs-nav">
            <?for($i = 0; $i < count($arResult['ITEMS']); $i++){?>
                <a href="#" class="<?=($i == 0)?'active':''?>"><?=$arResult['ITEMS'][$i]['NAME']?></a>
            <?}?>
            </div>
        </div>
        <div class="col col--lg-6">
        </div>
    </div>
    <div class="row">
        <div class="col col--lg-6">
            <div class="work-tab">
            <?for($i = 0; $i < count($arResult['ITEMS']); $i++){
                $section = $arResult['ITEMS'][$i];
                ?>
                <div class="loyalty__faq <?=($i == 0)?'active':''?>">
                <?foreach($section['ELEMENTS'] as $item){?>
                    <div
                        class="loyalty__faq__item window window_shadow window_bordered window_s-offset window_b-offset">
                        <div class="loyalty__faq__header"
                            onclick="$(this).parent().toggleClass('active').find('.loyalty__faq__content').slideToggle();">
                            <b><?=$item['NAME']?></b>
                            <span class="header__translate-arrow"></span>
                        </div>
                        <div class="loyalty__faq__content">
                            <?=$item['PREVIEW_TEXT']?>
                        </div>
                    </div>
                <?}?>   
                    
                </div>
            <?}?>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => '/include/_forms/vacancy_form.php',
        "EDIT_TEMPLATE" => "standard.php"
        )
        );?>  
    </div>
</div>