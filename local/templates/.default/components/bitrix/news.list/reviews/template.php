<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="section section_t-offset reviews mb-3">
<?foreach($arResult['ITEMS'] as $review){?>

   <div class="row reviews__item">
        <div class="col col--md-3 col--xl-2">
            <div class="reviews__item__details">
                <div class="pic"><img src="<?=resize($review['PREVIEW_PICTURE']['ID'], 114, 114)?>"></div>
                <h3><?=$review['NAME']?></h3>
                <p class="date"><?=$review['CREATED_AT']?></p>
            </div>
        </div>
        <div class="col col--md-9 col--xl-10">
            <div class="reviews__item__text window window_bordered window_shadow">
                <p class="grey"><?=$review['PREVIEW_TEXT']?></p>
            </div>
        </div>
    </div>    
<?}?>
</div>

<?=$arResult['NAV_STRING']?>

