<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
define('PAGINATION_MAX_VIEW_PAGES', 7);
define('PAGINATION_HEAD_PAGES', 3);
define('PAGINATION_TAIL_PAGES', 3);
?>


<!--<div class="pagination section_b-offset">
    <a href="" class="pagination__link_edge <?=(!$prev_page_en)?'pagination__link_disabled':''?>">
        <svg class="link-arrow">
            <use xlink:href="/upload/images/icons.svg#arrow-left"></use>
        </svg>
    </a>

	
			<span class="pagination__link pagination__link_active"><?=$page_number?></span>
	<a href="#" class="pagination__link">2</a>
	

    <a href="" class="pagination__link_edge <?=(!$next_page_en)?'pagination__link_disabled':''?>">
        <svg class="link-arrow">
            <use xlink:href="/upload/images/icons.svg#arrow-right"></use>
        </svg>
    </a>

</div>-->

<div class="pagination section_b-offset">
    <?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
if($arResult["bDescPageNumbering"] === true)
{
	// to show always first and last pages
	$arResult["nStartPage"] = $arResult["NavPageCount"];
	$arResult["nEndPage"] = 1;

	$sPrevHref = '';
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
	{
		$bPrevDisabled = false;
		if ($arResult["bSavePage"])
		{
			$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
		}
		else
		{
			if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1))
			{
				$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
			}
			else
			{
				$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
			}
		}
	}
	else
	{
		$bPrevDisabled = true;
	}
	
	$sNextHref = '';
	if ($arResult["NavPageNomer"] > 1)
	{
		$bNextDisabled = false;
		$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
	}
	else
	{
		$bNextDisabled = true;
	}
	?>

    <a href="<?=$sPrevHref;?>" class="pagination__link_edge <?=($bPrevDisabled)?'pagination__link_disabled':''?>">
        <svg class="link-arrow">
            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-left"></use>
        </svg>
    </a>





    <?
	$bFirst = true;
	$bPoints = false;
	do
	{
		$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;
		if ($arResult["nStartPage"] <= 2 || $arResult["NavPageCount"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
		{

			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
    <span class="pagination__link pagination__link_active"><?=$NavRecordGroupPrint?></span>
    <?
			elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
	?>
    <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"
        class="pagination__link"><?=$NavRecordGroupPrint?></a>
    <?
			else:
	?>
    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"
        class="pagination__link">
        <?=$NavRecordGroupPrint?>
    </a>
    <?
			endif;
			$bFirst = false;
			$bPoints = true;
		}
		else
		{
			if ($bPoints)
			{
	?>...
    <?
				$bPoints = false;
			}
		}
		$arResult["nStartPage"]--;
	} while($arResult["nStartPage"] >= $arResult["nEndPage"]);
	?>
    <a href="<?=$sNextHref;?>" class="pagination__link_edge <?=($bNextDisabled)?'pagination__link_disabled':''?>">
        <svg class="link-arrow">
            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-right"></use>
        </svg>
    </a>
    <?
}
else
{
	// to show always first and last pages
	$arResult["nStartPage"] = 1;
	$arResult["nEndPage"] = $arResult["NavPageCount"];

	$sPrevHref = '';
	if ($arResult["NavPageNomer"] > 1)
	{
		$bPrevDisabled = false;
		
		if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
		{
			$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
		}
		else
		{
			$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
		}
	}
	else
	{
		$bPrevDisabled = true;
	}

	$sNextHref = '';
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
	{
		$bNextDisabled = false;
		$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
	}
	else
	{
		$bNextDisabled = true;
	}
	?>
    <a href="<?=$sPrevHref;?>" class="pagination__link_edge <?=($bPrevDisabled)?'pagination__link_disabled':''?>">
        <svg class="link-arrow">
            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-left"></use>
        </svg>
    </a>



    <?
	$bFirst = true;
	$bPoints = false;
	do
	{
		if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
		{

			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
    <span class="pagination__link pagination__link_active"><?=$arResult["nStartPage"]?></span>

    <?
			elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
	?>
    <a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"
        class="pagination__link"><?=$arResult["nStartPage"]?></a>
    <?
			else:
	?>
    <a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"
        class="pagination__link">
        <?=$arResult["nStartPage"]?></a>
    <?
			endif;
			$bFirst = false;
			$bPoints = true;
		}
		else
		{
			if ($bPoints)
			{
	?>...
    <?
				$bPoints = false;
			}
		}
		$arResult["nStartPage"]++;
	} while($arResult["nStartPage"] <= $arResult["nEndPage"]);
?>
    <a href="<?=$sNextHref;?>" class="pagination__link_edge <?=($bNextDisabled)?'pagination__link_disabled':''?>">
        <svg class="link-arrow">
            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#arrow-right"></use>
        </svg>
    </a>
    <?
}
?>
</div>