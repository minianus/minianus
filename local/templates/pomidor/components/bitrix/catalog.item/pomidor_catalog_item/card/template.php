<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>


<?
$img=CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], Array('width'=>285, 'height'=>242), BX_RESIZE_IMAGE_EXACT, true,false,false,65); 

$img2=CFile::ResizeImageGet($item['DETAIL_PICTURE'], Array('width'=>285, 'height'=>242), BX_RESIZE_IMAGE_EXACT, true,false,false,65);
?>


<a href="#popup-menu-<?=$item["ID"]?>" class="menu-item__top popup-call">
    <img class="menu-item__image" src="<?=$img['src']?>" alt="">
    <img class="menu-item__image_second" src="<?=$img2['src']?>" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        <?=$productTitle?>
    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    <?if (!empty($item["PRODUCT"]["WEIGHT"])):?>

    <span class="weight menu-item__weight"><?=$item["PRODUCT"]["WEIGHT"]?> г</span>
    <?else:?>
    <span class="weight menu-item__weight">Вес не указан</span>
    <?endif;?>
    <?
    if (!empty($arParams['PRODUCT_BLOCKS_ORDER']))
    {
        foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName)
        {
            switch ($blockName)
            {
                case 'price': ?>


    <span class="price  products__price" id="<?=$itemIds['PRICE']?>">
        <?
                            if (!empty($price))
                            {
                                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
                                {
                                    echo Loc::getMessage(
                                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                                        array(
                                            '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                            '#VALUE#' => $measureRatio,
                                            '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                                        )
                                    );
                                }
                                else
                                {
                                    echo $price['PRINT_RATIO_PRICE'];
                                }
                            }
                            ?>
    </span>

    <?
                    break;




            }
        }
    }
    ?>


</div>


<span class="pomidor_hidden" id="<?=$itemIds['PICT_SLIDER']?>">
</span>
<?
if ($item['SECOND_PICT'])
{

    ?>
<span class="" id="<?=$itemIds['SECOND_PICT']?>">

</span>
<?
}

?>




<?
if (
    $arParams['DISPLAY_COMPARE']
    && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
)
{
    ?>
<div class="product-item-compare-container">
    <div class="product-item-compare">
        <div class="checkbox">
            <label id="<?=$itemIds['COMPARE_LINK']?>">
                <input type="checkbox" data-entity="compare-checkbox">
                <span data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></span>
            </label>
        </div>
    </div>
</div>
<?
}
?>



<div id="popup-menu-<?=$item["ID"]?>" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="<?=$item["DETAIL_PICTURE"]["SRC"]?>" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> <?=$productTitle?></h1>
                    <span class="card__desc">
                        <? echo $item["PROPERTIES"]["SHOT_ABOUT"]["VALUE"]?>
                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            <? echo $item["DETAIL_TEXT"]?>

                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                            <?foreach($item['PISHEVIE_HARAKTERISTIKI'] as $char){?>
                            <div class="add-w__info match-height" style="height: 0px;">
                                <div class="add-w__txt"><?=$char['PROPERTY_HEADER_VALUE']?></div>
                                <div class="add-w__txt add-w__txt-bold"><?=$char['PROPERTY_VALUE_VALUE']?></div>
                                <div class="add-w__txt add-w__txt-white"><?=$char['PROPERTY_SUTOCHNOE_POKRITIE_VALUE']?> %</div>
                            </div>
                            <?}?>
                        </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">

                            <a class="add-w__city  add-w__habarovsk <?=(get_city() == "khabarovsk")?'active':''?>">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na <?=(get_city() == "komsomolsk-na-amure")?'active':''?>">Комсомольск-на-Амуре</a>
                        </div>
                        <div class="menu_cart-address-tab">
                            <?foreach($arResult['restorans'][get_city()] as $restoran){?>
                                <?=$restoran['NAME']?><br>
                            <?}?>
                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                        <?foreach($arResult['restorans']["khabarovsk"] as $restoran){?>
                                <?=$restoran['NAME']?><br>
                            <?}?>
                        </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                        <?foreach($arResult['restorans']["komsomolsk-na-amure"] as $restoran){?>
                                <?=$restoran['NAME']?><br>
                            <?}?>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>