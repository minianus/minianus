<?php
$select = ['NAME', 'PROPERTY_HEADER', 'PROPERTY_VALUE', 'PROPERTY_SUTOCHNOE_POKRITIE'];
$filter = [
    "IBLOCK_ID" => get_iblock_id_by_code('pishevie_haracteristiki'), 
    'ACTIVE' => 'Y', 
    'ID' => $arResult['ITEM']["PROPERTIES"]["PISHEVIE_HARAKTERISTIKI"]['VALUE']
];
$res = CIBlockElement::GetList([], $filter, false, [], $select);
$data=[];

while($value = $res->getNext()){
    $data[] = $value;
}
$arResult['ITEM']['PISHEVIE_HARAKTERISTIKI'] = $data;

$restorans = [];

if(count($arResult['ITEM']['PROPERTIES']['COOCKING_RESTORANS']['VALUE']) > 0){
    $filter = ['IBLOCK_ID' => get_iblock_id_by_code('restaurants')];
    $select = ['ID', 'CODE'];

    $res = CIBlockSection::getList([], $filter, false, $select);

    $restoran_section_id_to_code_map = [];

    while($restoran_section = $res->getNext()){
        $restoran_section_id_to_code_map[$restoran_section['ID']] = $restoran_section['CODE'];
    }

    $filter = [
        'IBLOCK_ID' => get_iblock_id_by_code('restaurants'), 
        'ID' => $arResult['ITEM']['PROPERTIES']['COOCKING_RESTORANS']['VALUE'],
    
    ];
    $select = ['ID', 'NAME', 'IBLOCK_SECTION_ID'];
    $res = CIBlockElement::GetList([], $filter, false, [], $select);

    while($value = $res->getNext()){
        $restorans[$restoran_section_id_to_code_map[$value['IBLOCK_SECTION_ID']]][] = $value;
    }


}

$arResult['restorans'] = $restorans;

