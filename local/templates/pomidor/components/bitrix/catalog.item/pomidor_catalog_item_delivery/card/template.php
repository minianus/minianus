<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>


<?
$img = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], array('width' => 285, 'height' => 242), BX_RESIZE_IMAGE_EXACT, true, false, false, 65);

$img2 = CFile::ResizeImageGet($item['DETAIL_PICTURE'], array('width' => 285, 'height' => 242), BX_RESIZE_IMAGE_EXACT, true, false, false, 65);
?>

<a href="#popup-menu-<?=$item["ID"]?>" class="menu-item__top deliv_popup-call" data-item_id="<?=$item["ID"]?>"
    data-catalog_id="<?=$arParams['IBLOCK_ID']?>">
    <img class="menu-item__image" src="<?=$img['src']?>" alt="">
    <img class="menu-item__image_second" src="<?=$img2['src']?>" alt="">

</a>
<div class="menu-item__content match-height">
    <h4 class="menu-item__title">
        <?=$productTitle?>
    </h4>
    <p class="menu-item__desc"><?=$item["PREVIEW_TEXT"]?></p>
</div>
<div class="property">
    <?foreach ($item["PROPERTIES"] as $value): ?>
    <?if (!empty($value["VALUE"]) && $value["FILTRABLE"] == "Y"): ?>
    <input class="property_input" type="hidden" value="<?=$value["CODE"]?>" />
    <?endif;?>
    <?endforeach;?>
</div>
<div class="menu-item__bottom">


    <?
if (!empty($arParams['PRODUCT_BLOCKS_ORDER'])) {
    foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName) {
        switch ($blockName) {
            case 'price': ?>


    <span class="price  products__price" id="<?=$itemIds['PRICE']?>">
        <?
                if (!empty($price)) {
                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                        echo Loc::getMessage(
                            'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                            array(
                                '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                '#VALUE#' => $measureRatio,
                                '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE'],
                            )
                        );
                    } else {
                        echo $price['PRINT_RATIO_PRICE'];
                    }
                }
                ?>
    </span>

    <?
                break;

            case 'buttons':
                ?>
    <div class="product-item-info-container product-item-hidden" data-entity="buttons-block">
        <?
                if (!$haveOffers) {
                    if ($actualItem['CAN_BUY']) {
                        ?>
        <div class="" id="<?=$itemIds['BASKET_ACTIONS']?>">
            <button class="btn basket-btn add_basket-<?=$item["ID"]?>"  onclick="do_basket_action(event, {action: 'add_item', item_id: <?=$item['ID']?> })">
                <?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>
            </button>
        </div>
        <?}
          }?>

    </div>
    <?
                break;

        }
    }
}
?>


</div>


<span class="pomidor_hidden" id="<?=$itemIds['PICT_SLIDER']?>">
</span>
<?
if ($item['SECOND_PICT']) {

    ?>
<span class="" id="<?=$itemIds['SECOND_PICT']?>">

</span>
<?
}

?>




<?
if (
    $arParams['DISPLAY_COMPARE']
    && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
) {
    ?>
<div class="product-item-compare-container">
    <div class="product-item-compare">
        <div class="checkbox">
            <label id="<?=$itemIds['COMPARE_LINK']?>">
                <input type="checkbox" data-entity="compare-checkbox">
                <span data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></span>
            </label>
        </div>
    </div>
</div>
<?
}
?>


