<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'catalog-section-list-list',
    ),
    'LINE' => array(
        'TITLE' => 'catalog-section-list-item-title',
        'LIST' =>  'catalog-section-list-line-list mb-4',
        'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
    ),
    'TEXT' => array(
        'TITLE' => 'catalog-section-list-item-title',
        'LIST' =>  'catalog-section-list-text-list row mb-4'
    ),
    'TILE' => array(
        'TITLE' => 'catalog-section-list-item-title',
        'LIST' =>  'catalog-section-list-tile-list row mb-4',
        'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

switch ($arParams['LIST_COLUMNS_COUNT'])
{
    case "1":
        $listColumsClass = "col-12";
        break;
    case "2":
        $listColumsClass = "col-6";
        break;
    case "3":
        $listColumsClass = "col-sm-4 col-6";
        break;
    case "4":
        $listColumsClass = "col-md-3 col-sm-4 col-6";
        break;
    case "6":
        $listColumsClass = "col-lg-2 col-md-3 col-sm-4 col-6";
        break;
    case "12":
        $listColumsClass = "col-lg-1 col-md-3 col-sm-4 col-6";
        break;
}

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>



<?

if (0 < $arResult["SECTIONS_COUNT"])
{
    ?>
    <div class="page-nav">
        <a  href="/menu/">Все</a>
        <?

        switch ($arParams['VIEW_MODE'])
        {
            case 'LIST':
                $intCurrentDepth = 1;
                $boolFirst = true;
                foreach ($arResult['SECTIONS'] as &$arSection)
                {
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                    if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
                    {
                        if (0 < $intCurrentDepth)
                            echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']),'<ul>';
                    }
                    elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
                    {
                        if (!$boolFirst)
                            echo '</li>';
                    }
                    else
                    {
                        while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
                        {
                            echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                            $intCurrentDepth--;
                        }
                        echo str_repeat("\t", $intCurrentDepth-1),'</li>';
                    }

                    echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
                    ?>
                    <a id="<?=$this->GetEditAreaId($arSection['ID']);?>" href="<? echo $arSection["SECTION_PAGE_URL"]; ?>">
                        <? echo $arSection["NAME"];?></a>
                    <?

                    $intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
                    $boolFirst = false;
                }
                unset($arSection);
                while ($intCurrentDepth > 1)
                {
                    echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                    $intCurrentDepth--;
                }
                if ($intCurrentDepth > 0)
                {
                    echo '</li>',"\n";
                }
                break;
        }
        ?>
    </div>
    <?
}
?>
<?
if (0 < $arResult["SECTIONS_COUNT"])
{
    ?>
    <div class="custom-select nav-select">
    <select  class=" select_pomidor" aria-hidden="true">
        <option value="/menu/" selected="" >Все</option>
        <?

        switch ($arParams['VIEW_MODE'])
        {
            case 'LIST':
                $intCurrentDepth = 1;
                $boolFirst = true;
                foreach ($arResult['SECTIONS'] as &$arSection)
                {
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                    if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
                    {
                        if (0 < $intCurrentDepth)
                            echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']),'<ul>';
                    }
                    elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
                    {
                        if (!$boolFirst)
                            echo '</li>';
                    }
                    else
                    {
                        while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
                        {
                            echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                            $intCurrentDepth--;
                        }
                        echo str_repeat("\t", $intCurrentDepth-1),'</li>';
                    }

                    echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
                    ?>
                    <option  id="<?=$this->GetEditAreaId($arSection['ID']);?>" value="<? echo $arSection["SECTION_PAGE_URL"]; ?>"><? echo $arSection["NAME"];?></option>

                    <?

                    $intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
                    $boolFirst = false;
                }
                unset($arSection);
                while ($intCurrentDepth > 1)
                {
                    echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                    $intCurrentDepth--;
                }
                if ($intCurrentDepth > 0)
                {
                    echo '</li>',"\n";
                }
                break;
        }
        ?>
    </select>
    </div><?
}
?>
