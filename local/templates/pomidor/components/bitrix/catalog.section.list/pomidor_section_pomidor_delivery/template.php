<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strTitle = "";
?>

	<?

	$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
    $CURRENT_DEPTH = $TOP_DEPTH;
?>
<div class="page-nav">
<?
	foreach($arResult["SECTIONS"]  as $key =>  $arSection)
	{


        if($key == 0){?>

            <a  href="<?=$arSection["LIST_PAGE_URL"]?>" class="<?echo $APPLICATION->GetCurPage() ==  $arSection["LIST_PAGE_URL"] ? "active": ""?>"><? echo GetMessage("MESS_SECTION_ALL")?></a>
            <?
        }
	    if($arSection["RELATIVE_DEPTH_LEVEL"] == 1):?>
        <a href="<?=$arSection["SECTION_PAGE_URL"]?>"  class="<?echo $APPLICATION->GetCurPage() ==  $arSection["SECTION_PAGE_URL"] ? "active": ""?> "><?=$arSection["NAME"]?></a>

        <?endif;?>
    <?
	}
	?>
</div>
<div class="custom-select nav-select">
    <!-- test -->
    <select data-select2-id="1" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">
            <option 
                value="/menu-devilery/" 
                <? echo $APPLICATION->GetCurPage() ==  "/menu-devilery/" ? "selected": ""?> 
                data-select2-id="3"><? echo GetMessage("MESS_SECTION_ALL")?>
            </option>
        <?
        foreach($arResult["SECTIONS"]  as $key =>  $arSection)
        {
            if($key == 0){
                continue;
            }
            ?>

                <option 
                value="<?=$arSection["SECTION_PAGE_URL"]?>" 
                <? echo $APPLICATION->GetCurPage() ==  $arSection["SECTION_PAGE_URL"] ? "selected": ""?>
                >
                    <?=$arSection["NAME"]?>
                </option>


        <?}?>
    </select>
</div>
