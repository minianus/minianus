<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strTitle = "";
?>

	<?

	$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
    $CURRENT_DEPTH = $TOP_DEPTH;
?>

<?
	foreach($arResult["SECTIONS"]  as $key =>  $arSection)
	{


        if($key == 0){?>
    <div class="menu__tabs-item active">
      <span> <? echo GetMessage("MESS_SECTION_ALL")?></span>
    </div>

            <?
        }
	    if($arSection["RELATIVE_DEPTH_LEVEL"] == 1):?>
            <div class="menu__tabs-item" >
                <span>  <?=$arSection["NAME"]?></span>
            </div>


        <?endif;?>
    <?
	}
	?>


