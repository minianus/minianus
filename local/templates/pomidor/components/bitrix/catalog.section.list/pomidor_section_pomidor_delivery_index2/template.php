<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strTitle = "";
?>

	<?

	$TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
    $CURRENT_DEPTH = $TOP_DEPTH;
?>

<?
	foreach($arResult["SECTIONS"]  as $key =>  $arSection)
	{


        if($key == 0){?>
      
    <a href="?category=<?=$arSection['CODE']?>" class="menu-deliv menu-deliv__link active">
      <span> <? echo GetMessage("MESS_SECTION_ALL")?></span>
    </a>

            <?
        }
	    if($arSection["RELATIVE_DEPTH_LEVEL"] == 1):?>
            <a href="?category=<?=$arSection['CODE']?>" class="menu-deliv menu-deliv__link" >
                <span>  <?=$arSection["NAME"]?></span>
            </a>


        <?endif;?>
    <?
	}
	?>


