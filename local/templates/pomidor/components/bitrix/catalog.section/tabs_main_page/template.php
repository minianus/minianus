<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(count($arResult["ITEMS"]) > 0):?>
        <div class="menu__list">
	<?foreach($arResult["ITEMS"] as $arElement):?>
	<?
	$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
    $arFilter = Array("IBLOCK_ID"=>$arElement['IBLOCK_ID'], "ID"=>$arElement['ID']);
    $res = CIBlockElement::GetList(Array(), $arFilter);
    if ($ob = $res->GetNextElement()){;
        $arFields = $ob->GetFields(); // поля элемента
        $arProps = $ob->GetProperties(); // свойства элемента

    }
    $img=CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], Array('width'=>285, 'height'=>243), BX_RESIZE_IMAGE_EXACT, true,false,false,65);    
    $img2=CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], Array('width'=>285, 'height'=>243), BX_RESIZE_IMAGE_EXACT, true,false,false,65);    
            
            
	?>
            
            
    <div>
        <div id="<?=$this->GetEditAreaId($arElement['ID']);?>" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="<?=$arElement["ID"]?>">
                <img class="menu-item__image" src="<?=$img['src'];?>" alt="">
                <img class="menu-item__image_second" src="<?=$img2['src'];?>" alt="">
                <?if (!empty($arProps["LEADER"]["VALUE"])):?>
                <span class="menu-item__mark">Хит</span>
                <?endif;?>
                <?if (!empty($arProps["SPECIAL"]["VALUE"])):?>
                    <span class="menu-item__mark menu-item__mark_fav">
                                            <svg class="icon icon-favorite">
                                             <svg viewBox="0 0 512 504.37" id="icon-fav"><g id="Слой_2" data-name="Слой 2"><g id="Layer_1" data-name="Layer 1"><path d="M502.66 301.9a13.5 13.5 0 11-8.34 25.69l-23.19-7.53a13.5 13.5 0 018.34-25.69zm-21.9-138.61a33.61 33.61 0 01-8.55 34.62l-81.07 79a6.78 6.78 0 00-1.94 6l19.14 111.57a33.71 33.71 0 01-49 35.59l-100.2-52.67a6.71 6.71 0 00-6.28 0l-100.2 52.66a33.77 33.77 0 01-49-35.59L122.8 282.9a6.75 6.75 0 00-1.94-6l-81.07-79a33.79 33.79 0 0118.72-57.6l112-16.27a6.77 6.77 0 005.07-3.7l50.1-101.52a33.78 33.78 0 0160.56 0l50.11 101.52a6.75 6.75 0 005.08 3.7l112 16.27a33.59 33.59 0 0127.33 22.99zm-27.41 15.26a6.73 6.73 0 00-3.74-11.5l-112-16.29a33.73 33.73 0 01-25.43-18.47L262.06 30.77a6.42 6.42 0 00-3.18-3.15A7.25 7.25 0 00256 27a6.58 6.58 0 00-6 3.77l-50.16 101.52a33.74 33.74 0 01-25.42 18.47L62.39 167a6.76 6.76 0 00-3.74 11.51l81.06 79a33.72 33.72 0 019.71 29.88L130.28 399a6.76 6.76 0 009.81 7.12l100.2-52.68a33.69 33.69 0 0131.43 0l100.2 52.68a6.76 6.76 0 009.8-7.12l-19.14-111.54a33.75 33.75 0 019.71-29.88z"></path><path d="M455.07 171.64a6.55 6.55 0 01-1.72 6.91l-81.06 79a33.76 33.76 0 00-9.71 29.89L381.72 399a6.76 6.76 0 01-9.8 7.12l-100.2-52.68a33.69 33.69 0 00-31.43 0l-100.2 52.68a6.76 6.76 0 01-9.81-7.12l19.14-111.57a33.73 33.73 0 00-9.71-29.89l-81.06-79a6.74 6.74 0 013.74-11.5l112-16.29a33.75 33.75 0 0025.42-18.47L250 30.77a6.58 6.58 0 016-3.77 7.25 7.25 0 012.88.61 6.42 6.42 0 013.18 3.15l50.1 101.52a33.73 33.73 0 0025.43 18.47l112 16.28a6.56 6.56 0 015.48 4.61zM413.78 18.81a13.51 13.51 0 013 18.86L402.43 57.4a13.5 13.5 0 11-21.85-15.87l14.34-19.73a13.5 13.5 0 0118.86-2.99zM269.51 466.48v24.39a13.51 13.51 0 01-27 0v-24.39a13.51 13.51 0 0127 0zM131.36 41.45a13.51 13.51 0 11-21.85 15.88L95.17 37.6A13.51 13.51 0 11117 21.72zM49.54 303a13.5 13.5 0 01-8.67 17l-23.19 7.54a13.8 13.8 0 01-4.17.66 13.51 13.51 0 01-4.17-26.36l23.19-7.54a13.49 13.49 0 0117.01 8.7z"></path></g></g></svg>
                                            </svg>
                    </span>
                <?endif;?>
                <?if (!empty($arProps["NEW"]["VALUE"])):?>
                    <span class="menu-item__mark menu-item__mark_new">New</span>
                <?endif;?>
                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title"><?=$arElement["NAME"]?></h4>
                <p class="menu-item__desc">
                    <?=$arElement["PREVIEW_TEXT"]?>
                  </p>
            </div>
            <div class="menu-item__bottom">
                <?foreach($arResult["PRICES"] as $code=>$arPrice):?>

                        <?if($arPrice = $arElement["PRICES"][$code]):?>
                            <?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                <s><?=$arPrice["PRINT_VALUE"]?></s><br /><span class="menu-item__price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
                            <?else:?>
                                <span class="menu-item__price"><?=$arPrice["PRINT_VALUE"]?></span>
                            <?endif?>
                        <?else:?>
                            &nbsp;
                        <?endif;?>

                <?endforeach;?>
                <?if(count($arResult["PRICES"]) > 0):?>

                    <?if($arElement["CAN_BUY"]):?>
                            &nbsp;<a class="btn btn_add" href="<?echo $arElement["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD")?></a>
                    <?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
                        <?=GetMessage("CATALOG_NOT_AVAILABLE")?>
                        <?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
                            "NOTIFY_ID" => $arElement['ID'],
                            "NOTIFY_URL" => htmlspecialcharsback($arElement["SUBSCRIBE_URL"]),
                            "NOTIFY_USE_CAPTHA" => "N"
                        ),
                            $component
                        );?>
                    <?endif?>&nbsp;

                <?endif;?>

            </div>
        </div>
 </div>
	<?endforeach;?>
        </div>
<?endif;?>


