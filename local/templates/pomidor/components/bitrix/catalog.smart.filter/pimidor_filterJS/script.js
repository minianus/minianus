$(document).ready(() => {
	$('body').on('click', '.menu__tabs-item', function() {
		$(".menu__tabs-item").removeClass("active")
		$(this).addClass("active")
		let filter_property = $(this).attr("filter-property")
		$(".pomidor_delivery_item").hide()
		$(".property_input").each(function (index,element) {
			if($(this).val() === filter_property){
				$(this).parent().parent().parent().show()
			}else if(filter_property === "all"){
				$(".pomidor_delivery_item").show()
			}
					})
	})
})