<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)
{
	die();
}

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

\Bitrix\Main\Page\Asset::getInstance()->addCss(
	'/bitrix/css/main/system.auth/flat/style.css'
);

if ($arResult['AUTHORIZED'])
{
	echo Loc::getMessage('MAIN_AUTH_FORM_SUCCESS');
	return;
}
?>
<form   name="<?= $arResult['FORM_ID'];?>" method="post" target="_top" action="<?= POST_FORM_ACTION_URI;?>" class="form form-middle ">
    <h2 class="h2 form-middle__title">Вход</h2>
    <?if ($arResult['ERRORS']):?>
        <div class="alert alert-danger">
            <? foreach ($arResult['ERRORS'] as $error)
            {
                echo $error;
            }
            ?>
        </div>
    <?endif;?>

    <div class="tab-widget tab-widget__yellow">
        <div class="form__item form__item_s-offset">
            <div class="tab-nav form-middle__tab">
                <a href="#" class="tab-nav__link active btn btn_default ">
                    По номеру телефона
                </a>
                <a href="#" class="tab-nav__link btn btn_default ">
                    По номеру карты
                </a>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab active">
                <?if ($arResult['SECURE_AUTH']):?>
                    <div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
                        <div class="bx-authform-psw-protected-desc"><span></span>
                            <?= Loc::getMessage('MAIN_AUTH_FORM_SECURE_NOTE');?>
                        </div>
                    </div>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure').style.display = '';
                    </script>
                <?endif?>

                <div class="form__item form__item_s-offset">
                    <input type="tel" class="input input_default input_yellow phone-mask" placeholder="Номер телефона" name="<?= $arResult['FIELDS']['login'];?>"  value="<?= \htmlspecialcharsbx($arResult['LAST_LOGIN']);?>" />
                </div>
                <div class="bx-authform-formgroup-container">


                        <?if ($arResult['SECURE_AUTH']):?>

                            <script type="text/javascript">
                                document.getElementById('bx_auth_secure').style.display = '';
                            </script>
                        <?endif?>
                        <input type="password" name="<?= $arResult['FIELDS']['password'];?>"  class="input input_default input_yellow" placeholder="Пароль" autocomplete="off" />

                </div>

            </div>
            <div class="tab">
                <div class="form__item form__item_s-offset">
                    <input type="tel" class="input input_default input_yellow card-mask" placeholder="Номер карты">
                </div>
                <div class="form__item form__item_s-offset">
                    <input type="password" class="input input_default input_yellow" placeholder="Пароль">
                </div>
            </div>
        </div>
        <?if ($arResult['CAPTCHA_CODE']):?>
            <input type="hidden" name="captcha_sid" value="<?= \htmlspecialcharsbx($arResult['CAPTCHA_CODE']);?>" />
            <div class="bx-authform-formgroup-container dbg_captha">
                <div class="bx-authform-label-container">
                    <?= Loc::getMessage('MAIN_AUTH_FORM_FIELD_CAPTCHA');?>
                </div>
                <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?= \htmlspecialcharsbx($arResult['CAPTCHA_CODE']);?>" width="180" height="40" alt="CAPTCHA" /></div>
                <div class="bx-authform-input-container">
                    <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />
                </div>
            </div>
        <?endif;?>
        <div class="form__item form__item_s-b-offset">
            <div class="nav-links nav-links_sb">
                <a href="/smena-parolya.php" class="link grey link_decorated">Восстановить пароль</a>
                <?if ($arResult['AUTH_REGISTER_URL']):?>
                    <a href="<?= $arResult['AUTH_REGISTER_URL'];?>" class="link grey link_decorated" rel="nofollow">
                        <?= Loc::getMessage('MAIN_AUTH_FORM_URL_REGISTER_URL');?>
                    </a>
                <?endif;?>

            </div>
        </div>
    </div>

    <div class="form__item form__item_s-offset">
        <input type="submit" class="btn btn_large btn_default btn_yellow" name="<?= $arResult['FIELDS']['action'];?>" value="<?= Loc::getMessage('MAIN_AUTH_FORM_FIELD_SUBMIT');?>" />

    </div>

</form>








<script type="text/javascript">
	<?if ($arResult['LAST_LOGIN'] != ''):?>
	try{document.<?= $arResult['FORM_ID'];?>.USER_PASSWORD.focus();}catch(e){}
	<?else:?>
	try{document.<?= $arResult['FORM_ID'];?>.USER_LOGIN.focus();}catch(e){}
	<?endif?>
</script>