<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<div class="window _window_h window_bordered window_s-offset">
    <div class="window__header window__header_sb">
        <h3 class="s-large window__title">
            Введите дату рождения
        </h3>
        <svg class="icon icon-yellow">
            <svg viewBox="0 0 26 26" id="icon-gift"><path d="M23.7148 6.09375H20.6151C21.0944 5.45675 21.3789 4.66532 21.3789 3.80859C21.3789 1.70854 19.6704 0 17.5703 0C16.3053 0 15.3725 0.453223 14.6347 1.42629C14.0175 2.24042 13.5661 3.37711 13 4.8133C12.4339 3.37705 11.9825 2.24042 11.3653 1.42629C10.6275 0.453223 9.6947 0 8.42969 0C6.32963 0 4.62109 1.70854 4.62109 3.80859C4.62109 4.66532 4.90562 5.45675 5.38489 6.09375H2.28516C1.02512 6.09375 0 7.11887 0 8.37891V9.90234C0 10.8954 0.636848 11.742 1.52344 12.0565V23.7148C1.52344 24.9749 2.54856 26 3.80859 26H22.1914C23.4514 26 24.4766 24.9749 24.4766 23.7148V12.0565C25.3632 11.742 26 10.8954 26 9.90234V8.37891C26 7.11887 24.9749 6.09375 23.7148 6.09375ZM14.3982 5.42039C15.5651 2.45995 16.0087 1.52344 17.5703 1.52344C18.8303 1.52344 19.8555 2.54856 19.8555 3.80859C19.8555 5.06863 18.8303 6.09375 17.5703 6.09375H14.1318C14.2251 5.85939 14.3141 5.63377 14.3982 5.42039ZM8.42969 1.52344C9.99126 1.52344 10.4349 2.45995 11.6018 5.42039C11.6859 5.63377 11.7749 5.85939 11.8682 6.09375H8.42969C7.16965 6.09375 6.14453 5.06863 6.14453 3.80859C6.14453 2.54856 7.16965 1.52344 8.42969 1.52344ZM9.95312 24.4766H3.80859C3.38858 24.4766 3.04688 24.1349 3.04688 23.7148V12.1875H9.95312V24.4766ZM9.95312 10.6641H2.28516C1.86514 10.6641 1.52344 10.3224 1.52344 9.90234V8.37891C1.52344 7.95889 1.86514 7.61719 2.28516 7.61719H9.95312V10.6641ZM14.5234 24.4766H11.4766V7.61719C11.6362 7.61719 13.648 7.61719 14.5234 7.61719V24.4766ZM22.9531 23.7148C22.9531 24.1349 22.6114 24.4766 22.1914 24.4766H16.0469V12.1875H22.9531V23.7148ZM24.4766 9.90234C24.4766 10.3224 24.1349 10.6641 23.7148 10.6641H16.0469V7.61719H23.7148C24.1349 7.61719 24.4766 7.95889 24.4766 8.37891V9.90234Z" fill-rule="evenodd"></path></svg>
        </svg>
    </div>
    <hr class="w-total__hr">
    <div class="window__content _window__content_center window__content_offset">
            <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" enctype="multipart/form-data" class="form form-reset">
                <?=$arResult["BX_SESSION_CHECK"]?>
                <input type="hidden" name="lang" value="<?=LANG?>" />
                <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                <input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
                <input type="hidden" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />

                <div class="form__item form__item_s-offset">
                    <p class="gray block mb-2">Специально ко дню рождения мы делаем подарки нашим покупателям</p>
                </div>
                <div class="form__item form__item_s-offset">
                    <input type="text" id="PERSONAL_BIRTHDAY" name="PERSONAL_BIRTHDAY" placeholder="Число / Месяц / Год" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" class="input input_default input_yellow" onclick="BX.calendar({node:this, field:'PERSONAL_BIRTHDAY', form: 'form1', bTime: false, currentTime: '1607450480', bHideTime: false});" onmouseover="BX.addClass(this, 'calendar-icon-hover');" onmouseout="BX.removeClass(this, 'calendar-icon-hover');">

                </div>
                <?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
                    <h2><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></h2>
                    <?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
                        <strong><?=$arUserField["EDIT_FORM_LABEL"]?><?if ($arUserField["MANDATORY"]=="Y"):?><span class="starrequired">*</span><?endif;?></strong><br/>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:system.field.edit",
                            $arUserField["USER_TYPE"]["USER_TYPE_ID"],
                            array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y")
                        );?>
                    <?endforeach;?>
                <?endif;?>
                <div class="form__item form__item_s-offset">
                    <input name="save" value="<?=GetMessage("MAIN_SAVE")?>" class="bx_bt_button bx_big shadow btn btn_yellow btn_middle" type="submit">
                </div>
            </form>
    </div>
</div>




