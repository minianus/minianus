<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
?>
<?=ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>

<div class="window _window_h window_bordered window_s-offset">
    <div class="window__header window__header_sb">
        <h3 class="s-large window__title">
            Изменить пароль </h3>
        <svg class="icon icon-yellow">
        <svg viewBox="0 0 26 26" id="icon-lock"><path d="M13.0569 2.0315C13.038 2.03135 13.019 2.03125 13 2.03125C12.7196 2.03125 12.4922 2.2586 12.4922 2.53906C12.4922 2.81953 12.7196 3.04687 13 3.04687C13.015 3.04687 13.0301 3.04698 13.045 3.04713C13.0471 3.04713 13.0491 3.04713 13.0511 3.04713C13.3288 3.04713 13.5556 2.82364 13.5588 2.54526C13.562 2.26479 13.3374 2.0348 13.0569 2.0315Z" fill-rule="evenodd"></path><path d="M21.7344 9.85156H19.5508V6.55078C19.5508 2.93866 16.6121 0 13 0C9.38788 0 6.44922 2.93866 6.44922 6.55078V9.85156H4.26562C3.4256 9.85156 2.74219 10.535 2.74219 11.375V24.4766C2.74219 25.3166 3.4256 26 4.26562 26H21.7344C22.5744 26 23.2578 25.3166 23.2578 24.4766V11.375C23.2578 10.535 22.5744 9.85156 21.7344 9.85156ZM22.2422 24.4766C22.2422 24.7566 22.0144 24.9844 21.7344 24.9844H4.26562C3.98562 24.9844 3.75781 24.7566 3.75781 24.4766V11.375C3.75781 11.095 3.98562 10.8672 4.26562 10.8672H11.3242C11.6046 10.8672 11.832 10.6398 11.832 10.3594C11.832 10.0789 11.6046 9.85156 11.3242 9.85156H9.49609V6.55078C9.49609 5.14089 10.3369 3.8737 11.6381 3.32236C11.8963 3.21298 12.017 2.91495 11.9076 2.65667C11.7982 2.39845 11.5003 2.27774 11.2419 2.38723C9.56439 3.09796 8.48047 4.73225 8.48047 6.55078V9.85156H7.46484V6.55078C7.46484 3.49868 9.94789 1.01562 13 1.01562C16.0521 1.01562 18.5352 3.49868 18.5352 6.55078V9.85156H17.5195V6.55078C17.5195 4.77389 16.4699 3.15453 14.8455 2.42531C14.5896 2.3106 14.2891 2.42475 14.1742 2.68064C14.0593 2.93653 14.1737 3.23705 14.4295 3.35187C15.6897 3.91757 16.5039 5.17319 16.5039 6.55078V9.85156H14.8535C14.5731 9.85156 14.3457 10.0789 14.3457 10.3594C14.3457 10.6398 14.5731 10.8672 14.8535 10.8672H21.7344C22.0144 10.8672 22.2422 11.095 22.2422 11.375V24.4766Z" fill="#f7b137"></path>
            <path d="M13.0753 9.85156H13.0625C12.7821 9.85156 12.5547 10.0789 12.5547 10.3594C12.5547 10.6398 12.7821 10.8672 13.0625 10.8672H13.0753C13.3557 10.8672 13.5831 10.6398 13.5831 10.3594C13.5831 10.0789 13.3557 9.85156 13.0753 9.85156Z" fill-rule="evenodd"></path><path d="M14.3971 17.9126C14.8627 17.5105 15.1328 16.9284 15.1328 16.3008C15.1328 15.1247 14.176 14.168 13 14.168C11.824 14.168 10.8672 15.1247 10.8672 16.3008C10.8672 16.9359 11.1552 17.5379 11.6361 17.9397L11.2994 21.1224C11.2843 21.2655 11.3306 21.4084 11.4269 21.5155C11.5232 21.6225 11.6605 21.6836 11.8045 21.6836H14.2315C14.3755 21.6836 14.5127 21.6225 14.6091 21.5155C14.7053 21.4084 14.7517 21.2656 14.7366 21.1224L14.3971 17.9126ZM13.5919 17.2481C13.4275 17.3511 13.3362 17.5389 13.3566 17.7318L13.6671 20.668H12.3687L12.6771 17.7524C12.698 17.555 12.602 17.3636 12.4313 17.2623C12.0878 17.0585 11.8828 16.6991 11.8828 16.3008C11.8828 15.6848 12.384 15.1836 13 15.1836C13.616 15.1836 14.1172 15.6848 14.1172 16.3008C14.1172 16.6878 13.9208 17.0419 13.5919 17.2481Z" fill-rule="evenodd"></path>
        </svg>
        </svg>
    </div>
    <hr class="w-total__hr">

    <div class="window__content _window__content_center window__content_offset">
        <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?" class="form form-reset" enctype="multipart/form-data">
            <?=$arResult["BX_SESSION_CHECK"]?>
            <input type="hidden" name="lang" value="<?=LANG?>" />
            <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
            <input type="hidden" name="LOGIN" value=<?=$arResult["arUser"]["LOGIN"]?> />
            <input type="hidden" name="EMAIL" value=<?=$arResult["arUser"]["EMAIL"]?> />

            <div class="form__item form__item_s-offset">
                <input type="password" placeholder="<?=GetMessage('NEW_PASSWORD_REQ')?>" name="NEW_PASSWORD" maxlength="50" value="" autocomplete="off" class="input input_default input_yellow">
            </div>
            <div class="form__item form__item_s-offset">
                <input type="password" placeholder="<?=GetMessage('NEW_PASSWORD_CONFIRM')?>"  name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" autocomplete="off" class="input input_default input_yellow">
            </div>

            <div class="form__item form__item_s-offset">
                <input name="save" value="<?=GetMessage("MAIN_SAVE")?>" class="bx_bt_button bx_big shadow btn btn_yellow btn_middle" type="submit">
            </div>
        </form>
    </div>
</div>




