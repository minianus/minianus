<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <div class="footer__col footer__menu">
        <a class="footer__headline" href="<?=$arResult[0]["LINK"]?>"><?=$arResult[0]["TEXT"]?></a>
        <ul class="footer__list">
<?
foreach($arResult as $index => $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
<? if ($index < 1):
    continue;?>

    <?endif;?>

	<?if($arItem["SELECTED"]):?>
    <li class="footer__item"><a class="footer__link selected" href="#"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
    <li class="footer__item"><a class="footer__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>
        </ul>
    </div>
<?endif?>