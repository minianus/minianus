<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

$menuBlockId = "catalog_menu_".$this->randString();
?>
	<nav class="header__nav" id="cont_<?=$menuBlockId?>">
		<ul class="header__nav-list" id="ul_<?=$menuBlockId?>">
		<?

		foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns)
		{

		    //--first level--
			$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;
			$class = "header__nav-item  bx-nav-list-".(($existPictureDescColomn) ? count($arColumns)+1 : count($arColumns))."-col";
			if($arResult["ALL_ITEMS"][$itemID]["SELECTED"])
			{
				$class .= " header__nav-link-active";
			}
			if(is_array($arColumns) && count($arColumns) > 0)
			{
				$class .= " bx-nav-parent";
			}
		?>
			<li
				class="<?=$class?> <?if($arResult["ALL_ITEMS"][$itemID]["LINK"] == "/menu/"):?> menu-trigger<?endif;?>"
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
				<?if (is_array($arColumns) && count($arColumns) > 0):?>
					data-role="bx-menu-item"
					onclick="if (BX.hasClass(document.documentElement, 'bx-touch')) obj_<?=$menuBlockId?>.clickInMobile(this, event);"
				<?endif?>
			>
                
				<a
					class=" header__nav-link <?if(!empty($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS'])){?> menu-modal<?}?>"
					href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"
					<?if (is_array($arColumns) && count($arColumns) > 0 && $existPictureDescColomn):?>
						onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemID?>');"
					<?endif?>
				>
                    <?if($arResult["ALL_ITEMS"][$itemID]["LINK"] == "/menu/"):?>
<!--                    <svg class="icon icon-tomat">-->
<!--                        <svg viewBox="0 0 621.93 745.7" id="icon-tomat"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z"></path><path d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z"></path></g></g></svg>-->
<!--                    </svg>-->
                        <?endif?>
						<?=htmlspecialcharsbx($arResult["ALL_ITEMS"][$itemID]["TEXT"], ENT_COMPAT, false)?>
						<?if (is_array($arColumns) && count($arColumns) > 0):?>
                            <i class="bx-nav-angle-bottom"></i>
                        <?endif?>
				</a>
				<?
				if (is_array($arColumns) && count($arColumns) > 0)
				{
				?>
					<span class="bx-nav-parent-arrow" onclick="obj_<?=$menuBlockId?>.toggleInMobile(this)"><i class="bx-nav-angle-bottom"></i></span> <!-- for mobile -->
					<div class="bx-nav-2-lvl-container">
						<?
						foreach($arColumns as $key=>$arRow)
						{
						?>
							<ul class="bx-nav-list-2-lvl">

							<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  <!-- second level-->
								<li class="bx-nav-2-lvl">
									<a class="bx-nav-2-lvl-link"
										href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
										<?if ($existPictureDescColomn):?>
											onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_2?>');"
										<?endif?>
										data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]["picture_src"]?>"
										<?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?>class="bx-active"<?endif?>
									>
										<span class="bx-nav-2-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></span>
									</a>
								<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
									<ul class="bx-nav-list-3-lvl">
									<?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
										<li class="bx-nav-3-lvl">
											<a
												class="bx-nav-3-lvl-link"
												href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>"
												<?if ($existPictureDescColomn):?>
													onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_3?>');return false;"
												<?endif?>
												data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["picture_src"]?>"
												<?if($arResult["ALL_ITEMS"][$itemIdLevel_3]["SELECTED"]):?>class="bx-active"<?endif?>
											>
												<span class="bx-nav-3-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?></span>
											</a>
										</li>
									<?endforeach;?>
									</ul>
								<?endif?>
								</li>
							<?endforeach;?>
							</ul>
						<?
						}
						?>

					</div>
				<?
				}
				?>
			</li>
		<?
		}
		?>
		</ul>
	</nav>


<script>
	BX.ready(function () {
		window.obj_<?=$menuBlockId?> = new BX.Main.MenuComponent.CatalogHorizontal('<?=CUtil::JSEscape($menuBlockId)?>', <?=CUtil::PhpToJSObject($arResult["ITEMS_IMG_DESC"])?>);
	});
</script>