<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<nav class="mobile__nav">
    <ul class="mobile__nav-list">
    <?foreach($arResult as $item){?>
        <?if($item['PARAMS']['TYPE'] == 'menu'){?>
            <li class="mobile__nav-item"><a class="mobile__nav-link header__nav-link-active" href="<?=$item['LINK']?>">
                <svg class="icon icon-tomat">
                    <svg viewBox="0 0 621.93 745.7" id="icon-tomat">
                        <g id="Слой_2" data-name="Слой 2">
                            <g id="Слой_1-2" data-name="Слой 1">
                                <path
                                    d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z">
                                </path>
                                <path
                                    d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z">
                                </path>
                            </g>
                        </g>
                    </svg>
                </svg><?=$item['TEXT']?></a>
        </li>
            <?}else{?>
                <li class="mobile__nav-item"><a class="mobile__nav-link <?=$item['PARAMS']['CLASS']?>" href="<?=$item['LINK']?>"><?=$item['TEXT']?></a></li>
                <?}?>
    <?}?>
        
        
        
    </ul>
</nav>