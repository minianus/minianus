<?php

$filter = ['IBLOCK_ID' => get_iblock_id_by_code('restoran_staff'), 'ID' => $arResult['PROPERTIES']['STAFF']['VALUE']];
$select = ['NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT'];

$res = CIBlockElement::GetList(Array(), $filter, false, [], $select);

while($item = $res->getNext()){
    $arResult['STAFF'][] = $item;
}

