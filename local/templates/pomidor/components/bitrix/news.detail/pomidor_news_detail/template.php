<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?>

<div class="wrap">
    <!-- <h1 class="page__title"><?=$arResult['NAME']?></h1> -->
    <div class="full-slider-widget">
        <div class="full-nav ">
            <a href="#" class="full-nav__btn full-nav__btn_left">
                <svg class="slider-icon__arrow">
                    <use xlink:href="/local/templates/pomidor/images/icons.svg#arrow-left"></use>
                </svg>
            </a>
            <a href="#" class="full-nav__btn full-nav__btn_right">
                <svg class="slider-icon__arrow">
                    <use xlink:href="/local/templates/pomidor/images/icons.svg#arrow-right"></use>
                </svg>
            </a>
        </div>
        <div class="full-slider dots-control arrows-control">
        <?foreach($arResult['PROPERTIES']['SLIDER']['VALUE'] as $slide){
        //$pic = CFile::getPath($slide);
        $pic = CFile::ResizeImageGet($slide, Array('width'=>1420, 'height'=>479), BX_RESIZE_IMAGE_EXACT, true,false,false,65); 
            ?>
            <div>
                <div class="full-slider__item">
                    <img src="<?=$pic['src']?>" alt="">
                </div>
            </div>
        <?}?>
        </div>
    </div>
    <div class="section section_t-offset restoran-contacts">
        <h2 class="section__title">Всегда рады вас видеть</h2>
        <div class="view-w">
            <div class="row">
                <div class="col col--lg-6">
                    <?=$arResult['DETAIL_TEXT']?>
                    <?if(!empty($arResult['PROPERTIES']['PHONES']['VALUE'])){?>
                    <div class="view-w__item iconed iconed_start">
                        <div class="iconed__ico view-w__ico">
                            <img class="phone-ico" src="/local/templates/pomidor/images/phone-ico.svg" alt="">
                        </div>
                        <div class="view-w__content">
                            тел.
                            <?foreach($arResult['PROPERTIES']['PHONES']['VALUE'] as $phone){?>
                            <a href="tel:<?=clear_phone($phone)?>" class="view-w__phone link link_black"><?=$phone?></a> 
                            <?}?>
                        </div>
                    </div>
                    <?}?>
                    <div class="view-w__item iconed iconed_start">
                        <div class="iconed__ico view-w__ico">
                            <img class="chef-toque" src="/local/templates/pomidor/images/pdf-file.svg" alt="">
                        </div>
                        <div class="view-w__content">
                            <a href="<?=CFile::getPath($arResult['PROPERTIES']['MENU']['VALUE'])?>" class="view-w__menu link link_black link_decorated">просмотреть меню</a>
                        </div>
                    </div>
                </div>
                <?if(!empty($arResult['PROPERTIES']['MAP_POINT']['VALUE'])){?>
                <div class="col col--lg-6">
                 <?if(!empty($arResult['PROPERTIES']['MAP_PREVIEW']['VALUE'])){
                    $img = CFile::ResizeImageGet($arResult['PROPERTIES']['MAP_PREVIEW']['VALUE'], Array('width'=>695, 'height'=>435), BX_RESIZE_IMAGE_EXACT, true,false,false,65); 
                ?>
                    <div class="ymap-container restoran-map-wrapper" style="background-image: url('<?=$img['src'];?>')">
					<div id="map" class="restoran-map">
					</div>
				</div>
                <?}else{?>
                 <div class="ymap-container restoran-map-wrapper" style="background-image: url('/upload/images/map-bg.jpg')">
					<div id="map" class="restoran-map">
					</div>
                <?}?>
               <?}?>
                </div>
            </div>
        </div>
    </div>
    <div class="section section_n-offset">
        <h2 class="section__title">Персонал</h2>
        <div class="personal">
            <div class="row row_tablet-small">
            <?foreach($arResult['STAFF'] as $staff){?>
                <div class="col col--lg-3 col--md-6 col--xs-6">
                    <div class="person">
                        <div class="person__img">
                        <?$pic = CFile::getPath($staff['PREVIEW_PICTURE'])?>
                            <img src="<?=$pic?>" alt="">
                        </div>
                        <div class="person__desc">
                            <h4 class="h4 person__title"><?=$staff['NAME']?></h4>
                            <span class="job person__job"><?=$staff['PREVIEW_TEXT']?></span>
                        </div>
                    </div>
                </div>
            <?}?>
            </div>
        </div>
    </div>
</div>
<!-- 11111 --->


<script>
        //Переменная для включения/отключения индикатора загрузки
        var spinner = $('.ymap-container').children('.loader');
        //Переменная для определения была ли хоть раз загружена Яндекс.Карта (чтобы избежать повторной загрузки при наведении)
        var check_if_load = false;
        //Необходимые переменные для того, чтобы задать координаты на Яндекс.Карте
        var myMapTemp, myPlacemarkTemp;

        //Функция создания карты сайта и затем вставки ее в блок с идентификатором &#34;map-yandex&#34;
        function init() {
            var myMapTemp = new ymaps.Map("map", {
                center: [<?=$arResult['PROPERTIES']['MAP_POINT']['VALUE'];?>],
                zoom: 19,
                controls: ['zoomControl', 'fullscreenControl']
            });
            var myPlacemarkTemp = new ymaps.Placemark([<?=$arResult['PROPERTIES']['MAP_POINT']['VALUE'];?>], {
                balloonContent: "<?=$arResult['NAME']?>",
            });
            myMapTemp.geoObjects.add(myPlacemarkTemp);



            var layer = myMapTemp.layers.get(0).get(0);

            // Решение по callback-у для определения полной загрузки карты
            waitForTilesLoad(layer).then(function() {
                // Скрываем индикатор загрузки после полной загрузки карты
                spinner.removeClass('is-active');
            });
        }

        // Функция для определения полной загрузки карты (на самом деле проверяется загрузка тайлов) 
        function waitForTilesLoad(layer) {
            return new ymaps.vow.Promise(function(resolve, reject) {
                var tc = getTileContainer(layer),
                    readyAll = true;
                tc.tiles.each(function(tile, number) {
                    if (!tile.isReady()) {
                        readyAll = false;
                    }
                });
                if (readyAll) {
                    resolve();
                } else {
                    tc.events.once("ready", function() {
                        resolve();
                    });
                }
            });
        }

        function getTileContainer(layer) {
            for (var k in layer) {
                if (layer.hasOwnProperty(k)) {
                    if (
                        layer[k] instanceof ymaps.layer.tileContainer.CanvasContainer ||
                        layer[k] instanceof ymaps.layer.tileContainer.DomContainer
                    ) {
                        return layer[k];
                    }
                }
            }
            return null;
        }

        // Функция загрузки API Яндекс.Карт по требованию (в нашем случае при наведении)
        function loadScript(url, callback) {
            var script = document.createElement("script");

            if (script.readyState) { // IE
                script.onreadystatechange = function() {
                    if (script.readyState == "loaded" ||
                        script.readyState == "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // Другие браузеры
                script.onload = function() {
                    callback();
                };
            }
            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        }

        // Основная функция, которая проверяет когда мы навели на блок с классом &#34;ymap-container&#34;
        var ymap = function() {
            $('.ymap-container').mouseenter(function() {
                if (!check_if_load) { // проверяем первый ли раз загружается Яндекс.Карта, если да, то загружаем

                    // Чтобы не было повторной загрузки карты, мы изменяем значение переменной
                    check_if_load = true;

                    // Показываем индикатор загрузки до тех пор, пока карта не загрузится
                    spinner.addClass('is-active');

                    // Загружаем API Яндекс.Карт
                    loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1", function() {
                        // Как только API Яндекс.Карт загрузились, сразу формируем карту и помещаем в блок с идентификатором &#34;map-yandex&#34;
                        ymaps.load(init);
                    });
                }
            });
        }

        $(function() {
            ymap();
        });
    </script>