<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$weight = $arResult['PROPERTIES']['SHOT_ABOUT']['VALUE'];


if(!empty($arResult["DETAIL_PICTURE"])){
$img=CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], Array('width'=>595, 'height'=>507), BX_RESIZE_IMAGE_PROPORTIONAL, true,false,false,75); 
}else{
   $img = $cardNoPicLarge; 
}
?>


<div class="row card__row mb0">
    <div class="col col--lg-6">
        <div class="card__img">
            <img class="card__pic " src="<?=$img['src']?>" alt="">
        </div>
    </div>
    <div class="col col--lg-6">
        <div class="card__content">
            <div class="card__header">
                <h2 class="card__title"><?=$arResult["NAME"]?></h2>
                <span class="card__desc">
                  <?=$weight?></span>
            </div>
            <?if(!empty($arResult["PREVIEW_TEXT"])){?>
            <div class="box box_b-large">
                <div class="card__desc">
                    <div class="info">
                        <?=$arResult["PREVIEW_TEXT"];?>
                    </div>
                </div>
            </div>
            <?}?>
            <div class="box box_b-large">
                <div class="add-w">
                    <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                    <div class="add-w__btns mb30">
                    </div>
                    <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                    <div class="add-w__city-wrap">
                        <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                        <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                    </div>

                    <div class="menu_cart-address-tab">
                    </div>
                    <div class="menu_cart-address-habarovsk d-sm-none ">
                    </div>
                    <div class="menu_cart-address-komsomolsk d-sm-none">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

