<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="banner__wrap">
    <div class="banner__slider">
<?foreach($arResult["ITEMS"] as $arItem):?>

	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
        <div class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="banner__slide">
                <div>
                    <div class="banner__container">
		<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
			<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
				<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
			<?else:?>
				<?=$arProperty["DISPLAY_VALUE"];?>
			<?endif?>
		<?endforeach;?>
                </div>
                </div>
            </div>
	</div>
<?endforeach;?>

</div>
    <div class="slider-nav banner__arrows">
        <div class="slider-arrow banner-prev"><img class="icon-arrow arrow-prev" src="/upload/images/arrow-left.png" alt=""></div>
        <div class="slider-arrow banner-next"><img class="icon-arrow arrow-next" src="/upload/images/arrow-right.png" alt=""></div>
    </div>
</div>

