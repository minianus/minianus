<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wrap">
    <div class="address__container">

<?foreach($arResult["ITEMS"] as $arItem):?>
        <div <?=$this->GetEditAreaId($arItem['ID']);?>>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
            <div class="address__item" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title"><?echo $arItem["NAME"]?></span>
                    <div class="address__item-desc">
                        <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

                                <? if($arProperty["CODE"] == "PHONES"):?>
                                <?foreach($arProperty['VALUE'] as $phone){?>
                                    <a class="address__item-link" href="tel:<?=clear_phone($phone)?>"><?=$phone?></a><br>
                                <?}?>
                                <?endif?>
                                <? if($arProperty["CODE"]== "WORKTIME_ADDREESS"):?>
                                    <span><?echo $arProperty["NAME"]?></span>
                                    <span class="address__item-time"><?echo $arProperty["~VALUE"]['TEXT']?></span>
                                <?endif?>


                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
<?endforeach;?>
</div>
</div>
<div class="slider-arrow address-prev"><img class="icon-arrow arrow-prev" src="/upload/images/arrow-left.png" alt=""></div>
<div class="slider-arrow address-next"><img class="icon-arrow arrow-next" src="/upload/images/arrow-right.png" alt=""></div>
