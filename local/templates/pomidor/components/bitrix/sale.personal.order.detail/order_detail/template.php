<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$order_props_code_to_value_map = [];
foreach($arResult['ORDER_PROPS'] as $prop){
    $order_props_code_to_value_map[$prop['CODE']] = $prop['VALUE'];
}

$basket_items = [];

$general_item_ids = [];
$item_ids = [];
foreach($arResult['BASKET'] as $basket_item){
    $item_ids[] = $basket_item['PRODUCT_ID'];
}

$general_item_ids = filter_items_id_by_iblock_id($item_ids, get_iblock_id_by_code('menu-devilery'));
$general_item_data = get_items_data($general_item_ids, get_iblock_id_by_code('menu-devilery'));
foreach($arResult['BASKET'] as $basket_item){
    if(in_array($basket_item['PRODUCT_ID'], $general_item_ids)){
        $basket_items[] = array_merge($basket_item, $general_item_data[$basket_item['PRODUCT_ID']]);
    }
}
?>
<div class="wrap page page_t-offset">
    <h1 class="page__title">
        Заказ детально
    </h1>
    <div class="lk-row">
        <div class="lk-content">
            <div class="order-list order-list_b-offset">
                <div class="order-list__header">
                    <h4 class="h4 order-list__title">
                        Заказ №<?=$arResult['ID']?>
                    </h4>
                    <a href="/personal/" class="link link_black link_b-bordered">
                        Вернуться в ЛК
                    </a>
                </div>
                <div class="order-item">
                <?foreach($basket_items as $basket_item){?> 
                <?
                $props = parse_basket_item_props($basket_item['PROPS']);
                    ?>   
                    <div class="order-item__row">
                        <div class="order-item__col" style="width:230px">
                            <a href="#" class="order-item__img">
                                <?
                                $pic = resize($basket_item['PREVIEW_PICTURE'], 214, 214);
                                ?>
                                <img src="<?=$pic?>" alt="">
                            </a>
                        </div>
                        <div class="order-item__col">
                            <div class="order-item__content">
                                <a href="#" class="order-item__title order-item__title_b-offset">
                                    <?=$basket_item['NAME']?>
                                </a>
                                <div class="order-edited">
                                    <?if(count($props['add']) > 0){?>
                                    <span class="order-edited__item order-edited__item_offset">
                                        <i class="order-edited__ico order-edited__ico_plus"></i>
                                        <span class="order-edited__title">
                                            <?=implode(',', $props['add'])?>
                                        </span>
                                    </span>
                                    <?}?>
                                    <?if(count($props['remove']) > 0){?>
                                    <span class="order-edited__item order-edited__item_offset">
                                        <i class="order-edited__ico order-edited__ico_minus"></i>
                                        <span class="order-edited__title">
                                            <?=implode(',', $props['remove'])?>
                                        </span>
                                    </span>
                                    <?}?>
                                </div>
                                <p class="grey s-small mt-3">Состав: <?=$basket_item['PREVIEW_TEXT']?></p>
                            </div>
                        </div>
                        <div class="order-item__col">
                            <div class=" order-item__details">
                                <div class="order-item__buy">
                                    <a href="#!" class="btn _w-result__btn btn_default btn_green"
                                    onclick="add_to_basket(event)"
                                    data-add_ing="<?=implode(',', array_keys($props['add']))?>",
                                    data-remove_ing="<?=implode(',', array_keys($props['remove']))?>",
                                    data-item_id="<?=$basket_item['PRODUCT_ID']?>"
                                    >В&nbsp;КОРЗИНУ</a>
                                    <span class="order-item__price">
                                        <?=$basket_item['PRICE']?> Р
                                    </span>
                                </div>
                                <div class="order-item__settings">
                                    <span class="product-lite__weight s-large"><?=$basket_item['PROPERTY_SHOT_ABOUT_VALUE']?></span>
                                    <div class="counter">
                                        <button class="counter__btn counter__btn_minus"></button>
                                        <input type="text" class="counter__input" value="1">
                                        <button class="counter__btn counter__btn_plus "></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?}?>
                </div>

            </div>
            <div class="delivery-w">
                <div class="delivery-w__detail">
                    <h3 class="h3 delivery-w__title">
                        Доставка
                    </h3>
                    <span class="delivery-w__type">
                        <?=$arResult['DELIVERY']['NAME']?>
                    </span>
                    <span class="delivery-w__addr gray">
                        <?=$order_props_code_to_value_map['ADDRESS']?>
                    </span>
                </div>
                <div class="delivery-w__detail">
                    <span class="delivery-w__type">
                        10.09.2020 13:29
                    </span>
                </div>
            </div>
        </div>
        <div class="lk-aside">
            <aside class="aside">
                <div class="w-total">
                    <h4 class="h4 w-total__title">
                        Стоимость
                    </h4>
                    <hr class="w-total__hr">
                    <div class="w-total__content">
                        <div class="key-val w-total__line">
                            <span class="key w-total__key">
                                Товары
                            </span>
                            <span class="val w-total__val green">
                                <?=$arResult['PRICE'] + $arResult['DISCOUNT_VALUE']?> Р
                            </span>
                        </div>
                        <div class="key-val w-total__line">
                            <span class="key w-total__key">
                                С учетом скидки
                            </span>
                            <span class="val w-total__val green">
                            <?=$arResult['PRICE']?> Р
                            </span>
                        </div>
                        <div class="key-val w-total__line">
                            <span class="key w-total__key">
                                Доставка
                            </span>
                            <span class="val w-total__val green">
                            <?=$arResult['PRICE_DELIVERY']?> Р
                            </span>
                        </div>
                    </div>
                    <div class="w-total__balls">
                        <div class="key-val w-total__line">
                            <span class="key middle w-total__key">
                                Начислится баллов
                            </span>
                            <span class="val w-total__val">
                                20
                            </span>
                        </div>
                    </div>
                    <div class="w-result">
                        <span class="w-result__title">
                            Итог
                        </span>
                        <span class="val w-total__val w-total__val green">
                            <?=$arResult['PRICE'] + $arResult['PRICE_DELIVERY']?> Р
                        </span>
                    </div>
                    <a href="/personal/copy_order/?ID=<?=$arResult['ID']?>" class="btn w-result__btn btn_large btn_green">
                        Повторить заказ
                    </a>
                </div>
            </aside>
        </div>
    </div>
</div>
<script>
function add_to_basket(event){
    let parent = $(event.currentTarget).closest('.order-item__row');
    let count = parent.find('input.counter__input').val();
    if(!(count > 0)){
        count = 1;
    }
    let add_ing_ids = $(event.currentTarget).attr('data-add_ing').split(',');
    let add_ing = {};
    for(id of add_ing_ids){
        add_ing[id] = {
            set: 1
        }
    }
    let remove_ing_ids = $(event.currentTarget).attr('data-remove_ing').split(',');
    let remove_ing = {};
    for(id of remove_ing_ids){
        remove_ing[id] = {
            set: 1
        }
    }
    let item_id = $(event.currentTarget).attr('data-item_id');
    let data = {
        action: 'add_item',
        item_id,
        add_ing,
        remove_ing,
        count
    };
    //console.log(data);
    do_basket_action(event, data);


}

</script>