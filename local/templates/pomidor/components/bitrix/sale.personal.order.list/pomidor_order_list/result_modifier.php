<?
$filter = ["USER_ID" => $USER->GetID(), 'DEBIT' => 'Y', '!ORDER_ID' => false];
$select = ['ORDER_ID', 'AMOUNT'];

$res = CSaleUserTransact::GetList([], $filter, false, false, $select);

$order_id_to_bonus_value_map = [];

while($bonus = $res->getNext()){
    $order_id_to_bonus_value_map[$bonus['ORDER_ID']] = (integer)$bonus['AMOUNT'];
}

$arResult['order_id_to_bonus_value_map'] = $order_id_to_bonus_value_map;

$res = CSaleStatus::GetList();

$status_code_to_desc_map = [];

while($status = $res->getNext()){
    $status_code_to_desc_map[$status['ID']] = $status['NAME'];
}

$arResult['status_code_to_desc_map'] = $status_code_to_desc_map;