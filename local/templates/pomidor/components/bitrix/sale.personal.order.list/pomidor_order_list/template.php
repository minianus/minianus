<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/bootstrap_v4/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/bootstrap_v4/style.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

?>
<div class="window__header window__header_sb">
    <h3 class="s-large window__title">
        История заказов </h3>
</div>
<hr class="w-total__hr">

<?
if (!empty($arResult['ERRORS']['FATAL']))
{
    foreach($arResult['ERRORS']['FATAL'] as $code => $error)
    {
        if ($code !== $component::E_NOT_AUTHORIZED)
            ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
    {
        ?>
<div class="row">
    <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
        <div class="alert alert-danger"><?=$arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]?></div>
    </div>
    <? $authListGetParams = array(); ?>
    <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
        <?$APPLICATION->AuthForm('', false, false, 'N', false);?>
    </div>
</div>
<?
    }

}else{ if (!count($arResult['ORDERS'])){?>
<div class="row mb-3">
    <div class="col">
        Пока что здесь пусто но вы можете <a class="header__link" href="/menu-devilery/">начать вкусную жизнь тут</a>!
    </div>
</div>
<?
}

$paymentChangeData = array();
?>
<div class="window__content window__content_offset">
    <div class="cu-table history-table">
        <?
foreach ($arResult['ORDERS'] as $key => $order){?>

        <?
        $showDelimeter = false;
        $status_result = get_order_status_for_lk($order['ORDER']['STATUS_ID'], true);
        ?>
        <div class="cu-table__tr">
            <div class="cu-table__td">
                <span class="cu-table__title">

                    <a
                        href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>"><?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?></a>
                </span>
            </div>

            <div class="cu-table__td">
                <span class="status status_gray">
                    <?=$status_result['STATUS']?>

                </span>
            </div>
            <div class="cu-table__td">
                <span class="cu-table__title">
                    <?=Loc::getMessage('SPOL_TPL_FROM_DATE')?>
                    <?=$order['ORDER']['DATE_INSERT_FORMATED']?>
                </span>
            </div>
            <div class="cu-table__td">
                <span class="cu-table__title">
                    <?=$order['ORDER']['FORMATED_PRICE']?>
                </span>
            </div>
            <div class="cu-table__td">
                <span class="cu-table__title">
                    <?$bonus_value = $arResult['order_id_to_bonus_value_map'][$order['ORDER']['ID']]?>
                    + <?=($bonus_value)?$bonus_value:0?> баллов
                </span>
            </div>
            <div class="cu-table__td">
                <a class="g-font-size-15  cu-table__link"
                    href="/personal/copy_order/?ID=<?=$order['ORDER']['ID']?>"><?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?></a>
            </div>
            <div class="cu-table__td">
                <?if($status_result['CAN_CANCELED']){?>
                <a class="g-font-size-15 sale-order-list-cancel-link cu-table__link"
                    href="/personal/cancel_order/?ID=<?=$order['ORDER']['ID']?>"><?=Loc::getMessage('SPOL_TPL_CANCEL_ORDER')?></a>
                <?}?>
            </div>
        </div>






        <?
        }?>
    </div>
</div>
<?
        }
        ?>