<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

ShowMessage($arParams["~AUTH_RESULT"]);

?>

<form action="#" class="form error form-middle c" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

    <?
    if ($arResult["BACKURL"] <> '')
    {
        ?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?
    }
    ?>
    <input type="hidden" name="AUTH_FORM" value="Y">
    <input type="hidden" name="TYPE" value="SEND_PWD">
    <h2 class="h2 form-middle__title">Восстановить пароль</h2>
    <div class="form__item form__item_s-offset ">

        <div>
            <input required class="input input_default input_yellow re-phone-mask" type="text" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" placeholder="Номер телефона, Логин или Email" />
            <input type="hidden" name="USER_EMAIL" />
        </div>
        <div class="alert alert-success tablebodytext">Контрольная строка, а также ваши регистрационные данные были высланы на email. Пожалуйста, дождитесь письма, так как контрольная строка изменяется при каждом запросе.<br>
        </div>
    </div>



    <div class="form__item form__item_s-offset">
        <input type="submit" name="send_account_info" class="btn btn_large btn_default btn_yellow" value="<?=GetMessage("AUTH_SEND")?>" />

    </div>

    <div class="form-nav">
        <a href="/personal/" class="form-nav__link">
            Вход
        </a>
        <a href="/registratsiya.php" class="form-nav__link">
            Регистрация
        </a>
    </div>

    <?if($arResult["PHONE_REGISTRATION"]):?>

        <div style="margin-top: 16px">
            <div><b><?=GetMessage("sys_forgot_pass_phone")?></b></div>
            <div><input type="text" name="USER_PHONE_NUMBER" value="<?=$arResult["USER_PHONE_NUMBER"]?>" /></div>
            <div><?echo GetMessage("sys_forgot_pass_note_phone")?></div>
        </div>
    <?endif;?>

</form>
<!--<div class="smena_pass tablebodytext">-->
<!--   --><?//$APPLICATION->IncludeComponent("bitrix:main.auth.changepasswd", "pomidor_smena", Array(
//        "AUTH_AUTH_URL" => "",	// Страница для авторизации
//        "AUTH_REGISTER_URL" => "",	// Страница для регистрации
//    ),
//        false
//    );?>
<!--</div>-->




<script type="text/javascript">
document.bform.onsubmit = function(e){
    e.preventDefault();

    document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;

$(".alert-success").show();
$(".smena_pass").show();
};
//document.bform.USER_LOGIN.focus();
</script>
