
var currentLocation;
var changeLocation = function () {
    var activeTab=$('.tabs__content_list.active');
    currentLocation = this.getNodeByLocationId(this.getValue());
    saveToSess('LOCATION', JSON.stringify(currentLocation));
    $('input[name=LOCATION]').val(currentLocation.CODE);
    $('.active').removeClass('active');
    $('.preloader').addClass("active");
    $.ajax({
        method: 'POST',
        url: '/order/ajax.php',
        data: {
            action: 'getAvaliableDelivery',
            location_id: currentLocation,
            sessid: BX.bitrix_sessid()
        },
        success: function (json) {
            console.log(json);
           $(activeTab).click();
            $answer = $.parseJSON(json);
            
            $('.delivery_element').addClass('non_active');
            $('input[name="DELIVERY_ID"]').addClass('non_active_check');
            $.each($answer, function (index, value) {
               
                //Если Boxberry не присылает код кнопки «Выбрать местоположение», то не показываем доставку
                //Т.к. Boxberrry не знает некоторых городов (Например, Шатура)
                //В Битриксе ограничения на Боксберри по Местоположению не стоят
                if (value.ID == 28 && typeof value.PERIOD_DESCRIPTION == 'undefined') {
                    return;
                }
                $('.delivery_element[data-delivery-id=' + value.ID + ']').removeClass('non_active');
                $('.delivery_element[data-delivery-id=' + value.ID + ']').removeClass('non_active').find('input[name="DELIVERY_ID"]').removeClass("non_active_check");
                $('.delivery_element[data-delivery-id=' + value.ID + '] .delivery_element_period_description').html(value.PERIOD_DESCRIPTION);
                $('.delivery_element[data-delivery-id=' + value.ID + '] .price_del_block').html(value.PRICE);
                var curDeliv = $('#current-delivery').val();
                if (curDeliv == value.ID)
                    $('.delivery_element[data-delivery-id=' + value.ID + ']').click();
            });

            if($('input[name="DELIVERY_ID"]:checked').closest(".delivery_element").hasClass("non_active")) {
                $('input[name="DELIVERY_ID"]').not(".non_active_check").first().trigger("click").trigger("change");
            }
            
			if(typeof(IPOLSDEK_componentFixer) !== 'undefined' && typeof(IPOLSDEK_componentFixer.addData) != 'undefined'){
				IPOLSDEK_componentFixer.newLocation();
            }
            
            $('.preloader').removeClass("active");
        }
    })
};



function disableCoupon() {
    var couponField = $('input[name=coupon_name]'),
        current_coupon = couponField.val(),
        couponBlock = $('.coupon_block');

    $.ajax({
        method: 'POST',
        url: '/order/ajax.php',
        data: {
            action: 'disableCoupon',
            coupon: current_coupon,
            sessid: BX.bitrix_sessid()
        },
        beforeSend: function () {
            couponBlock.addClass('load');
        },
        success: function (json) {
           
            couponField.val('');
            $('input[name=promo_code]').val('');
            $('input[name=coupon_name]').prop('disabled', false);
            $('.cupon_status').addClass('ok').text('Активировать купон');
            $('.reset_coupon_link').fadeOut();
            $('.couponCheckButton a').fadeIn();
            couponBlock.removeClass('load');
            totalPrice();
        }
    })
}


function checkCoupon() {
    var couponField = $('input[name=coupon_name]'),
        current_coupon = couponField.val(),
        couponBlock = $('.coupon_block');

    if (!(current_coupon.length)) {
        return false;
    }

    $('.cupon_status').removeClass('error');

    $.ajax({
        method: 'POST',
        url: '/order/ajax.php',
        data: {
            action: 'checkCoupon',
            coupon: current_coupon,
            sessid: BX.bitrix_sessid()
        },
        beforeSend: function () {
            couponBlock.addClass('load');
        },
        success: function (json) {
            console.log(json);
            var $answer = $.parseJSON(json);
            if ($answer.result) {
                if ($answer.result === 2) {
                    $('input[name=coupon_name]').prop('disabled', true);
                    $('.couponCheckButton a').fadeOut();
                    $('.cupon_status').addClass('ok').text('Купон применён');
                    $('input[name=promo_code]').val($answer.coupon);
                    $('.reset_coupon_link').fadeIn();
                    totalPrice();//Выводим итоговую сумму
                } else {
                    $('.cupon_status').addClass('error').text('Купон не действителен');
                }
            }
            couponBlock.removeClass('load');
        },
        done: function () {
            //couponBlock.removeClass('load');
        }
    })

}



/**
 * 
 * ОФОРМЛЕНИЕ ЗАКАЗА
 */
$(document).on("change","input[name='DELIVERY_ID']",function(){
    if($(this).val()==40){
        $('#show_map_link').show();
        $('#enterAdress').hide();
        
    }
    else if($(this).val()==7||$(this).val()==39){
        $('#show_map_link').hide();
        $('#enterAdress').show();
        $('#SDEK_ADDRESS').val('');
    }
    else{
        $('#enterAdress').hide();
        $('#show_map_link').hide();
        $('#SDEK_ADDRESS').val('');
    }      
    //saveToSess('DELIVERY_ID',$(this).val());
    $('#data_delivery_id').val($(this).val());
    totalPrice();
})
$(document).on("change","input[name='data_pay_id']",function(){
    saveToSess('data_pay_id',$(this).val());
})
$(document).on("change",".save2Sess",function(){
    saveToSess($(this).attr("name"),$(this).val());
})

function totalPrice() {
    $('.preloader').addClass("active");
    $.ajax({
        method: 'POST',
        url: '/order//ajax.php',
        data: {
            data_delivery_id: $('input[name=DELIVERY_ID]:checked').val(),
            data_pay_id: $('input[name=data_pay_id]:checked').val(),
            promo_code: $('input[name=coupon_name]').val(),
            action: 'getTotalPrice',
            location_id: currentLocation,
            minusBalls:$('input[name=minusBalls]').val(),
            sessid: BX.bitrix_sessid()
        },
        success: function (json) {          
            $answer = $.parseJSON(json);
            console.log($answer);
            $('.total_price').html($answer.ORDER_TOTAL_PRICE);
            $('.ORDER_PRICE').html($answer.ORDER_PRICE);
            $('.PRICE_WITHOUT_DISCOUNT_VALUE').html($answer.PRICE_WITHOUT_DISCOUNT_VALUE);
            $('.DELIVERY_PRICE').html($answer.DELIVERY_PRICE);
            $('.preloader').removeClass("active");
        }
    })
}
$(document).on("change","input[name='minusBalls']",function(){
    if(parseInt($(this).val())>parseInt($(this).data("max"))) {
        $(this).val(parseInt($(this).data("max")));
    }
    totalPrice();
});

function saveToSess(field, value) {
    $.ajax({
        url: '/local/ajax/order2sess.php',
        data: {
            field: field,
            value: value
        }
    })
}