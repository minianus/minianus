<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use     
	Bitrix\Sale\PaySystem,
	Bitrix\Sale;

$context = Bitrix\Main\Application::getInstance()->getContext();	
if(class_exists('sdekExtension')){ // mod 4 fixing component [ipolh]		
	sdekExtension::loadComponentFix();
	
}
?> 

	<form class="row form__column delivery_calc_form"  
		action="<?= $APPLICATION->GetCurPage(); ?>"
		method="POST"
		name="ordForm"
	>
	<?= bitrix_sessid_post() ?>
	

	
			<div class="personal__content personal-bg" style="background-color: #f5f5f6;">
		
												<?
												if(!$arResult['DATA']['LOCATION']) {
													$locationStart="0000386590";
												}
												else{
													$locationStart=$arResult['DATA']['LOCATION']->CODE;
												}
												?>

				<div class="project-form">
					<div class="row">
						<div class="col col--lg-8 input_row">
							<label class="label label_space">
								Ваш город:
							</label>
							<? $APPLICATION->IncludeComponent(
                            "bitrix:sale.location.selector.search",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ID" => "",
                                "CODE" => $locationStart,
                                "INPUT_NAME" => "LOCATION",
                                "PROVIDE_LINK_BY" => "id",
                                "JSCONTROL_GLOBAL_ID" => "",
                                "JS_CALLBACK" => "changeLocation()",
                                "FILTER_BY_SITE" => "Y",
                                "SHOW_DEFAULT_LOCATIONS" => "N",
                                "CACHE_TYPE" => "N",
                                "CACHE_TIME" => "36000000",
                                "FILTER_SITE_ID" => "s1",
                                "INITIALIZE_BY_GLOBAL_EVENT" => "",
                                "SUPPRESS_ERRORS" => "N"
                            )
                        ); ?>
							
						</div>
					</div>
					<input type="hidden" name="data_delivery_id" id="data_delivery_id">
					<div class="project-radio__content">
						<?$i=0;
						foreach($arResult['DELIVERY_ARRAY'] as $delivery){ if($delivery['ID']!=39&&$delivery['ID']!=40) continue;?>
						<div class="project-radio__list delivery_element" data-delivery-id="<?= $delivery['ID']; ?>">
							<div class="project-radio__item " >
								<input type="radio"  name="DELIVERY_ID" id="radio<?= $delivery['ID']; ?>"  value="<?= $delivery['ID']; ?>" <?if($i==0){?>checked<?}?>>
								<label for="radio<?= $delivery['ID']; ?>">
									<div class="project-bank">
										<?=$delivery['NAME']?>
									</div>
									<div class="project-bank_text ">
										<?=$delivery['DESCRIPTION']?>	
										<div class="delivery_element_period_description">
										</div>								
									</div>
									
								</label>
							</div>
							<div class="project-radio__price"><span class='price_del_block'><?=$delivery['PRICE']?></span><span class="rub">i</span></div>
						</div>
					
						<?$i++;}?>				
					</div>
				</div>
			</div>		

	
	
</form>