<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use     
	Bitrix\Sale\PaySystem,
	Bitrix\Sale;

$context = Bitrix\Main\Application::getInstance()->getContext();	
if(class_exists('sdekExtension')){ // mod 4 fixing component [ipolh]		
	sdekExtension::loadComponentFix();
	
}
$this->addExternalJS(SITE_TEMPLATE_PATH."/js/select2.full.min.js");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/select2.min.css');
?> 
<? if ($_REQUEST['ORDER_ID']) {?>

	<main class="main">
<h1>Ваш заказ №<?=$_REQUEST['ORDER_ID']?> успешно оформлен</h1>
<div>
    Менеджер свяжется с вами для подтверждения заказа.    
</div>
<br><br>
<div class="payment_wrapper">
<?if($arResult['onePayment']){?>
<?

	
	$service = Sale\PaySystem\Manager::getObjectById($arResult['onePayment']->getPaymentSystemId());
	$context = \Bitrix\Main\Application::getInstance()->getContext();
	$service->initiatePay($arResult['onePayment'], $context->getRequest());

	?>
	<?}?>
	</div>
	</main>

<?}else{?>
<?if($arResult['ERRORS']){
	foreach($arResult['ERRORS'] as $error) {
		
		if ($error=="Пользователь с таким Email уже зарегистрирован") {
			?>
		<div class="flex_error"><?ShowError($error);?>
		<a href="/auth/" class="iconed btn-green ml15">
			<i class="btn-more__ico">
				<svg class="iconed__ico btn-more__r-icon">
					<use xlink:href="/local/templates/main/images/icons.svg#r-icon">
					</use>
				</svg>
			</i>
			<span class="btn-more__title title title_secondary title_bold">
				Авторизация
			</span>
		</a>
		</div>
		<?}
		else{
			ShowError($error);
		}
	}
}?>
	<form class="row form__column"  
		action="<?= $APPLICATION->GetCurPage(); ?>"
		method="POST"
		name="ordForm"
	>
	<?= bitrix_sessid_post() ?>
	
	<div class="col col--xl-8 col--lg-12">
		<div class="personal__content personal-bg">
			<h2 class="personal__content_title border-bottom">У вас есть промо-код?</h2>
			<div class="password__res form__ld form__addr">
				<div class="form__row-content">
					<div class="row">
						<div class="col col--lg-6">
							<div class="project-delivery__box_list">
								<label class="label label_default">
									Промокод:
								</label>
								<div class="cupon-widget couponField">
								<? if (empty(reset($arResult['COUPONS']))){ 
									
									?>
									<input class="input input_default input_b-offset" name="coupon_name" type="text" placeholder="Введите промокод">
									<div class="cupon_status">Введите промокод</div>
									<input type="hidden" name="promo_code" value="">
								<?}else{
									$coupon=reset($arResult['COUPONS']);								
									?>
									<input class="input input_default input_b-offset" name="coupon_name" type="text" placeholder="Введите промокод" value="<?= $coupon['DATA']['COUPON']; ?>"> 
									<div class="cupon_status green ok">Ваш промо-код применен к заказу</div>
									<input type="hidden" name="promo_code" value="<?= $coupon['DATA']['COUPON'] ?>">
								<?}?>
								<div class="reset_coupon_link" title="Отменить промокод"
									<? if (empty(reset($arResult['COUPONS']))):?>
										style="display: none;"
									<? endif; ?>
									onclick="disableCoupon();return false;"
								>Отменить купон
								</div>
								</div>
								
							</div>
						</div>
						<div class="col col--lg-6 project_center">
							<div class="project-warning">
								<img class="warning-img" src="<?=SITE_TEMPLATE_PATH?>/images/warning.png" alt="warning">
								<p>Вы можете использовать только один промокод в одном заказе</p>
							</div>
						</div>
					</div>
				</div>
				<div class="form__button form__button_res couponCheckButton">
					<a href="#" class="iconed btn-green" onclick="checkCoupon();return false;">
						<i class="btn-more__ico">
							<svg class="iconed__ico btn-more__r-icon">
								<use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#r-icon">
								</use>
							</svg>
						</i>
						<span class="btn-more__title title title_secondary title_bold">
							Применить промокод
						</span>
					</a>
				</div>
			</div>
		</div>
		<div action="#">
			<div class="personal__content personal-bg">
				<h2 class="personal__content_title border-bottom">Личные данные</h2>
				<div class="password__res">
					<div class="row">
						<? 
                            if (!empty($arResult['PROPERTY_COLLECTION']['properties'])){ ?>
                                <? foreach ($arResult['PROPERTY_COLLECTION']['properties'] as $key => $item){ ?>
                                    <? if ($item['UTIL'] == 'Y') {
                                        continue;
                                    }; ?>
								<div class="col col--lg-4">
									<div class="project-delivery__box_list">
										<label class="label label_default">
											<?= $item['NAME'] ?>
										</label>
										<input 
											class="input input_default input_b-offset <?if($item['CODE']=="PHONE"){?>input-phone<?}?> save2Sess" 
											type="text" 
											<?if($item['CODE']=="PHONE"){?>required<?}?>
											placeholder="Введите <?= strtolower($item['NAME']) ?>"
											name="<?= $item['CODE'] ?>" id="<?= $item['CODE'] ?>"
											<?if($item['CODE']=="EMAIL"){?>value="<?=$arResult['DATA']['EMAIL']?>"<?}else if($item['CODE']=="FIO"){?>value="<?=$arResult['DATA']['FIO']?$arResult['DATA']['FIO']:$arResult['DATA']['NAME']?>"<?}
											else if($item['CODE']=="PHONE"){?>value="<?=$arResult['DATA']['PHONE']?>"<?}
											?>
											<? if ($item['REQUIRED'] == 'Y'): ?>
												required
											<? endif; ?>
										>
									</div>
								</div>
							<?}?>
						<?}?>
					</div>
                    <div class="row">
                       <div class="col col--lg-12">
                           <div class="enter_email">
                               Пожалуйста, укажите электронную почту. Мы автоматически создадим личный кабинет, в котором будут начислены баллы за покупку. Данные для входа будут отправлены на E-mail
                           </div>
                        
                        </div>
                    </div>
				</div>
			</div>
			<div class="personal__content personal-bg">
				<div class="project__title_list border-bottom">
					<h2 class="personal__content_title ">Доставка</h2>
					<div class="project__link">
						<a href="/delivery/" class="bonus-content__link">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/info.svg" class="sidebar-svg" alt="">
							Подробнее о способах доставки
						</a>
					</div>
				</div>
												<?
												if(!$arResult['DATA']['LOCATION']) {
													$locationStart="0000386590";
												}
												else{
													$locationStart=$arResult['DATA']['LOCATION']->CODE;
												} 
												
												?>

				<div class="project-form">
					<div class="row">
						<div class="col col--lg-8 input_row">
							<label class="label label_space">
								Ваш город:
							</label>
							<? $APPLICATION->IncludeComponent(
                            "bitrix:sale.location.selector.search",
                            "",
                            Array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "ID" => "",
                                "CODE" => $locationStart,
                                "INPUT_NAME" => "LOCATION",
                                "PROVIDE_LINK_BY" => "id",
                                "JSCONTROL_GLOBAL_ID" => "",
                                "JS_CALLBACK" => "changeLocation()",
                                "FILTER_BY_SITE" => "Y",
                                "SHOW_DEFAULT_LOCATIONS" => "N",
                                "CACHE_TYPE" => "N",
                                "CACHE_TIME" => "36000000",
                                "FILTER_SITE_ID" => "s1",
                                "INITIALIZE_BY_GLOBAL_EVENT" => "",
                                "SUPPRESS_ERRORS" => "N"
                            )
                        ); ?>
							
						</div>
					</div>
					<input type="hidden" name="data_delivery_id" id="data_delivery_id">
					<div class="project-radio__content">
						<?$i=0;
						foreach($arResult['DELIVERY_ARRAY'] as $delivery){ if($delivery['ID']==38) continue;?>
						<div class="project-radio__list delivery_element" data-delivery-id="<?= $delivery['ID']; ?>">
							<div class="project-radio__item " >
								<input type="radio"  name="DELIVERY_ID" id="radio<?= $delivery['ID']; ?>"  value="<?= $delivery['ID']; ?>" <?if($i==0){?>checked<?}?>>
								<label for="radio<?= $delivery['ID']; ?>">
									<div class="project-bank">
										<?=$delivery['NAME']?>
									</div>
									<div class="project-bank_text ">
										<?=$delivery['DESCRIPTION']?>	
										<div class="delivery_element_period_description">
										</div>								
									</div>
									
								</label>
							</div>
							<div class="project-radio__price"><span class='price_del_block'><?=$delivery['PRICE']?></span><span class="rub">i</span></div>
						</div>
						<?if($delivery['ID']==40){?>
						<div class="project-button_select" id="show_map_link" style="display:none"></div>						
							<input type="text" name="SDEK_ADDRESS" id="SDEK_ADDRESS" style="display:none">					
						<?}?>
						<?$i++;}?>
						<div class="project-delivery" id="enterAdress" style="display:none">
							<div class="project-delivery_list">
							<?
                            if (!empty($arResult['PROPERTY_COLLECTION']['properties'])){ ?>
                                <? foreach ($arResult['PROPERTY_COLLECTION']['properties'] as $key => $item){ ?>
                                    <? if ($item['UTIL'] != 'Y') {
                                        continue;
									};
									if ($item['CODE'] == 'LOCATION'||$item['CODE'] == 'SDEK_ADDRESS'||$item['CODE'] == 'CURIER') {
                                        continue;
                                    };
									?>
								
								<div class="project-delivery__item <?if($item['CODE']=="STREET"){?>project-delivery__item_large<?}?>">
									<label class="label label_default"><?=$item['NAME']?></label>
									<?if($item['CODE']=="TIME_DELIVERY"){?>
										<div class="select custom-select select_default">
											<select name="<?=$item['CODE']?>" id="<?=$item['CODE']?>">
												<?foreach($item['OPTIONS'] as $ids=> $opt){?>
													<option value="<?=$ids?>"><?=$opt?></option>
												<?}?>
											</select>
										</div>
									<?}else{?>
									<input 
										class="input input_default input_b-offset save2Sess" 
										type="text" 
										value="<?=$arResult['DATA'][$item['CODE']]?>"
										placeholder="<?=$item['NAME']?>"
										name="<?= $item['CODE'] ?>" id="<?= $item['CODE'] ?>"
											<? if ($item['REQUIRED'] == 'Y'): ?>
												required
											<? endif; ?>
									>
									<?}?>
								</div>
							<?}
							}
							?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="personal__content personal-bg">
				<div class="project__title_list border-bottom">
					<h2 class="personal__content_title ">Оплата</h2>
					<div class="project__link">
						<a href="/payment/" class="bonus-content__link">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/info.svg" class="sidebar-svg" alt="">
							Подробнее о способах оплаты
						</a>
					</div>
				</div>


				<div class="project-form">

					<div class="project-radio__content">
						
					<?$i=0;
					foreach ($arResult['PAY_SYSTEMS'] as $pay){
						if($pay['ID']==1&&!$USER->IsAuthorized()) {
							continue;
						}
						if($pay['ID']==4) {
							continue;
						}
						?>
						<div class="project-radio__list <?if($pay['ID']==1){?>project-radio__column<?}?>">
							<input type="radio" id="pay<?=$pay['ID']?>" name="data_pay_id" value="<?=$pay['ID']?>" <?if($i==0){?>checked<?}?>>
							<label for="pay<?=$pay['ID']?>">
								<div class="project-bank">
									<?=$pay['NAME']?>
								</div>
								<?if($pay['ID']==1){?>
								<div class="project-bank_text">
									Ваш баланс: <?=intval($arResult['DEPOZIT']['CURRENT_BUDGET'])?> руб.
								</div>
								<?}?>
							</label>
							<?if($pay['ID']==1){?>
								<?if($arResult['ORDER_TOTAL_PRICE']>$arResult['DEPOZIT']['CURRENT_BUDGET']){?>
								<div class="project-summ__buy">						
									<div class="project-warning">
										<img class="warning-img" src="<?=SITE_TEMPLATE_PATH?>/images/warning.png" alt="warning">
										<p>Недостаточно средств для оплаты. Вы можете пополнить баланс в разделе <a href="/depozity/">Депозиты</a></p>
									</div>
								</div>
								<?}?>
							<?}?>
						</div>
						
					<?$i++;}?>
					</div>
				</div>
			</div>
			<?global $USER;
			if($USER->IsAuthorized()){?>
				<div class="personal__content personal-bg">
					<div class="project__title_list border-bottom">
						<h2 class="personal__content_title ">Списание баллов</h2>
					</div>
					<div class="project__subtitle-form">
						Оплата баллами производится в виде скидки на заказ в размере до 30% от стоимости товара
					</div>
					<div class="project__init">
						<div class="project__summ">Ваш баланс:</div>
						<div class="project__numbers"><?=plural_form($arResult['BONUSES'],array("балл","балла","баллов"))?></div>
					</div>
					<div class="project__init">
						<div class="project__summ">Возможность списать:</div>
						<div class="project__numbers"><?=plural_form($arResult['CAN_BONUSES'],array("балл","балла","баллов"))?></div>
					</div>
					
					<div class="balls-widget">
						<label class="checkbox project-checkbox checkbox-js project-checkbox-balls">
							Списать баллы
							<input type="checkbox" class="checkbox__input">
							<span class="checkbox__label checkbox_border title_black">
							</span>
						</label>
						<div class="balls-widget__controls ">
							<input type="text" class="input input_default only-numb" name="minusBalls" data-max='<?=$arResult['BONUSES']?>'>
						</div>
					</div>				
				</div>
			<?}?>

			<div class="project__text">			
				После оплаты вам начисляется <?=plural_form($arResult['NEW_BALLS'],array("балл","балла","баллов"))?>
			</div>

			<div class="prises__text init_top">
				<div class="prises__text_name">
					Общая стоимость
				</div>
				<div class="prises__text_price total_price">
					<?=CCurrencyLang::CurrencyFormat($arResult['ORDER_TOTAL_PRICE'],"RUB")?>
				</div>
			</div>

			<div>
			<label class="checkbox checkbox_take-res checkbox-js">
				Подтверждая заказ, вы соглашаетесь
				с <a class="project-under_link" target="_blank" href="/politic/">Политикой в отношении обработки
					персональных данных</a> и принимаете условия <a class="project-under_link" target="_blank" href="/oferta/">Оферты.</a>
				<input type="checkbox" class="checkbox__input" id="order_checkbox_agree">
				<div class="checkbox__label checkbox_take checkbox_border title_black">
				</div>
			</label>
							
			</div>

			<div class="project-btn">
				 <button type="submit" class="iconed btn-green" name="final" value="yes" id="final_order">
					<i class="btn-more__ico">
						<svg class="iconed__ico btn-more__r-icon">
							<use xlink:href="<?=SITE_TEMPLATE_PATH?>/images/icons.svg#r-icon">
							</use>
						</svg>
					</i>
					<span class="btn-more__title title title_secondary title_bold">
						Заказать
					</span>
				</button> 
						<div id="order_check"></div>
			</div>

		</div>
	</div>
	<div class="col col--xl-4 col--lg-12">

		<div class="basket-project">

			<div class="basket-content_column">
				<div class="basket-content__title border-bottom">
					Корзина
				</div>

				<div class="basket-content__block">
				<?
				$allGoods=$arResult['allGoods'];
				foreach ($arResult['ORDER']->getBasket() as $item) {
					$good=$allGoods[$item->getProductId()];
					if ($good['PREVIEW_PICTURE']) {
						$src=resize($good['PREVIEW_PICTURE'],132,90);
					}
					else {
						$src=SITE_TEMPLATE_PATH."/images/nophoto132.jpg";
					}

					$curPrice=$prices[$item->getId()]['PRICE']*$item->getQuantity();
				?>
					<div class="basket-content__box border-bottom">
						<div class="basket-content_left">
							<div class="basket-content__box_image">
								<img src="<?=$src?>" alt="<?=$item->getField('NAME');?>">
							</div>
							<div class="basket-content__box_text">
								<div class="basket-content_title">
									<a href="<?=$good['DETAIL_PAGE_URL']?>">
										<?=$item->getField('NAME');?>
									</a>
								</div>
								<div class="basket-content_price"><?=CCurrencyLang::CurrencyFormat($item->getFinalPrice(),"RUB")?></div>
								<div class="basket-content_summ"><?if($good['CATALOG_MEASURE']==5){?>
										<span> <?=$item->getQuantity()?> </span> <span class="numbers-weight">ШТ</span>
										<?}
										else if($good['CATALOG_MEASURE']==4){?>
										<span>~ <?=$item->getQuantity()?> </span> <span class="numbers-weight">КГ</span>
										<?}else{?>
										<span>~ <?=$item->getQuantity()?> </span> <span class="numbers-weight">Г</span>
										<?}?></div>
							</div>
						</div>

					</div>
				<?}?>
				</div>
				<div class="prises__column border-bottom">
					<div class="prises__column_list">
						<div class="prises__column_name">Товары</div>
						<div class="prises__column_nmbers PRICE_WITHOUT_DISCOUNT_VALUE"><?=CCurrencyLang::CurrencyFormat($arResult['PRICE_WITHOUT_DISCOUNT_VALUE'],"RUB")?></div>
					</div>
					<div class="prises__column_list">
						<div class="prises__column_name">Доставка</div>
						<div class="prises__column_nmbers DELIVERY_PRICE"><?=CCurrencyLang::CurrencyFormat($arResult['DELIVERY_PRICE'],"RUB")?></div>
					</div>
					<div class="prises__column_list">
						<div class="prises__column_name">С учетом скидки</div>
						<div class="prises__column_nmbers ORDER_PRICE"><?=CCurrencyLang::CurrencyFormat($arResult['ORDER_PRICE'],"RUB")?></div>
					</div>
					<div class="prises__column_list">
						<div class="prises__column_name">Будет начислено баллов</div>
						<div class="prises__column_nmbers"><?=$arResult['NEW_BALLS']?></div>
					</div>
					<div class="prises__column_list">
						<div class="prises__column_name">Списано баллов</div>
						<div class="prises__column_nmbers">0 ₽</div>
					</div>
				</div>
				<div class="prises__text">
					<div class="prises__text_name">
						Общая стоимость
					</div>
					<div class="prises__text_price total_price">
					<?=CCurrencyLang::CurrencyFormat($arResult['ORDER_TOTAL_PRICE'],"RUB")?>
					</div>
				</div>

			</div>

		</div>
		<div class="basket-warning">
			<div class="basket__img">
				<div class="basket__img">
					<img class="svg-white" src="<?=SITE_TEMPLATE_PATH?>/images/delivery-car.svg" alt="car">
				</div>
			</div>
			<div class="basket__text">
			<?
			$freeDelivery=\Bitrix\Main\Config\Option::get("grain.customsettings","free_del");
			?>
			Бесплатная доставка от <?=$freeDelivery?> р.
			</div>
		</div>

	</div>
</form>
<?}?>