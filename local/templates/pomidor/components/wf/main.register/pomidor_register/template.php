<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}
?>
<?if($USER->IsAuthorized()):?>

<?else:?>
    <?
    if (count($arResult["ERRORS"]) > 0):
        foreach ($arResult["ERRORS"] as $key => $error)
            if (intval($key) == 0 && $key !== 0)
                $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);

        ShowError(implode("<br />", $arResult["ERRORS"]));

    elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
        ?>
        <p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
    <?endif?>

        <?
        if($arResult["BACKURL"] <> ''):
            ?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?
        endif;
        ?>
    <form  class="form form-middle pomidor_reg_form" method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data" style="margin-top: 0px">
    <?
    if($arResult["BACKURL"] <> ''):
        ?>
        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" class="input input_default input_yellow" />
    <?
    endif;
    ?>
    <h2 class="h2 form-middle__title">Регистрация</h2>
        <?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
        <?if($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true):?>
        <?else:?>


                <?
                switch ($FIELD)
                {
                    case "PASSWORD":
                        ?>
                        <div class="form__item form__item_s-offset">
                        <input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off"  class="input input_default input_yellow"  placeholder=" <?=GetMessage("REGISTER_FIELD_".$FIELD)?>"/>

                        </div>
                        <?
                        break;
                case "CONFIRM_PASSWORD":
                    ?>
        <div class="form__item form__item_s-offset">

                    <input size="30" type="password" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" autocomplete="off" class="input input_default input_yellow" placeholder=" <?=GetMessage("REGISTER_FIELD_".$FIELD)?>" />
        </div>
                <?
                break;

                default:
                ?>
        <div class="form__item form__item_s-offset">
                <?if($FIELD == 'LOGIN'){?>
                <input size="30" type="tel" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>"  placeholder=" <?=GetMessage("REGISTER_FIELD_".$FIELD)?>" class="input phone-mask input_default input_yellow" />
                <?}?>
                <?if($FIELD == 'EMAIL'){?>
                <input size="30" type="email" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>"  placeholder=" <?=GetMessage("REGISTER_FIELD_".$FIELD)?>" class="input input_default input_yellow" />
                <?}?>
        </div>
                <?
                }?>

                <?endif?>
                <?endforeach?>

    <div class="form__item form__item_s-offset">
        <label class="checkbox checkbox_yellow">
            <input required type="checkbox" class="checkbox__input">
            <span class="checkbox__label">
                            Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
                        </span>
        </label>
    </div>
    <div class="form__item form__item_s-offset">
        <input type="submit" name="register_submit_button" value="<?=GetMessage("AUTH_REGISTER")?>" class="btn btn_large btn_default btn_yellow"/>

    </div>
    <div class="form__item ">
        <div class="form-nav form-nav_center">
            <a href="/personal/" class="form-nav__link">
                Вход
            </a>
        </div>

    </div>
</form>
<?endif?>












