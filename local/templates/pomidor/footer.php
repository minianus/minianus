
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

			</div><!--end row-->

		</div><!--end .container.bx-content-section-->
	</div><!--end .workarea-->
</main>
<footer class="footer">
    <div class="footer__container">
        <div class="wrap">
            <div class="footer-top">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "bottom",	// Тип меню для остальных уровней
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                        0 => "",
                    ),
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                ),
                    false
                );?>
                <div class="footer__col footer__address">
                    <h4 class="footer__headline">Хабаровск</h4>
                    <ul class="footer__list">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/footer_habarovsk.php"
                        ), false);?>
                    </ul>
                </div>
                <div class="footer__col footer__address">
                    <h4 class="footer__headline">Комсомольск-на-Амуре</h4>
                    <ul class="footer__list">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/footer_komsomolsk.php"
                        ), false);?>

                    </ul>
                </div>
                <div class="footer__col footer__inst">
                    <h4 class="footer__headline">Мы в Instagram</h4><img class="inst-logo" src="/upload/images/instagram.png" alt="">
                    <div class="inst-feed">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_DIR."include/footer_instagramm.php"
                        ), false);?>
                    </div>
                </div>
            </div>
            <div class="footer-bot">
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."include/copyright.php"
                ), false);?>
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_DIR."include/footer_bot.php"
                ), false);?>

        </div>
    </div>
</footer>


<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
"AREA_FILE_SHOW" => "file",
"PATH" => '/include/_forms/cooperation_form.php',
"EDIT_TEMPLATE" => "standard.php"
)
);?>  



<div id="popup-book" class="mfp-hide popup-block">

    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => '/include/_forms/table_book_popup.php',
    "EDIT_TEMPLATE" => "standard.php"
    )
    );?>  

</div>
<div id="popup-city" class="mfp-hide popup-block text-center">
    <div class="window__header">
        <h4 class="h4 window__title text-center">
            Выберите город
        </h4>
    </div>
    <div class="city-change" style="justify-content: space-between">
        <div class="city-change__item city-change__item_yellow city_select_item" data-cityname="komsomolsk-na-amure" onclick="$.magnificPopup.close()">
            <img src="/upload/images/i-city1.png" height="93">
            <div class="city-change__title">Комсомольск-на-Амуре</div>
        </div>
        <div class="city-change__item city-change__item_green city_select_item" data-cityname="khabarovsk" onclick="$.magnificPopup.close()">
            <img src="/upload/images/i-city2.png" height="93">
            <div class="city-change__title">Хабаровск</div>
        </div>
    </div>
</div>



	<div class="col d-sm-none">
		<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "bootstrap_v4", array(
				"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
				"PATH_TO_PERSONAL" => SITE_DIR."personal/",
				"SHOW_PERSONAL_LINK" => "N",
				"SHOW_NUM_PRODUCTS" => "Y",
				"SHOW_TOTAL_PRICE" => "Y",
				"SHOW_PRODUCTS" => "N",
				"POSITION_FIXED" =>"Y",
				"POSITION_HORIZONTAL" => "center",
				"POSITION_VERTICAL" => "bottom",
				"SHOW_AUTHOR" => "Y",
				"PATH_TO_REGISTER" => SITE_DIR."login/",
				"PATH_TO_PROFILE" => SITE_DIR."personal/"
			),
			false,
			array()
		);?>
	</div>
</div> <!-- //bx-wrapper -->


<script>
	BX.ready(function(){
		var upButton = document.querySelector('[data-role="eshopUpButton"]');
		BX.bind(upButton, "click", function(){
			var windowScroll = BX.GetWindowScrollPos();
			(new BX.easing({
				duration : 500,
				start : { scroll : windowScroll.scrollTop },
				finish : { scroll : 0 },
				transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
				step : function(state){
					window.scrollTo(0, state.scroll);
				},
				complete: function() {
				}
			})).animate();
		})
	});
</script>


<div id="card-popup" class="mfp-hide popup-block popup-block_big"></div>

<div class="popup middle-popup tnx-popup mfp-hide" id="tnx">
    <img class="tnx-popup__img" src="images/pizza-popup.svg" alt="">
    <div class="">
        <h3 class="tnx-popup__title">Спасибо, ваша заявка принята!</h3>
        <div class="tnx-popup__desc">
            <p>
               Бронь действительна только после подтверждения гостевым менеджером 
            </p>
        </div>
        <a href="#" class="btn btn_green tnx-popup__btn" onclick="$.magnificPopup.close();">
            ok
        </a>
    </div>
</div>
<script>

    $(document).ready(function(){
        $('input[name="street"]').suggestions({
        token: "<?=DADATA_API_KEY?>",
        type: "ADDRESS",
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function(suggestion) {
        }
        
    });
    $('input[name="UF_STREET"]').suggestions({
        token: "<?=DADATA_API_KEY?>",
        type: "ADDRESS",
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function(suggestion) {
        }
        
    });
    });
</script>
</body>
</html>
