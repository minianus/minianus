<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("fx"));

\Bitrix\Main\UI\Extension::load("ui.bootstrap4");

if (isset($_GET["theme"]) && in_array($_GET["theme"], array("blue", "green", "yellow", "red")))
{
    COption::SetOptionString("main", "wizard_eshop_bootstrap_theme_id", $_GET["theme"], false, SITE_ID);
}
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "green", SITE_ID);

$curPage = $APPLICATION->GetCurPage(true);

?><!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
    <? $APPLICATION->ShowHead(); ?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-2.2.4.min.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.matchHeight.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.magnific-popup.min.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/nouislider.min.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/select2.min.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/liba.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/wNumb.min.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/aos.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/slick.js")?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/script.js")?>

    <style>
        @font-face {
            font-family: 'rub';
            font-weight: normal;
            font-style: normal;

            src: url('<?=SITE_TEMPLATE_PATH?>/fonts/rouble.woff') format('woff2'), url('<?=SITE_TEMPLATE_PATH?>/fonts/rouble.woff') format('woff');
        }
    </style>
</head>
<body class="bx-background-image bx-theme-<?=$theme?>" <?$APPLICATION->ShowProperty("backgroundImage");?> data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<?$APPLICATION->IncludeComponent(
    "bitrix:eshop.banner",
    "",
    array()
);?>
<div class="bx-wrapper" id="bx_eshop_wrap">
    <header class="header">
        <div class="header-des">
            <div class="header-top">
                <div class="wrap">
                    <div class="header-top__container">
                        <div class="block-left">
                            <a class="header__logo" href="<?=SITE_DIR?>">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    array(
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => SITE_DIR."include/company_logo.php"),
                                    false
                                );?>
                            </a>
                            <div class="header__translate"><a href="https://verstka.digitalwf.ru/pomidor/index.html#">RU<span class="header__translate-arrow"></span></a></div>
                            <div class="header__delivery"><span class="header__delivery-icon">
                                    <svg class="icon icon-del">
                                       <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                                    </svg>
                                </span><span>Доставка еды <a class="underline popup-call" href="https://verstka.digitalwf.ru/pomidor/index.html#popup-city">Хабаровск</a></span></div>
                            <div class="header__phone"><span class="header__phone-icon">
                                <span class="header__phone-icon">
                                    <svg class="icon icon-phone">
                                            <svg viewBox="0 0 480.55 480.56" id="icon-phone">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path d="M365.35 317.9c-15.7-15.5-35.3-15.5-50.9 0-11.9 11.8-23.8 23.6-35.5 35.6-3.2 3.3-5.9 4-9.8 1.8-7.7-4.2-15.9-7.6-23.3-12.2-34.5-21.7-63.4-49.6-89-81-12.7-15.6-24-32.3-31.9-51.1-1.6-3.8-1.3-6.3 1.8-9.4 11.9-11.5 23.5-23.3 35.2-35.1 16.3-16.4 16.3-35.6-.1-52.1-9.3-9.4-18.6-18.6-27.9-28-9.6-9.6-19.1-19.3-28.8-28.8-15.7-15.3-35.3-15.3-50.9.1-12 11.8-23.5 23.9-35.7 35.5-11.3 10.7-17 23.8-18.2 39.1-1.9 24.9 4.2 48.4 12.8 71.3 17.6 47.4 44.4 89.5 76.9 128.1 43.9 52.2 96.3 93.5 157.6 123.3 27.6 13.4 56.2 23.7 87.3 25.4 21.4 1.2 40-4.2 54.9-20.9 10.2-11.4 21.7-21.8 32.5-32.7 16-16.2 16.1-35.8.2-51.8q-28.5-28.65-57.2-57.1zm-19.1-79.7l36.9-6.3A165.63 165.63 0 00243.05 96l-5.2 37.1a128 128 0 01108.4 105.1z"></path><path d="M404 77.8A272.09 272.09 0 00248 0l-5.2 37.1a237.42 237.42 0 01200.9 194.7l36.9-6.3A274.08 274.08 0 00404 77.8z">
                                                        </path>
                                                    </g>
                                                </g>
                                            </svg>
                                    </svg>
                                </span>

									<?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => SITE_DIR."include/telephone.php"
                                        ),
                                        false
                                    );?>

                            </div>
                        </div>
                        <div class="block-right"><a class="header__link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Карта доставки </a><a class="btn header__btn header__btn_del btn_del" href="https://verstka.digitalwf.ru/pomidor/index.html#">Заказать доставку</a><a class="btn header__btn header__btn_log" href="https://verstka.digitalwf.ru/pomidor/index.html#">
                                <svg class="icon">
                                    <use xlink:href="http://pomidor/upload/images/icons.svg#icon-prof"></use>
                                </svg></a></div>
                    </div>
                </div>
            </div>
            <div class="header-bot">
                <div class="wrap">
                    <div class="header-bot__container"><a class="btn__reserv popup-call" href="https://verstka.digitalwf.ru/pomidor/index.html#popup-book"> <span class="btn__reserv-text">Забронировать стол</span>
                            <div class="btn__reserv-bg">
                                <svg class="icon reserv-icon">
                                    <use xlink:href="http://pomidor/upload/images/icons.svg#reserv"></use>
                                    <svg viewBox="0 0 341.25 55.69" id="reserv"><g id="Слой_2" data-name="Слой 2"><path d="M338.35 1.41c.7.6.9 1.1.5 1.8a16.67 16.67 0 01-2.5 3.6 23.29 23.29 0 00-3.4 3.7c-.6 1.8-3.3 5.9-3.7 8-1.4 3.2-1.8 6.4-3 8.8 2.3 2.9 4.6 5.9 6.9 8.9s4.6 10.1 6.8 13c1.5 2.1 1.5 2.4 1 5a3.49 3.49 0 01-2.5.9c-1.6 0-3.2.1-4.8.1-9.6.5-19.2.5-28.8.4-2.4 0 15.1-.2 12.7-.3-2.8-.1-5.5-.1-8.3-.1-3.1-.1-6.2-.1-9.3-.2-4.1-.1-100.2-.3-104.3-.3h-21.1c-4.1 0-8.1 0-12.2.1-3.1 0-6.2 0-9.3.1-3.8.1-27.7.2-31.5.3s-7.9.1-11.8.2-8 .1-12 .2H43a36.17 36.17 0 00-5.7-.1c-1.9.2-3.8 0-5.7.1q-3.15.15-6.3 0l73.7-.2H38.75c-2.2 0-4.5-.1-6.7-.1L3.85 55a31.06 31.06 0 01-3.3-.3c-.7-.7-.7-1.3-.2-1.9 1.6-1.7 3.3-3.3 4.8-5 3.2-3.6 6.8-14.1 9.7-18a8.79 8.79 0 01-.6-1c-2-3-4-10.9-6-13.9-.3-.4-.6-1.8-.9-2.2-1.9-2.5-3.8-4.9-5.7-7.4-.3-.8-.8-1.7-1.2-2.6S.35.5 2.45.3l7.6-.3c7.8-.1 15.6.4 23.5.3 4.4 0 58.7.3 63.1.3 2.9.1-74.3 0-71.4 0 5.4 0 10.9.1 16.3.1 4.4 0 58.7 0 63.1-.1 1.4 0 2.8-.1 4.1-.1 4.5-.1 8.9-.3 13.4-.4 4.9 0 29.8 0 34.7.1h2.9a48.74 48.74 0 015.4.2c2.1.2 4.2.1 6.4.1 4.1 0 8.3.1 12.4-.2 2.2-.1 4.5.2 6.7.2h13.1c5.5 0 90.7-.5 96.2.1.5.1-95.6-.1-95.1-.1 4.8.1 98.6.5 103.4.6 4.3.1 8.5.5 12.7.7 3.9.1 7.9.4 11.8-.2 1.9-.09 3.8-.09 5.6-.19z" id="Слой_1-2" data-name="Слой 1"></path></g></svg>
                                </svg>
                            </div>
                        </a>
                        <nav class="header__nav">
                            <ul class="header__nav-list">
                                <li class="header__nav-item"><a class="header__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">О нас</a></li>
                                <li class="header__nav-item"><a class="header__nav-link header__nav-link-active" href="https://verstka.digitalwf.ru/pomidor/index.html#">
                                        <svg class="icon icon-tomat">
                                            <use xlink:href="http://pomidor/upload/images/icons.svg#icon-tomat">
                                                <svg viewBox="0 0 621.93 745.7" id="icon-tomat"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z"></path><path d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z"></path></g></g></svg>
                                            </use>
                                        </svg>Меню</a></li>
                                <li class="header__nav-item"><a class="header__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Рестораны</a></li>
                                <li class="header__nav-item"><a class="header__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Программа лояльности</a></li>
                                <li class="header__nav-item"><a class="header__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Акции</a></li>
                                <li class="header__nav-item"><a class="header__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Работа у нас</a></li>
                                <li class="header__nav-item"><a class="header__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Сотрудничество</a></li>
                            </ul>
                        </nav><span class="header__price">1080 <span>р</span></span>
                        <a class="btn header__btn btn__cart popup-call" href="https://verstka.digitalwf.ru/pomidor/index.html#popup-cart">
                            <svg class="icon icon-cart-h">
                                <use xlink:href="http://pomidor/upload/images/icons.svg#icon-cart-h"></use>
                            </svg><span class="cart__count">12</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-mobile mobile">
            <div class="mobile-top">
                <div class="mobile__logo"><img src="./Синьор Помидор_files/logo.svg" alt=""></div>
                <div class="mobile__buttons"><a class="btn header__btn header__btn_log mobile__btn" href="https://verstka.digitalwf.ru/pomidor/index.html#">
                        <svg class="icon mobile__icon">
                            <use xlink:href="http://pomidor/upload/images/icons.svg#icon-prof"></use>
                        </svg></a><a class="btn btn__cart mobile__btn popup-call" href="https://verstka.digitalwf.ru/pomidor/index.html#popup-cart">
                        <svg class="icon icon-cart-h mobile__icon">
                            <use xlink:href="http://pomidor/upload/images/icons.svg#icon-cart-h"></use>
                        </svg><span class="cart__count">12</span></a><a class="mobile-burger" href="https://verstka.digitalwf.ru/pomidor/index.html#"><span></span><span></span><span></span></a></div>
            </div>
            <div class="mobile-middle">
                <nav class="mobile__nav">
                    <ul class="mobile__nav-list">
                        <li class="mobile__nav-item"><a class="mobile__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">О нас</a></li>
                        <li class="mobile__nav-item"><a class="mobile__nav-link header__nav-link-active" href="https://verstka.digitalwf.ru/pomidor/index.html#">
                                <svg class="icon icon-tomat">
                                    <use xlink:href="http://pomidor/upload/images/icons.svg#icon-tomat"></use>
                                </svg>Меню</a></li>
                        <li class="mobile__nav-item"><a class="mobile__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Рестораны</a></li>
                        <li class="mobile__nav-item"><a class="mobile__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Программа лояльности</a></li>
                        <li class="mobile__nav-item"><a class="mobile__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Акции</a></li>
                        <li class="mobile__nav-item"><a class="mobile__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Работа у нас</a></li>
                        <li class="mobile__nav-item"><a class="mobile__nav-link" href="https://verstka.digitalwf.ru/pomidor/index.html#">Сотрудничество</a></li>
                    </ul>
                </nav>
                <div class="mobile-bot">
                    <div class="mobile-bot__buttons">
                        <div class="mobile-bot__del"><a class="btn header__btn header__btn_del btn_del" href="https://verstka.digitalwf.ru/pomidor/index.html#">Заказать доставку</a></div><a class="btn btn__reserv" href="https://verstka.digitalwf.ru/pomidor/index.html#"> <span class="btn__reserv-text popup-call" href="#popup-book">Забронировать стол</span>
                            <div class="btn__reserv-bg">
                                <svg class="icon reserv-icon">
                                    <use xlink:href="http://pomidor/upload/images/icons.svg#reserv"></use>
                                </svg>
                            </div>
                        </a>
                    </div>
                    <div class="mobile-bot__info">
                        <div class="mobile-bot__link">
                            <svg class="icon icon-phone">
                                <use xlink:href="http://pomidor/upload/images/icons.svg#icon-phone"></use>
                            </svg><a class="header__link" href="https://verstka.digitalwf.ru/pomidor/index.html#">+7 (421) 296 06 06</a>
                        </div>
                        <div class="mobile-bot__link">
                            <svg class="icon icon-del">
                                <use xlink:href="http://pomidor/upload/images/icons.svg#icon-del"></use>
                            </svg><span>Доставка еды <a class="underline popup-call" href="https://verstka.digitalwf.ru/pomidor/index.html#popup-city">Хабаровск</a></span>
                        </div><a href="https://verstka.digitalwf.ru/pomidor/index.html#">RU<span class="header__translate-arrow"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <header class="bx-header">
        <div class="bx-header-section container">
            <!--region bx-header-->
            <div class="row pt-0 pt-md-3 mb-3 align-items-center" style="position: relative;">
                <div class="d-block d-md-none bx-menu-button-mobile" data-role='bx-menu-button-mobile-position'></div>
                <div class="col-12 col-md-auto bx-header-logo">

                    <a class="bx-logo-block d-block d-md-none text-center" href="<?=SITE_DIR?>">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => SITE_DIR."include/company_logo_mobile.php"
                            ),
                            false
                        );?>
                    </a>
                </div>

                <div class="col-auto d-none d-md-block bx-header-personal">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket.line",
                        "bootstrap_v4",
                        array(
                            "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                            "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                            "SHOW_PERSONAL_LINK" => "N",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_TOTAL_PRICE" => "Y",
                            "SHOW_PRODUCTS" => "N",
                            "POSITION_FIXED" =>"N",
                            "SHOW_AUTHOR" => "Y",
                            "PATH_TO_REGISTER" => SITE_DIR."login/",
                            "PATH_TO_PROFILE" => SITE_DIR."personal/"
                        ),
                        false,
                        array()
                    );?>
                </div>

                <div class="col bx-header-contact">
                    <div class="d-flex align-items-center justify-content-between justify-content-md-center flex-column flex-sm-row flex-md-column flex-lg-row">
                        <div class="p-lg-3 p-1">
                            <div class="bx-header-phone-block">
                                <i class="bx-header-phone-icon"></i>
                                <span class="bx-header-phone-number">
									<?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => SITE_DIR."include/telephone.php"
                                        ),
                                        false
                                    );?>
								</span>
                            </div>
                        </div>
                        <div class="p-lg-3 p-1">
                            <div class="bx-header-worktime">
                                <div class="bx-worktime-title"><?=GetMessage('HEADER_WORK_TIME'); ?></div>
                                <div class="bx-worktime-schedule">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        array(
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => SITE_DIR."include/schedule.php"
                                        ),
                                        false
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--endregion-->

            <!--region menu-->
            <div class="row mb-4 d-none d-md-block">
                <div class="col">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bootstrap_v4",
                        array(
                            "ROOT_MENU_TYPE" => "top",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "36000000",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_THEME" => "site",
                            "CACHE_SELECTED_ITEMS" => "N",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "3",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N",
                            "COMPONENT_TEMPLATE" => "bootstrap_v4"
                        ),
                        false
                    );?>
                </div>
            </div>
            <!--endregion-->

            <!--region search.title -->
            <?if ($curPage != SITE_DIR."index.php"):?>
                <div class="row mb-4">
                    <div class="col">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "bootstrap_v4",
                            array(
                                "NUM_CATEGORIES" => "1",
                                "TOP_COUNT" => "5",
                                "CHECK_DATES" => "N",
                                "SHOW_OTHERS" => "N",
                                "PAGE" => SITE_DIR."catalog/",
                                "CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS") ,
                                "CATEGORY_0" => array(
                                    0 => "iblock_catalog",
                                ),
                                "CATEGORY_0_iblock_catalog" => array(
                                    0 => "all",
                                ),
                                "CATEGORY_OTHERS_TITLE" => GetMessage("SEARCH_OTHER"),
                                "SHOW_INPUT" => "Y",
                                "INPUT_ID" => "title-search-input",
                                "CONTAINER_ID" => "search",
                                "PRICE_CODE" => array(
                                    0 => "BASE",
                                ),
                                "SHOW_PREVIEW" => "Y",
                                "PREVIEW_WIDTH" => "75",
                                "PREVIEW_HEIGHT" => "75",
                                "CONVERT_CURRENCY" => "Y"
                            ),
                            false
                        );?>
                    </div>
                </div>
            <?endif?>
            <!--endregion-->

            <!--region breadcrumb-->
            <?if ($curPage != SITE_DIR."index.php"):?>
                <div class="row mb-4">
                    <div class="col" id="navigation">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:breadcrumb",
                            "universal",
                            array(
                                "START_FROM" => "0",
                                "PATH" => "",
                                "SITE_ID" => "-"
                            ),
                            false,
                            Array('HIDE_ICONS' => 'Y')
                        );?>
                    </div>
                </div>
                <h1 id="pagetitle"><?$APPLICATION->ShowTitle(false);?></h1>
            <?endif?>
            <!--endregion-->
        </div>
    </header>

    <div class="workarea">
        <div class="container bx-content-section">
            <div class="row">
                <?$needSidebar = preg_match("~^".SITE_DIR."(catalog|personal\/cart|personal\/order\/make)/~", $curPage);?>
                <?//($needSidebar ? "col" : "col-md-9 col-sm-8")?>
                <div class="bx-content col">