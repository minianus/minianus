function redraw_basket_top(data) {
    $.get('/ajax/basket_top_content_get.php', data, function(result) {
        result = JSON.parse(result);
        var basket_icon = result.basket_icon;
        var popup_content = result.popup_content;
        let mob_items_count = result.mob_items_count;
        $('#mob_items_count').html(mob_items_count);
        $('#basket_icon').html(basket_icon);
        $('#popup_content').html(popup_content);
        $('.popup-call').magnificPopup({
            type: 'inline',
            closeBtnInside: false
        });
        $(".cart-slider").not('.slick-initialized').slick({
            infinite: true,
            slidesToShow: 3,
            arrows: true,
            prevArrow: '<span class="cart-slider__icon-arrow" id="arrow-left"></span>',
            nextArrow: '<span class="cart-slider__icon-arrow" id="arrow-right"></span>',
            slidesToScroll: 1,
            variableWidth: true,
            centerMode: false,
            responsive: [{
                breakpoint: 420,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
        let main_page_delivery_display = $('.menu__count .menu__count-content');
        if(main_page_delivery_display.length > 0){
            main_page_delivery_display.html(result.delivery_summ_message);
        }
        let header_price = $('.header__price');
        header_price.html(result.header_price);
        if(data.action == 'add_item'){
            Swal.fire({
                toast: true,
                position: 'center',
                showConfirmButton: false,
                timer: 2000,
                icon: 'success',
                title: 'Добавлено!'
                })
        }
    })
}

function do_basket_action(e, data) {
    e.preventDefault();
    redraw_basket_top(data);
}

$(document).ready(function(){

    var manager = new EventManager();
    window.eventBus = manager;

    window.eventBus.addListener('basket_changed', redraw_basket_top);

    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: 'Пред',
        nextText: 'След',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Нед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $( ".js-datepicker" ).datepicker({
        dateFormat : 'dd.mm.yy'
    });

    $('.tabs-nav a').click(function(e){
        e.preventDefault();
        $('.work-tab .loyalty__faq').removeClass('active');
        $('.work-tab .loyalty__faq').eq($(this).index()).addClass('active');
        $('.tabs-nav a').removeClass('active');
        $(this).addClass('active');
        
    })
   var options =  {
       onComplete: function(cep) {
        console.log('CEP Completed!:' + cep);
      },
       onChange: function(cep, event, currentField,){
         if(cep.length < 17){
             $(currentField).closest('form').addClass('error');
         }else{
             $(currentField).closest('form').removeClass('error');
         }
      }
   };
    $('.re-phone-mask').mask('+7(000) 000-00-00', options);
    
  $('.counter__btn_plus').click(function(e){
      e.preventDefault();
     var el = $(this).closest('.counter');
     var inputVal = parseInt(el.find('.counter__input').val())+1;
     el.find('.counter__input').val(inputVal);
  });
  $('.counter__btn_minus').click(function(e){
    e.preventDefault();
     var el = $(this).closest('.counter');
     var inputVal = parseInt(el.find('.counter__input').val());
     if(inputVal < 2){
         inputVal == 1;
     }else{
         inputVal = inputVal - 1;
     }
      console.log(inputVal)
     el.find('.counter__input').val(inputVal);
  })
   
  $('body').on('click', '.popup-card', function (e){
      e.preventDefault();
  });
    
  $('.datepicker-here').datepicker({
      autoClose:true,
  })
  $('.phone-mask').mask('+7(000) 000-00-00'); 
   
$(document).on("click", ".popup-card", function (e) {
   e.preventDefault();
   var id = $(this).attr("data-id");
   $.magnificPopup.open({
       items: {
           src: $(this).attr('href')
       },
       type: 'inline',
       callbacks: {
           open: function () {
               $('#card-popup').addClass("loading");
           }
       }
   });
   $.ajax({
       url: "/local/ajax/popup-card.php?id=" + id, // адрес, на который будет отправлен запрос       
       success: function (data) { // если запрос успешен вызываем функцию
           $('#card-popup').html(data);

           
           setTimeout(function () {
               $('#work-popup').removeClass("loading");
           }, 1000)

       }
   });
})    

$(document).on("click", ".deliv_popup-call", function (e) {
    e.preventDefault();
    $('#card-popup').html('');
    var id = $(this).attr("data-item_id");
    $.magnificPopup.open({
        items: {
            src: $('#card-popup')
        },
        type: 'inline',
        callbacks: {
            open: function () {
                $('#card-popup').addClass("loading");
            }
        }
    });
    $.ajax({
        url: "/ajax/deliv_card_popup.php?id=" + id, // адрес, на который будет отправлен запрос       
        success: function (data) { // если запрос успешен вызываем функцию
            data = $.parseJSON(data);
            $('#card-popup').append(data.html);
 
            
            setTimeout(function () {
                $('#card-popup').removeClass("loading");
            }, 1000)
 
        }
    });
 })   
   
   
})

function show_preloader(){

    $('body').addClass('preload');
    $('body').append('<img class="preload-pic" src="/upload/preloader-pic.gif" id="preloader">')
}

function hide_preloader(){
    $('body').removeClass('preload');
    $('body').remove('#preloader');

}

function form_clear_messages(form){

    var success = form.find('#success_message');
    var error = form.find('#error_message');
    success.html('');
    error.html('');
}

function form_show_success(form, data){

    form_clear_messages(form);
    var success = form.find('#success_message');
    success.html(data);
}

function form_show_error(form, data){

    form_clear_messages(form);
    var error = form.find('#error_message');
    var s = '';
    if(typeof(data) == 'string'){
        s = data;
    }else if(typeof(data) == 'object'){
        data.forEach(function(item, i, arr) {
            s += item + '<br>';
            });
    }
    error.html(s);

}