
$(document).ready(function() {
    console.log('--->');
    
    
    $('.menu-item__content').matchHeight();
    
    $('body').on('click', '.menu-modal', function (){
       $.magnificPopup.open({
        items: {
            src: $(this).attr('href')
        },
        type: 'inline'
    });
    });
    
    $('.file-w__input').change(function(){
        var file = $(this).val();
        if(file.length > 1){
            var fileName = file.replace(/^.*[\\\/]/, '');   
        }else{
            var fileName = $(this).closest('.file-w').data('title');
        }
        $(this).closest('.file-w').find('.file-w__title').text(fileName);
    })
    
    
    $('.menu-trigger').hover(function(){
        $('.menu-window').addClass('active');
    });
    $('.header__nav-item:not(.menu-trigger)').hover(function(){
        $('.menu-window').removeClass('active');
    });
    $('.header-top').hover(function(){
        $('.menu-window').removeClass('active');
    });
    $('.header').mouseleave(function(){
        $('.menu-window').removeClass('active');
    });
   $('.relative-slider').slick({
       slidesToShow:4,
       responsive: [{
           breakpoint: 1100,
           settings: {
               slidesToShow: 3,
           }
       },
           {
               breakpoint: 990,
               settings: {
                   slidesToShow: 3,
                   dots:true
               }
           },
           {
               breakpoint: 680,
               settings: {
                   slidesToShow: 2,
                   dots:true
               }
           },
           {
               breakpoint: 500,
               settings: {
                   slidesToShow: 1,
                   dots:true
               }
           }]
   })

    $('.full-nav__btn_left').click(function(e){
        e.preventDefault();
        $(this).closest('.full-slider-widget').find('.slick-slider').slick('prev');
    });
    $('.full-nav__btn_right').click(function(e){
        e.preventDefault();
        $(this).closest('.full-slider-widget').find('.slick-slider').slick('next');
    })
    $('.full-slider').slick({
        responsive: [
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 1,
                    infinite: true,
                    dots: true
                }
            }]
    })
    $(".button_edit_name").click(function () {
        $(".edit_name").slideToggle()
    })


    $('.custom-select select').select2({
        //minimumResultsForSearch: Infinity,
        width:'100%'
    })

    //$('.datepicker-here').data('datepicker');

    $('.edit-link').click(function(e){
        e.preventDefault();
        var id = $(this).attr('href');
        $(id).stop().slideToggle();
    })
    if($('#ball-toggle').length){
        var slider1 = document.getElementById('ball-toggle');
        var start = $(slider1).data('start');
        var min = $(slider1).data('min');
        var max = $(slider1).data('max');
        noUiSlider.create(slider1, {
            start: start,
            connect: 'lower',
            animate: false,
            range: {
                min: min,
                max: max
            }
        });
        slider1.noUiSlider.on('update', function( values, handle ) {
            $('#balls-val').val(parseInt(values[0]));
        });
        slider1.noUiSlider.on('change', function( values, handle ) {
            $('#balls-val').trigger("change");
        });
    }

    $('.cafe-slider').slick({
        slidesToShow:4,
        responsive: [{
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                dots:true
            }
        },
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 2,
                    dots:true
                }
            },
            {
                breakpoint:760,
                settings: {
                    slidesToShow: 1,
                    dots:true
                }
            }
        ]
    })
    $('#balls-val').change(function(){
        slider1.noUiSlider.set($(this).val());
    })
    $('.match-height').matchHeight();

    $('.tab-nav__link').click(function(e){
        e.preventDefault();
        var parent = $(this).closest('.tab-widget');
        parent.find('.tab-nav__link').removeClass('active');
        $(this).addClass('active');
        parent.find('.tab').removeClass('active');
        parent.find('.tab').eq($(this).index()).addClass('active');
    })
    $('.banner__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
        // prevArrow: $('.banner-prev'),
        // nextArrow: $('.banner-next')
    });

    $('.banner-next').on('click', function(e) {
        e.preventDefault();
        $('.banner__slider').slick('slickNext');
    });

    $('.banner-prev').on('click', function(e) {
        e.preventDefault();
        $('.banner__slider').slick('slickPrev');
    });

    $('.menu__list').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        draggable: true,
        arrows: true,
        responsive: [
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.menu-next').on('click', function(e) {
        e.preventDefault();
        $('.menu__list').slick('slickNext');
    });

    $('.menu-prev').on('click', function(e) {
        e.preventDefault();
        $('.menu__list').slick('slickPrev');
    });

    // $('.menu-item').hover(
    //     function(){ $(this).addClass('menu-item_active') },
    //     function(){ $(this).removeClass('menu-item_active') }
    // );
    $('.select2-hidden-accessible').change(function(){
        window.location.href = $(this).val();
    });
    $('.menu__tabs-item').click(function(e){

        var el = $(this).closest('.menu__container');
        $('.menu__tabs-item').removeClass('active');
        $(this).addClass('active');
        el.find('.menu__content').removeClass('menu__content_active');
        el.find('.menu__content').eq($(this).index()).addClass('menu__content_active');
        $(this).closest('.menu__container').find('.slick-slider').slick('setPosition');
        
    });
    
    
    $('.menu-deliv').click(function(e){
    });

    $('.mobile-burger').click(function () {
        $(this).toggleClass('active');
        $('.mobile-middle').stop().slideToggle();
    })

    $('.address__container').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        draggable: true,
        arrows: true,
        responsive: [
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.history-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<svg class="slider-icon__arrow" id="arrow-left"><svg viewBox="0 0 70.91 48.02" id="arrow-left"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M23.81 42.12a1.27 1.27 0 01-.6-.1c-.8-.1-2.5-.4-20.5-15.9l-.5-.4A6 6 0 010 21.42a5 5 0 011.4-3.9c.2-.2 19.7-18.2 23-17.5 2.4.5 2.9 4.1 2.8 9.5 4.7.2 18.9 1.8 28.1 5.6 10.3 4.3 15.3 15.5 15.6 20.6a1.3 1.3 0 01-.8 1.2 1.2 1.2 0 01-1.4-.3c-4.2-4.9-15.5-5.9-24.6-6.7l-3.1-.3c-7.6-.7-12.3.1-14.1.6l-.4 7.8a4 4 0 01-.9 3.2 2 2 0 01-1.79.9zm.1-39.5c-3 1-19.1 14.9-20.7 16.7a2.72 2.72 0 00-.7 2 3.39 3.39 0 001.3 2.5l.5.4c15.9 13.6 18.8 15.2 19.3 15.3h.3a2.87 2.87 0 00.2-1.2v-.2l.5-8.8a1.33 1.33 0 01.8-1.1c.2-.1 5.5-1.9 15.9-.9l3.1.3c8.4.7 17.6 1.5 23.3 5-1.6-4.7-5.7-11.9-13.3-15.1-10-4.1-26.4-5.6-28.3-5.5a1.69 1.69 0 01-1-.3 1.09 1.09 0 01-.4-.9v-.7c.2-5.7-.5-7.12-.8-7.5z"></path><path class="cls-1" d="M25.71 48c-3.9 0-9.5-5.1-16.2-11.7l-.5-.4c-2.3-2.2-7.2-8.3-8.7-12.9a.8.8 0 01.6-1.1.8.8 0 011.1.6c1.4 4.2 6.1 10.1 8.2 12.1l.5.4c4.9 4.8 12.3 12.1 15.5 11.1 2.4-.7 3.5-6.5 3.6-10.2a2.5 2.5 0 01.8-1.8c1.5-1.3 4.9-.9 9.2-.4 1.2.1 2.4.3 3.6.4a81.93 81.93 0 0118.3 4.2c2 .7 6.1-1.7 7.6-3.1a.92.92 0 111.3 1.3c-1.4 1.3-6.3 4.6-9.4 3.5a79.75 79.75 0 00-17.9-4.1c-1.3-.1-2.5-.3-3.7-.4-3.4-.4-6.8-.8-7.8 0a.59.59 0 00-.2.5c-.1 1.8-.7 10.6-4.9 11.9a2.79 2.79 0 01-1 .1z"></path></g></g></svg></svg>',
        nextArrow: '<svg class="slider-icon__arrow" id="arrow-right"><svg viewBox="0 0 70.9 47.82" id="arrow-right"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M45.3 41.22a3.7 3.7 0 01-.9-3.2l-.4-7.8a45 45 0 00-14.1-.6l-3.1.3c-9.1.8-20.4 1.8-24.6 6.7a1.2 1.2 0 01-1.4.3 1.3 1.3 0 01-.8-1.2c.3-5.1 5.3-16.3 15.6-20.6 9.2-3.8 23.4-5.4 28.1-5.6-.1-5.3.4-8.9 2.8-9.5 3.3-.7 22.9 17.3 23 17.5a5.82 5.82 0 011.4 3.9 6.45 6.45 0 01-2.2 4.3l-.5.4c-18 15.4-19.7 15.7-20.5 15.9-.2 0-.4.1-.6.1a5 5 0 01-1.8-.9zM46.2 10v.7a1 1 0 01-.4.9 1.21 1.21 0 01-1 .3c-1.9-.2-18.4 1.3-28.3 5.5C8.9 20.6 4.8 27.8 3.2 32.5c5.7-3.5 14.9-4.3 23.3-5l3.1-.3c10.4-1 15.7.8 15.9.9a1.17 1.17 0 01.8 1.1l.5 8.8v.2a1.84 1.84 0 00.2 1.2h.3c.5-.1 3.4-1.7 19.3-15.3l.5-.4a3.39 3.39 0 001.3-2.5 2.72 2.72 0 00-.7-2C66 17.42 50 3.62 47 2.52c-.3.3-.9 1.7-.8 7.48z"></path><path class="cls-1" d="M45.2 47.82c3.9 0 9.5-5.1 16.2-11.7l.5-.4c2.3-2.2 7.2-8.3 8.7-12.9a.89.89 0 10-1.7-.5c-1.4 4.2-6.1 10.1-8.2 12.1l-.5.4c-4.9 4.8-12.3 12.1-15.5 11.1-2.4-.7-3.5-6.5-3.6-10.2a2.5 2.5 0 00-.8-1.8c-1.5-1.3-4.9-.9-9.2-.4-1.2.1-2.4.3-3.6.4a81.93 81.93 0 00-18.3 4.2c-2 .7-6.1-1.7-7.6-3.1A1.05 1.05 0 00.3 35a1.05 1.05 0 000 1.3c1.4 1.3 6.3 4.6 9.4 3.5a79.75 79.75 0 0117.9-4.1c1.3-.1 2.5-.3 3.7-.4 3.4-.4 6.8-.8 7.8 0a.59.59 0 01.2.5c.1 1.8.7 10.6 4.9 11.9a3.08 3.08 0 001 .12z"></path></g></g></svg></svg>',
        arrows: true,
        dots: false,
        infinite: false,
        responsive: [
            {
                breakpoint: 860,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $(".cart-slider").slick({
        infinite: true,
        slidesToShow: 3,
        arrows: true,
        prevArrow: '<span class="cart-slider__icon-arrow" id="arrow-left"></span>',
        nextArrow: '<span class="cart-slider__icon-arrow" id="arrow-right"></span>',
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: false,
        responsive: [
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.city_select_item').on('click', function(e) {
        let city = $(e.currentTarget).attr('data-cityname');
        document.cookie = "city=" + city + ';path=/;';
        document.location.reload();


    });

    if(getCookie("city")== undefined){
        
    }
    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    /*$('.delivery-change').on('click', function(e) {
        $.magnificPopup.open({
            items: {
                src: $('#popup-delivery')
            },
            type: 'inline'
        });
    });*/
    $('.delivery-ready').on('click', function(e) {
        $.magnificPopup.open({
            items: {
                src: $('#popup-ready')
            },
            type: 'inline'
        });
    });

    $('.popup-call').magnificPopup();

    $('.address-next').on('click', function(e) {
        e.preventDefault();
        $('.address__container').slick('slickNext');
    });

    $('.address-prev').on('click', function(e) {
        e.preventDefault();
        $('.address__container').slick('slickPrev');
    });

    AOS.init({
        easing: 'ease',
        once: true,
        mirror: false,
        anchorPlacement: 'top-bottom'

    });
    $('body').on('click', '.add_basket_big', function() {
        $(`.add_basket-${$(this).attr("basket-id")}`)[0].click()
        $(".add-exten").each(function (index,element) {
            if($(this).prop('checked') === true){
                $(this).parent().find(".btn_extensions_buy")[0].click()
            }
        })

    })

    $('body').on('click', '.pomidor_filter .menu__tabs-item', function() {
        $(".pomidor_input_filter").prop('checked', false);
        $(this).find(".pomidor_input_filter").prop('checked', true)
        // $("#del_filter")[0].click()
        setTimeout(function(){
           // smartFilter.click()
           //  $("#del_filter")[0].click()
            $('#set_filter')[0].click()
           // $('#set_filter')[0].click()
        }, 300);
    });
    $(".btn-dop").click(function () {
        $(this).parent().find(".btn-dop").removeClass("no-active")
        $(this).parent().find(".add-exten").trigger('click')
        $(this).toggleClass("no-active")
    })
    $(".add-w__habarovsk").click(function () {
        $(this).parent().find("a").removeClass("active")
        $(this).addClass("active")
        $(".menu_cart-address-tab").hide()
        $(".menu_cart-address-habarovsk").removeClass("d-sm-none")
        $(".menu_cart-address-komsomolsk").addClass("d-sm-none")
    })
    $(".add-w__komsomolsk").click(function () {
        $(this).parent().find("a").removeClass("active")
        $(this).addClass("active")
        $(".menu_cart-address-tab").hide()
        $(".menu_cart-address-komsomolsk").removeClass("d-sm-none")
        $(".menu_cart-address-habarovsk").addClass("d-sm-none")
    })
})
