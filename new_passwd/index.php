<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(" ");
?>

<form id="set_new_passwd_form" class="form form-middle " onsubmit="set_new_passwd_form_send(event)">
    <h2 class="h2 form-middle__title">Изменение пароля</h2>
    <input type="hidden" name="action" value="set_new_passwd">
    <input type="hidden" name="checkword" value="<?=$_REQUEST['checkword']?>">
    <input type="hidden" name="login" value="<?=$_REQUEST['login']?>">
    <div class="form__item form__item_s-offset">
        <input type="password" class="input input_default input_yellow" placeholder="Новый пароль" name="passwd">
    </div>
    <div class="form__item form__item_s-offset">
        <input type="password" class="input input_default input_yellow" placeholder="Повторите пароль" name="passwd_confirm">
    </div>
    <span id="success_message" style="color: green;"></span>
    <span id="error_message" style="color: red;"></span>
    <div class="form__item form__item_s-offset">
        <button class="btn btn_large btn_default btn_yellow">
            Продолжить
        </button>
    </div>
</form>

<script>
function set_new_passwd_form_send(event) {
    event.preventDefault();

    var form = $('#set_new_passwd_form');

    var data = form.serialize();

    event.preventDefault();
    $.ajax({
        url: '/ajax/forgot_password.php',
        method: 'get',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.error == false) {
                form_show_success(form, 'Ваш пароль успешно изменен.');
            } else {
                form_show_error(form, data.errors);

            }
        },
        error: function(data) {
            form_show_error(form, 'Произошла ошибка. Попробуйте пожалуйста позже.');
        }
    });
}
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
