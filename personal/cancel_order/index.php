<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
<?php

global $USER;
 
use Bitrix\Main;
use Bitrix\Sale\Basket;
use Bitrix\Sale;
use Bitrix\Sale\Fuser;
 
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

$order_id = $_GET['ID'];
$order = null;
if ($order_id) {
    $order = \Bitrix\Sale\Order::load($order_id);
}
if ($order) {
    $order_status = get_order_status_for_lk($order->getField('STATUS_ID'), $order->isPaid());
    if ($order_status['CAN_CANCELED']) {
        $order->setField('CANCELED', 'Y');
        $order->save();
    }
} else {
    LocalRedirect('/personal/');
}
?>
<?php
LocalRedirect('/personal/');
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>