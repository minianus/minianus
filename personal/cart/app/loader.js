import $ from "jquery";

export default {
     show(){

        $('body').addClass('preload');
        $('body').append('<img class="preload-pic" src="/upload/preloader-pic.gif" id="preloader">')
    },
    
     hide(){
        $('body').removeClass('preload');
        $('body').remove('#preloader');
    
    }
}