import app from './components/app.vue'
import Vue from "vue"
import store from './store.js'

var application = new Vue({
    el: '#basket_page',
    store,
    components:{
        app
    }
  });

