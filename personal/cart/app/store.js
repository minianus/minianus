import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import loader from "./loader";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    basket_data: {
      TOTAL: {
        ITEMS_PRICE: 0,
        DISCOUNT_PRICE: 0,
        DELIVERY_PRICE: 0,
        BONUSES: 0,
        TOTAL_PRICE: 0,
      },
      DELIVERY_STEP_TO_FREE: 0,
      ITEMS: [],
    },

  },
  mutations: {
    set_basket_data(state, basket_data) {
      state.basket_data = basket_data;
    },
  },
  actions: {
    load_basket_data(context) {
      loader.show();
      axios("/api/handler.php?action=get_basket_data", {
        method: "GET",
      })
        .then((basket_data) => {
          context.commit("set_basket_data", basket_data.data);
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
    remove_item(context, id) {
        loader.show();
      axios("/api/handler.php?action=remove_item&item_id="+id, {
        method: "GET",
      })
        .then((basket_data) => {
          context.commit("set_basket_data", basket_data.data);
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
    inc_item_count(context, id) {
        loader.show();
      axios("/api/handler.php?action=inc_item_count&item_id="+id, {
        method: "GET",
      })
        .then((basket_data) => {
          context.commit("set_basket_data", basket_data.data);
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
    dec_item_count(context, id) {
        loader.show();
        axios("/api/handler.php?action=dec_item_count&item_id="+id, {
          method: "GET",
        })
          .then((basket_data) => {
            context.commit("set_basket_data", basket_data.data);
            loader.hide();
          })
          .catch((error) => {
            console.log(error);
            loader.hide();
          });
    },
  },
  getters: {
    basket_data_total(state) {
      return state.basket_data.TOTAL;
    },
    basket_data_items(state) {
      return state.basket_data.ITEMS;
    },
    get_step_to_free_delivery(state){
      return state.basket_data.DELIVERY_STEP_TO_FREE
    },
    get_total_basket_price(state){
      return state.basket_data.TOTAL.TOTAL_PRICE
    }

  },
});
