<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

?>

<div id="basket_page">
<app></app>
</div>
<?$APPLICATION->IncludeComponent('bitrix:news.list','basket_recomendations',Array(
    'AJAX_MODE' => 'N',
    'IBLOCK_TYPE' => 'catalog',
    'IBLOCK_ID' => get_iblock_id_by_code('menu-devilery'),
    'NEWS_COUNT' => '20',
    'SORT_BY1' => 'SORT',
    'SORT_ORDER1' => 'DESC',
    'SORT_BY2' => 'ID',
    'SORT_ORDER2' => 'ASC',
    'FILTER_NAME' => '',
    'FIELD_CODE' => Array('ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE'),
    'PROPERTY_CODE' => ['SHOT_ABOUT'],
    'CHECK_DATES' => 'Y',
    'DETAIL_URL' => '',
    'PREVIEW_TRUNCATE_LEN' => '',
    'ACTIVE_DATE_FORMAT' => 'd.m.Y',
    'SET_TITLE' => 'N',
    'SET_BROWSER_TITLE' => 'N',
    'SET_META_KEYWORDS' => 'N',
    'SET_META_DESCRIPTION' => 'N',
    'SET_LAST_MODIFIED' => 'Y',
    'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
    'ADD_SECTIONS_CHAIN' => 'N',
    'HIDE_LINK_WHEN_NO_DETAIL' => 'Y',
    'PARENT_SECTION' => '',
    'PARENT_SECTION_CODE' => '',
    'INCLUDE_SUBSECTIONS' => 'Y',
    'CACHE_TYPE' => 'A',
    'CACHE_TIME' => '3600',
    'CACHE_FILTER' => 'Y',
    'CACHE_GROUPS' => 'Y',
    'DISPLAY_TOP_PAGER' => 'N',
    'DISPLAY_BOTTOM_PAGER' => 'N',
    'SET_STATUS_404' => 'N',
    'SHOW_404' => 'N',
    'MESSAGE_404' => '',
    'AJAX_OPTION_JUMP' => 'N',
    'AJAX_OPTION_STYLE' => 'Y',
    'AJAX_OPTION_HISTORY' => 'N',
    'AJAX_OPTION_ADDITIONAL' => ''
)
);?> 


<script  src="script.js"></script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>