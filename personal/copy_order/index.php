<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
<?php

global $USER;
 
use Bitrix\Main;
use Bitrix\Sale\Basket;
use Bitrix\Sale;
use Bitrix\Sale\Fuser;
 
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

$order_id = $_GET['ID'];
$order = null;
if ($order_id) {
    $order = \Bitrix\Sale\Order::load($order_id);
}
if ($order) {
    CSaleBasket::DeleteAll(Fuser::getId());

    $basket = $order->getBasket();
    $basketNew = Basket::create(SITE_ID);
 
    foreach ($basket as $key => $basketItem) {
            $props = [];
            $props_val = $basketItem->getPropertyCollection()->getPropertyValues();
            
            $item = $basketNew->createItem('catalog', $basketItem->getProductId());
            $item->setFields(
                array(
                    'QUANTITY' => $basketItem->getQuantity(),
                    'CURRENCY' => 'RUB',
                    'LID' => SITE_ID,
                    'PRODUCT_PROVIDER_CLASS'=>'\CCatalogProductProvider',
                    
                )
            );
            $new_props = $item->getPropertyCollection();
            foreach($props_val as $prop){
                $new_props->setProperty(array($prop));
            }
            $item->setPropertyCollection($new_props);


    }
    $basketNew->save();

} else {
    LocalRedirect('/personal/');
}
?>
<?php
LocalRedirect('/personal/order/make');
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>