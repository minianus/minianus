<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if ($USER->IsAuthorized()) {
    $filter = array(
        "ID"=> $USER->GetID()
    );
    $rsUser = CUser::GetList(($by="ID"), ($order="ID"), $filter);
    $currUser = $rsUser->Fetch();
}

?>
<?if(!empty($currUser)):?>
<?$APPLICATION->SetTitle("Мой кабинет");?>

<div class="lk-grid">
    <aside class="aside lk-grid__aside">
        <div class="window window_shadow window_bordered aside__window aside__item">
            <div class="js-switch" id="name_edit">
                <div class="js-show">
                    <div class="window__header window__header_sb">
                        <h4 class="h4 window__title js-data">
                            <?if($currUser['NAME']){?>
                                <?=$currUser['NAME']?>
                            <?}else{?>
                                Укажите имя
                            <?}?>
                        </h4>
                        <a href="#" class="edit-link link link_b-bordered gray" onclick="switch_to_edit(event)">
                            Редактировать
                        </a>
                    </div>
                </div>

                <div class="js-edit" style="display: none;">
                    <div class="window__header window__header_sb">
                        <input class="input input_default input_yellow" name="name" value="<?=$currUser['NAME']?>">
                        <a class="edit-link link link_b-bordered gray" style="margin-left: 20px;"
                            onclick="save_name(event)">
                            Сохранить
                        </a>
                    </div>
                    <span class="js-error" style="color: red;"></span>
                </div>
            </div>


            <div class="window__content">
                <?if(\Pomidor\Helpers\BonusHelper::isBonusSystemEnabled()){?>
                <div class="window-sum window__section window__section_offset window-sum_bordered">
                    <div class="key-val window__key-val">
                        <span class="key window__key">
                            Всего </span> <span class="s-large window__val orange medium">
                            <?=\Pomidor\Helpers\BonusHelper::getUserBonuses($currUser['ID'])?> баллов
                        </span>
                    </div>
                    <div class="key-val window__key-val">
                        <span class="key window__key">
                            Баллы с покупок </span> <span class="s-large window__val orange medium">15 баллов</span>
                    </div>
                    <div class="key-val window__key-val">
                        <span class="key window__key"> <a href="#" class="window-sum__title orange">Баллы по акции</a>
                            <span class="lose gray block s-small">сгорят 12.03.2019</span> </span> <span
                            class="s-large window__val orange medium">15</span>
                    </div>
                </div>
                <?}else{?>
                <div class="window-sum window__section window__section_offset window-sum_bordered">
                    В текущем выбранном городе бонусная система недоступна.
                </div>
                <?}?>
                <div class="js-switch" id="phone_edit" style="padding-bottom: 10px;">
                    <div class="phone-w window__section window__section_offset phone-w_offset">
                        <span class="phone-w__label block ">
                            Номер телефона
                        </span>
                        <div class="js-show window__header_sb">
                            <a href="tel:+<?echo $currUser["PERSONAL_PHONE"]?>?>" class="phone-w__tel s-large orange
                                medium
                                block js-data">
                                <?echo $currUser["PERSONAL_PHONE"]?>
                            </a>
                            <a href="#" class="edit-link link link_b-bordered gray" onclick="switch_to_edit(event)">
                                Редактировать
                            </a>
                        </div>
                        <div class="js-edit " style="display: none;">
                            <div class="window__header_sb">
                            <input class="input input_default input_yellow phone-mask" name="phone"
                                    value="<?=$currUser['PERSONAL_PHONE']?>"
                                   >
                                <a class="edit-link link link_b-bordered gray" style="margin-left: 20px;"
                                    onclick="save_phone(event)">
                                    Сохранить
                                </a>
                            </div>
                                

                            <span class="js-error" style="color: red;"></span>
                        </div>
                    </div>
                </div>
                <div class="js-switch" id="email_edit" style="padding-bottom: 10px;">
                    <div class="phone-w window__section window__section_offset phone-w_offset">
                        <span class="phone-w__label block ">
                            Email
                        </span>
                        <div class="js-show window__header_sb">
                            <a href="<?echo $currUser["EMAIL"]?>" class="phone-w__tel s-large orange
                                medium
                                block js-data">
                                <?echo $currUser["EMAIL"]?>
                            </a>
                            <a href="#" class="edit-link link link_b-bordered gray" onclick="switch_to_edit(event)">
                                Редактировать
                            </a>
                        </div>
                        <div class="js-edit " style="display: none;">
                            <div class="window__header_sb">
                            <input class="input input_default input_yellow" name="email"
                                    value="<?=$currUser['EMAIL']?>"
                                   >
                                <a class="edit-link link link_b-bordered gray" style="margin-left: 20px;"
                                    onclick="save_email(event)">
                                    Сохранить
                                </a>
                            </div>
                                

                            <span class="js-error" style="color: red;"></span>
                        </div>
                    </div>
                </div>

                <div class="bonus-w bonus-w_orange">
                    <img class="bonus-w__ico" src="/upload/images/ticket.png" alt="">
                    <span class="bonus-w__title block">
                        Номер бонусной карты
                    </span>
                    <span class="bonus-w__numb s-large block bold">
                        851 7711
                    </span>
                </div>
            </div>
        </div>
        <div class="window window_shadow window_bordered aside__window aside-sale aside__item">
            <span class="flag flag_red">
                До 30.12.2019 </span>
            <div class="window__header">
                <h4 class="h4 window__title">
                    Любимое блюдо </h4>
                <span class="gray s-small">При заказе начисляется 15% баллов</span>
            </div>
            <div class="product-lite">
                <a href="#" class="product-lite__img"> <img src="<?=SITE_TEMPLATE_PATH?>/images/pizza-item-small.png"
                        alt=""> </a>
                <div class="product-lite__content">
                    <a href="#" class="product-lite__title">Санта Лючиа</a> <span class="product-lite__price green">
                        450 Р </span>
                </div>
            </div>
        </div>
        <div class="btn-exit aside__item">
            <a href="/?logout=yes" class=" link link_b-bordered">
                Выйти </a>
        </div>
    </aside>
    <div class="lk-grid__content js_tabs">
        <div class="line-filter line-filter_offset">
            <div class="menu__tabs menu__tabs_light js_tabs_selector">
                <a class="menu__tabs-item active" href="#!">РЕДАКТИРОВАТЬ ДАННЫЕ </a>
                <a class="menu__tabs-item" href="#!">ИСТОРИЯ ЗАКАЗОВ </a>
                <a class="menu__tabs-item" href="#!">ПОЛЕЗНАЯ ИНФОРМАЦИЯ </a>
            </div>
        </div>
        <div class="js_tabs_items">
            <div class="js_tab">

                <div class="window window_bordered window_s-offset aside__item">
                    <div class="window__header window__header_sb">
                        <h3 class="s-large window__title">
                            Адреса доставки </h3>
                    </div>
                    <hr class="w-total__hr">
                    <?
                    $address_list = get_saved_user_address_list();
                    ?>
                    <div class="window__content window__content_offset">
                        <div class="addr-list mb-2">
                            <?foreach($address_list as $id => $address){?>
                            <div class="addr-list__item ">
                                <div class="addr-list__header">
                                    <p>
                                        <?=$address?>
                                    </p>
                                    <div class="addr-list__nav">
                                        <a href="#target-toggle" class="edit-link addr-list__link orange s-small">
                                            Изменить </a>
                                        <a href="#!" class="addr-list__link gray s-small" data-id="<?=$id?>"
                                            onclick="remove_saved_addr(event)">
                                            Удалить </a>
                                    </div>
                                </div>
                            </div>
                            <?}?>
                        </div>
                        <div class="locate-w">
                            <form action="#" id="target-toggle" class="form edit-form" style="display: none;">
                                <input type="hidden" name="address_id" value="">
                                <div class="row">
                                    <div class="col col--lg-4">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Улица, дом"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Подъезд"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Домофон"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Этаж"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Кв./офис"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn_default btn_yellow">
                                    Сохранить адрес </a>
                            </form>

                        </div>
                        <div class="locate-w">
                            <form class="form edit-form" style="display: block;" id="address_add_form">
                                <div class="row">
                                    <div class="col col--lg-4">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Улица, дом" name="street"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Подъезд" name="section"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Домофон" name="flat_call"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Этаж" name="floor"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                    <div class="col col--xs-6 col--lg-2">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Кв./офис" name="flat"
                                                class="input input_default input_yellow">
                                        </div>
                                    </div>
                                </div>
                                <a href="#!" class="btn btn_default btn_yellow" onclick="address_add_form_send(event)">
                                    Добавить адрес </a>
                            </form>

                        </div>

                    </div>
                </div>
                <div class="box">
                    <div class="row">
                        <div class="col col--lg-6">
                            <div class="window window_bordered window_s-offset window_b-offset match-height">
                                <img class="ticket-ico window__ticket-ico"
                                    src="<?=SITE_TEMPLATE_PATH?>/images/lock1.svg" alt="" />
                                <div class="window__header window__header_sb">
                                    <h3 class="s-large window__title">Изменить пароль</h3>
                                </div>
                                <hr class="w-total__hr" />
                                <div class="window__content window__content_offset">
                                    <form action="#" class="form form_default" onsubmit="save_password(event)" id="password_edit">
                                        <div class="form__item form__item_s-offset">
                                            <input type="password" placeholder="Введите новый пароль"
                                                class="input input_default input_yellow" name="password"/>
                                        </div>
                                        <div class="form__item form__item_s-offset">
                                            <input type="password" placeholder="Подтвердите новый пароль"
                                                class="input input_default input_yellow" name="confirm_password"/>
                                        </div>
                                        <div class="form__item form__item_s-offset">
                                            <button class="btn btn_full btn_default btn_yellow">
                                                Сохранить
                                            </button>
                                        </div>
                                        <span class="js-error" style="color: red;"></span>
                                        <span class="js-success" style="color: green;"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col col--lg-6">
                            <div class="window window_bordered window_s-offset window_b-offset match-height">
                                <img class="ticket-ico window__ticket-ico"
                                    src="<?=SITE_TEMPLATE_PATH?>/images/gift1.svg" alt="" />
                                <div class="window__header window__header_sb">
                                    <h3 class="s-large window__title">Введите дату рождения</h3>
                                </div>
                                <hr class="w-total__hr" />
                                <div class="window__content window__content_offset">
                                    <div class="window__line window__line_offset gray normal-lh">
                                        <p>
                                            Специально ко дню рождения мы делаем подарки нашим покупателям
                                        </p>
                                    </div>
                                    <form action="#" class="form form_default" onsubmit="save_birthday(event)" id="birthday_edit">
                                        <div class="form__item form__item_s-offset">
                                            <input type="text" placeholder="Число.Месяц.Год"
                                                class="input input_default input_yellow js-datepicker" name="birthday"/>
                                        </div>
                                        <div class="form__item form__item_s-offset">
                                            <button class="btn btn_full btn_default btn_yellow">
                                                Сохранить
                                            </button>
                                        </div>
                                        <span class="js-error" style="color: red;"></span>
                                        <span class="js-success" style="color: green;"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js_tab" style="display: none;">
                <div class="window window_bordered window_s-offset aside__item ">
                    <?$APPLICATION->IncludeComponent(
                    "bitrix:sale.personal.order.list",
                    "pomidor_order_list",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_TYPE" => "A",
                        "COMPONENT_TEMPLATE" => ".default",
                        "DEFAULT_SORT" => "STATUS",
                        "DISALLOW_CANCEL" => "N",
                        "HISTORIC_STATUSES" => array(0=>"F",),
                        "ID" => $ID,
                        "NAV_TEMPLATE" => "",
                        "ORDERS_PER_PAGE" => "20",
                        "PATH_TO_BASKET" => "/personal/cart",
                        "PATH_TO_CANCEL" => "cancel/#ID#",
                        "PATH_TO_CATALOG" => "/catalog/",
                        "PATH_TO_COPY" => "/about/contacts",
                        "PATH_TO_DETAIL" => "/personal/orders/#ID#",
                        "PATH_TO_PAYMENT" => "payment.php",
                        "REFRESH_PRICES" => "N",
                        "RESTRICT_CHANGE_PAYSYSTEM" => array(0=>"0",),
                        "SAVE_IN_SESSION" => "Y",
                        "SET_TITLE" => "N",
                        "STATUS_COLOR_F" => "gray",
                        "STATUS_COLOR_N" => "green",
                        "STATUS_COLOR_P" => "yellow",
                        "STATUS_COLOR_PSEUDO_CANCELLED" => "red"
                    )
                );?>

                </div>

            </div>
            <div class="js_tab" style="display: none;">

                <div class="row row_stretch">
                    <div class="col col--md-6">
                        <div class="window window_dashed window_s-offset aside__item">
                            <div class="window__header window__header_sb">
                                <h3 class="s-large window__title">
                                    Правила участия </h3>
                            </div>
                            <hr class="w-total__hr">
                            <div class="window__content window__content_offset">
                                <div class="window__line window__line_offset">
                                    <p>
                                        Узнать больше о бонусной программе
                                    </p>
                                </div>
                                <a href="/loyalty-program/" class="btn btn_default btn_yellow">
                                    Подробнее </a>
                            </div>
                        </div>
                    </div>
                    <div class="col col--md-6">
                        <div class="window window_dashed window_s-offset ">
                            <div class="window__header window__header_sb">
                                <h3 class="s-large window__title">
                                    Остались вопросы? </h3>
                            </div>
                            <hr class="w-total__hr">
                            <div class="window__content window__content_offset">
                                <div class="window__line window__line_offset">
                                    <p>
                                        Возможно вы найдете ответы здесь
                                    </p>
                                </div>
                                <a href="/loyalty-program/#faq" class="btn btn_default btn_yellow">
                                    Вопрос/ответ </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</div>
<?else:?>
<?$APPLICATION->IncludeComponent(
        "bitrix:main.auth.form",
        "pomidor_autoreg",
        Array(
            "AUTH_FORGOT_PASSWORD_URL" => "",
            "AUTH_REGISTER_URL" => "/registratsiya.php",
            "AUTH_SUCCESS_URL" => "/personal/"
        )
    );?>
<?endif;?>
<br>
<script>
$(document).ready(function() {
    $('.js_tabs_selector a').on('click', function(event) {
        let index = $(event.target).index();
        let tabs_parent = $(event.target).closest('.js_tabs');
        let tabs_container = tabs_parent.find('.js_tabs_items');
        tabs_container.find('.js_tab').hide();
        tabs_container.find('.js_tab').eq(index).show();

    });
});

function remove_saved_addr(event) {
    event.preventDefault();
    let id = $(event.currentTarget).attr('data-id');
    $.ajax({
        url: '/api/handler.php',
        method: 'get',
        dataType: 'json',
        data: {
            id,
            action: 'remove_user_address'
        },
        success: function(data) {
            location.reload();
        },
        error: function(data) {
            location.reload();
        }
    });

}

function show_invalid_fields(form, errors) {

    form.find('input').removeClass('invalid_field');
    for (error of errors) {
        form.find('input[name="' + error.FIELD + '"]').addClass('invalid_field');
    }
}

function address_add_form_send(event) {
    event.preventDefault();
    let form = $('#address_add_form');
    let data = form.serializeArray();
    data.push({
        name: 'action',
        value: 'save_user_address_action'
    })
    $.ajax({
        url: '/api/handler.php',
        method: 'get',
        dataType: 'json',
        data,
        success: function(data) {
            if (data.ERRORS.length > 0) {
                //console.log(data.ERRORS);
                show_invalid_fields(form, data.ERRORS);
            } else {
                location.reload();
            }

        },
        error: function(data) {
            location.reload();
        }
    });
}

function switch_to_edit(event) {
    event.preventDefault();
    let _switch = $(event.currentTarget).closest('.js-switch');
    let _show_form = _switch.find('.js-show');
    let _edit_form = _switch.find('.js-edit');
    _show_form.hide();
    _edit_form.show();
}

function switch_to_show(id) {
    let _switch = $('#' + id);
    let _show_form = _switch.find('.js-show');
    let _edit_form = _switch.find('.js-edit');
    _show_form.show();
    _edit_form.hide();
}

function api_action(action, data, callback) {
    $.ajax({
        url: '/ajax/lk_api.php',
        method: 'get',
        dataType: 'json',
        data,
        success: function(data) {
            callback(data);
        },
        error: function(data) {

        }
    });
}

function save_name(event) {
    event.preventDefault();
    let name_input = $(event.currentTarget).siblings('input[name="name"]');
    let name = name_input.val();
    let action = 'change_name';
    let data = [{
            name: 'action',
            value: action
        },
        {
            name: 'name',
            value: name
        }
    ];

    api_action(action, data, function(data) {
        let _switch = $('#name_edit');
        if (data.error == false) {

            let _show_form = _switch.find('.js-show');
            let _edit_form = _switch.find('.js-edit');
            let _error = _edit_form.find('.js-error');
            _error.html('');
            let _shower = _show_form.find('.js-data');
            if(data.name){
                _shower.html(data.name);
            }else{
                _shower.html('Укажите имя');
            }
            
            switch_to_show('name_edit');
        } else {
            let _error = _edit_form.find('.js-error');
            _error.html(data.errors.join('<br>'));
        }
    });
}

function save_phone(event) {
    event.preventDefault();
    let phone_input = $(event.currentTarget).siblings('input[name="phone"]');
    let phone = phone_input.val();
    let action = 'change_phone';
    let data = [{
            name: 'action',
            value: action
        },
        {
            name: 'phone',
            value: phone
        }
    ];

    api_action(action, data, function(data) {
        let _switch = $('#phone_edit');
        if (data.error == false) {

            let _show_form = _switch.find('.js-show');
            let _edit_form = _switch.find('.js-edit');
            let _error = _edit_form.find('.js-error');
            _error.html('');
            let _shower = _show_form.find('.js-data');
            _shower.html(data.phone);
            switch_to_show('phone_edit');
        } else {
            let _error = _edit_form.find('.js-error');
            _error.html(data.errors.join('<br>'));
        }
    });
}

function save_email(event) {
    event.preventDefault();
    let email_input = $(event.currentTarget).siblings('input[name="email"]');
    let email = email_input.val();
    let action = 'change_email';
    let data = [{
            name: 'action',
            value: action
        },
        {
            name: 'email',
            value: email
        }
    ];

    api_action(action, data, function(data) {
        let _switch = $('#email_edit');
        let _show_form = _switch.find('.js-show');
        let _edit_form = _switch.find('.js-edit');
        let _error = _edit_form.find('.js-error');
        if (data.error == false) {
            _error.html('');
            let _shower = _show_form.find('.js-data');
            _shower.html(data.email);
            switch_to_show('email_edit');
        } else {
            console.log(_error);
            console.log(data.errors);
            _error.html(data.errors.join());
        }
    });
}

function save_password(event) {
    event.preventDefault();
    let password_input = $(event.currentTarget).find('input[name="password"]');
    let password = password_input.val();
    let confirm_password_input = $(event.currentTarget).find('input[name="password"]');
    let confirm_password = confirm_password_input.val();
    let action = 'change_password';
    let data = [{
            name: 'action',
            value: action
        },
        {
            name: 'password',
            value: password
        },
        {
            name: 'confirm_password',
            value: confirm_password
        },
    ];

    api_action(action, data, function(data) {
        let _password_form = $('#password_edit')
        let _error = _password_form.find('.js-error');
        let _success = _password_form.find('.js-success');
        if (data.error == false) {
            $('#password_edit').trigger("reset");
            _error.html('');
            _success.html('Сохранено успешно!');
        } else {
            _success.html('');
            _error.html(data.errors.join());
        }
    });
}

function save_birthday(event) {
    event.preventDefault();
    let birthday_input = $(event.currentTarget).find('input[name="birthday"]');
    let birthday = birthday_input.val();
    let action = 'change_birthday';
    let data = [{
            name: 'action',
            value: action
        },
        {
            name: 'birthday',
            value: birthday
        },
    ];

    api_action(action, data, function(data) {
        let _birthday_form = $('#birthday_edit')
        let _error = _birthday_form.find('.js-error');
        let _success = _birthday_form.find('.js-success');
        if (data.error == false) {
            $('#birthday_edit').trigger("reset");
            _error.html('');
            _success.html('Сохранено успешно!');
        } else {
            _success.html('');
            _error.html(data.errors.join());
        }
    });
}
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>