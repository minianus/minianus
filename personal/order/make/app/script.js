import app from './components/app.vue';
import Vue from "vue";
import store from './store';

import Modal from "@burhanahmeed/vue-modal-2";
import VueDirectiveMask from '@vuejs-community/vue-directive-mask';



Vue.use(Modal);
Vue.use(VueDirectiveMask);

var application = new Vue({
    el: '#order_form',
    store,
    components:{
        app
    }
  });

