import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import loader from "./loader";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user_state: {
      is_auth: false,
    },
    user_input: {
      delivery: {
        type: "delivery_man",
        cafe: 0,
        user_address: 0,
        address: {
          street: "",
          section: "",
          flat_call: "",
          floor: "",
          flat: "",
        },
        delivery_time_type: "time_moment",
        delivery_time: "",
      },
      promocode: "",
      reciever: {
        phone: "",
        fio: "",
      },
      comment: "",
      paying: {
        method: 0,
      },
      acception_type: "",
    },
    basket_data: {
      TOTAL: {
        items_price: 0,
        discount_price: 0,
        delivery_price: 0,
        bonuses: 0,
        total_price: 0,
      },
      ITEMS: {},
    },
    source_data: {
      cafe_list: [],
      user_saved_address_list: [],
      pay_method_list: [],
      promocodes: [],
    },
    agree: false,
    order_result: {
      number: 0,
      show_popup: false,
      pay_html: "",
    },
    errors: {
      agree: false,
      delivery: false,
      paying: false,
      reciever: false,
    },
  },
  mutations: {
    update_agree(state, agree) {
      state.agree = agree;
    },
    set_source_data(state, source_data) {
      state.source_data.cafe_list = source_data.cafe_list;
      state.source_data.user_saved_address_list =
        source_data.user_saved_address_list;
      state.source_data.pay_method_list = source_data.pay_method_list;

      for (let key in source_data.pay_method_list) {
        if (source_data.pay_method_list[key].default == true) {
          state.user_input.paying.method = source_data.pay_method_list[key].id;
        }
      }

      if (state.source_data.cafe_list.length > 0) {
        state.user_input.delivery.cafe = state.source_data.cafe_list[0].ID;
      }
      if (state.source_data.user_saved_address_list.length > 0) {
        state.user_input.delivery.user_address =
          state.source_data.user_saved_address_list[0].id;
      }

      state.user_state.is_auth = source_data.user.is_auth;
    },
    set_basket_data(state, basket_data) {
      state.basket_data = basket_data;
    },
    update_comment(state, comment) {
      state.user_input.comment = comment;
    },
    update_promocode(state, promocode) {
      state.user_input.promocode = promocode;
    },
    update_delivery_type(state, delivery_type) {
      state.user_input.delivery.type = delivery_type;
    },
    update_delivery_street(state, street) {
      state.user_input.delivery.address.street = street;
    },
    update_delivery_section(state, section) {
      state.user_input.delivery.address.section = section;
    },
    update_delivery_flat_call(state, flat_call) {
      state.user_input.delivery.address.flat_call = flat_call;
    },
    update_delivery_floor(state, floor) {
      state.user_input.delivery.address.floor = floor;
    },
    update_delivery_flat(state, flat) {
      state.user_input.delivery.address.flat = flat;
    },
    update_reciever_phone(state, phone) {
      state.user_input.reciever.phone = phone;
    },
    update_reciever_fio(state, fio) {
      state.user_input.reciever.fio = fio;
    },
    set_user_state(state, user_state) {
      state.user_state = user_state;
    },
    set_order_result(state, order_result) {
      state.order_result.number = order_result.order_id;
      state.order_result.pay_html = order_result.pay_template;
    },
    set_errors(state, errors) {
      state.errors = errors;
    },
    update_delivery_cafe(state, cafe) {
      state.user_input.delivery.cafe = cafe;
    },
    set_pay_systems(state, pay_systems) {
      state.source_data.pay_method_list = pay_systems;
    },
    update_selected_pay_system(state, selected_pay_system) {
      state.user_input.paying.method = selected_pay_system;
    },
    update_delivery_time(state, delivery_time) {
      state.user_input.delivery.delivery_time = delivery_time;
    },
    set_delivery_time_type(state, delivery_time_type) {
      state.user_input.delivery.delivery_time_type = delivery_time_type;
    },
    update_user_address(state, address) {
      state.user_input.delivery.user_address = address;
    },
    clean_errors(state) {
      state.errors.agree = false;
      state.errors.delivery = false;
      state.errors.paying = false;
      state.errors.reciever = false;
    },
    set_errors(state, errors) {
      if (errors.agree) {
        state.errors.agree = true;
      }
      if (errors.delivery) {
        state.errors.delivery = true;
      }
      if (errors.paying) {
        state.errors.paying = true;
      }
      if (errors.reciever) {
        state.errors.reciever = true;
      }
    },
  },
  actions: {
    apply_promocode(context, promocode) {
      loader.show();
      return axios(
        "/api/handler.php?action=apply_promocode&promocode=" + promocode,
        {
          method: "GET",
        }
      )
        .then((result) => {
          context.commit("set_basket_data", result.data.basket_data);
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
    save_user_address(context) {
      loader.show();
      return axios("/api/handler.php?action=save_user_address", {
        method: "GET",
        params: context.getters.user_address,
      })
        .then((result) => {
          context.commit("set_source_data", result.data.source_data);
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
    load_order_data(context) {
      loader.show();
      return axios("/api/handler.php?action=get_order_data", {
        method: "GET",
      })
        .then((result) => {
          context.commit("set_basket_data", result.data.basket_data);
          context.commit("set_user_state", result.data.user);
          context.commit("set_source_data", result.data.source_data);
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
    make_new_order(context, success_popup_callback) {
      //check errors
      let error = false;
      let errors = {};
      if (context.state.agree == false) {
        error = true;
        errors.agree = true;
      }
      if (context.state.user_input.reciever.phone.length == 0) {
        error = true;
        errors.reciever = true;
      }
      if (context.state.user_input.delivery.type == "delivery_man") {
        if (context.state.user_state.is_auth) {
          if(context.state.user_input.delivery.cafe == 0){
            error = true;
            errors.delivery = true;
          }
        } else {
          if (
            context.state.user_input.delivery.address.street.length < 3 ||
            context.state.user_input.delivery.address.flat.length == 0
          ) {
            error = true;
            errors.delivery = true;
          }
        }
      } else if (context.state.user_input.delivery.type == "delivery_self") {
      } else {
        error = true;
        errors.delivery = true;
      }
      if(context.state.user_input.paying.method == 0){
        error = true;
        errors.paying = true;
      }
      context.commit("clean_errors");
      context.commit("set_errors", errors);

      if (error == false) {
        loader.show();
        return axios("/api/handler.php?action=make_new_order", {
          method: "GET",
          params: context.getters.for_order_making,
        })
          .then((result) => {
            console.log(result);
            if (result.data.order_result == true) {
              context.commit("set_order_result", result.data);

              success_popup_callback();
            }
            loader.hide();
          })
          .catch((error) => {
            console.log(error);
            loader.hide();
          });
      }
    },
    get_pay_systems(context) {
      loader.show();
      return axios("/api/handler.php?action=change_delivery_type", {
        method: "GET",
        params: {
          delivery_type: context.getters.delivery_type,
        },
      })
        .then((result) => {
          console.log(result);
          if (result.data.ERRORS.length == 0) {
            context.commit("set_pay_systems", result.data.pay_systems);
          }
          loader.hide();
        })
        .catch((error) => {
          console.log(error);
          loader.hide();
        });
    },
  },
  getters: {
    errors(state) {
      return state.errors;
    },
    basket_data_total(state) {
      return state.basket_data.TOTAL;
    },
    basket_data_items(state) {
      return state.basket_data.ITEMS;
    },
    for_order_making(state) {
      let data = {
        phone: state.user_input.reciever.phone,
        fio: state.user_input.reciever.fio,
        comment: state.user_input.comment,
        paying_method: state.user_input.paying.method,
        delivery_time_type: state.user_input.delivery.delivery_time_type,
        delivery_time: state.user_input.delivery.delivery_time,
        delivery_type: state.user_input.delivery.type,
      };

      if (state.user_input.delivery.delivery_type == "delivery_man") {
        data.selected_delivery = state.user_input.delivery.user_address;
      } else {
        if (state.user_state.is_auth) {
          data.selected_delivery = state.user_input.delivery.cafe;
        } else {
          data.delivery_address = state.user_input.delivery.address;
        }
      }
      data.address = state.user_input.address;

      return data;
    },
    delivery_type(state) {
      return state.user_input.delivery.type;
    },
    cafe_list(state) {
      return state.source_data.cafe_list;
    },
    user_address(state) {
      return state.user_input.delivery.address;
    },
    user_saved_address_list(state) {
      return state.source_data.user_saved_address_list;
    },
    user_is_auth(state) {
      return state.user_state.is_auth;
    },
    pay_systems(state) {
      return state.source_data.pay_method_list;
    },
  },
});
