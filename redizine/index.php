
<!DOCTYPE html>
<html xml:lang="ru" lang="ru">

<head>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;600;700;800;900&amp;display=swap"
        rel="stylesheet">
    <title>
        Сеньор Помидор    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="cmsmagazine" content="cd4d311df3276962226d4c5d3b0c994d" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="index, follow" />
<meta name="keywords" content="Синьор Помидор" />
<meta name="description" content="Синьор Помидор" />
<link href="/bitrix/js/main/core/css/core.css?16172195213963" type="text/css" rel="stylesheet" />

<script type="text/javascript" data-skip-moving="true">(function(w, d, n) {var cl = "bx-core";var ht = d.documentElement;var htc = ht ? ht.className : undefined;if (htc === undefined || htc.indexOf(cl) !== -1){return;}var ua = n.userAgent;if (/(iPad;)|(iPhone;)/i.test(ua)){cl += " bx-ios";}else if (/Android/i.test(ua)){cl += " bx-android";}cl += (/(ipad|iphone|android|mobile|touch)/i.test(ua) ? " bx-touch" : " bx-no-touch");cl += w.devicePixelRatio && w.devicePixelRatio >= 2? " bx-retina": " bx-no-retina";var ieVersion = -1;if (/AppleWebKit/.test(ua)){cl += " bx-chrome";}else if ((ieVersion = getIeVersion()) > 0){cl += " bx-ie bx-ie" + ieVersion;if (ieVersion > 7 && ieVersion < 10 && !isDoctype()){cl += " bx-quirks";}}else if (/Opera/.test(ua)){cl += " bx-opera";}else if (/Gecko/.test(ua)){cl += " bx-firefox";}if (/Macintosh/i.test(ua)){cl += " bx-mac";}ht.className = htc ? htc + " " + cl : cl;function isDoctype(){if (d.compatMode){return d.compatMode == "CSS1Compat";}return d.documentElement && d.documentElement.clientHeight;}function getIeVersion(){if (/Opera/i.test(ua) || /Webkit/i.test(ua) || /Firefox/i.test(ua) || /Chrome/i.test(ua)){return -1;}var rv = -1;if (!!(w.MSStream) && !(w.ActiveXObject) && ("ActiveXObject" in w)){rv = 11;}else if (!!d.documentMode && d.documentMode >= 10){rv = 10;}else if (!!d.documentMode && d.documentMode >= 9){rv = 9;}else if (d.attachEvent && !/Opera/.test(ua)){rv = 8;}if (rv == -1 || rv == 8){var re;if (n.appName == "Microsoft Internet Explorer"){re = new RegExp("MSIE ([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}else if (n.appName == "Netscape"){rv = 11;re = new RegExp("Trident/.*rv:([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}}return rv;}})(window, document, navigator);</script>


<link href="/bitrix/js/ui/fonts/opensans/ui.font.opensans.css?16172195202003" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/popup/dist/main.popup.bundle.css?161721952126339" type="text/css"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/news.list/Banner_main/style.css?1617219572150" type="text/css"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/news.list/address_slider/style.css?1617219572150" type="text/css"  rel="stylesheet" />
<link href="/local/templates/pomidor/styles.css?1617219572175798" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/menu/main_menu/style.css?161721957211894" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/catalog.section.list/menu_razdel/style.css?16172195727151" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/sale.basket.basket.line/bootstrap_v5/style.css?16172195724718" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/menu/footer_menu/style.css?1617219572581" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/components/bitrix/sale.basket.basket.line/templates/bootstrap_v4/style.css?16172195534718" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/aos.css?161721957226053" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/datepicker.min.css?161721957212257" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/magnific-popup.css?16172195727300" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/slick.css?16172195721735" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/variables.css?16172195722763" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/stylesheet.css?161721957284463" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/variables.min.css?16172195722501" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/custom.css?16182236821510" type="text/css"  data-template-style="true"  rel="stylesheet" />







                                    <!--    -->
                                                    
    <style>
    @font-face {
        font-family: 'rub';
        font-weight: normal;
        font-style: normal;

        src: url('/local/templates/pomidor/fonts/rouble.woff') format('woff2'), url('/local/templates/pomidor/fonts/rouble.woff') format('woff');
    }
    </style>
</head>

<body class="bx-background-image bx-theme-blue"     data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
                <div id="panel">
            </div>
        <div class="bx-wrapper" id="bx_eshop_wrap">
        <header class="header">
            <div class="header-des">
                <div class="header-top">
                    <div class="wrap">
                        <div class="header-top__container">
                            <div class="block-left">
                                <a class="header__logo" href="/">
                                    <img src="/upload/images/logo.svg" alt="">                                </a>
                                <div class="header__translate"><a href="index.html#">RU<span
                                            class="header__translate-arrow"></span></a></div>
                                <div class="header__delivery"><span class="header__delivery-icon">
                                        <svg class="icon icon-del">
                                            <svg viewBox="0 0 361.3 240.8" id="icon-del">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path
                                                            d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z">
                                                        </path>
                                                        <path
                                                            d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z">
                                                        </path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </svg>
                                    </span><span>Доставка еды <a class="underline popup-call" href="#popup-city">
                                            

                                        </a></span></div>
                                <div class="header__phone"><span class="header__phone-icon">
                                        <span class="header__phone-icon">
                                            <svg class="icon icon-phone">
                                                <svg viewBox="0 0 480.55 480.56" id="icon-phone">
                                                    <g id="Слой_2" data-name="Слой 2">
                                                        <g id="Capa_1" data-name="Capa 1">
                                                            <path
                                                                d="M365.35 317.9c-15.7-15.5-35.3-15.5-50.9 0-11.9 11.8-23.8 23.6-35.5 35.6-3.2 3.3-5.9 4-9.8 1.8-7.7-4.2-15.9-7.6-23.3-12.2-34.5-21.7-63.4-49.6-89-81-12.7-15.6-24-32.3-31.9-51.1-1.6-3.8-1.3-6.3 1.8-9.4 11.9-11.5 23.5-23.3 35.2-35.1 16.3-16.4 16.3-35.6-.1-52.1-9.3-9.4-18.6-18.6-27.9-28-9.6-9.6-19.1-19.3-28.8-28.8-15.7-15.3-35.3-15.3-50.9.1-12 11.8-23.5 23.9-35.7 35.5-11.3 10.7-17 23.8-18.2 39.1-1.9 24.9 4.2 48.4 12.8 71.3 17.6 47.4 44.4 89.5 76.9 128.1 43.9 52.2 96.3 93.5 157.6 123.3 27.6 13.4 56.2 23.7 87.3 25.4 21.4 1.2 40-4.2 54.9-20.9 10.2-11.4 21.7-21.8 32.5-32.7 16-16.2 16.1-35.8.2-51.8q-28.5-28.65-57.2-57.1zm-19.1-79.7l36.9-6.3A165.63 165.63 0 00243.05 96l-5.2 37.1a128 128 0 01108.4 105.1z">
                                                            </path>
                                                            <path
                                                                d="M404 77.8A272.09 272.09 0 00248 0l-5.2 37.1a237.42 237.42 0 01200.9 194.7l36.9-6.3A274.08 274.08 0 00404 77.8z">
                                                            </path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </svg>
                                        </span>
                                                                                <a class="header__link" href="tel:+74212960606">+7 (4212) 96 06 06</a>

                                </div>
                            </div>
                            <div class="block-right">
                                <a class="header__link" href="/territoriya-dostavki/">Карта доставки </a><a
                                    class="btn header__btn header__btn_del btn_del" href="/menu-devilery/">Заказать
                                    доставку</a>
                                <a class="btn header__btn header__btn_log" href="/personal">

                                    <svg class="icon">
                                        <use xlink:href="http://pomidor/upload/images/icons.svg#icon-prof"></use>
                                        <svg viewBox="0 0 352.28 440.6" id="icon-prof">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <path
                                                    d="M205.6 222.4a113.1 113.1 0 10-56-.1c-60.3 5.4-111.8 54-123.4 91-1.8 5.8-13.5 37.8 1.4 41.4 13.4 3.2 55 6.1 78.4 7.7a14.31 14.31 0 0113.4 14.2v4.4a14.85 14.85 0 01-15.8 14.9c-33.5-2.3-72.2-3-87.9-9.4-4.7-1.9-9.4.2-11.9 5-2.7 5.2-3.8 7.8-3.8 18.9 0 18.4 11.9 30.2 30.3 30.2h291.6c17.5 0 31.6-14.7 30.3-32.2-6.2-83.2-38.8-172.2-146.6-186z"
                                                    id="Layer_1" data-name="Layer 1"></path>
                                            </g>
                                        </svg>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bot">
                    <div class="wrap">
                        <div class="header-bot__container"><a class="btn__reserv popup-call" href="#popup-book"> <span
                                    class="btn__reserv-text">Забронировать стол</span>
                                <div class="btn__reserv-bg">
                                    <svg class="icon reserv-icon">
                                        <use xlink:href="http://pomidor/upload/images/icons.svg#reserv"></use>
                                        <svg viewBox="0 0 341.25 55.69" id="reserv">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <path
                                                    d="M338.35 1.41c.7.6.9 1.1.5 1.8a16.67 16.67 0 01-2.5 3.6 23.29 23.29 0 00-3.4 3.7c-.6 1.8-3.3 5.9-3.7 8-1.4 3.2-1.8 6.4-3 8.8 2.3 2.9 4.6 5.9 6.9 8.9s4.6 10.1 6.8 13c1.5 2.1 1.5 2.4 1 5a3.49 3.49 0 01-2.5.9c-1.6 0-3.2.1-4.8.1-9.6.5-19.2.5-28.8.4-2.4 0 15.1-.2 12.7-.3-2.8-.1-5.5-.1-8.3-.1-3.1-.1-6.2-.1-9.3-.2-4.1-.1-100.2-.3-104.3-.3h-21.1c-4.1 0-8.1 0-12.2.1-3.1 0-6.2 0-9.3.1-3.8.1-27.7.2-31.5.3s-7.9.1-11.8.2-8 .1-12 .2H43a36.17 36.17 0 00-5.7-.1c-1.9.2-3.8 0-5.7.1q-3.15.15-6.3 0l73.7-.2H38.75c-2.2 0-4.5-.1-6.7-.1L3.85 55a31.06 31.06 0 01-3.3-.3c-.7-.7-.7-1.3-.2-1.9 1.6-1.7 3.3-3.3 4.8-5 3.2-3.6 6.8-14.1 9.7-18a8.79 8.79 0 01-.6-1c-2-3-4-10.9-6-13.9-.3-.4-.6-1.8-.9-2.2-1.9-2.5-3.8-4.9-5.7-7.4-.3-.8-.8-1.7-1.2-2.6S.35.5 2.45.3l7.6-.3c7.8-.1 15.6.4 23.5.3 4.4 0 58.7.3 63.1.3 2.9.1-74.3 0-71.4 0 5.4 0 10.9.1 16.3.1 4.4 0 58.7 0 63.1-.1 1.4 0 2.8-.1 4.1-.1 4.5-.1 8.9-.3 13.4-.4 4.9 0 29.8 0 34.7.1h2.9a48.74 48.74 0 015.4.2c2.1.2 4.2.1 6.4.1 4.1 0 8.3.1 12.4-.2 2.2-.1 4.5.2 6.7.2h13.1c5.5 0 90.7-.5 96.2.1.5.1-95.6-.1-95.1-.1 4.8.1 98.6.5 103.4.6 4.3.1 8.5.5 12.7.7 3.9.1 7.9.4 11.8-.2 1.9-.09 3.8-.09 5.6-.19z"
                                                    id="Слой_1-2" data-name="Слой 1"></path>
                                            </g>
                                        </svg>
                                    </svg>
                                </div>
                            </a>

                            	<nav class="header__nav" id="cont_catalog_menu_LkGdQn">
		<ul class="header__nav-list" id="ul_catalog_menu_LkGdQn">
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/about/"
									>
                    						О нас										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col  menu-trigger"
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/menu/"
									>
                                        <svg class="icon icon-tomat">
                        <svg viewBox="0 0 621.93 745.7" id="icon-tomat"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z"></path><path d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z"></path></g></g></svg>
                    </svg>
                        						Меню кафе										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/restaurants/"
									>
                    						Кафе										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/loyalty-program/"
									>
                    						Программа лояльности										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/shares/"
									>
                    						Акции										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/rabota-u-nas/"
									>
                    						Работа у нас										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link  menu-modal"
					href="#cooperation"
									>
                    						Сотрудничество										</a>
							</li>
				</ul>
	</nav>


                            <!--region menu-->

                            
<div class="menu-window ">    <div class="menu-window__wrapp">

                        <a href="/menu/hot-meals/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/4f4/4f4e71527e1182b842dad98ab86a3b7f.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Горячие блюда                       </span>
        </a>
                                              <a href="/menu/desserts/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/1ec/1ecee1f62965e83dccf58674dcd133cd.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Десерты                       </span>
        </a>
                                                                    <a href="/menu/paste/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/65a/65a506ed38e19a197cf348e824d6592b.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Паста                       </span>
        </a>
                                              <a href="/menu/pizza/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/19e/19eab2327c7f27a2cf6cb80470ee31c3.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Пицца                       </span>
        </a>
                                              <a href="/menu/rolls/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/7c1/7c13f00bfd8d29815a5f9b1b895bafb9.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Роллы                       </span>
        </a>
                                              <a href="/menu/salads/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/da0/da0ca150512fe741bd8f8c6fdbdd22f1.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Салаты                       </span>
        </a>
                                              <a href="/menu/soups-ramen/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/315/3158f3faf295ccbc37b9ec8ded336afa.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Супы                       </span>
        </a>
                      
    </div>
</div>
                            <!--endregion-->
                            <div id="bx_basketFKauiI " class="bx-basket bx-opener d-flex align-items-center"><!--'start_frame_cache_bx_basketFKauiI'-->		                <span class="header__price page-nav">0 &#8381;                        </span>


                
			<a class="btn header__btn btn__cart popup-call" href="#popup-cart">
<!--                -->                <svg class="icon icon-cart-h">
                    <svg viewBox="0 0 69.65 73.8" id="icon-cart-h"><g id="Слой_2" data-name="Слой 2"><g id="Layer_1" data-name="Layer 1"><path d="M34.77 3.5c-21.2 0-21.7 25.3-21.7 25.3h43.4S56 3.5 34.77 3.5z" fill="none" stroke-miterlimit="10" stroke-width="7"></path><path d="M68.67 30a4.84 4.84 0 00-4.8-4H5.77A4.84 4.84 0 001 30C.2 34.5-.7 42.4 1 50.2c.1 0 .1 0 .2.1s10.7 3.3 29 4.8c1.7.1 2.9 2.6 2.8 4.2s-1.4 3.8-3 3.8h-.2C17.87 62.2 8.87 61 3.67 59 8 67.7 17 73.8 34.87 73.8c38.2 0 35.9-32.7 33.8-43.8z"></path></g></g></svg>
                </svg>
                                        <span class="cart__count">0</span>
                        
            </a>
			
<!--'end_frame_cache_bx_basketFKauiI'--></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-mobile mobile">
                <div class="mobile-top">
                    <div class="mobile__logo">
                        <img src="/upload/images/logo.svg" alt="">                    </div>
                    <div class="mobile__buttons">
                        <a class="btn header__btn header__btn_log mobile__btn" href="index.html#">
                            <svg class="icon mobile__icon">
                                <svg viewBox="0 0 352.28 440.6" id="icon-prof">
                                    <g id="Слой_2" data-name="Слой 2">
                                        <path
                                            d="M205.6 222.4a113.1 113.1 0 10-56-.1c-60.3 5.4-111.8 54-123.4 91-1.8 5.8-13.5 37.8 1.4 41.4 13.4 3.2 55 6.1 78.4 7.7a14.31 14.31 0 0113.4 14.2v4.4a14.85 14.85 0 01-15.8 14.9c-33.5-2.3-72.2-3-87.9-9.4-4.7-1.9-9.4.2-11.9 5-2.7 5.2-3.8 7.8-3.8 18.9 0 18.4 11.9 30.2 30.3 30.2h291.6c17.5 0 31.6-14.7 30.3-32.2-6.2-83.2-38.8-172.2-146.6-186z"
                                            id="Layer_1" data-name="Layer 1"></path>
                                    </g>
                                </svg>
                            </svg></a>
                        <div id="bx_basketT0kNhm " class="bx-basket bx-opener d-flex align-items-center"><!--'start_frame_cache_bx_basketT0kNhm'-->		                <span class="header__price page-nav">0 &#8381;                        </span>


                
			<a class="btn header__btn btn__cart popup-call" href="#popup-cart">
<!--                -->                <svg class="icon icon-cart-h">
                    <svg viewBox="0 0 69.65 73.8" id="icon-cart-h"><g id="Слой_2" data-name="Слой 2"><g id="Layer_1" data-name="Layer 1"><path d="M34.77 3.5c-21.2 0-21.7 25.3-21.7 25.3h43.4S56 3.5 34.77 3.5z" fill="none" stroke-miterlimit="10" stroke-width="7"></path><path d="M68.67 30a4.84 4.84 0 00-4.8-4H5.77A4.84 4.84 0 001 30C.2 34.5-.7 42.4 1 50.2c.1 0 .1 0 .2.1s10.7 3.3 29 4.8c1.7.1 2.9 2.6 2.8 4.2s-1.4 3.8-3 3.8h-.2C17.87 62.2 8.87 61 3.67 59 8 67.7 17 73.8 34.87 73.8c38.2 0 35.9-32.7 33.8-43.8z"></path></g></g></svg>
                </svg>
                                        <span class="cart__count">0</span>
                        
            </a>
			
<!--'end_frame_cache_bx_basketT0kNhm'--></div>
                        <div class="mobile-burger" href="#"><span></span><span></span><span></span></div>
                    </div>
                </div>
                <div class="mobile-middle">
                    <nav class="mobile__nav">
                        <ul class="mobile__nav-list">
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/about/">О нас</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link header__nav-link-active"
                                    href="/menu/">
                                    <svg class="icon icon-tomat">
                                        <svg viewBox="0 0 621.93 745.7" id="icon-tomat">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <g id="Слой_1-2" data-name="Слой 1">
                                                    <path
                                                        d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z">
                                                    </path>
                                                    <path
                                                        d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z">
                                                    </path>
                                                </g>
                                            </g>
                                        </svg>
                                    </svg>Меню</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/restaurants/">Рестораны</a>
                            </li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/loyalty-program/">Программа
                                    лояльности</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/shares/">Акции</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/rabota-u-nas/">Работа у
                                    нас</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link"
                                    href="/cooperation/">Сотрудничество</a></li>
                        </ul>
                    </nav>
                    <div class="mobile-bot">
                        <div class="mobile-bot__buttons">
                            <div class="mobile-bot__del"><a class="btn header__btn header__btn_del btn_del"
                                    href="index.html#">Заказать доставку</a></div><a class="btn btn__reserv"
                                href="index.html#"> <span class="btn__reserv-text popup-call"
                                    href="#popup-book">Забронировать стол</span>
                                <div class="btn__reserv-bg">
                                    <svg class="icon reserv-icon">
                                        <svg viewBox="0 0 341.25 55.69" id="reserv">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <path
                                                    d="M338.35 1.41c.7.6.9 1.1.5 1.8a16.67 16.67 0 01-2.5 3.6 23.29 23.29 0 00-3.4 3.7c-.6 1.8-3.3 5.9-3.7 8-1.4 3.2-1.8 6.4-3 8.8 2.3 2.9 4.6 5.9 6.9 8.9s4.6 10.1 6.8 13c1.5 2.1 1.5 2.4 1 5a3.49 3.49 0 01-2.5.9c-1.6 0-3.2.1-4.8.1-9.6.5-19.2.5-28.8.4-2.4 0 15.1-.2 12.7-.3-2.8-.1-5.5-.1-8.3-.1-3.1-.1-6.2-.1-9.3-.2-4.1-.1-100.2-.3-104.3-.3h-21.1c-4.1 0-8.1 0-12.2.1-3.1 0-6.2 0-9.3.1-3.8.1-27.7.2-31.5.3s-7.9.1-11.8.2-8 .1-12 .2H43a36.17 36.17 0 00-5.7-.1c-1.9.2-3.8 0-5.7.1q-3.15.15-6.3 0l73.7-.2H38.75c-2.2 0-4.5-.1-6.7-.1L3.85 55a31.06 31.06 0 01-3.3-.3c-.7-.7-.7-1.3-.2-1.9 1.6-1.7 3.3-3.3 4.8-5 3.2-3.6 6.8-14.1 9.7-18a8.79 8.79 0 01-.6-1c-2-3-4-10.9-6-13.9-.3-.4-.6-1.8-.9-2.2-1.9-2.5-3.8-4.9-5.7-7.4-.3-.8-.8-1.7-1.2-2.6S.35.5 2.45.3l7.6-.3c7.8-.1 15.6.4 23.5.3 4.4 0 58.7.3 63.1.3 2.9.1-74.3 0-71.4 0 5.4 0 10.9.1 16.3.1 4.4 0 58.7 0 63.1-.1 1.4 0 2.8-.1 4.1-.1 4.5-.1 8.9-.3 13.4-.4 4.9 0 29.8 0 34.7.1h2.9a48.74 48.74 0 015.4.2c2.1.2 4.2.1 6.4.1 4.1 0 8.3.1 12.4-.2 2.2-.1 4.5.2 6.7.2h13.1c5.5 0 90.7-.5 96.2.1.5.1-95.6-.1-95.1-.1 4.8.1 98.6.5 103.4.6 4.3.1 8.5.5 12.7.7 3.9.1 7.9.4 11.8-.2 1.9-.09 3.8-.09 5.6-.19z"
                                                    id="Слой_1-2" data-name="Слой 1"></path>
                                            </g>
                                        </svg>
                                    </svg>
                                </div>
                            </a>
                        </div>
                        <div class="mobile-bot__info">
                            <div class="mobile-bot__link">
                                <svg class="icon icon-phone">
                                    <svg viewBox="0 0 480.55 480.56" id="icon-phone">
                                        <g id="Слой_2" data-name="Слой 2">
                                            <g id="Capa_1" data-name="Capa 1">
                                                <path
                                                    d="M365.35 317.9c-15.7-15.5-35.3-15.5-50.9 0-11.9 11.8-23.8 23.6-35.5 35.6-3.2 3.3-5.9 4-9.8 1.8-7.7-4.2-15.9-7.6-23.3-12.2-34.5-21.7-63.4-49.6-89-81-12.7-15.6-24-32.3-31.9-51.1-1.6-3.8-1.3-6.3 1.8-9.4 11.9-11.5 23.5-23.3 35.2-35.1 16.3-16.4 16.3-35.6-.1-52.1-9.3-9.4-18.6-18.6-27.9-28-9.6-9.6-19.1-19.3-28.8-28.8-15.7-15.3-35.3-15.3-50.9.1-12 11.8-23.5 23.9-35.7 35.5-11.3 10.7-17 23.8-18.2 39.1-1.9 24.9 4.2 48.4 12.8 71.3 17.6 47.4 44.4 89.5 76.9 128.1 43.9 52.2 96.3 93.5 157.6 123.3 27.6 13.4 56.2 23.7 87.3 25.4 21.4 1.2 40-4.2 54.9-20.9 10.2-11.4 21.7-21.8 32.5-32.7 16-16.2 16.1-35.8.2-51.8q-28.5-28.65-57.2-57.1zm-19.1-79.7l36.9-6.3A165.63 165.63 0 00243.05 96l-5.2 37.1a128 128 0 01108.4 105.1z">
                                                </path>
                                                <path
                                                    d="M404 77.8A272.09 272.09 0 00248 0l-5.2 37.1a237.42 237.42 0 01200.9 194.7l36.9-6.3A274.08 274.08 0 00404 77.8z">
                                                </path>
                                            </g>
                                        </g>
                                    </svg>
                                </svg><a class="header__link" href="index.html#">+7 (421) 296 06 06</a>
                            </div>
                            <div class="mobile-bot__link">
                                <svg class="icon icon-del">
                                    <use xlink:href="http://pomidor/upload/images/icons.svg#icon-del"></use>
                                </svg><span>Доставка еды <a class="underline popup-call"
                                        href="index.html#popup-city">Хабаровск</a></span>
                            </div><a href="index.html#">RU<span class="header__translate-arrow"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main class="main">
            <div class="wrap">
            </div>
            <div class="page">
                <div class="wrap">


                    <div class="row">
                                                                        <div class="bx-content col">
                            <!--region breadcrumb-->
                                                        <!--endregion--></div>
    </div>
    </div>

    <main>
        <section class="banner">
        
<div class="banner__wrap">
    <div class="banner__slider">

	        <div class="news-item" id="bx_3218110189_611">
            <div class="banner__slide">
                <div>
                    <div class="banner__container">
									<div class="banner__leaf banner__leaf-top">
	<img src="/upload/images/leaf-1.png" alt="">
</div>
<div class="banner__leaf banner__leaf-bot">
	<img src="/upload/images/leaf-2.png" alt="">
</div>
<div class="banner__slide-content">
	<div class="banner__slider-item">
		<div class="banner__image">
			<div class="banner__image-wrapper">
				<div class="banner__image-pizza" data-aos="slide-down" data-aos-duration="1000" data-aos-delay="200">
					<img src="/upload/images/pizza.png" alt="">
				</div>
				<div class="banner__image-plate" data-aos="slide-down" data-aos-duration="1000" data-aos-easing="ease-out-cubic">
					<img src="/upload/images/plate.png" alt="">
				</div>
				<div class="banner__price" data-aos="fade" data-aos-duration="500" data-aos-delay="500">
					430
<span>рублей</span>
				</div>
			</div>
		</div>
		<div class="banner__content" data-aos="slide-left" data-aos-duration="1000" data-aos-delay="500">
			<div class="banner__offer">
				<span>спецпредложение</span>
			</div>
			<h2 class="banner__title">Пицца делает жизнь лучше</h2>
			<p class="banner__text">
				Особенно пицца пепперони ветчина и грибы по специальной цене
			</p>
			<div class="banner__buttons">
				<a class="btn btn_del" href="/menu-devilery/">Заказать доставку</a><a class="btn btn_del btn_play" href="#" data-aos="slide-right" data-aos-duration="1000" data-aos-delay="700">
				<div class="icon-play">
					<img src="/upload/images/play.png" alt="">
				</div>
 </a>
			</div>
		</div>
	</div>
</div>					                </div>
                </div>
            </div>
	</div>

	        <div class="news-item" id="bx_3218110189_610">
            <div class="banner__slide">
                <div>
                    <div class="banner__container">
									<div class="banner__leaf banner__leaf-top">
	<img src="/upload/images/leaf-1.png" alt="">
</div>
<div class="banner__leaf banner__leaf-bot">
	<img src="/upload/images/leaf-2.png" alt="">
</div>
<div class="banner__slide-content">
	<div class="banner__slider-item">
		<div class="banner__image">
			<div class="banner__image-wrapper">
				<div class="banner__image-pizza" data-aos="slide-down" data-aos-duration="1000" data-aos-delay="200">
					<img src="/upload/images/pizza.png" alt="">
				</div>
				<div class="banner__image-plate" data-aos="slide-down" data-aos-duration="1000" data-aos-easing="ease-out-cubic">
					<img src="/upload/images/plate.png" alt="">
				</div>
				<div class="banner__price" data-aos="fade" data-aos-duration="500" data-aos-delay="500">
					430
<span>рублей</span>
				</div>
			</div>
		</div>
		<div class="banner__content" data-aos="slide-left" data-aos-duration="1000" data-aos-delay="500">
			<div class="banner__offer">
				<span>спецпредложение</span>
			</div>
			<h2 class="banner__title">Пицца делает жизнь лучше</h2>
			<p class="banner__text">
				Особенно пицца пепперони ветчина и грибы по специальной цене
			</p>
			<div class="banner__buttons">
				<a class="btn btn_del" href="/menu-devilery/">Заказать доставку</a><a class="btn btn_del btn_play" href="#" data-aos="slide-right" data-aos-duration="1000" data-aos-delay="700">
				<div class="icon-play">
					<img src="/upload/images/play.png" alt="">
				</div>
 </a>
			</div>
		</div>
	</div>
</div>					                </div>
                </div>
            </div>
	</div>

</div>
    <div class="slider-nav banner__arrows">
        <div class="slider-arrow banner-prev"><img class="icon-arrow arrow-prev" src="/upload/images/arrow-left.png" alt=""></div>
        <div class="slider-arrow banner-next"><img class="icon-arrow arrow-next" src="/upload/images/arrow-right.png" alt=""></div>
    </div>
</div>

        </section>

        <section class="section menu">
            <div class="wrap">
                <div class="title-wrap title-wrap-mb" data-aos="slide-up" data-aos-duration="2500"><span class="subtitle">лучшая пицца</span>
                    <a href="/menu-devilery/" class="h2 block title">Меню доставки</a>
                    <svg class="icon title-wrap__line">
                        <svg viewBox="0 0 36.6 4.21" id="orange-line"><g id="Слой_2" data-name="Слой 2"><g id="Режим_изоляции" data-name="Режим изоляции" fill="#f7b035"><path d="M36.5 2.71l.1-.2c-.1 0-.1.3-.1.2zm-1 .3v-.1l-.2.2.2-.1zm-.8-.2c0 .4.3 0 .4.2v.1l.1-.3c-.2 0-.4.1-.5 0zm-.3-.9c0 .2-.1.1 0 0zm.1 0c.1.1 0-.1 0 0zm-.4 0l.1-.1c0-.1-.1-.1 0-.1-.2 0-.2.1-.1.2zm0 .09l-.2.1c.1.1.1.2.2-.1-.1.21 0 .11 0 0zm.1.11c0 .1.1.2 0 .3a.15.15 0 000-.3zm.3 1l.1.1.1-.1c-.1.1-.1-.11-.2 0zm-1.1-1.5v.3l.1-.1c-.1 0-.1-.1-.1-.2zm0 .3l-.2.1c.1-.01.2-.01.2-.1zm.4.6l.3-.2zm-.9-1l.1-.2-.2.3.1-.1zm-.3-.1c.1-.1 0-.3.1-.3-.1 0-.2.1-.3.2a.31.31 0 01.2.1zm-.4 0l.1-.1a.1.1 0 01-.1.1zm.8.5s0-.2-.1-.2l.1.2zm.5.4c-.1.1-.3.2-.2.4.1-.2.1-.3.2-.4zm-.5-.1l.1.1c0-.1 0-.1.1-.2zm-.6-.5c-.1.1-.1.1-.2.1.1.1.1.1.2.1s.1-.2 0-.2zm-.9-.1zm.9.3c0 .1.1 0 .1 0zm-.4-.4a.1.1 0 00.1.1.1.1 0 01-.1-.1zm.6.3l-.1.1a.1.1 0 00.1-.1zM31 2.91c0-.2.2-.3.3-.1.2-.1.2-.8.2-1.1 0 .1 0 .2-.2.3 0 .1.2.3.1.5s-.1-.1-.2-.2c0 .3-.3.3-.3.7l.1-.3zm.7-1.2c0-.1 0-.1-.1-.3a.31.31 0 00-.1.2l.2.1zm.3.1c-.1.2-.2.3-.2.5.2 0 .1-.2.3-.2-.3 0 .1-.1-.1-.3h.1l-.1-.1c.1 0 .1 0 0 .1.1-.1 0-.1 0 0v-.1h-.2c0 .1.1.1.2.1zM33 3l.1-.1c.1.1 0 .1-.1.1.1.1.2 0 .1-.2-.1-.1-.2-.4-.1-.4zm-.5-.59l-.2.1v.1c.2 0 .2-.1.2-.2zm-1-.9v-.2l-.1.3.1-.1zm-.3 0v-.2l-.1.1.1.1zm-7-1c-.1-.2-.2-.5-.2-.3v.2a.31.31 0 00.2.1zm-4.8 1.1l.1-.1c-.1 0-.1 0-.1.1zm-.1.9l-.1-.1zM12.7 3l-.1.2c.1.11.1.01.1-.2zm15.6-1.89l-.2-.2c.1.2.1.4.2.2zm-13-.7l-.1.1a.1.1 0 00.1-.1zm12.6.5h.2c-.1-.1-.1-.1-.2 0zM.7 2.41h.1c0-.1 0 0-.1 0zm7.8.3c-.1.1-.1 0 0 0zm14 0v-.1.1zm-.3.1c.1-.3.2-.2.2-.2V2z"></path><path d="M14.1 2.81l.2.2c-.2.3-.3.2-.1.5 0-.7.6-.1.6-.8l.1.1h-.1c.2.3.1-.5.4-.4.1 0 .1.1.1.2l.2-.2c.1 0 0 .1 0 .1l.3-.1c0-.1.1-.3.2-.3s0 .1 0 .2l.2-.2c.1.2-.1.3-.2.3.2.7.1-.2.5.2l-.2.1c.1.4.2.2.4.5 0-.1-.4-.6-.2-.8 0 .1.1.1.2-.1-.2.5-.1.4 0 .4V3c.1-.1.1-.3.2-.4s.2.3.2.5c.2.1-.2-.4.1-.5.2.1.3 0 .5-.2.1.1 0 .2 0 .3l.1-.3v.1c.1-.1 0-.3.2-.4 0-.4.3.5.5.1v.3a1.76 1.76 0 00.4-.5l.2.2a.88.88 0 01.2-.8c0-.1-.1-.2-.1-.3h.3v.2c.1.1 0 .3 0 .5 0-.1-.1-.1-.1-.2a1.27 1.27 0 00-.1.6.1.1 0 01.1-.1v.1l.1-.1v.3l.3.2c.2-.4.4-.9.8-1a1.48 1.48 0 00-.1.7c-.1-.3-.3.1-.5.1.1 0 .1.2 0 .2l.3-.3c0 .3.1.2.1.3 0-.5.3-.3.4-.5 0 .2 0 .3-.2.3.2.4.3-.6.4-.2a.1.1 0 00-.1.1.19.19 0 01.3.1.7.7 0 00.9-.4c0 .1-.1.5-.1.6l.6-1.4c-.1.4 0 1.2-.2 1.4.1.1.2.2.3.1s-.1-.5 0-.6c.1.4.1.2.3.6-.1-.1.1-.6.2-.6s0 .6-.1.6l.3-.5c-.1.1 0 .5 0 .6 0-.1.2 0 .3 0l-.1-.1c.3.1.3-.8.6-.7 0 .1-.1.7 0 .8.1-.3.4-1.4.7-1.6v.2l.2-.2c-.2.4-.6 1.3-.8 1.6.1.1 0 .2.2.2 0 .2-.1.2-.2.2l.4.1c0-.3.3-.3.3-.6l-.4.3c0-.3.3-.9.6-.8.1.1-.1.6-.1.7 0-.1.3-.2.3-.1l-.1.2c.1.1.3-.4.4-.1.1 0 .1.5.2.3a5.84 5.84 0 01.2-1.9 1.63 1.63 0 01.3.6c.1.3-.2.7-.2 1a.35.35 0 01.1-.2c.1.1-.1.3.1.3.1-.3.3.1.1-.3 0-.2 0 0 .2 0a1.23 1.23 0 01.4-.8v.3c.3.1.4.2.7.3 0 .1.1.2 0 .3a.22.22 0 01.2-.2c.1.3-.2 0-.1.4 0-.2.2-.2.4-.3 0-.3-.3-.1-.4 0a1.7 1.7 0 01.6-1c.2-.1.1.3.1.2.4.1.5-.7.8-.3.1.3-.1.5-.1.7-.2.1-.2-.2-.4-.2l.1.1c-.1.1-.2.1-.2 0 .1.3.7.1.9.4.1-.1.2-.2.2-.3h-.1c-.1-.2.2-.3.1-.4.2-.1.3-.3.5-.2V2c0-.1-.1-.2-.2-.1.1-.1.2.5.4.1l-.1-.1c.2-.1.4-.4.5-.3 0-.3 0 0-.1-.3 0 .5-.4-.1-.3.4 0-.4-.2-.2-.3-.6 0 .1 0 .2-.1.1v.3c-.1.2-.3.2-.4.3-.1-.1.1-.1 0-.2l-.2.1c0-.2-.2-.4.1-.5h-.2c-.1-.1-.2-.2-.3-.1-.1-.1-.2.2-.1.1l-.3.1v-.1c-.3-.1-.6.3-.9-.2-.1 0-.2.1-.4 0V.81c-.1 0-.3-.2-.4 0-.1-.5-.6-.2-.8-.3v.3a1.09 1.09 0 00-.5.1v.2c-.2.2-.4-.2-.6-.2 0-.1.1-.1.1 0-.2-.6-.6.2-.8-.2-.1.1-.1 0-.2 0l-.1-.1c0 .1 0 .1.1.1-.1 0-.1 0-.2.1v-.2c-.1-.1-.1.1 0 .3L24 1c0-.4-.2.1-.3-.3-.2.1-.5 0-.5.5v-.1c-.1-.1-.2-.1-.2 0V.81l-.2.4a.45.45 0 010-.5c-.3.3-.2.1-.5.5V1c-.1 0-.2.3-.3.1-.2-.4-.9-.1-1.3-.5.1.6-.2-.3-.2.2 0-.1-.1-.2 0-.3-.2.1-.4-.2-.5.1 0-.1.1-.2 0-.3s-.1.1-.1 0 0-.1.1-.2c-.2 0-.3.4-.3.8-.1-.2-.2-.2-.3.1 0-.1-.1-.3 0-.3-.1 0-.5-.2-.5.1 0-.1-.1 0-.1 0-.2 0-.2-.1-.4 0l.1.1c-.1.4-.2.1-.3.1-.5-.3-.3.1-.7-.2V1c0 .5-.3-.3-.5-.1l.1.1c-.1.2-.3-.7-.4-.8a.1.1 0 01.1-.1c-.3-.3 0 .3-.2.4a.75.75 0 00-.1-.5c-.1-.1-.4.5-.6.2 0 .1.1.3 0 .4s-.4-.5-.5-.1V.41c-.1.1-.4.2-.4.4-.1-.8-.7.4-.7-.3l-.6.2v-.1c-.2 0-.3.2-.4.3s0-.1 0-.2c-.3-.1-.4-.1-.6.3v-.2c-.1.1-.5-.4-.8 0v-.1c-.3.3-.9-.6-1 .3l-.2.2c.3 0 0 .4.1.5-.1.1-.3-.2-.2-.3 0-.5-.4-.1-.4-.4a10.87 10.87 0 01-1.8-.1l.1.4h-.2c-.1-.1-.1-.3 0-.3-.1-.2-.2.2-.2.3-.3-.1.1-.4 0-.4H8c-.1.2-.1.4-.2.4s-.1-.2-.2-.2h-.4l.1.3c.1.1-.1-.1.1.3-.2-.5-1-.1-1.1-.2-.1.2-.3.2-.5.2.1.1.1.4 0 .4 0-.7-.3-.2-.6-.7.2.3-.4.1-.2.6-.1 0 0-.2-.2-.4-.2.3-.7-.1-1.1.1 0 .1.1.2 0 .4l-.2-.5c-.1.1 0 .6-.2.3 0 .1.1.2 0 .2-.9-.3-1.5.7-2.5.1.1.1.1.2 0 .2.1 0 0 .2.1.3l-.4-.3c0 .3-.4.1-.3.4l.1-.1c-.1.5.4.8.5 1.3.1-.4.5.5.7-.1.1.1 0 .2 0 .3 0-.3 0-.3.2-.3V4a2.16 2.16 0 011.1.1l-.1-.3c.1 0 .1.1.2.1 0-.4-.3 0-.3-.4.3.5.7-.1.9.5.2-.1-.1-.3.1-.3l.1.1v-.2c.1 0 .2.2.2.3a.1.1 0 00-.1.1c.2.1.3-.1.4-.1H4c.4-.3.9-.2 1.3-.5v-.1c.2-.3.2.1.5-.1A.22.22 0 016 3c-.1.2.2.3.1.4.3-.3.1-.2.3-.6l.1.2c0-.2 0-.3.2-.4-.1.2.1.3 0 .5.4.3.5-.5.7.1.1-.5-.3-.2-.3-.4s.1-.4.2-.3.3.6.6.6h-.1c.1.1.2-.2.3 0 0-.3.2 0 .1-.4l-.1.1c.1-.1.2-.6.5-.4 0 .1-.1.2-.1.3s.1-.1.2 0c0 .4-.3.1-.4.4s.2-.3.2.1c0-.6.3 0 .4-.4V3l.1-.2v.4c.1-.3.3.2.5 0-.3 0-.1-.3-.3-.4.3-.3.3.6.7.5a.22.22 0 01-.2-.2c.1 0 .2.1.3.2.3-.1-.1-.2 0-.4.1.3.1-.1.2-.2v.2c.3.1 0-.5.3-.4l-.1.3.1-.1V3c.1-.2.2-.2.4-.1a.44.44 0 01.1-.5c.1.1 0 .2.2.2 0 .2-.1.3-.2.1v.2c.1.2.3 0 .3 0h-.1l.3-.4a.14.14 0 010 .2c.1 0 .1-.1.2-.2a.75.75 0 01-.1.5l.3-.2c0 .1.2.2.1.3.2.1.4-.4.6-.3a.1.1 0 01.1-.1c.2 0 .4.3.6 0l.2.5c.2-.1-.2-.4.1-.6s.1.3.2.4c.1-.2.3-.5.5-.3a.31.31 0 01-.2.11zm2.2-.2c0 .1-.1.2-.2.1.1-.3.1-.2.2-.1zM19.7 2c0 .1-.1 0-.1-.1 0 .1 0 .1.1.1zm9.7-.7zm.2.1c-.1.01-.1.11 0 .01zm-22-.3z"></path><path d="M19.3 2.41v-.1c-.1 0-.1 0 0 .1zm6.9.59l-.1-.1a.1.1 0 00.1.1zm4.5-1.79h-.1a.1.1 0 01.1.1zm.8 1.7c.1-.1.1 0 0 0l.1-.2c-.1 0-.1.1-.1.2zM30.7 2c0 .2.1.1.2.1 0-.19-.1-.19-.2-.1zm-.9-.69c0 .1.1-.3.1-.2.1-.2-.3.3-.1.2zm.8 1.4v-.3.3zm-.6-.3l.1.1a.1.1 0 01.1-.1c-.1 0-.2-.1-.2 0zm.1 0zm.3.3l-.1-.1.1.1zm-2-.6h-.2c0 .1 0 .1.1.1s.1 0 .1-.1zm.4.7v-.2l-.1.3zm-.5 0c0 .1-.3.2-.1.2s.3-.1.1-.2zm-2.3-.7h.1v-.5l-.1.5zm-2.2 1c.2.1.1-.3.2-.1.1-.1-.1-.1-.1-.2 0 .1-.1 0-.1.3zM.2 2.21l-.1-.1.1.2v-.1zm-.1.4l-.1-.3v.3zm16.5.6c0-.2 0-.2-.1-.2zm-1.6-.2v-.3.3zm-1.3.4c.1 0 .1-.1.2 0-.2.1-.2-.3-.2 0z"></path></g></g></svg>
                    </svg>
                </div>
                
                
                <div class="menu__container">
                    <div class="menu__tabs" data-aos="fade" data-aos-duration="1000" data-aos-delay="1000">
                        
	
    <div class="menu__tabs-item active">
      <span> Все</span>
    </div>

                        <div class="menu__tabs-item" >
                <span>  Пицца</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Салаты</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Роллы</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Супы и рамен</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Закуски</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Десерты</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Паста</span>
            </div>


                        <div class="menu__tabs-item" >
                <span>  Комбо</span>
            </div>


            

                    </div>
                    
                   
                    
                    <div class="menu__content menu__content_active">
                    <div id="comp_a33f4ccd4918b9b03a947a44157ca975">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_3966226736_682" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="682">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/eaf/285_243_2/eaf5c4b09e5f745e8974c02f88e0a768.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/8c5/285_243_2/8c54917f518c6ad1ca540807a755f30f.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Крем-суп с курицей и грибами</h4>
                <p class="menu-item__desc">
                    Суп на основе мясного бульона и сливок с обжаренными шампиньонами и куриной грудкой, украшенный свежей зеленью                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">270 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=682" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=682&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_674" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="674">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/1ca/285_243_2/1cabfe38274e574edf4bdd296ea72b67.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e8a/285_243_2/e8a664c051ee50d5b47f9fc0206dc898.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Цезарь с курицей Гранд</h4>
                <p class="menu-item__desc">
                    Лист салата, куриная грудка, пармезан, помидоры черри, чесночный сухарики, соус &quot;Цезарь&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">270 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=674" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=674&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_667" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="667">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/578/285_243_2/578cce2239072f5371212432d5416cd7.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/f99/285_243_2/f993835a4900663999c8b4071806d9c4.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Калифорния со снежным крабом</h4>
                <p class="menu-item__desc">
                    Рис, снежный краб &#40;имитация крабового мяса&#41;, огурец, мягкий творожный сыр, водоросли нори, икра масаго. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=667" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=667&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_662" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="662">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/14a/285_243_2/14ab11936e162db69973ba7063c8d47e.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/447/285_243_2/447e968072359f166e634fd9387f85b5.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Карбонара</h4>
                <p class="menu-item__desc">
                    Паста на выбор, бекон, сливочный соус, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=662" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=662&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_661" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="661">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/ec6/285_243_2/ec6608b0ce119766b3d793a1de78f794.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/af1/285_243_2/af1445e97dc98dba2bedc2410094d248.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">2 две пиццы за 659 руб</h4>
                <p class="menu-item__desc">
                    В комбо: пицца Грибная Бьянко, пицца Кампанья                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">659 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=661" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=661&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_657" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="657">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/ae9/285_243_2/ae9d86a4d71ca9aeb1d8d966c77e2631.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a61/285_243_2/a611daeed0e767f547ae354fa4472447.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Картофель фри</h4>
                <p class="menu-item__desc">
                    Подаётся с кетчупом                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">90 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=657" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=657&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_655" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="655">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/9c0/285_243_2/9c0c41af64333edb55808ce9d21df205.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ec7/285_243_2/ec7a724b304c8a96288cedeaac27ea57.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Чизкейк сливочный</h4>
                <p class="menu-item__desc">
                    Сыр Маскарпоне, творог, сливки, клубничный джем, нежный бисквит                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=655" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=655&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_644" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="644">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/916/285_243_2/9165d5bac4d65d5398062168672abe06.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ac9/285_243_2/ac97efe301a92891b0a6622dc9e72286.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Палермо</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, свежие шампиньоны, копчёно-варёный окорок, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">390 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=644" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=644&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_686" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="686">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/a44/285_243_2/a44a162eb1ad659f929bb02b1dd7ed85.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/dcb/285_243_2/dcbbbf8eaf9157f4a4a32a55988aaf93.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">3 три пиццы за 969 руб</h4>
                <p class="menu-item__desc">
                    В комбо: пицца Палермо, пицца Кампанья, пицца Три сыра                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">969 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=686" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=686&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_683" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="683">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/15e/285_243_2/15e21162568938141654316a824bd561.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e8c/285_243_2/e8c37bc4fc6cb904d9735f39cc1997e0.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Минестроне</h4>
                <p class="menu-item__desc">
                    Классический итальянский густой овощной суп на основе томатов с цукини, беконом, приправленный фирменным соусом &quot;Песто&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=683" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=683&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_675" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="675">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/5f0/285_243_2/5f01ea84a9f443f6bd784e6e466c3364.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/132/285_243_2/132bf5c614b6f0a9c42585a470fda1f2.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Синьор Помидор с соусом Цезарь</h4>
                <p class="menu-item__desc">
                    Лист салата, пармезан, копчёно-варёный окорок, каперсы, помидоры черри, маринованные в соусе &quot;Песто&quot;, кунжут, соус &quot;Цезарь&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=675" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=675&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_668" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="668">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/058/285_243_2/058e8e2779f59013270f6f8950d2ebcf.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/24e/285_243_2/24e4db218346d3495c42da60487d5fae.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Филадельфия с сыром</h4>
                <p class="menu-item__desc">
                    Рис, лосось слабосолёный, огурец, соус, водоросли нори, мягкий творожный сыр, кунжут. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=668" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=668&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_663" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="663">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/178/285_243_2/178bec861c384bc1a77e99548c2362eb.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/da3/285_243_2/da31bf0df90bae101a7b1120393948fb.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Паста под сливочным соусом &quot;Альфредо&quot;</h4>
                <p class="menu-item__desc">
                    Паста на выбор, сливочный соус Альфредо, свежие шампиньоны, куриная грудка, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=663" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=663&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_658" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="658">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/d32/285_243_2/d327d390dba0cdd2c725860ae39088f3.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a44/285_243_2/a44ed3b8ba9974e7c5b4cf76cf6f1991.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Наггетсы</h4>
                <p class="menu-item__desc">
                                      </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=658" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=658&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_656" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="656">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/61c/285_243_2/61c64b970893ffb0497c664c704ce5d0.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/b3b/285_243_2/b3b40e7bfb3dcaa7893c60c93c10c7cf.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Яблочный десерт</h4>
                <p class="menu-item__desc">
                    Десерт из кисло-сладких яблок, томлённый в янтарном карамельном соусе на песочном тесте                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=656" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=656&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_645" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="645">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/110/285_243_2/11086081b8fafd55d5f2d029a8d850c0.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a0d/285_243_2/a0da43a20380d36a0cef82af5c2892ec.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Кампанья</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, колбаса полукопчёная, помидоры, маслины                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">370 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=645" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=645&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_687" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="687">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/665/285_243_2/6658850ed727c34b1f9a1ec9769e6cfb.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e76/285_243_2/e7605ab899d696282d45f32fcf8ede3b.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">4 на двоих за 699 руб</h4>
                <p class="menu-item__desc">
                    В комбо: пицца Грибная бьянко, два салата &quot;Цезарь&quot; с курицей                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">699 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=687" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=687&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3966226736_684" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="684">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/d9e/285_243_2/d9e8eb8494d555dbd68ae0b5cdf0213b.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ee1/285_243_2/ee1612babf008ed22f1c5330e3cac229.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Суп на курином бульоне</h4>
                <p class="menu-item__desc">
                    Куриная грудка, морковь, петрушка, куриное яйцо                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">120 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=684" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=684&amp;bxajaxid=a33f4ccd4918b9b03a947a44157ca975', 'comp_a33f4ccd4918b9b03a947a44157ca975'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_1387cbbf7756ea6c9045750432f47d17">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_1970176138_644" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="644">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/916/285_243_2/9165d5bac4d65d5398062168672abe06.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ac9/285_243_2/ac97efe301a92891b0a6622dc9e72286.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Палермо</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, свежие шампиньоны, копчёно-варёный окорок, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">390 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=644" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=644&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_645" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="645">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/110/285_243_2/11086081b8fafd55d5f2d029a8d850c0.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a0d/285_243_2/a0da43a20380d36a0cef82af5c2892ec.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Кампанья</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, колбаса полукопчёная, помидоры, маслины                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">370 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=645" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=645&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_646" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="646">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/fdd/285_243_2/fddedb9e0a629471b3d9b28b18fc3ff9.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/69c/285_243_2/69cc694960a7541098d626c6d14b77af.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Пепперони</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, пармезан, колбаса Пепперони                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">380 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=646" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=646&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_647" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="647">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/c65/285_243_2/c65924b7ae723a4830cd1017a5ec81b6.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/88a/285_243_2/88a03bfb74a31a56016d0097a76854d9.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Тоскана Бьянко</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, копчёно-варёный окорок, цветной перец, помидоры, маслины                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">380 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=647" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=647&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_648" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="648">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/a92/285_243_2/a92f484aca3b4dabb9f4444bd3ef531a.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/273/285_243_2/273de0aabf02f621f053745a248398a6.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Грибная Бьянко</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, свежие шампиньоны, душистый чёрный перец                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">390 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=648" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=648&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_649" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="649">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/0fb/285_243_2/0fbfec89540d25d51332dd7d3852b94b.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/aa2/285_243_2/aa24e8b005247475792f315a07c63c82.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Кватростаджиони Бьянко/четыре сезона белая</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, свежие шампиньоны, копчёно-варёный окорок, курица копчёная, цветной перец, лук маринованный, каперсы                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">430 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=649" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=649&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_650" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="650">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/ef1/285_243_2/ef1e41e8272482c2a21f92486b9ce7c6.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/946/285_243_2/946b4307f587f9c05c3a3bb3109430c2.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Капричиоза</h4>
                <p class="menu-item__desc">
                    Неаполитанский соус, моцарелла, помидоры, копчёно-варёный окорок, шампиньоны свежие, маслины                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">380 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=650" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=650&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_651" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="651">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/ba6/285_243_2/ba69c2cc8729b24878764c439358c96b.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/aba/285_243_2/abaa4b35c6024a827e52ae4153e88e3a.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Четыре сыра белый соус</h4>
                <p class="menu-item__desc">
                    Белый соус, оригинальное сочетание сыров: моцарелла, ольтермани, пармезан, сыр с благородной голубой плесенью                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">410 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=651" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=651&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_652" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="652">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/55b/285_243_2/55bc2e3f1a57f49a1e87c71e703beced.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/f9c/285_243_2/f9ce34dee84a15ae1ff18d04f0007074.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Дьябло</h4>
                <p class="menu-item__desc">
                    Неаполитанский соус, моцарелла, колбаса полукопчёная, колбаса Пепперони, курица копчёная, пармезан, жгучий перец Халапеньо                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">450 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=652" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=652&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_653" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="653">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/13a/285_243_2/13a688fd4d65d3d8504124d60ca8f8c4.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/1c9/285_243_2/1c9b24a5c2dc29691073e0ddc43f5d40.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Калабрия</h4>
                <p class="menu-item__desc">
                    Неаполитанский соус, моцарелла, свежие шампиньоны, куриная грудка, маслины                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">390 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=653" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=653&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_689" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="689">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/61e/285_243_2/61eea5a7224d813c15fbf496bcb27576.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/544/285_243_2/544b565aaac0521ce0aca11c9f423c2b.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Карбонара alla pesto</h4>
                <p class="menu-item__desc">
                    Классический итальянский соус &quot;Песто&quot;, бекон, свежие шампиньоны, помидоры черри, перец цветной маринованный, моцарелла                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">430 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=689" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=689&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_690" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="690">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/094/285_243_2/094a377d5fd80137c09012345aa7b311.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/919/285_243_2/919b74d89bb699d47e2f86f89c92409e.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Кватростаджиони Россо/четыре сезона красная</h4>
                <p class="menu-item__desc">
                    Неаполитанский соус, моцарелла, колбаса копчёная, копчёно-варёный окорок, курица копчёная, лук маринованный, помидоры, цветной перец, каперсы                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">430 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=690" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=690&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_691" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="691">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/d65/285_243_2/d65ce62086e01802e34ceb3af26ded96.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/1c7/285_243_2/1c72d27ca6c9d14262f3a4ec194d60c8.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Три сыра</h4>
                <p class="menu-item__desc">
                    Неаполитанский соус, оригинальное сочетание сыров: моцарелла, ольтермани, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=691" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=691&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_692" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="692">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/84f/285_243_2/84f288994b130d4a33ee2bc7c89127be.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/b15/285_243_2/b15c5414fc76d089d95822c6748457ba.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Маргарита</h4>
                <p class="menu-item__desc">
                    Неаполитанский соус, моцарелла, помидоры, базилик                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=692" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=692&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_693" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="693">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/377/285_243_2/3775babdc8a0d1f7efbc6a87dea23305.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/39e/285_243_2/39e30fa6da8337c709f689ed4e99d503.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Гавайская NEW</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, ананасы, куриная грудка, сыр ольтермани, базилик, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=693" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=693&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_694" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="694">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/0e2/285_243_2/0e2174a126852239a1bfb1063e5458f4.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/2fc/285_243_2/2fcba7ba7a01c9ebd4e758030e090190.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Веджи Бьянко NEW</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, руккола, маслины, помидоры черри, цукини, брокколи, цветной перец. Поливается бальзамическим соусом                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">370 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=694" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=694&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_640" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="640">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/126/285_243_2/12693150dd5b3e0dcde3082d68caa060.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/163/285_243_2/163fdf1f679896a6561df75366af6e8e.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Санта Лючия</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, бекон, куриная грудка, перец цветной маринованный, цукини, пармезан, руккола                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">450 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=640" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=640&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1970176138_695" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="695">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/3cb/285_243_2/3cb2e345fc33d068b9186e685fbd4bcc.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/6b2/285_243_2/6b29851594e0cae7d30688a3dbfe362e.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Мирамарэ</h4>
                <p class="menu-item__desc">
                    Белый соус, моцарелла, два вида креветок, кальмары, перец цветной маринованный                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">580 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=695" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=695&amp;bxajaxid=1387cbbf7756ea6c9045750432f47d17', 'comp_1387cbbf7756ea6c9045750432f47d17'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_ac74fdb050e17cff594153f6a4026500">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_40480796_674" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="674">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/1ca/285_243_2/1cabfe38274e574edf4bdd296ea72b67.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e8a/285_243_2/e8a664c051ee50d5b47f9fc0206dc898.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Цезарь с курицей Гранд</h4>
                <p class="menu-item__desc">
                    Лист салата, куриная грудка, пармезан, помидоры черри, чесночный сухарики, соус &quot;Цезарь&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">270 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=674" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=674&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_675" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="675">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/5f0/285_243_2/5f01ea84a9f443f6bd784e6e466c3364.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/132/285_243_2/132bf5c614b6f0a9c42585a470fda1f2.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Синьор Помидор с соусом Цезарь</h4>
                <p class="menu-item__desc">
                    Лист салата, пармезан, копчёно-варёный окорок, каперсы, помидоры черри, маринованные в соусе &quot;Песто&quot;, кунжут, соус &quot;Цезарь&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=675" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=675&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_676" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="676">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/fe2/285_243_2/fe2e07523bc2b3b314aaaed2ac374cbb.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/35b/285_243_2/35b3bac8855890b04601cedde09c3ecb.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Синьор Помидор с соусом  Адарим</h4>
                <p class="menu-item__desc">
                    Лист салата, пармезан, копчёно-варёный окорок, каперсы, помидоры черри, маринованные в соусе &quot;Песто&quot;, кунжут, соус &quot;Адарим&quot;                   </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=676" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=676&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_681" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="681">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/4d6/285_243_2/4d6314a24b9d95fa2d594860bc80fc68.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/7b5/285_243_2/7b5f54dd99db605d85b0d143d77a6866.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Средиземноморский</h4>
                <p class="menu-item__desc">
                    Лист салата, руккола, креветки, помидоры черри, пармезан, ореховый соус                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">390 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=681" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=681&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_654" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="654">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/2b1/285_243_2/2b1f2f7999e2e9bddcead2cd103c2b5d.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/f2e/285_243_2/f2ecd2ce95fc67be9801a08105fb46ac.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Сицилия</h4>
                <p class="menu-item__desc">
                    Лист салата, куриная грудка, ананасы, шампиньоны, обжаренные, помидоры черри, сыр с благородной голубой плесенью, соус &quot;Цезарь&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=654" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=654&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_677" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="677">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/7e2/285_243_2/7e24fe8abfe3c167ec531c3bb6e84138.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ee7/285_243_2/ee7cd095ceed68bdfd198b50117f239a.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Салат с тунцом</h4>
                <p class="menu-item__desc">
                    Лист салата, тунец консервированный, помидоры черри, яйцо отварное, отварной картофель, корчично- медовый соус                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=677" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=677&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_678" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="678">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/802/285_243_2/8020163d4ca5651fa6ae35cbd3b8ca34.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/714/285_243_2/714a80c112b42953c5cdadf65889a5f1.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Греческий</h4>
                <p class="menu-item__desc">
                    Лист салата, огурец, перец цветной, помидоры черри, маслины, брынза, лук репчатый красный, соус Базилик                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=678" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=678&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_679" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="679">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/c80/285_243_2/c80a197378f4b8ae4324056043a8db19.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/b12/285_243_2/b1252f285537d5e8363cc1402a232952.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Цезарь с креветкой</h4>
                <p class="menu-item__desc">
                    Лист салата, креветки, пармезан, помидоры черри, чесночный сухарики, соус &quot;Цезарь&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">390 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=679" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=679&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_40480796_680" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="680">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/284/285_243_2/284a6da615c265212a6966e577353c1a.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/7a6/285_243_2/7a6adb33c1043bc3437687bb6f8d97ca.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Панчетта</h4>
                <p class="menu-item__desc">
                    Лист салата, бекон, помидоры черри, карамелизированная груша, кедровые орешки, бальзамический соус                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=680" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=680&amp;bxajaxid=ac74fdb050e17cff594153f6a4026500', 'comp_ac74fdb050e17cff594153f6a4026500'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_b5a838c9f6d813ec4c52f223c62d53e3">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_2618107327_667" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="667">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/578/285_243_2/578cce2239072f5371212432d5416cd7.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/f99/285_243_2/f993835a4900663999c8b4071806d9c4.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Калифорния со снежным крабом</h4>
                <p class="menu-item__desc">
                    Рис, снежный краб &#40;имитация крабового мяса&#41;, огурец, мягкий творожный сыр, водоросли нори, икра масаго. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=667" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=667&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2618107327_668" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="668">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/058/285_243_2/058e8e2779f59013270f6f8950d2ebcf.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/24e/285_243_2/24e4db218346d3495c42da60487d5fae.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Филадельфия с сыром</h4>
                <p class="menu-item__desc">
                    Рис, лосось слабосолёный, огурец, соус, водоросли нори, мягкий творожный сыр, кунжут. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=668" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=668&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2618107327_669" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="669">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/016/285_243_2/016052dc62a4e5880ce633207c57e3f1.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/0b6/285_243_2/0b611b2a3bf914fc966550a893c7aea6.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Унаги</h4>
                <p class="menu-item__desc">
                    Рис, угорь копчёный, огурец, соус &quot;Терияки&quot;, водоросли нори, кунжут. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=669" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=669&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2618107327_670" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="670">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/527/285_243_2/5277fcb1de5fc1b97e4aef94c9fe7a47.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a1c/285_243_2/a1cdc44a35eebd14fc98b8afacf46fac.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Крабовый с семгой</h4>
                <p class="menu-item__desc">
                    Рис, снежный краб &#40;имитация крабового мяса&#41;, сёмга слабосолёная, мягкий творожный сыр, огурец маринованный, водоросли нори. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=670" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=670&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2618107327_671" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="671">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/f91/285_243_2/f91d1ca30fadf808481b7565f9efb98b.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/148/285_243_2/14805dd176f751e1ec59802cb32e96b9.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Филадельфия</h4>
                <p class="menu-item__desc">
                    Рис, лосось слабосолёный, огурец, соус, водоросли нори, кунжут. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">250 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=671" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=671&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2618107327_672" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="672">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/8f1/285_243_2/8f1ea1021b35ec4866109f2df61e974d.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/025/285_243_2/02510f120af159b772f3ce07a467cf81.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Крабовый Спайси</h4>
                <p class="menu-item__desc">
                    Рис, снежный краб &#40;имитация крабового мяса&#41;, омлет, огурец маринованный, водоросли нори, соус &quot;Спайси&quot;. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">260 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=672" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=672&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2618107327_673" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="673">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/2d1/285_243_2/2d1bd64b9f1327fc6487bf47b0f3aa22.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/c90/285_243_2/c90438b5036edc4e698627dda1311891.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Дракон с семгой</h4>
                <p class="menu-item__desc">
                    Рис, угорь копчёный, водоросли нори, сёмга слабосолёная, мягкий творожный сыр, огурец, соус &quot;Унаги&quot;, кунжут. Соус соевый, васаби, имбирь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">290 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=673" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=673&amp;bxajaxid=b5a838c9f6d813ec4c52f223c62d53e3', 'comp_b5a838c9f6d813ec4c52f223c62d53e3'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_7581cccff4c21541659f7a72344a6002">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_3943306537_682" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="682">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/eaf/285_243_2/eaf5c4b09e5f745e8974c02f88e0a768.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/8c5/285_243_2/8c54917f518c6ad1ca540807a755f30f.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Крем-суп с курицей и грибами</h4>
                <p class="menu-item__desc">
                    Суп на основе мясного бульона и сливок с обжаренными шампиньонами и куриной грудкой, украшенный свежей зеленью                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">270 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=682" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=682&amp;bxajaxid=7581cccff4c21541659f7a72344a6002', 'comp_7581cccff4c21541659f7a72344a6002'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3943306537_683" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="683">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/15e/285_243_2/15e21162568938141654316a824bd561.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e8c/285_243_2/e8c37bc4fc6cb904d9735f39cc1997e0.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Минестроне</h4>
                <p class="menu-item__desc">
                    Классический итальянский густой овощной суп на основе томатов с цукини, беконом, приправленный фирменным соусом &quot;Песто&quot;                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=683" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=683&amp;bxajaxid=7581cccff4c21541659f7a72344a6002', 'comp_7581cccff4c21541659f7a72344a6002'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3943306537_684" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="684">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/d9e/285_243_2/d9e8eb8494d555dbd68ae0b5cdf0213b.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ee1/285_243_2/ee1612babf008ed22f1c5330e3cac229.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Суп на курином бульоне</h4>
                <p class="menu-item__desc">
                    Куриная грудка, морковь, петрушка, куриное яйцо                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">120 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=684" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=684&amp;bxajaxid=7581cccff4c21541659f7a72344a6002', 'comp_7581cccff4c21541659f7a72344a6002'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3943306537_685" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="685">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/f30/285_243_2/f30ad443a6510a6100515a4f33c9f844.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/fc1/285_243_2/fc16ff6a97891d115b0100ef6bfcff34.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Сливочно-томатный супчик</h4>
                <p class="menu-item__desc">
                    Суп, приготовленный на основе томатов, цукини и натуральных сливок, украшенный свежей зеленью                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=685" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=685&amp;bxajaxid=7581cccff4c21541659f7a72344a6002', 'comp_7581cccff4c21541659f7a72344a6002'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_b35811aee8e283c375b0b49f21f43e71">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_1912816787_657" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="657">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/ae9/285_243_2/ae9d86a4d71ca9aeb1d8d966c77e2631.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a61/285_243_2/a611daeed0e767f547ae354fa4472447.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Картофель фри</h4>
                <p class="menu-item__desc">
                    Подаётся с кетчупом                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">90 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=657" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=657&amp;bxajaxid=b35811aee8e283c375b0b49f21f43e71', 'comp_b35811aee8e283c375b0b49f21f43e71'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1912816787_658" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="658">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/d32/285_243_2/d327d390dba0cdd2c725860ae39088f3.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/a44/285_243_2/a44ed3b8ba9974e7c5b4cf76cf6f1991.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Наггетсы</h4>
                <p class="menu-item__desc">
                                      </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=658" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=658&amp;bxajaxid=b35811aee8e283c375b0b49f21f43e71', 'comp_b35811aee8e283c375b0b49f21f43e71'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1912816787_659" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="659">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/657/285_243_2/657c92c8b15edadbf122d043580b8f36.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/bab/285_243_2/bab8539f8557ebea5c3b7cbf825efd9d.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Кольца кальмара</h4>
                <p class="menu-item__desc">
                                      </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">270 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=659" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=659&amp;bxajaxid=b35811aee8e283c375b0b49f21f43e71', 'comp_b35811aee8e283c375b0b49f21f43e71'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_1912816787_660" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="660">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/b5e/285_243_2/b5e6305609e099eef522c965f1c74e16.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/dd2/285_243_2/dd215cc5fef5a18fa93bbc6f44b9a01e.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Гренки</h4>
                <p class="menu-item__desc">
                    Подаются с соусом Цезарь                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">90 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=660" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=660&amp;bxajaxid=b35811aee8e283c375b0b49f21f43e71', 'comp_b35811aee8e283c375b0b49f21f43e71'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_eeba5b709c404c27298cc35a3d3acec4">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_84177925_655" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="655">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/9c0/285_243_2/9c0c41af64333edb55808ce9d21df205.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ec7/285_243_2/ec7a724b304c8a96288cedeaac27ea57.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Чизкейк сливочный</h4>
                <p class="menu-item__desc">
                    Сыр Маскарпоне, творог, сливки, клубничный джем, нежный бисквит                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=655" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=655&amp;bxajaxid=eeba5b709c404c27298cc35a3d3acec4', 'comp_eeba5b709c404c27298cc35a3d3acec4'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_84177925_656" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="656">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/61c/285_243_2/61c64b970893ffb0497c664c704ce5d0.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/b3b/285_243_2/b3b40e7bfb3dcaa7893c60c93c10c7cf.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Яблочный десерт</h4>
                <p class="menu-item__desc">
                    Десерт из кисло-сладких яблок, томлённый в янтарном карамельном соусе на песочном тесте                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">190 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=656" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=656&amp;bxajaxid=eeba5b709c404c27298cc35a3d3acec4', 'comp_eeba5b709c404c27298cc35a3d3acec4'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_65911dd20c005794e6a209aabc210cdc">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_2512087444_662" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="662">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/14a/285_243_2/14ab11936e162db69973ba7063c8d47e.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/447/285_243_2/447e968072359f166e634fd9387f85b5.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Карбонара</h4>
                <p class="menu-item__desc">
                    Паста на выбор, бекон, сливочный соус, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=662" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=662&amp;bxajaxid=65911dd20c005794e6a209aabc210cdc', 'comp_65911dd20c005794e6a209aabc210cdc'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2512087444_663" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="663">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/178/285_243_2/178bec861c384bc1a77e99548c2362eb.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/da3/285_243_2/da31bf0df90bae101a7b1120393948fb.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Паста под сливочным соусом &quot;Альфредо&quot;</h4>
                <p class="menu-item__desc">
                    Паста на выбор, сливочный соус Альфредо, свежие шампиньоны, куриная грудка, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=663" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=663&amp;bxajaxid=65911dd20c005794e6a209aabc210cdc', 'comp_65911dd20c005794e6a209aabc210cdc'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2512087444_664" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="664">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/b0f/285_243_2/b0fdbb2bf615e2bcdf0f13ad24fa771f.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/558/285_243_2/558a3212fef8b689ab8cf70c0c0c5d49.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Песто с беконом</h4>
                <p class="menu-item__desc">
                    Паста на выбор, сливочный соус, соус &quot;Песто&quot;, бекон, цукини, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">350 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=664" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=664&amp;bxajaxid=65911dd20c005794e6a209aabc210cdc', 'comp_65911dd20c005794e6a209aabc210cdc'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2512087444_665" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="665">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/349/285_243_2/3498cf606e3347019660cbec1c6e9404.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/454/285_243_2/454f016bdae02fa4fa9dcebf3adbc4a2.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Болоньез</h4>
                <p class="menu-item__desc">
                    Паста на выбор, фирменный томатный соус, фарш из говядины, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">330 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=665" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=665&amp;bxajaxid=65911dd20c005794e6a209aabc210cdc', 'comp_65911dd20c005794e6a209aabc210cdc'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_2512087444_666" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="666">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/1e2/285_243_2/1e2a23866a3e6408853845f6588f18bc.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/77d/285_243_2/77dd72cafea7f0cb9c35ba9762fe7cb1.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">Паста с морепродуктами</h4>
                <p class="menu-item__desc">
                    Паста на выбор, сливочный соус, кальмары, креветки, помидоры черри, зелень, пармезан                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">550 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=666" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=666&amp;bxajaxid=65911dd20c005794e6a209aabc210cdc', 'comp_65911dd20c005794e6a209aabc210cdc'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                    <div class="menu__content ">
                    <div id="comp_39fc56aeb6aadc0d038d7cce50b22fec">
        <div class="menu__list">
		            
            
    <div>
        <div id="bx_3803994370_661" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="661">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/ec6/285_243_2/ec6608b0ce119766b3d793a1de78f794.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/af1/285_243_2/af1445e97dc98dba2bedc2410094d248.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">2 две пиццы за 659 руб</h4>
                <p class="menu-item__desc">
                    В комбо: пицца Грибная Бьянко, пицца Кампанья                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">659 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=661" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=661&amp;bxajaxid=39fc56aeb6aadc0d038d7cce50b22fec', 'comp_39fc56aeb6aadc0d038d7cce50b22fec'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3803994370_686" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="686">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/a44/285_243_2/a44a162eb1ad659f929bb02b1dd7ed85.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/dcb/285_243_2/dcbbbf8eaf9157f4a4a32a55988aaf93.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">3 три пиццы за 969 руб</h4>
                <p class="menu-item__desc">
                    В комбо: пицца Палермо, пицца Кампанья, пицца Три сыра                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">969 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=686" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=686&amp;bxajaxid=39fc56aeb6aadc0d038d7cce50b22fec', 'comp_39fc56aeb6aadc0d038d7cce50b22fec'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3803994370_687" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="687">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/665/285_243_2/6658850ed727c34b1f9a1ec9769e6cfb.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e76/285_243_2/e7605ab899d696282d45f32fcf8ede3b.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">4 на двоих за 699 руб</h4>
                <p class="menu-item__desc">
                    В комбо: пицца Грибная бьянко, два салата &quot;Цезарь&quot; с курицей                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">699 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=687" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=687&amp;bxajaxid=39fc56aeb6aadc0d038d7cce50b22fec', 'comp_39fc56aeb6aadc0d038d7cce50b22fec'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
		            
            
    <div>
        <div id="bx_3803994370_688" class="menu__item menu-item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1500">
            <a href="#card-popup" class="popup-card menu-item__top" data-id="688">
                <img class="menu-item__image" src="/upload/resize_cache/iblock/c9f/285_243_2/c9f6b5f8d44b4630d7c73f0cdad541e2.jpg" alt="">
                <img class="menu-item__image_second" src="/upload/resize_cache/iblock/efe/285_243_2/efedd7fa053e0e6a144fa9f0be5b0150.jpg" alt="">
                                                                <div class="menu-item__icons">
                    <div class="coffee__wrap"><span class="coffee__number">4</span>
                        <svg class="icon coffee__icon">
                            <svg viewBox="0 0 382.46 453.12" id="icon-coffee"><g id="Слой_2" data-name="Слой 2"><path d="M70.66 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM146.43 0c-9.21.51-16.89 7.68-16.89 16.9v106.49c0 10.24 7.68 17.41 16.89 17.41a17 17 0 0016.9-16.9v-107A17 17 0 00146.43 0zm75.78 33.79a17 17 0 00-16.9 16.9v73.73a16.61 16.61 0 0016.9 16.38 17 17 0 0016.89-16.9V50.69a17 17 0 00-16.89-16.9zM306.18 214h-12.29v-36.85A17 17 0 00277 160.26H16.9A17 17 0 000 177.15v169.47a106.42 106.42 0 00106.5 106.5h79.87a106.91 106.91 0 00105-87h14.85a76.47 76.47 0 0076.28-76.29C382 248.32 348.16 214 306.18 214zm0 119.29h-12.29v-85.5h12.8a42.38 42.38 0 0142.49 42.49c-.51 23.58-19.96 43.03-43 43.03z" id="Capa_1" data-name="Capa 1"></path></g></svg>
                        </svg>
                    </div>
                    <svg class="icon icon-del icon-del_menu">
                        <svg viewBox="0 0 361.3 240.8" id="icon-del"><g id="Слой_2" data-name="Слой 2"><g id="Capa_1" data-name="Capa 1"><path d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z"></path><path d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z"></path></g></g></svg>
                    </svg>
                </div>
            </a>
            <div class="menu-item__content">
                <h4 class="menu-item__title">5 роллы за 669 руб</h4>
                <p class="menu-item__desc">
                    В комбо: роллы Филадельфия, роллы Калифорния со снежным крабом, роллы Унаги. Васаби, соевый соус, имбирь.                  </p>
            </div>
            <div class="menu-item__bottom">
                
                                                                                    <span class="menu-item__price">669 &#8381;</span>
                                                    
                                
                                                &nbsp;<a href="/?action=ADD2BASKET&amp;id=688" onclick="BX.ajax.insertToNode('/?action=ADD2BASKET&amp;id=688&amp;bxajaxid=39fc56aeb6aadc0d038d7cce50b22fec', 'comp_39fc56aeb6aadc0d038d7cce50b22fec'); return false;"  class="btn btn_add" rel="nofollow" >В корзину</a>
                    &nbsp;

                
            </div>
        </div>
 </div>
	        </div>


</div>                    </div>
                </div>
            </div>
            <div class="menu__count">
                <svg class="menu__count-bg">
                    <svg viewBox="0 0 65.08 28.6" id="menu-count"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="cls-1" d="M18.28 25.1c.2 0 .3.1.4.1.4-.2-.7 0-.2-.2-.2 0-.3 0-.4.1l.2.2c.4-.1-.2-.1 0-.2zm5.6.1zm36.8 2.9c.1-.2-.1 0-.4-.1l-.3.1a1.63 1.63 0 00.7 0zm3-28a3.4 3.4 0 00-1 .1l.5.2a1.63 1.63 0 01-.7 0c.1-.1-.2-.3 0-.3-1.2.4-.9-.2-1.9 0a14 14 0 01-2.3.8l-.4-.1c.6 0 0-.3.7-.2-.6-.1-1.1.1-1.7 0 .1.1-7.1.2-7.3.2s-.4-.2-.8-.2l.4-.1a.82.82 0 00-.7.1c-.1-.1-.7-.2-.6-.3a5.07 5.07 0 01-1.3.2c.6.2-.1.2-.1.3l.7-.1c-.9-.1.3 0 .2-.3.4.1-.6.4.1.4a11.08 11.08 0 00-1.9.2c.5 0-.1.2.2.2-.7 0-.2-.1-.4-.3-.4 0-.5-.1-.8.1-.6-.1.1-.2-.2-.3.2-.2 1.7.2 1.5-.3.3 0 .7-.2 1.1-.2a2.25 2.25 0 00-1.4-.1v.3c-.4 0-.4.2-.7.2.1-.1-.1-.2-.4-.2.1 0 .2 0 .2.1-.6 0-.6.2-1.3.3.5.1.7.2 1.2.3-.1.2-.4 0-.7.1 0 0 .1 0 0-.1l-.3.1c-.7-.1.1-.3-.5-.4-.4-.2-12.6.1-12.9 0l.1.1h-.1c-.4 0-.7 0-.8.2 0-.1-.3 0-.4.1s-.7.3-1 0c-.4 0-.8-.1-.7-.2l-.7-.1.1-.2c-.7.1-.6.2-1 .4a4 4 0 00-1.6.1h-.3l.3.1c-.5 0-.5.1-.8.1h-.1l-.1-.1c-.4-.2-.6.1-1.1 0h.2c-.2-.1-.6-.4-.7-.2-.1 0-.1 0-.2.1a.31.31 0 01-.2.1h-1L20 1.2c-.2 0-.2 0-.2-.1-.8-.3-1.2.2-2.1-.1-.2.1.2.2.2.3a4.65 4.65 0 01-.9.3c-.4-.2-1.6-.4-1.4-.7-.7 0 .2.2-.1.3h-.1a1.42 1.42 0 01.6.2c.1.2-.6.2-.8.2l.1-.2c-.7.1-.5.4-.9.7-.5-.1-.2-.3-.3-.5l-.5.1v-.5c-.4 0-.4.2-.9.1.4-.2-.7-.2-.2-.4-.3-.1-.4.3-.5 0-.2.2.2.1.4.1.1.3-.6.1-.9.2 0-.1.2-.2-.2-.3a.44.44 0 01-.5.1c0 .2-.4 0-.7 0 .2 0 .6.2.4.3a1.94 1.94 0 01-1.2.3l.3.1c-.3.2-.6-.2-1.1-.1a1.33 1.33 0 01.5-.3 8.11 8.11 0 01-1.1.3l-.4-.2c-.1.1-.5-.1-.5.1-.5-.1.4-.2.5-.3-.7-.1-.5.3-1.2 0l.2-.2c-.2 0-.7.1-.7 0 0 .1.1 0 .2 0-.1.3-1 .2-.9.6h.3c-.4.2-.7.4-1.1.3s.2-.3.4-.3c-.5-.1-.4.3-.9.3.3.1.2.2 0 .3.3 0 .9.1.8-.1h-.1c0-.1.2-.1.3-.2.4 0 .2.2.3.2.7.1.1-.4.7-.3l.1.1c.3-.1.8-.1.8-.3.3.2.7-.1 1 0 .1.5-1.1 0-1.4.2.3.1-.2.3-.3.4L7 2c.1.1.4.3.6.1h-.1c.4-.1.3-.3.7-.2.1 0-.1.1-.1.2.3 0 .3-.1.5-.2s-.2.2.1.3c.4-.2.6 0 .9 0-.1-.3 1.3-.4.6-.6l.8-.2c.2.1-.5.4-.3.6h-.6c.1.4 1.3-.2 1.7.2a3.87 3.87 0 01.9.1l-.1-.3c.3.1.4.2.4.3h.5a.35.35 0 01-.2.1c.2.2.3.1.4 0a.75.75 0 01.5-.1l-.2.1a1 1 0 00.3-.1h.2A1.07 1.07 0 0014 2a10.58 10.58 0 012 .3c0 .1-.3.2-.2.2.3 0 .7-.1.7-.3h.2c.1.1.1.2.3.2a.35.35 0 00.2.1c.1-.3.7-.1.9-.2h-.5c.3-.2.5-.4.7-.2-.5.2.5.3-.2.3.5.3.3-.2.8-.1l.4.1c-.1.2-.6.1-.5.2h.2a8.21 8.21 0 00-1.8.3h1.3c1.4-.2 3.3 0 4.5-.4h-.1v-.1h.1l.2-.1a4.22 4.22 0 001.2.3c-.3-.1-.3-.1-.3-.2s.1-.3.5-.2c0 .1-.2.1.1.2-.2-.1 0-.2.2-.2s.6.1.6.2h-.3c-.1 0 .4.1.1.2h.4c.3.1-.1.2 0 .3.7-.1 1.1-.4 1.7-.5v.3c.5.1 1-.3 1.6-.3a.75.75 0 01-.5.1c1.7.1 15.2.1 16.7.4l.4-.3.5.1c1.7.2 10.1 0 11.7-.1.5 0 .3.2.5.3a20 20 0 002.4-.4c-.1.1-.4.4.1.4.1-.1.1-.2.4-.2l-.1.1c.3.1.5 0 1 .1a9.85 9.85 0 001.7-.2 23.48 23.48 0 01-.12-2.6zm-39.3 1c-.6.1-.4 0-.4-.1.6.1.9 0 1.4.1-.1-.1-.1-.1-.1-.2a.76.76 0 01.5.2c-.4.3-1.7-.1-1.4 0z"></path><path class="cls-1" d="M65.08 25.9a9 9 0 00-1.6.3c.4-.2 0-.3-.3-.4.5 0 .5-.3.5-.4-.3 0-.5.3-.3.4-.5-.1-.3.2-.8 0-.3-.1.1-.2 0-.3-.5 0-.1.3-.7.2 0 0 .1 0 0-.1-.3-.1-.7.1-.7.2-.3-.3-1 0-.9-.3-.8.1.1.2 0 .3a4 4 0 01-1.1 0c.2 0 .4-.1.6 0l-.2-.2c-1-.2-.6.5-1.5.3.5-.1.2-.2.4-.3-.1.1-.9 0-.4.2-.4 0-.6-.1-.8-.1s-7.8.1-8 .3a2.43 2.43 0 01-1.1-.3c-1 .2-2-.1-3.1.1h.1c-.8 0 .4.3-.5.3.1-.3-.5-.2-.8-.3-1.1-.1-14 .2-14.9 0 0-.1.1-.1.3-.1a13.28 13.28 0 00-2 0 11.45 11.45 0 00-3.2.1c.1 0 .1-.1.2-.2a3.93 3.93 0 00-1.9.1c-.7-.3-.4.1-1.3-.2-.1 0 .1 0 .1-.1a4.48 4.48 0 00-1.2-.1 5.87 5.87 0 01-2.3.4c-.1-.4.7-.1.7-.4a1.48 1.48 0 01-.7.1c.2-.1 0-.1.2-.2-.2-.1-.9.4-.9.3 0 .1.1.1.3.1-.3.2-1.1 0-1.6.1.2 0 .7.1.5.3-.7-.1-1 .3-1.5.4-.7-.2.5-.3.4-.6-.1-.1-.4-.3-.1-.4-.6-.1-1 .2-1.6.2v-.1a16.35 16.35 0 01-2.2.2l.3.1a1.08 1.08 0 00-.7.2c0-.1-.5-.2-.3-.3-.3.4-1.7.2-2.1.6-.2-.1 0-.3 0-.3.5 0 .6.1.9 0-.4 0-.7-.1-.6-.2-.4.2-1.3.3-1.5.6-.3-.1-.6-.2-.5-.4-1.2.1.6.6-.8.6-.6-.2.6-.3-.2-.3h.2c-.6-.5-.4.4-1.1.4-.1-.3.5-.2 0-.4-.6.1-.9.5-1.6.4l.1-.1H2c.1-.1-.2-.3-.1-.3H2c0-.1.1-.2-.1-.4.2.1.1-.2.2-.1.3.4-.2.2-.1.4.1 0 .1.2.2.3v-.3c.1 0 .3.2.2 0 0-.1-.1-.2 0-.3l.1.1c.1 0 0-.1 0-.2s.1.2.2.2c-.1-.2.1-.1.2-.1-.2-.2 0-.5-.3-.6V25c.1.1.2.4.4.5l-.1.1c.3.2.1-.4.4-.2.1 0 0-.1.3 0l-.6-.6c.1 0 .2.1.3.1l.1-.1v.1c.2.1.1 0 .1-.1s0-.1.1-.1v-.1a1.44 1.44 0 01-.3-.2c.2 0 .3-.2.6-.1a.31.31 0 01.1.2c.1 0 .1-.2 0-.3.1 0 .1.1.2.1h.1c-.1-.2.1-.2 0-.3a.1.1 0 00-.1.1c-.1-.2-.1-.4 0-.3s.3.1.2.3c.3.1-.1-.2.1-.2h.2a.14.14 0 010 .2V24c0 .1-.2.2-.2.5l.3-.1c.1-.4.6-.5.6-1l-.1-.1v-.1c.2.1.3 0 .5 0-.1 0-.1 0-.2-.1s-.2-.2 0-.2 0 .1.2.2c-.1 0-.1-.1-.1-.2a.37.37 0 01.3.1h-.1s.2 0 .2.1l.1-.1c.1 0 .1.2.1.2.1-.2-.1-.5 0-.6 0 .1.2.3.2.2s0-.4.1-.5v.1c.4-.2.6-2.4 1.1-2.5l-.1-.3.2.1c.4-.1.5-.5.8-.8-.2-.1-.1.1 0 .1.2-.2 0-.5.2-.7a.45.45 0 00.3.3c0-.1-.1-.2 0-.2v.1c.1.1.1-.1.2-.1a1.25 1.25 0 00.1-.9c.2-.1.2.3.3.3-.2 0 0 .1 0 .2 0-.1 0-.2.2-.1 0-.1.1-.1.1-.3 0 .1-.1 0-.1-.1s.2-.2.4 0a13.07 13.07 0 011.2-1.1c0-.2-.1 0-.1-.1s.1 0 .1-.2l.1.1c.1 0 .1-.1.1-.3s.1-.3.1-.4c.1.1.3 0 .2.2.1-.1.8.2.8.1-.2-.1-.5-.2-.7-.3s.1-.1.1-.1a.75.75 0 00.5-.1c.2.1 0 .2-.1.2.2.2.5-.2.6.1.2-.2-.2 0-.2-.2l.2-.1-.2.1c.1-.2-.1-.3.2-.4a.22.22 0 00-.2-.2c.3.1.2-.1.3-.2l.1.1c.1-.1.2-.1.4-.2.2.1-.2.1 0 .2l.2-.1c-.2 0-.2-.1-.4 0-.2-.2-.4-.2-.1-.5.4-.1.1.1.5.1.3-.2 0-.3-.2-.4s-.3-.1-.4-.1c-.1-.2-.4-.1-.6-.3.3 0 .1-.1.2-.2h.3c-.1 0-.1-.1 0-.1-.2.1-.5-.2-.8-.3l-.1.1c-.2-.1-.4-.1-.5-.2s-.3 0-.6 0c.1-.1.2-.3.4-.4 0 .1.1.1.1.2.3.1.9.1 1.1.3.6-.1-.2-.1-.2-.3h.1a1.48 1.48 0 00.7.1c-.2-.1-.6.1-.6-.2-.3-.1-.1-.3-.2-.4-.3 0-.5.2-.7 0l.3-.1c-.3-.2-.4.1-.6-.1.1.1-.2.1-.1.2a.52.52 0 01-.4-.2l-.1.1c0 .2.3 0 .2.3.1 0 .2-.1.3-.1.1.2-.1.3-.2.3a.1.1 0 01.1.1c-.3.3 0-.2-.3 0h.1c-.2.2-.3 0-.4 0 0-.2-.1-.3 0-.4h.2c0-.2-.2 0-.3-.1v-.2c-.1-.1-.2 0-.2-.1l-.2.3.1-.1c.1 0 .1.2-.1.2s0-.3 0-.4c.2-.2.1.1.2.1.1-.2-.1-.1-.1-.3.2-.2.1.1.3-.1-.1-.1-.2 0-.3-.1.1-.1.2 0 .3-.1v-.1c.2.1.2-.3.4-.3 0-.1-.2.1-.2-.1a.54.54 0 01-.3.4V11c0-.1.3-.1.3-.3h-.2c0 .1 0 .1-.1.2 0-.1-.3.2-.4.2s-.1-.1 0-.2c-.4.1-.5-.1-.8 0a9.34 9.34 0 00.2-2.4c.1.1.3-.1.4-.3-.1 0-.3.1-.3.2 0-.2-.2.1-.2-.1s.2-.2.2-.3-.2.3-.3.2l.1-.1c0-.1-.2-.1-.3 0 .1-.2-.6-1-.4-1.2-.2 0 .3 1.1.2 1.1s-.6-1-.6-1.1h.1l.1-.2c-.1-.3-.4.3-.5 0 .2 0 .1-.1.3-.2-.1 0-.2-.1-.2.1v-.1a.37.37 0 00-.3.1.3.3 0 010-.4c-.3 0-.3-.4-.6-.4-.3-.2-.3.3-.5.1.2-.2 0-.2 0-.4s-.4-.1-.5-.4l.1-.1-.3-.3c-.1-.4-.5-.3-.7-.4l.1-.1c-.1-.2-.2-.4-.4-.2 0-.3-.1 0-.2-.3h.1c-.1-.2-.1-.3-.2-.3-.3 0-.6.2-.7 0 .2-.3.2 0 .4-.2-.1 0-.2 0-.2-.1s0-.1.1-.2-.4.2-.4.1c0 .1-.1.1 0 .1-.2.2-.2-.1-.4-.1a.22.22 0 01-.1.3c0-.2-.3.1-.5.1 0-.2.3-.2.4-.4s.1-.3.2-.3c-.1-.1-.3 0-.4-.1v-.1c-.2-.1-.5 0-.6-.2v.1c-.1 0-.1-.1-.2 0 .1-.1 0-.3.1-.3-.2.4-.3 0-.6.3 0-.1.1-.2.2-.3.1.1.1.2.2.1-.1-.1-.1-.2 0-.2-.1 0-.1.1-.2.1l.1-.1h-.6c0-.1 0-.1.1-.2-.1 0-.1.1-.2.2h-.1s.1 0 0-.1h.1c.2-.5-.3.2-.4.1s.1-.1.1-.3l.7-.1-.7.1c-.1-.1-.4.2-.5 0l.1-.1c-.1 0-.2-.2-.3-.3h.1c-.2-.1-.2.1-.2-.1l-.1.1c0 .1 0 .3.2.3-.1 0-.1.2-.2.3a.35.35 0 01-.1.2L.38 2a3.54 3.54 0 00.3 1.1c.3-.1.2.3.5.3.2-.2.1 0 .3 0a.19.19 0 00-.1.3h.2c0 .1 0 .5.2.3 0 .2-.1.1-.1.1-.1.2.2 0 .1.1a1.38 1.38 0 01.3.4c.2-.2.2 0 .4 0a.14.14 0 000 .2l.2-.1c.2-.1 0 .2.2.2v-.1c.1 0 .2.3.2.5 1.1 0 .8 1.1 1.6 1.5.1 0 .1-.2.2-.2s.1 0 .2.1c0-.1-.1 0 0-.1h-.2v-.1h-.2c.1 0 .3-.2.3-.1h-.1s.1-.1.1 0 .1 0 0 .1l.1.3c.3-.2.3.1.3.2-.2.1.2.8 0 1v.2l.1-.1.1.1c.1 0 .1-.1.1 0v.1h.1c-.2.1 0 .1-.1.2a7.94 7.94 0 01.6 2.2c0-.1.3-.2.3-.1a.1.1 0 00-.1.1h.1c-.1.3.2.2.3.4l-.1.1c.1 0 .2-.1.3 0l-.1.1c0 .2.3-.2.4-.1a.76.76 0 01.1-.4c-.1.2 0 .3.1.2s.1.1.1.1h.1c.1.1-.1.2-.2.3.2-.1.2.1.3.2h-.1l.1.1c.2-.1.2.2.2.3H8c0 .2.1 0 .1-.1.2 0 0 .3 0 .5.3-.1 0 .6.4.4l-.1.1c.4-.1.3.4.8.2-.1.6.4-.3.4.3v-.1l.2.2c.1.1 0 .1.1.1v.1c0 .1 0 .1.1.1h-.1l-.2.2a19.2 19.2 0 01-1.5 1.4c0 .2.2.3.2.4a.14.14 0 01-.2 0 .35.35 0 00-.1-.2c-.1.1-.4.1-.4.3h.2a.1.1 0 01-.1.1c0-.1-.2-.2-.2-.2.1.5-.3 0-.4.3a1.8 1.8 0 01.2 1h-.1c.1-.1-.2-.2 0-.3-.2 0-.2.2-.3.3.1 0 .2.1.1.2s-.2-.1-.3-.1v-.1c-.1-.1-.1.1-.1.2-.1-.1-.2 0-.3-.2 0 .2-.1.2-.1.4.2 0 .1.2.2.3l.1-.2c-.2.1 0-.1-.1-.2.2 0 .2.4.3.3-.1.2-.2.2-.2.5.1-.1.1.1.2.1-.1.1-.1 0-.2-.1-.1.1-.2 0-.1.2-.2 0-.1-.2-.2-.2-.1-.2.5-.1.1-.5.1-.1 0-.3.1-.3-.1 0-.4 0-.4.2l.2.2c-.1.1.1.2 0 .3 0-.1-.2-.1-.2-.1h.1c-.1.1 0 .3 0 .4h.4c.1.2 0 .1-.1.2l-.1-.1v.1c-.2 0-.2-.3-.4-.2-.6.2-.1.9-.3.8 0 .1-.1.1 0 .3-.1 0 0 .1 0 .1s0 .3-.2.2a.19.19 0 01-.3 0l-.2.1-.1-.2c-.1.2 0 .2.1.5-.2 0-.6 1.8-.6 1.9h.1c-.1.1 0 .2-.1.2h-.1c-.2-.1 0 .2-.2.2-.1 0-.4-.2-.2 0v.1c0 .1.1.1 0 .1l-.1.1a.1.1 0 00-.1.1h-.2c-.4-.1-.1.3-.5.3.1.1.2.1.2.2v.4c-.2-.1-.6 0-.7-.3-.1.1.2.1.2.2a.37.37 0 01.3.1.14.14 0 010 .2l-.1-.2c-.1.2.2.4.3.6-.2 0-.2-.2-.4-.3l-.1.1-.1-.1c0 .1.1.2-.1.3 0-.2-.2 0-.3-.2-.1 0 .1.3-.1.1.1.2.1 0 .2 0 .2.2 0 .2 0 .3-.1-.1-.1-.2-.2-.2.1.1.1.2-.1.1.1.1-.1.1-.1.1s.2 0 .3.2 0 .1-.1.2h.1c.1.3-.2 0-.3.1 0-.1 0-.1-.1-.3v.4l-.2-.1c.1.1-.1.1 0 .2-.2 0-.1-.2-.1-.3-.2 0 .1.3-.2.2l-.1-.2s-.1.2-.1.1h.1c.2.4 0 .5.2.7h.1v.3h-.1a.37.37 0 01-.1-.3c-.1 0 0 .2.1.3-.4 0-.2.1-.7 0l-.2.1a1.48 1.48 0 00.7.1c0 .1.1.2 0 .2.1.1.2-.2.4 0 .2 0-.1-.1-.1-.2h.1a.1.1 0 00.1.1c.1 0 .2-.1.1-.1h.1c-.5.1.5.3.3.6l.1-.1a12.89 12.89 0 003.6.6c.8-.3 1.4.1 2.3-.1.1-.3.2-.1.9-.1-.4 0 0 .3.2.3a.75.75 0 01.5-.1c.2.1 1.1.4 1.4.2.5.1-.1.1-.2.2.2.2.4-.1.5 0l-.1.1c.7 0 1.1.1 1.8.1.1-.3.7-.1 1-.2-.1.1.4.1.6.2l.4-.2c.4-.2.5.1 1 0l-.1-.1a3.42 3.42 0 011.7.3c3.4-.6 5.2.4 8.8.2.2-.1-.2-.2 0-.3h.8c-.3-.1-.2 0-.2-.1l-.5.1v-.1c-.2 0-.4.1-.6.1.3-.1.4-.3.8-.2a.31.31 0 00-.2.1c.4 0 .4-.1.6 0s.2 0 .3.1l1 .1c.5-.3 1.1 0 1.3 0-.1.2-.7.1-.9.3l.4.2v-.2h.5c.1-.1.1-.1.3-.1 0 0 .1.1 0 .1l.4-.1c-.2.2.3.1.3.3 1.2.1 13.6-.3 14.7 0-.2-.1.3-.3.7-.3.2.1.2.1.1.2l.4-.1c.5.3 1.1.1 1.8.2v.1a3 3 0 011-.2l-.1-.1c.5.1 7.6-.4 8.2-.3 0-.2-.4-.3-.4-.5.1.2.8.2.6.1.6-.1.4 0 .5.1l.3-.1c.4 0 .1.3 0 .4a1.66 1.66 0 011.2 0l-.2.1h.6a1.7 1.7 0 011.4-.2l-.3.3c.3.1.2-.1.3-.1a.9.9 0 01.9.2c.9-.3 1.7.4 2.4 0l.1.2zm-60.6-3.8c.2 0 .5-1.8.6-1.8L5 20.2a.31.31 0 01.2.1c.1.2-.7 1.7-.5 1.8-.12.2-.22.1-.22 0zm-1 2.7z"></path><path class="cls-1" d="M3.68 22.9c0 .1.1.1 0 0zm-2.4 3.5a.1.1 0 01-.1-.1c.1 0 .1 0 .1.1zm.3-.2c0-.1-.1-.1 0 0v-.1c.1-.1.1.1.2.1zm62.9-.9c-.1.1-.5 0-.6-.1l.1.1c.2.1.5.2.5 0zm-5.4 3h.2c.1-.1 0-.1 0-.2zm-.7-.3l.1-.1-.5.2a3.09 3.09 0 001.1-.2c-.2 0-.5.2-.7.1zm-28.8-.4a7.08 7.08 0 01-1.5.1c.1.1-.3.3-.1.2l.6-.1-.1.1c.5.1.3-.2.8-.1 0-.2-.1-.2.3-.2zM3.28 26c-.4.1.2.2.1.2s-.1-.1-.1-.2zM1 26.8c.2 0 .3-.1.1-.3-.02.2-.22.1-.1.3zm-.22-.3l.1.1v-.2l-.1.1zm0 0v-.2l-.1.2h.1zm5.8-9c.1 0 .1-.1 0 0zm-3.7 7.8c.1 0 .3.3 0 0 .2 0 .1-.1.2-.2L3 25h-.1c-.12.1-.02.2-.02.3zm1.7-22.1c.1 0 0 .1 0 0 .2 0-.1 0 .1-.1-.1 0-.1 0-.1.1l-.1.1c.1 0 0 0 .1-.1zM11 11.6c.1-.1.1-.1.1-.2-.1.1-.2.3-.4.4l.1.1c-.02-.1.08-.2.2-.3zm-.32.3v.1c.1 0 .1-.1 0-.1zm-2.8-.1c.2-.2 0 0 0-.2l-.2.1c0 .1.1.1.2.1zm3.12.4l.2.2v-.1c-.12.1-.12-.1-.2-.1zM.88 25.5a.52.52 0 00.2.4c0-.2.1-.2.2-.3h-.2v-.1zm2.5.1h-.2c.1.1.3.2.2 0zm-1.5-.9c0 .1-.1 0-.1 0v.1c.1 0 .1 0 .1-.1zm1.8.5c0 .1-.2 0 0 .2-.1-.2.1 0 0-.2zm-1.6-1.1c.1 0 .3 0 .2-.1s-.1.1-.2.1zm1.7.4c.1.1 0 .1 0 .2 0-.1-.1-.1 0-.2-.1.2-.1.2-.1.3a.6.6 0 01.4.1c-.08-.1-.2-.4-.3-.4zm-1.6-.6s.2.1.2 0l.1.1c.2 0-.2-.1-.1-.2-.1.1-.2 0-.2.1zm2 1c.1-.1.2-.3.1-.4h-.1c.1.2-.1.2 0 .4zm-1.5-1c.1-.1.1.1.2 0l-.2-.2c-.1 0 .1 0 0 .2.1-.1 0-.1 0 0zm.32-.6l-.2-.1c0 .2.1.1.2.3zm.08-.2a.6.6 0 00.1.4zm2.7 0c0 .1.2.1.1.1-.1-.2-.2-.2-.1-.1zm-1.1-3a12.86 12.86 0 00-.4 1.9s.4-1.8.4-1.9zm1.6-2.7c0-.1.1-.2 0-.3s-.1 0 0 .1zm.72-.5v-.1s-.12 0 0 .1zm6.18-1.8c.2-.1.2-.1 0-.2-.1.1.1.1 0 .2zm.4-.3h-.3.3zm.6-.6c.1.1.1 0 0 0zm.1.2c0-.1-.1-.1 0 0z"></path><path class="cls-1" d="M14.58 14.2l-.1-.1-.4.1c0 .1.2-.2.5 0zm-1.3-1c.1.3.9 0 1 .3.1-.1.3.1.5.1 0-.1-.7-.2-.4-.3-.6-.2-.6.2-1.1-.1zm.72.4s-.2.1 0 .1h.1c.18 0-.02-.1-.1-.1zm-.12-.5V13h-.2l.2.1zm-.4-.3h.3c-.1 0-.3-.1-.3 0zm-.4-.6c-.1-.1-.5 0-.2.1s0-.1.2-.1zm-1.7-.6zm-.38.9c-.1-.1 0-.1-.1-.2-.12.2.1.1.1.2zm.38-1.2c.1 0 .1 0 .1.1.1-.2 0-.2-.1-.1zm-.3-.4c0 .1-.2.3-.3.4.2 0 .5-.2.3 0l.3-.2c-.1-.1-.2.2-.3.1s.1-.1.1-.2 0-.1-.1-.1zm-.2 1c-.2.2.1 0 .2.1v-.1zm-.6-1.1zm.2-.1v-.1l-.2.1h.2zm-.1-2.2l.1 2.1-.1-2.1zm-.2-.1c-.1 0-.1 0-.1-.1v.1s0 .1.1 0zm-2.7 3.3l.1-.1-.1.1zm-.2-.3c.1.1.2 0 .3 0s-.2 0-.2-.1h.1zM5.08 7l.2-.2c-.1 0-.2-.1-.3-.1s-.2.1-.2.2l.3.1-.2-.1c.12.2.2-.1.2.1zM.88 1.7A.1.1 0 001 1.6a.1.1 0 00-.12.1zm-.5.1h-.3c-.2.2 0 0 .3 0zm1.5-.1l-.3.1-.1.1.4-.2zm1.5.6c.6.1 0-.1.3-.2h-.4c-.4-.2.3-.3 0-.4-.5 0-.2.4-.9.3.1.2 1.1 0 1 .3zm46-1.8c.1 0 7.4 0 7.5.1s-7.3 0-7.3-.1zm-24.1.6c.1 0 .1.1.3.1a.37.37 0 00-.3-.1zm-14.9 1.2h.3s-.1-.1-.3 0zm.4 0c.2.1.2.4.5.1h-.1c.2.1.4-.1.6-.1a.1.1 0 00-.1-.1h-.4c-.2-.2-.28 0-.5.1zM28.28 1zm-23.6.2h.2l-.4-.1c-.6.1-.6.2-.7.4a3.21 3.21 0 011.4-.1zm.6 1.2h-.4c.1 0 .2.2.3.2zm6.1.5l-.5-.1c-.2.1.4.2.5.1zm-1.9-1.8c-.4.1-.1-.1-.4-.1l-.3.2a.76.76 0 00.7-.1zm3.3 1.8c.2-.2.5.1.6-.1-.1 0-.6-.1-.6.1zm-.3-2c-.2-.2-.5 0-.8 0 .32 0 .6.2.8 0zm2.1 1.5c.3.1.8.2.8.4.2-.1.5-.3.2-.5 0 .1-.2.1-.5.1.1 0 .1-.1.2-.1-.4.1-.4 0-.7.1zM13 .9l-.1.2c.3.1-.1-.2.3-.2a1.88 1.88 0 00-.8-.2.5.5 0 00.6.2zm3.88 1.7c-.4.1-.9.1-1.1.3a4.44 4.44 0 001.3-.2zm-2.3-1.4l.1-.2c-.4 0 0 .1-.1.1s-.3-.1-.4 0c.4 0 0 .2.4.1zm1.2-.2l.5-.1L16 .6c-.62.1-.12.1-.22.4zm1.4-.2a1.91 1.91 0 00-.8.3h.1zm8 1.8c-.1.1.5.2.2.1 0-.1-.38-.3-.2-.1zm-.4-2c-.4 0-1.2 0-1.2.1a2.55 2.55 0 001.2-.1zm24.6-.5c-.2 0-.6 0-.7.1.3-.1 7.8-.1 8.2-.2-.2.1-7.5 0-7.5.1zm10.1.1c-.2.1-.4 0-.3.1s.4-.1.3-.1z"></path><path class="cls-1" d="M65.08 26.5c0-1-.1-2.1-.1-3.1-.2-5.3-1.1-15.8-1.2-21.3l-61.2-.2L11 14.8 1.18 26.5z"></path></g></g></svg>
                </svg>
                <div class="menu__count-content"><span class="menu__count-number">1080 руб</span><span class="menu__count-text">до бесплатной доставки</span></div>
            </div>
            <div class="slider-arrow menu-prev">
                <svg class="slider-icon__arrow">
                    <use xlink:href="/local/templates/pomidor/images/icons.svg#arrow-left"></use>
                </svg>
            </div>
            <div class="slider-arrow menu-next">
                <svg class="slider-icon__arrow">
                    <use xlink:href="/local/templates/pomidor/images/icons.svg#arrow-right"></use>
                </svg>
            </div>
        </section>
        <section class="section favorite">
            <div class="wrap">
                <div class="title-wrap"><span class="subtitle">любимые блюда</span>
                    <h2 class="title">Меню кафе</h2>
                    <svg class="icon title-wrap__line">
                        <svg viewBox="0 0 36.6 4.21" id="orange-line"><g id="Слой_2" data-name="Слой 2"><g id="Режим_изоляции" data-name="Режим изоляции" fill="#f7b035"><path d="M36.5 2.71l.1-.2c-.1 0-.1.3-.1.2zm-1 .3v-.1l-.2.2.2-.1zm-.8-.2c0 .4.3 0 .4.2v.1l.1-.3c-.2 0-.4.1-.5 0zm-.3-.9c0 .2-.1.1 0 0zm.1 0c.1.1 0-.1 0 0zm-.4 0l.1-.1c0-.1-.1-.1 0-.1-.2 0-.2.1-.1.2zm0 .09l-.2.1c.1.1.1.2.2-.1-.1.21 0 .11 0 0zm.1.11c0 .1.1.2 0 .3a.15.15 0 000-.3zm.3 1l.1.1.1-.1c-.1.1-.1-.11-.2 0zm-1.1-1.5v.3l.1-.1c-.1 0-.1-.1-.1-.2zm0 .3l-.2.1c.1-.01.2-.01.2-.1zm.4.6l.3-.2zm-.9-1l.1-.2-.2.3.1-.1zm-.3-.1c.1-.1 0-.3.1-.3-.1 0-.2.1-.3.2a.31.31 0 01.2.1zm-.4 0l.1-.1a.1.1 0 01-.1.1zm.8.5s0-.2-.1-.2l.1.2zm.5.4c-.1.1-.3.2-.2.4.1-.2.1-.3.2-.4zm-.5-.1l.1.1c0-.1 0-.1.1-.2zm-.6-.5c-.1.1-.1.1-.2.1.1.1.1.1.2.1s.1-.2 0-.2zm-.9-.1zm.9.3c0 .1.1 0 .1 0zm-.4-.4a.1.1 0 00.1.1.1.1 0 01-.1-.1zm.6.3l-.1.1a.1.1 0 00.1-.1zM31 2.91c0-.2.2-.3.3-.1.2-.1.2-.8.2-1.1 0 .1 0 .2-.2.3 0 .1.2.3.1.5s-.1-.1-.2-.2c0 .3-.3.3-.3.7l.1-.3zm.7-1.2c0-.1 0-.1-.1-.3a.31.31 0 00-.1.2l.2.1zm.3.1c-.1.2-.2.3-.2.5.2 0 .1-.2.3-.2-.3 0 .1-.1-.1-.3h.1l-.1-.1c.1 0 .1 0 0 .1.1-.1 0-.1 0 0v-.1h-.2c0 .1.1.1.2.1zM33 3l.1-.1c.1.1 0 .1-.1.1.1.1.2 0 .1-.2-.1-.1-.2-.4-.1-.4zm-.5-.59l-.2.1v.1c.2 0 .2-.1.2-.2zm-1-.9v-.2l-.1.3.1-.1zm-.3 0v-.2l-.1.1.1.1zm-7-1c-.1-.2-.2-.5-.2-.3v.2a.31.31 0 00.2.1zm-4.8 1.1l.1-.1c-.1 0-.1 0-.1.1zm-.1.9l-.1-.1zM12.7 3l-.1.2c.1.11.1.01.1-.2zm15.6-1.89l-.2-.2c.1.2.1.4.2.2zm-13-.7l-.1.1a.1.1 0 00.1-.1zm12.6.5h.2c-.1-.1-.1-.1-.2 0zM.7 2.41h.1c0-.1 0 0-.1 0zm7.8.3c-.1.1-.1 0 0 0zm14 0v-.1.1zm-.3.1c.1-.3.2-.2.2-.2V2z"></path><path d="M14.1 2.81l.2.2c-.2.3-.3.2-.1.5 0-.7.6-.1.6-.8l.1.1h-.1c.2.3.1-.5.4-.4.1 0 .1.1.1.2l.2-.2c.1 0 0 .1 0 .1l.3-.1c0-.1.1-.3.2-.3s0 .1 0 .2l.2-.2c.1.2-.1.3-.2.3.2.7.1-.2.5.2l-.2.1c.1.4.2.2.4.5 0-.1-.4-.6-.2-.8 0 .1.1.1.2-.1-.2.5-.1.4 0 .4V3c.1-.1.1-.3.2-.4s.2.3.2.5c.2.1-.2-.4.1-.5.2.1.3 0 .5-.2.1.1 0 .2 0 .3l.1-.3v.1c.1-.1 0-.3.2-.4 0-.4.3.5.5.1v.3a1.76 1.76 0 00.4-.5l.2.2a.88.88 0 01.2-.8c0-.1-.1-.2-.1-.3h.3v.2c.1.1 0 .3 0 .5 0-.1-.1-.1-.1-.2a1.27 1.27 0 00-.1.6.1.1 0 01.1-.1v.1l.1-.1v.3l.3.2c.2-.4.4-.9.8-1a1.48 1.48 0 00-.1.7c-.1-.3-.3.1-.5.1.1 0 .1.2 0 .2l.3-.3c0 .3.1.2.1.3 0-.5.3-.3.4-.5 0 .2 0 .3-.2.3.2.4.3-.6.4-.2a.1.1 0 00-.1.1.19.19 0 01.3.1.7.7 0 00.9-.4c0 .1-.1.5-.1.6l.6-1.4c-.1.4 0 1.2-.2 1.4.1.1.2.2.3.1s-.1-.5 0-.6c.1.4.1.2.3.6-.1-.1.1-.6.2-.6s0 .6-.1.6l.3-.5c-.1.1 0 .5 0 .6 0-.1.2 0 .3 0l-.1-.1c.3.1.3-.8.6-.7 0 .1-.1.7 0 .8.1-.3.4-1.4.7-1.6v.2l.2-.2c-.2.4-.6 1.3-.8 1.6.1.1 0 .2.2.2 0 .2-.1.2-.2.2l.4.1c0-.3.3-.3.3-.6l-.4.3c0-.3.3-.9.6-.8.1.1-.1.6-.1.7 0-.1.3-.2.3-.1l-.1.2c.1.1.3-.4.4-.1.1 0 .1.5.2.3a5.84 5.84 0 01.2-1.9 1.63 1.63 0 01.3.6c.1.3-.2.7-.2 1a.35.35 0 01.1-.2c.1.1-.1.3.1.3.1-.3.3.1.1-.3 0-.2 0 0 .2 0a1.23 1.23 0 01.4-.8v.3c.3.1.4.2.7.3 0 .1.1.2 0 .3a.22.22 0 01.2-.2c.1.3-.2 0-.1.4 0-.2.2-.2.4-.3 0-.3-.3-.1-.4 0a1.7 1.7 0 01.6-1c.2-.1.1.3.1.2.4.1.5-.7.8-.3.1.3-.1.5-.1.7-.2.1-.2-.2-.4-.2l.1.1c-.1.1-.2.1-.2 0 .1.3.7.1.9.4.1-.1.2-.2.2-.3h-.1c-.1-.2.2-.3.1-.4.2-.1.3-.3.5-.2V2c0-.1-.1-.2-.2-.1.1-.1.2.5.4.1l-.1-.1c.2-.1.4-.4.5-.3 0-.3 0 0-.1-.3 0 .5-.4-.1-.3.4 0-.4-.2-.2-.3-.6 0 .1 0 .2-.1.1v.3c-.1.2-.3.2-.4.3-.1-.1.1-.1 0-.2l-.2.1c0-.2-.2-.4.1-.5h-.2c-.1-.1-.2-.2-.3-.1-.1-.1-.2.2-.1.1l-.3.1v-.1c-.3-.1-.6.3-.9-.2-.1 0-.2.1-.4 0V.81c-.1 0-.3-.2-.4 0-.1-.5-.6-.2-.8-.3v.3a1.09 1.09 0 00-.5.1v.2c-.2.2-.4-.2-.6-.2 0-.1.1-.1.1 0-.2-.6-.6.2-.8-.2-.1.1-.1 0-.2 0l-.1-.1c0 .1 0 .1.1.1-.1 0-.1 0-.2.1v-.2c-.1-.1-.1.1 0 .3L24 1c0-.4-.2.1-.3-.3-.2.1-.5 0-.5.5v-.1c-.1-.1-.2-.1-.2 0V.81l-.2.4a.45.45 0 010-.5c-.3.3-.2.1-.5.5V1c-.1 0-.2.3-.3.1-.2-.4-.9-.1-1.3-.5.1.6-.2-.3-.2.2 0-.1-.1-.2 0-.3-.2.1-.4-.2-.5.1 0-.1.1-.2 0-.3s-.1.1-.1 0 0-.1.1-.2c-.2 0-.3.4-.3.8-.1-.2-.2-.2-.3.1 0-.1-.1-.3 0-.3-.1 0-.5-.2-.5.1 0-.1-.1 0-.1 0-.2 0-.2-.1-.4 0l.1.1c-.1.4-.2.1-.3.1-.5-.3-.3.1-.7-.2V1c0 .5-.3-.3-.5-.1l.1.1c-.1.2-.3-.7-.4-.8a.1.1 0 01.1-.1c-.3-.3 0 .3-.2.4a.75.75 0 00-.1-.5c-.1-.1-.4.5-.6.2 0 .1.1.3 0 .4s-.4-.5-.5-.1V.41c-.1.1-.4.2-.4.4-.1-.8-.7.4-.7-.3l-.6.2v-.1c-.2 0-.3.2-.4.3s0-.1 0-.2c-.3-.1-.4-.1-.6.3v-.2c-.1.1-.5-.4-.8 0v-.1c-.3.3-.9-.6-1 .3l-.2.2c.3 0 0 .4.1.5-.1.1-.3-.2-.2-.3 0-.5-.4-.1-.4-.4a10.87 10.87 0 01-1.8-.1l.1.4h-.2c-.1-.1-.1-.3 0-.3-.1-.2-.2.2-.2.3-.3-.1.1-.4 0-.4H8c-.1.2-.1.4-.2.4s-.1-.2-.2-.2h-.4l.1.3c.1.1-.1-.1.1.3-.2-.5-1-.1-1.1-.2-.1.2-.3.2-.5.2.1.1.1.4 0 .4 0-.7-.3-.2-.6-.7.2.3-.4.1-.2.6-.1 0 0-.2-.2-.4-.2.3-.7-.1-1.1.1 0 .1.1.2 0 .4l-.2-.5c-.1.1 0 .6-.2.3 0 .1.1.2 0 .2-.9-.3-1.5.7-2.5.1.1.1.1.2 0 .2.1 0 0 .2.1.3l-.4-.3c0 .3-.4.1-.3.4l.1-.1c-.1.5.4.8.5 1.3.1-.4.5.5.7-.1.1.1 0 .2 0 .3 0-.3 0-.3.2-.3V4a2.16 2.16 0 011.1.1l-.1-.3c.1 0 .1.1.2.1 0-.4-.3 0-.3-.4.3.5.7-.1.9.5.2-.1-.1-.3.1-.3l.1.1v-.2c.1 0 .2.2.2.3a.1.1 0 00-.1.1c.2.1.3-.1.4-.1H4c.4-.3.9-.2 1.3-.5v-.1c.2-.3.2.1.5-.1A.22.22 0 016 3c-.1.2.2.3.1.4.3-.3.1-.2.3-.6l.1.2c0-.2 0-.3.2-.4-.1.2.1.3 0 .5.4.3.5-.5.7.1.1-.5-.3-.2-.3-.4s.1-.4.2-.3.3.6.6.6h-.1c.1.1.2-.2.3 0 0-.3.2 0 .1-.4l-.1.1c.1-.1.2-.6.5-.4 0 .1-.1.2-.1.3s.1-.1.2 0c0 .4-.3.1-.4.4s.2-.3.2.1c0-.6.3 0 .4-.4V3l.1-.2v.4c.1-.3.3.2.5 0-.3 0-.1-.3-.3-.4.3-.3.3.6.7.5a.22.22 0 01-.2-.2c.1 0 .2.1.3.2.3-.1-.1-.2 0-.4.1.3.1-.1.2-.2v.2c.3.1 0-.5.3-.4l-.1.3.1-.1V3c.1-.2.2-.2.4-.1a.44.44 0 01.1-.5c.1.1 0 .2.2.2 0 .2-.1.3-.2.1v.2c.1.2.3 0 .3 0h-.1l.3-.4a.14.14 0 010 .2c.1 0 .1-.1.2-.2a.75.75 0 01-.1.5l.3-.2c0 .1.2.2.1.3.2.1.4-.4.6-.3a.1.1 0 01.1-.1c.2 0 .4.3.6 0l.2.5c.2-.1-.2-.4.1-.6s.1.3.2.4c.1-.2.3-.5.5-.3a.31.31 0 01-.2.11zm2.2-.2c0 .1-.1.2-.2.1.1-.3.1-.2.2-.1zM19.7 2c0 .1-.1 0-.1-.1 0 .1 0 .1.1.1zm9.7-.7zm.2.1c-.1.01-.1.11 0 .01zm-22-.3z"></path><path d="M19.3 2.41v-.1c-.1 0-.1 0 0 .1zm6.9.59l-.1-.1a.1.1 0 00.1.1zm4.5-1.79h-.1a.1.1 0 01.1.1zm.8 1.7c.1-.1.1 0 0 0l.1-.2c-.1 0-.1.1-.1.2zM30.7 2c0 .2.1.1.2.1 0-.19-.1-.19-.2-.1zm-.9-.69c0 .1.1-.3.1-.2.1-.2-.3.3-.1.2zm.8 1.4v-.3.3zm-.6-.3l.1.1a.1.1 0 01.1-.1c-.1 0-.2-.1-.2 0zm.1 0zm.3.3l-.1-.1.1.1zm-2-.6h-.2c0 .1 0 .1.1.1s.1 0 .1-.1zm.4.7v-.2l-.1.3zm-.5 0c0 .1-.3.2-.1.2s.3-.1.1-.2zm-2.3-.7h.1v-.5l-.1.5zm-2.2 1c.2.1.1-.3.2-.1.1-.1-.1-.1-.1-.2 0 .1-.1 0-.1.3zM.2 2.21l-.1-.1.1.2v-.1zm-.1.4l-.1-.3v.3zm16.5.6c0-.2 0-.2-.1-.2zm-1.6-.2v-.3.3zm-1.3.4c.1 0 .1-.1.2 0-.2.1-.2-.3-.2 0z"></path></g></g></svg>
                    </svg>
                </div>
                <div class="favorite__container">
	<div class="favorite__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
		<div class="favorite__image">
 <img alt="image-pasta.png" src="/upload/medialibrary/0d3/0d328fe540a015c90c65a786e45941a6.png" title="image-pasta.png"><br>
 <br>
		</div>
		<div class="favorite__link-wrap">
 <a class="btn btn_fav" href="/menu/">Кухня</a>
		</div>
	</div>
	<div class="favorite__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="800">
		<div class="favorite__image">
 <img alt="image-bar.png" src="/upload/medialibrary/110/1101f76f1f6e01a05a5d4c265f1b90c5.png" title="image-bar.png"><br>
 <br>
		</div>
		<div class="favorite__link-wrap">
 <a class="btn btn_fav" href="/upload/files/bannaya-karta.pdf">Барная карта</a>
		</div>
	</div>
	<div class="favorite__item" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="1000">
        <div class="fav-window">
                            <span class="fav-window__title">
                                Только на Ленина, 53
                            </span>
        </div>
		<div class="favorite__image">
 <img alt="image-vine.png" src="/upload/medialibrary/699/69964b7b24eab8b8efeaf7ca953ccbcf.png" title="image-vine.png">
		</div>
		<div class="favorite__link-wrap">
 <a class="btn btn_fav" href="/upload/files/vinnaya-karta.pdf" title="Винная карта имеется только в меню кафе на ул. Ленина, 53">Винная карта</a>
		</div>
	</div>
</div>
 <br>            </div>
        </section>

        <section class="section address">
            <div class="wrap address__wrap">
                <div class="title-wrap"><span class="subtitle">всегда рады вам</span>
                    <h2 class="title">Адреса наших кафе</h2>
                    <svg class="icon title-wrap__line">
                        <svg viewBox="0 0 36.6 4.21" id="orange-line"><g id="Слой_2" data-name="Слой 2"><g id="Режим_изоляции" data-name="Режим изоляции" fill="#f7b035"><path d="M36.5 2.71l.1-.2c-.1 0-.1.3-.1.2zm-1 .3v-.1l-.2.2.2-.1zm-.8-.2c0 .4.3 0 .4.2v.1l.1-.3c-.2 0-.4.1-.5 0zm-.3-.9c0 .2-.1.1 0 0zm.1 0c.1.1 0-.1 0 0zm-.4 0l.1-.1c0-.1-.1-.1 0-.1-.2 0-.2.1-.1.2zm0 .09l-.2.1c.1.1.1.2.2-.1-.1.21 0 .11 0 0zm.1.11c0 .1.1.2 0 .3a.15.15 0 000-.3zm.3 1l.1.1.1-.1c-.1.1-.1-.11-.2 0zm-1.1-1.5v.3l.1-.1c-.1 0-.1-.1-.1-.2zm0 .3l-.2.1c.1-.01.2-.01.2-.1zm.4.6l.3-.2zm-.9-1l.1-.2-.2.3.1-.1zm-.3-.1c.1-.1 0-.3.1-.3-.1 0-.2.1-.3.2a.31.31 0 01.2.1zm-.4 0l.1-.1a.1.1 0 01-.1.1zm.8.5s0-.2-.1-.2l.1.2zm.5.4c-.1.1-.3.2-.2.4.1-.2.1-.3.2-.4zm-.5-.1l.1.1c0-.1 0-.1.1-.2zm-.6-.5c-.1.1-.1.1-.2.1.1.1.1.1.2.1s.1-.2 0-.2zm-.9-.1zm.9.3c0 .1.1 0 .1 0zm-.4-.4a.1.1 0 00.1.1.1.1 0 01-.1-.1zm.6.3l-.1.1a.1.1 0 00.1-.1zM31 2.91c0-.2.2-.3.3-.1.2-.1.2-.8.2-1.1 0 .1 0 .2-.2.3 0 .1.2.3.1.5s-.1-.1-.2-.2c0 .3-.3.3-.3.7l.1-.3zm.7-1.2c0-.1 0-.1-.1-.3a.31.31 0 00-.1.2l.2.1zm.3.1c-.1.2-.2.3-.2.5.2 0 .1-.2.3-.2-.3 0 .1-.1-.1-.3h.1l-.1-.1c.1 0 .1 0 0 .1.1-.1 0-.1 0 0v-.1h-.2c0 .1.1.1.2.1zM33 3l.1-.1c.1.1 0 .1-.1.1.1.1.2 0 .1-.2-.1-.1-.2-.4-.1-.4zm-.5-.59l-.2.1v.1c.2 0 .2-.1.2-.2zm-1-.9v-.2l-.1.3.1-.1zm-.3 0v-.2l-.1.1.1.1zm-7-1c-.1-.2-.2-.5-.2-.3v.2a.31.31 0 00.2.1zm-4.8 1.1l.1-.1c-.1 0-.1 0-.1.1zm-.1.9l-.1-.1zM12.7 3l-.1.2c.1.11.1.01.1-.2zm15.6-1.89l-.2-.2c.1.2.1.4.2.2zm-13-.7l-.1.1a.1.1 0 00.1-.1zm12.6.5h.2c-.1-.1-.1-.1-.2 0zM.7 2.41h.1c0-.1 0 0-.1 0zm7.8.3c-.1.1-.1 0 0 0zm14 0v-.1.1zm-.3.1c.1-.3.2-.2.2-.2V2z"></path><path d="M14.1 2.81l.2.2c-.2.3-.3.2-.1.5 0-.7.6-.1.6-.8l.1.1h-.1c.2.3.1-.5.4-.4.1 0 .1.1.1.2l.2-.2c.1 0 0 .1 0 .1l.3-.1c0-.1.1-.3.2-.3s0 .1 0 .2l.2-.2c.1.2-.1.3-.2.3.2.7.1-.2.5.2l-.2.1c.1.4.2.2.4.5 0-.1-.4-.6-.2-.8 0 .1.1.1.2-.1-.2.5-.1.4 0 .4V3c.1-.1.1-.3.2-.4s.2.3.2.5c.2.1-.2-.4.1-.5.2.1.3 0 .5-.2.1.1 0 .2 0 .3l.1-.3v.1c.1-.1 0-.3.2-.4 0-.4.3.5.5.1v.3a1.76 1.76 0 00.4-.5l.2.2a.88.88 0 01.2-.8c0-.1-.1-.2-.1-.3h.3v.2c.1.1 0 .3 0 .5 0-.1-.1-.1-.1-.2a1.27 1.27 0 00-.1.6.1.1 0 01.1-.1v.1l.1-.1v.3l.3.2c.2-.4.4-.9.8-1a1.48 1.48 0 00-.1.7c-.1-.3-.3.1-.5.1.1 0 .1.2 0 .2l.3-.3c0 .3.1.2.1.3 0-.5.3-.3.4-.5 0 .2 0 .3-.2.3.2.4.3-.6.4-.2a.1.1 0 00-.1.1.19.19 0 01.3.1.7.7 0 00.9-.4c0 .1-.1.5-.1.6l.6-1.4c-.1.4 0 1.2-.2 1.4.1.1.2.2.3.1s-.1-.5 0-.6c.1.4.1.2.3.6-.1-.1.1-.6.2-.6s0 .6-.1.6l.3-.5c-.1.1 0 .5 0 .6 0-.1.2 0 .3 0l-.1-.1c.3.1.3-.8.6-.7 0 .1-.1.7 0 .8.1-.3.4-1.4.7-1.6v.2l.2-.2c-.2.4-.6 1.3-.8 1.6.1.1 0 .2.2.2 0 .2-.1.2-.2.2l.4.1c0-.3.3-.3.3-.6l-.4.3c0-.3.3-.9.6-.8.1.1-.1.6-.1.7 0-.1.3-.2.3-.1l-.1.2c.1.1.3-.4.4-.1.1 0 .1.5.2.3a5.84 5.84 0 01.2-1.9 1.63 1.63 0 01.3.6c.1.3-.2.7-.2 1a.35.35 0 01.1-.2c.1.1-.1.3.1.3.1-.3.3.1.1-.3 0-.2 0 0 .2 0a1.23 1.23 0 01.4-.8v.3c.3.1.4.2.7.3 0 .1.1.2 0 .3a.22.22 0 01.2-.2c.1.3-.2 0-.1.4 0-.2.2-.2.4-.3 0-.3-.3-.1-.4 0a1.7 1.7 0 01.6-1c.2-.1.1.3.1.2.4.1.5-.7.8-.3.1.3-.1.5-.1.7-.2.1-.2-.2-.4-.2l.1.1c-.1.1-.2.1-.2 0 .1.3.7.1.9.4.1-.1.2-.2.2-.3h-.1c-.1-.2.2-.3.1-.4.2-.1.3-.3.5-.2V2c0-.1-.1-.2-.2-.1.1-.1.2.5.4.1l-.1-.1c.2-.1.4-.4.5-.3 0-.3 0 0-.1-.3 0 .5-.4-.1-.3.4 0-.4-.2-.2-.3-.6 0 .1 0 .2-.1.1v.3c-.1.2-.3.2-.4.3-.1-.1.1-.1 0-.2l-.2.1c0-.2-.2-.4.1-.5h-.2c-.1-.1-.2-.2-.3-.1-.1-.1-.2.2-.1.1l-.3.1v-.1c-.3-.1-.6.3-.9-.2-.1 0-.2.1-.4 0V.81c-.1 0-.3-.2-.4 0-.1-.5-.6-.2-.8-.3v.3a1.09 1.09 0 00-.5.1v.2c-.2.2-.4-.2-.6-.2 0-.1.1-.1.1 0-.2-.6-.6.2-.8-.2-.1.1-.1 0-.2 0l-.1-.1c0 .1 0 .1.1.1-.1 0-.1 0-.2.1v-.2c-.1-.1-.1.1 0 .3L24 1c0-.4-.2.1-.3-.3-.2.1-.5 0-.5.5v-.1c-.1-.1-.2-.1-.2 0V.81l-.2.4a.45.45 0 010-.5c-.3.3-.2.1-.5.5V1c-.1 0-.2.3-.3.1-.2-.4-.9-.1-1.3-.5.1.6-.2-.3-.2.2 0-.1-.1-.2 0-.3-.2.1-.4-.2-.5.1 0-.1.1-.2 0-.3s-.1.1-.1 0 0-.1.1-.2c-.2 0-.3.4-.3.8-.1-.2-.2-.2-.3.1 0-.1-.1-.3 0-.3-.1 0-.5-.2-.5.1 0-.1-.1 0-.1 0-.2 0-.2-.1-.4 0l.1.1c-.1.4-.2.1-.3.1-.5-.3-.3.1-.7-.2V1c0 .5-.3-.3-.5-.1l.1.1c-.1.2-.3-.7-.4-.8a.1.1 0 01.1-.1c-.3-.3 0 .3-.2.4a.75.75 0 00-.1-.5c-.1-.1-.4.5-.6.2 0 .1.1.3 0 .4s-.4-.5-.5-.1V.41c-.1.1-.4.2-.4.4-.1-.8-.7.4-.7-.3l-.6.2v-.1c-.2 0-.3.2-.4.3s0-.1 0-.2c-.3-.1-.4-.1-.6.3v-.2c-.1.1-.5-.4-.8 0v-.1c-.3.3-.9-.6-1 .3l-.2.2c.3 0 0 .4.1.5-.1.1-.3-.2-.2-.3 0-.5-.4-.1-.4-.4a10.87 10.87 0 01-1.8-.1l.1.4h-.2c-.1-.1-.1-.3 0-.3-.1-.2-.2.2-.2.3-.3-.1.1-.4 0-.4H8c-.1.2-.1.4-.2.4s-.1-.2-.2-.2h-.4l.1.3c.1.1-.1-.1.1.3-.2-.5-1-.1-1.1-.2-.1.2-.3.2-.5.2.1.1.1.4 0 .4 0-.7-.3-.2-.6-.7.2.3-.4.1-.2.6-.1 0 0-.2-.2-.4-.2.3-.7-.1-1.1.1 0 .1.1.2 0 .4l-.2-.5c-.1.1 0 .6-.2.3 0 .1.1.2 0 .2-.9-.3-1.5.7-2.5.1.1.1.1.2 0 .2.1 0 0 .2.1.3l-.4-.3c0 .3-.4.1-.3.4l.1-.1c-.1.5.4.8.5 1.3.1-.4.5.5.7-.1.1.1 0 .2 0 .3 0-.3 0-.3.2-.3V4a2.16 2.16 0 011.1.1l-.1-.3c.1 0 .1.1.2.1 0-.4-.3 0-.3-.4.3.5.7-.1.9.5.2-.1-.1-.3.1-.3l.1.1v-.2c.1 0 .2.2.2.3a.1.1 0 00-.1.1c.2.1.3-.1.4-.1H4c.4-.3.9-.2 1.3-.5v-.1c.2-.3.2.1.5-.1A.22.22 0 016 3c-.1.2.2.3.1.4.3-.3.1-.2.3-.6l.1.2c0-.2 0-.3.2-.4-.1.2.1.3 0 .5.4.3.5-.5.7.1.1-.5-.3-.2-.3-.4s.1-.4.2-.3.3.6.6.6h-.1c.1.1.2-.2.3 0 0-.3.2 0 .1-.4l-.1.1c.1-.1.2-.6.5-.4 0 .1-.1.2-.1.3s.1-.1.2 0c0 .4-.3.1-.4.4s.2-.3.2.1c0-.6.3 0 .4-.4V3l.1-.2v.4c.1-.3.3.2.5 0-.3 0-.1-.3-.3-.4.3-.3.3.6.7.5a.22.22 0 01-.2-.2c.1 0 .2.1.3.2.3-.1-.1-.2 0-.4.1.3.1-.1.2-.2v.2c.3.1 0-.5.3-.4l-.1.3.1-.1V3c.1-.2.2-.2.4-.1a.44.44 0 01.1-.5c.1.1 0 .2.2.2 0 .2-.1.3-.2.1v.2c.1.2.3 0 .3 0h-.1l.3-.4a.14.14 0 010 .2c.1 0 .1-.1.2-.2a.75.75 0 01-.1.5l.3-.2c0 .1.2.2.1.3.2.1.4-.4.6-.3a.1.1 0 01.1-.1c.2 0 .4.3.6 0l.2.5c.2-.1-.2-.4.1-.6s.1.3.2.4c.1-.2.3-.5.5-.3a.31.31 0 01-.2.11zm2.2-.2c0 .1-.1.2-.2.1.1-.3.1-.2.2-.1zM19.7 2c0 .1-.1 0-.1-.1 0 .1 0 .1.1.1zm9.7-.7zm.2.1c-.1.01-.1.11 0 .01zm-22-.3z"></path><path d="M19.3 2.41v-.1c-.1 0-.1 0 0 .1zm6.9.59l-.1-.1a.1.1 0 00.1.1zm4.5-1.79h-.1a.1.1 0 01.1.1zm.8 1.7c.1-.1.1 0 0 0l.1-.2c-.1 0-.1.1-.1.2zM30.7 2c0 .2.1.1.2.1 0-.19-.1-.19-.2-.1zm-.9-.69c0 .1.1-.3.1-.2.1-.2-.3.3-.1.2zm.8 1.4v-.3.3zm-.6-.3l.1.1a.1.1 0 01.1-.1c-.1 0-.2-.1-.2 0zm.1 0zm.3.3l-.1-.1.1.1zm-2-.6h-.2c0 .1 0 .1.1.1s.1 0 .1-.1zm.4.7v-.2l-.1.3zm-.5 0c0 .1-.3.2-.1.2s.3-.1.1-.2zm-2.3-.7h.1v-.5l-.1.5zm-2.2 1c.2.1.1-.3.2-.1.1-.1-.1-.1-.1-.2 0 .1-.1 0-.1.3zM.2 2.21l-.1-.1.1.2v-.1zm-.1.4l-.1-.3v.3zm16.5.6c0-.2 0-.2-.1-.2zm-1.6-.2v-.3.3zm-1.3.4c.1 0 .1-.1.2 0-.2.1-.2-.3-.2 0z"></path></g></g></svg>
                    </svg>
                </div>
                <div class="wrap">
    <div class="address__container">

        <div bx_651765591_622>
	            <div class="address__item" style="background-image: url(/upload/iblock/cb2/cb2d3eb6e91bd2f68e0ddb73e5a1bd95.jpg); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title">ул. Суворова, 32</span>
                    <div class="address__item-desc">
                        
                                                                                                                                    <span>График работы</span>
                                    <span class="address__item-time">с 11:00 до 23:00</span>
                                

                                                        
                                                                                                    <a class="address__item-link" href="tel:+74212352727">+7 (4212) 35 27 27</a>
                                                                

                                                                            </div>
                </div>
            </div>
        </div>
        <div bx_651765591_621>
	            <div class="address__item" style="background-image: url(/upload/iblock/373/3732def6241cbff17117b57f477ca5e0.jpg); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title">ул. Карла Маркса, 202</span>
                    <div class="address__item-desc">
                        
                                                                                                                                    <span>График работы</span>
                                    <span class="address__item-time">с 11:00 до 23:00</span>
                                

                                                        
                                                                                                    <a class="address__item-link" href="tel:+74212352727">+7 (4212) 35 27 27</a>
                                                                

                                                                            </div>
                </div>
            </div>
        </div>
        <div bx_651765591_620>
	            <div class="address__item" style="background-image: url(/upload/iblock/cb2/cb2d3eb6e91bd2f68e0ddb73e5a1bd95.jpg); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title">ул. Ленина, 53</span>
                    <div class="address__item-desc">
                        
                                                                                                                                    <span>График работы</span>
                                    <span class="address__item-time">с 11:00 до 23:00</span>
                                

                                                        
                                                                                                    <a class="address__item-link" href="tel:+74212352727">+7 (4212) 35 27 27</a>
                                                                

                                                                            </div>
                </div>
            </div>
        </div>
        <div bx_651765591_619>
	            <div class="address__item" style="background-image: url(/upload/iblock/373/3732def6241cbff17117b57f477ca5e0.jpg); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title">ул. Суворова, 32</span>
                    <div class="address__item-desc">
                        
                                                                                                                                    <span>График работы</span>
                                    <span class="address__item-time">с 11:00 до 23:00</span>
                                

                                                        
                                                                                                    <a class="address__item-link" href="tel:+74212352727">+7 (4212) 35 27 27</a>
                                                                

                                                                            </div>
                </div>
            </div>
        </div>
        <div bx_651765591_618>
	            <div class="address__item" style="background-image: url(/upload/iblock/cb2/cb2d3eb6e91bd2f68e0ddb73e5a1bd95.jpg); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title">ул. Ленина, 53</span>
                    <div class="address__item-desc">
                        
                                                                                                                                    <span>График работы</span>
                                    <span class="address__item-time">с 11:00 до 23:00</span>
                                

                                                        
                                                                                                    <a class="address__item-link" href="tel:+74212352727">+74212352727</a>
                                                                

                                                                            </div>
                </div>
            </div>
        </div>
        <div bx_651765591_617>
	            <div class="address__item" style="background-image: url(/upload/iblock/cb2/cb2d3eb6e91bd2f68e0ddb73e5a1bd95.jpg); background-repeat: no-repeat; background-size: cover;" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="600">
                <div class="address__item-content">
                    <span class="address__item-title">ул. Карла Маркса, 202</span>
                    <div class="address__item-desc">
                        
                                                                                                                                    <span>График работы</span>
                                    <span class="address__item-time">с 11:00 до 23:00</span>
                                

                                                        
                                                                                                    <a class="address__item-link" href="tel:+74212352727">+7 (4212) 35 27 27</a>
                                                                

                                                                            </div>
                </div>
            </div>
        </div>
</div>
</div>
<div class="slider-arrow address-prev"><img class="icon-arrow arrow-prev" src="/upload/images/arrow-left.png" alt=""></div>
<div class="slider-arrow address-next"><img class="icon-arrow arrow-next" src="/upload/images/arrow-right.png" alt=""></div>
            </div>
        </section>
        <section class="section delivery">
<div class="wrap">
	<div class="delivery__container">
		<div class="delivery__image" data-aos="slide-right" data-aos-duration="1500" data-aos-delay="300">
			<div class="delivery__image-container">
				 <img  alt="auto.png" src="/upload/medialibrary/bd4/bd4ceb931f4eb7600abdd8d4af626f77.png"  title="auto.png">
				<div class="delivery__image-message" data-aos="fade" data-aos-duration="1000" data-aos-delay="1500">
					 Доставка за<span class="message__time">1 час</span>
				</div>
			</div>
		</div>
		<div class="delivery__content">
			<div class="title-wrap">
 <span class="subtitle">это просто</span>
				<h2 class="title">Доставка любимых блюд</h2>
                <svg class="icon title-wrap__line">
                    <svg viewBox="0 0 36.6 4.21" id="orange-line"><g id="Слой_2" data-name="Слой 2"><g id="Режим_изоляции" data-name="Режим изоляции" fill="#f7b035"><path d="M36.5 2.71l.1-.2c-.1 0-.1.3-.1.2zm-1 .3v-.1l-.2.2.2-.1zm-.8-.2c0 .4.3 0 .4.2v.1l.1-.3c-.2 0-.4.1-.5 0zm-.3-.9c0 .2-.1.1 0 0zm.1 0c.1.1 0-.1 0 0zm-.4 0l.1-.1c0-.1-.1-.1 0-.1-.2 0-.2.1-.1.2zm0 .09l-.2.1c.1.1.1.2.2-.1-.1.21 0 .11 0 0zm.1.11c0 .1.1.2 0 .3a.15.15 0 000-.3zm.3 1l.1.1.1-.1c-.1.1-.1-.11-.2 0zm-1.1-1.5v.3l.1-.1c-.1 0-.1-.1-.1-.2zm0 .3l-.2.1c.1-.01.2-.01.2-.1zm.4.6l.3-.2zm-.9-1l.1-.2-.2.3.1-.1zm-.3-.1c.1-.1 0-.3.1-.3-.1 0-.2.1-.3.2a.31.31 0 01.2.1zm-.4 0l.1-.1a.1.1 0 01-.1.1zm.8.5s0-.2-.1-.2l.1.2zm.5.4c-.1.1-.3.2-.2.4.1-.2.1-.3.2-.4zm-.5-.1l.1.1c0-.1 0-.1.1-.2zm-.6-.5c-.1.1-.1.1-.2.1.1.1.1.1.2.1s.1-.2 0-.2zm-.9-.1zm.9.3c0 .1.1 0 .1 0zm-.4-.4a.1.1 0 00.1.1.1.1 0 01-.1-.1zm.6.3l-.1.1a.1.1 0 00.1-.1zM31 2.91c0-.2.2-.3.3-.1.2-.1.2-.8.2-1.1 0 .1 0 .2-.2.3 0 .1.2.3.1.5s-.1-.1-.2-.2c0 .3-.3.3-.3.7l.1-.3zm.7-1.2c0-.1 0-.1-.1-.3a.31.31 0 00-.1.2l.2.1zm.3.1c-.1.2-.2.3-.2.5.2 0 .1-.2.3-.2-.3 0 .1-.1-.1-.3h.1l-.1-.1c.1 0 .1 0 0 .1.1-.1 0-.1 0 0v-.1h-.2c0 .1.1.1.2.1zM33 3l.1-.1c.1.1 0 .1-.1.1.1.1.2 0 .1-.2-.1-.1-.2-.4-.1-.4zm-.5-.59l-.2.1v.1c.2 0 .2-.1.2-.2zm-1-.9v-.2l-.1.3.1-.1zm-.3 0v-.2l-.1.1.1.1zm-7-1c-.1-.2-.2-.5-.2-.3v.2a.31.31 0 00.2.1zm-4.8 1.1l.1-.1c-.1 0-.1 0-.1.1zm-.1.9l-.1-.1zM12.7 3l-.1.2c.1.11.1.01.1-.2zm15.6-1.89l-.2-.2c.1.2.1.4.2.2zm-13-.7l-.1.1a.1.1 0 00.1-.1zm12.6.5h.2c-.1-.1-.1-.1-.2 0zM.7 2.41h.1c0-.1 0 0-.1 0zm7.8.3c-.1.1-.1 0 0 0zm14 0v-.1.1zm-.3.1c.1-.3.2-.2.2-.2V2z"></path><path d="M14.1 2.81l.2.2c-.2.3-.3.2-.1.5 0-.7.6-.1.6-.8l.1.1h-.1c.2.3.1-.5.4-.4.1 0 .1.1.1.2l.2-.2c.1 0 0 .1 0 .1l.3-.1c0-.1.1-.3.2-.3s0 .1 0 .2l.2-.2c.1.2-.1.3-.2.3.2.7.1-.2.5.2l-.2.1c.1.4.2.2.4.5 0-.1-.4-.6-.2-.8 0 .1.1.1.2-.1-.2.5-.1.4 0 .4V3c.1-.1.1-.3.2-.4s.2.3.2.5c.2.1-.2-.4.1-.5.2.1.3 0 .5-.2.1.1 0 .2 0 .3l.1-.3v.1c.1-.1 0-.3.2-.4 0-.4.3.5.5.1v.3a1.76 1.76 0 00.4-.5l.2.2a.88.88 0 01.2-.8c0-.1-.1-.2-.1-.3h.3v.2c.1.1 0 .3 0 .5 0-.1-.1-.1-.1-.2a1.27 1.27 0 00-.1.6.1.1 0 01.1-.1v.1l.1-.1v.3l.3.2c.2-.4.4-.9.8-1a1.48 1.48 0 00-.1.7c-.1-.3-.3.1-.5.1.1 0 .1.2 0 .2l.3-.3c0 .3.1.2.1.3 0-.5.3-.3.4-.5 0 .2 0 .3-.2.3.2.4.3-.6.4-.2a.1.1 0 00-.1.1.19.19 0 01.3.1.7.7 0 00.9-.4c0 .1-.1.5-.1.6l.6-1.4c-.1.4 0 1.2-.2 1.4.1.1.2.2.3.1s-.1-.5 0-.6c.1.4.1.2.3.6-.1-.1.1-.6.2-.6s0 .6-.1.6l.3-.5c-.1.1 0 .5 0 .6 0-.1.2 0 .3 0l-.1-.1c.3.1.3-.8.6-.7 0 .1-.1.7 0 .8.1-.3.4-1.4.7-1.6v.2l.2-.2c-.2.4-.6 1.3-.8 1.6.1.1 0 .2.2.2 0 .2-.1.2-.2.2l.4.1c0-.3.3-.3.3-.6l-.4.3c0-.3.3-.9.6-.8.1.1-.1.6-.1.7 0-.1.3-.2.3-.1l-.1.2c.1.1.3-.4.4-.1.1 0 .1.5.2.3a5.84 5.84 0 01.2-1.9 1.63 1.63 0 01.3.6c.1.3-.2.7-.2 1a.35.35 0 01.1-.2c.1.1-.1.3.1.3.1-.3.3.1.1-.3 0-.2 0 0 .2 0a1.23 1.23 0 01.4-.8v.3c.3.1.4.2.7.3 0 .1.1.2 0 .3a.22.22 0 01.2-.2c.1.3-.2 0-.1.4 0-.2.2-.2.4-.3 0-.3-.3-.1-.4 0a1.7 1.7 0 01.6-1c.2-.1.1.3.1.2.4.1.5-.7.8-.3.1.3-.1.5-.1.7-.2.1-.2-.2-.4-.2l.1.1c-.1.1-.2.1-.2 0 .1.3.7.1.9.4.1-.1.2-.2.2-.3h-.1c-.1-.2.2-.3.1-.4.2-.1.3-.3.5-.2V2c0-.1-.1-.2-.2-.1.1-.1.2.5.4.1l-.1-.1c.2-.1.4-.4.5-.3 0-.3 0 0-.1-.3 0 .5-.4-.1-.3.4 0-.4-.2-.2-.3-.6 0 .1 0 .2-.1.1v.3c-.1.2-.3.2-.4.3-.1-.1.1-.1 0-.2l-.2.1c0-.2-.2-.4.1-.5h-.2c-.1-.1-.2-.2-.3-.1-.1-.1-.2.2-.1.1l-.3.1v-.1c-.3-.1-.6.3-.9-.2-.1 0-.2.1-.4 0V.81c-.1 0-.3-.2-.4 0-.1-.5-.6-.2-.8-.3v.3a1.09 1.09 0 00-.5.1v.2c-.2.2-.4-.2-.6-.2 0-.1.1-.1.1 0-.2-.6-.6.2-.8-.2-.1.1-.1 0-.2 0l-.1-.1c0 .1 0 .1.1.1-.1 0-.1 0-.2.1v-.2c-.1-.1-.1.1 0 .3L24 1c0-.4-.2.1-.3-.3-.2.1-.5 0-.5.5v-.1c-.1-.1-.2-.1-.2 0V.81l-.2.4a.45.45 0 010-.5c-.3.3-.2.1-.5.5V1c-.1 0-.2.3-.3.1-.2-.4-.9-.1-1.3-.5.1.6-.2-.3-.2.2 0-.1-.1-.2 0-.3-.2.1-.4-.2-.5.1 0-.1.1-.2 0-.3s-.1.1-.1 0 0-.1.1-.2c-.2 0-.3.4-.3.8-.1-.2-.2-.2-.3.1 0-.1-.1-.3 0-.3-.1 0-.5-.2-.5.1 0-.1-.1 0-.1 0-.2 0-.2-.1-.4 0l.1.1c-.1.4-.2.1-.3.1-.5-.3-.3.1-.7-.2V1c0 .5-.3-.3-.5-.1l.1.1c-.1.2-.3-.7-.4-.8a.1.1 0 01.1-.1c-.3-.3 0 .3-.2.4a.75.75 0 00-.1-.5c-.1-.1-.4.5-.6.2 0 .1.1.3 0 .4s-.4-.5-.5-.1V.41c-.1.1-.4.2-.4.4-.1-.8-.7.4-.7-.3l-.6.2v-.1c-.2 0-.3.2-.4.3s0-.1 0-.2c-.3-.1-.4-.1-.6.3v-.2c-.1.1-.5-.4-.8 0v-.1c-.3.3-.9-.6-1 .3l-.2.2c.3 0 0 .4.1.5-.1.1-.3-.2-.2-.3 0-.5-.4-.1-.4-.4a10.87 10.87 0 01-1.8-.1l.1.4h-.2c-.1-.1-.1-.3 0-.3-.1-.2-.2.2-.2.3-.3-.1.1-.4 0-.4H8c-.1.2-.1.4-.2.4s-.1-.2-.2-.2h-.4l.1.3c.1.1-.1-.1.1.3-.2-.5-1-.1-1.1-.2-.1.2-.3.2-.5.2.1.1.1.4 0 .4 0-.7-.3-.2-.6-.7.2.3-.4.1-.2.6-.1 0 0-.2-.2-.4-.2.3-.7-.1-1.1.1 0 .1.1.2 0 .4l-.2-.5c-.1.1 0 .6-.2.3 0 .1.1.2 0 .2-.9-.3-1.5.7-2.5.1.1.1.1.2 0 .2.1 0 0 .2.1.3l-.4-.3c0 .3-.4.1-.3.4l.1-.1c-.1.5.4.8.5 1.3.1-.4.5.5.7-.1.1.1 0 .2 0 .3 0-.3 0-.3.2-.3V4a2.16 2.16 0 011.1.1l-.1-.3c.1 0 .1.1.2.1 0-.4-.3 0-.3-.4.3.5.7-.1.9.5.2-.1-.1-.3.1-.3l.1.1v-.2c.1 0 .2.2.2.3a.1.1 0 00-.1.1c.2.1.3-.1.4-.1H4c.4-.3.9-.2 1.3-.5v-.1c.2-.3.2.1.5-.1A.22.22 0 016 3c-.1.2.2.3.1.4.3-.3.1-.2.3-.6l.1.2c0-.2 0-.3.2-.4-.1.2.1.3 0 .5.4.3.5-.5.7.1.1-.5-.3-.2-.3-.4s.1-.4.2-.3.3.6.6.6h-.1c.1.1.2-.2.3 0 0-.3.2 0 .1-.4l-.1.1c.1-.1.2-.6.5-.4 0 .1-.1.2-.1.3s.1-.1.2 0c0 .4-.3.1-.4.4s.2-.3.2.1c0-.6.3 0 .4-.4V3l.1-.2v.4c.1-.3.3.2.5 0-.3 0-.1-.3-.3-.4.3-.3.3.6.7.5a.22.22 0 01-.2-.2c.1 0 .2.1.3.2.3-.1-.1-.2 0-.4.1.3.1-.1.2-.2v.2c.3.1 0-.5.3-.4l-.1.3.1-.1V3c.1-.2.2-.2.4-.1a.44.44 0 01.1-.5c.1.1 0 .2.2.2 0 .2-.1.3-.2.1v.2c.1.2.3 0 .3 0h-.1l.3-.4a.14.14 0 010 .2c.1 0 .1-.1.2-.2a.75.75 0 01-.1.5l.3-.2c0 .1.2.2.1.3.2.1.4-.4.6-.3a.1.1 0 01.1-.1c.2 0 .4.3.6 0l.2.5c.2-.1-.2-.4.1-.6s.1.3.2.4c.1-.2.3-.5.5-.3a.31.31 0 01-.2.11zm2.2-.2c0 .1-.1.2-.2.1.1-.3.1-.2.2-.1zM19.7 2c0 .1-.1 0-.1-.1 0 .1 0 .1.1.1zm9.7-.7zm.2.1c-.1.01-.1.11 0 .01zm-22-.3z"></path><path d="M19.3 2.41v-.1c-.1 0-.1 0 0 .1zm6.9.59l-.1-.1a.1.1 0 00.1.1zm4.5-1.79h-.1a.1.1 0 01.1.1zm.8 1.7c.1-.1.1 0 0 0l.1-.2c-.1 0-.1.1-.1.2zM30.7 2c0 .2.1.1.2.1 0-.19-.1-.19-.2-.1zm-.9-.69c0 .1.1-.3.1-.2.1-.2-.3.3-.1.2zm.8 1.4v-.3.3zm-.6-.3l.1.1a.1.1 0 01.1-.1c-.1 0-.2-.1-.2 0zm.1 0zm.3.3l-.1-.1.1.1zm-2-.6h-.2c0 .1 0 .1.1.1s.1 0 .1-.1zm.4.7v-.2l-.1.3zm-.5 0c0 .1-.3.2-.1.2s.3-.1.1-.2zm-2.3-.7h.1v-.5l-.1.5zm-2.2 1c.2.1.1-.3.2-.1.1-.1-.1-.1-.1-.2 0 .1-.1 0-.1.3zM.2 2.21l-.1-.1.1.2v-.1zm-.1.4l-.1-.3v.3zm16.5.6c0-.2 0-.2-.1-.2zm-1.6-.2v-.3.3zm-1.3.4c.1 0 .1-.1.2 0-.2.1-.2-.3-.2 0z"></path></g></g></svg>
                </svg>
			</div>
			<p class="delivery__content-text">
				 Доставка еды из ресторанов "Синьор Помидор" в Хабаровске и Комсомольск-на-Амуре. Традиционная итальянская пицца, домашняя паста, свежие салаты, ароматные супы и разнообразие горячих блюд. Так же вкусно, как в кафе, только дома.
			</p>
 <a class="btn btn_del" href="/menu-devilery/">Заказать доставку</a>
		</div>
	</div>
</div>
 </section>    </main>






			</div><!--end row-->

		</div><!--end .container.bx-content-section-->
	</div><!--end .workarea-->
</main>
<footer class="footer">
    <div class="footer__container">
        <div class="wrap">
            <div class="footer-top">
                

    <div class="footer__col footer__menu">
        <a class="footer__headline" href="/menu/">Меню</a>
        <ul class="footer__list">

	    <li class="footer__item"><a class="footer__link" href="/reviews/">Отзывы</a></li>
		

	    <li class="footer__item"><a class="footer__link" href="/news/">Новости</a></li>
		

	    <li class="footer__item"><a class="footer__link" href="/rabota-u-nas/">Карьера</a></li>
		

	    <li class="footer__item"><a class="footer__link" href="/restaurants/">Рестораны</a></li>
		
        </ul>
    </div>
                <div class="footer__col footer__address">
                    <h4 class="footer__headline">Хабаровск</h4>
                    <ul class="footer__list">
                        <li class="footer__item">Доставляем еду: <span>с 8:00 до 21:00</span></li>
<li class="footer__item"><a class="footer__link footer__phone" href="tel:+74212960606">+7 (4212) 96 06 06</a></li>                    </ul>
                </div>
                <div class="footer__col footer__address">
                    <h4 class="footer__headline">Комсомольск-на-Амуре</h4>
                    <ul class="footer__list">
                        <li class="footer__item">Доставляем еду: <span>с 8:00 до 21:00</span></li>
<li class="footer__item"><a class="footer__link footer__phone" href="tel:+74217244040">+7 (4212) 24 40 40</a></li>
                    </ul>
                </div>
                <div class="footer__col footer__inst">
                    <h4 class="footer__headline">Мы в Instagram</h4><img class="inst-logo" src="/upload/images/instagram.png" alt="">
                    <div class="inst-feed">
                        <a class="inst-feed__item" href="https://www.instagram.com/spomidordv/"><img src="/upload/images/inst-1.png" alt=""></a>
<a class="inst-feed__item" href="https://www.instagram.com/spomidordv/"><img src="/upload/images/inst-2.png" alt=""></a>
<a class="inst-feed__item" href="https://www.instagram.com/spomidordv/"><img src="/upload/images/inst-3.png" alt=""></a>                    </div>
                </div>
            </div>
            <div class="footer-bot">
                <span class="footer-bot__copyright">© Синьор Помидор</span>
<span class="footer-bot__copyright">© Японский помидор</span>                <a class="footer-bot__confidenc" href="/politika-konfidentsialnosti/">Политика конфиденциальности</a>
<a class="footer-bot__wf" href="https://webformula.pro/">Разработка сайта — <span>Webformula</span></a></div>
        </div>
    </div>
</footer>


<div id="cooperation" class="mfp-hide popup-block">
    <div class="window__header">
        <h4 class="h4 window__title window__title_b-offset text-center">
            Заявка на сотрудничество
        </h4>
        <p class="block mb-2 text-center">Заполните, пожалуйста, форму заказа </p>
        <form action="#" class="form form-reset">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Номер телефона" class="input input_default input_yellow time-w__input phone-mask">
            </div>
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Название компании" class="input input_default input_yellow time-w__input phone-mask">
            </div>
            <div class="form__item form__item_s-offset">
                <label class="file-w file-w_offset" data-title="Прикрепить коммерческое предложение">
                    <input type="file" class="file-w__input">
                    <span class="file-w__label">
                        <img class="file-w__ico" src="/local/templates/pomidor/images/file-w-ico.svg" alt="">
                        <span class="file-w__title">
                            Прикрепить коммерческое предложение
                        </span>
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <label class="checkbox checkbox_yellow">
                    <input type="checkbox" class="checkbox__input">
                    <span class="checkbox__label">
                        Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <button class="btn btn_rounded btn_yellow btn_middle">
                    Откликнуться
                </button>
            </div>
        </form>
    </div>
</div>



<div id="popup-book" class="mfp-hide popup-block">
    <div class="window__header">
        <h4 class="h4 window__title text-center">
            Забронировать стол
        </h4>
    </div>
    <p class="block mb-2 text-center">Заполните, пожалуйста, форму заказа </p>
    <form action="#" class="form form-reset">
        <div class="form__item form__item_s-offset -custom-select">
            <select class="input input_default input_yellow">
                <option>Выберите кафе</option>
                <option value="Волочаевская, 8щ">Волочаевская, 8щ</option>
<option value="Ленина, 53">Ленина, 53</option>
<option value="Ленинградская, 28">Ленинградская, 28</option>
<option value="Муравьева-Амурского, 40">Муравьева-Амурского, 40</option>
<option value="Суворова, 32">Суворова, 32</option>
<option value="Карла Маркса, 202 (2 этаж)">Карла Маркса, 202 (2 этаж)</option>
<option value="Вахова, 2">Вахова, 2</option>
<option value="Ким Ю Чена, 44 (5 этаж)">Ким Ю Чена, 44 (5 этаж)</option>
<option value="Стрельникова, 16а">Стрельникова, 16а</option>
 
            </select>
        </div>
        <div class="row">
            <div class="col--md-6">
                <div class="form__item form__item_s-offset">
                    <div class="time-w">
                        <img class="time-w__ico" src="/upload/images/i-calendar.svg" alt="">
                        <input readonly type="text" placeholder="дд.мм.гггг" class="input input_default input_yellow time-w__input datepicker-here">
                    </div>
                </div>
            </div>
            <div class="col--md-6">
                <div class="form__item form__item_s-offset">
                    <div class="time-w">
                        <img class="time-w__ico" src="/upload/images/i-clock.png" alt="">
                        <input type="time"  class="input input_default input_yellow time-w__input time-w__input_s">
                    </div>
                </div>
            </div>
        </div>
        <p class="block mb-2 small">Для бронирования стола в выходные звоните по номеру желаемого кафе</p>
        <div class="form__item form__item_s-offset">
            <p class="block mb small">Количество человек</p>
            <div class="counter">
                <span class="counter__btn counter__btn_minus"></span>
                <input type="text" class="counter__input" value="2">
                <span class="counter__btn counter__btn_plus "></span>
            </div>
        </div>
        <div class="row">
            <div class="col--md-6">
                <div class="form__item form__item_s-offset">
                    <input type="text" placeholder="Имя" class="input input_default input_yellow">
                </div>
            </div>
            <div class="col--md-6">
                <div class="form__item form__item_s-offset">
                    <input type="text" placeholder="Телефон" class="input input_default input_yellow phone-mask">
                </div>
            </div>
        </div>
        <div class="form__item form__item_s-offset">
            <p class="block mb small">Приготовить блюда к приходу</p>
            <input type="text" placeholder="Пицца 4 сыра" class="input input_default input_yellow">
        </div>
        <div class="form__item form__item_s-offset">
            <textarea placeholder="Комментарии" class="input input_default input_yellow"></textarea>
        </div>
        <div class="form__item form__item_s-offset mt-1">
            <label class="checkbox checkbox_yellow">
                <input type="checkbox" class="checkbox__input">
                <span class="checkbox__label">
                        Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
                    </span>
            </label>
        </div>
        <div class="form__item form__item_s-offset">
            <a href="#" class="btn btn_yellow btn_middle">
                Забронировать
            </a>
        </div>
    </form>
</div>
<div id="popup-city" class="mfp-hide popup-block text-center">
    <div class="window__header">
        <h4 class="h4 window__title text-center">
            Выберите город
        </h4>
    </div>
    <div class="city-change" style="justify-content: space-between">
        <div class="city-change__item city-change__item_yellow city_select_item" data-cityname="komsomolsk" onclick="$.magnificPopup.close()">
            <img src="/upload/images/i-city1.png" height="93">
            <div class="city-change__title">Комсомольск-на-Амуре</div>
        </div>
        <div class="city-change__item city-change__item_green city_select_item" data-cityname="habarovsk" onclick="$.magnificPopup.close()">
            <img src="/upload/images/i-city2.png" height="93">
            <div class="city-change__title">Хабаровск</div>
        </div>
    </div>
</div>



	<div class="col d-sm-none">
		<div id="bx_basketpCwjw4" class="bx-basket-fixed right bottom"><!--'start_frame_cache_bx_basketpCwjw4'--><div class="basket-line">
	<div class="mb-1 basket-line-block">
					<a class="basket-line-block-icon-profile" href="/login/?login=yes&backurl=%2F">Войти</a>
							<a style="margin-right: 0;" href="/login/?register=yes&backurl=%2F">Регистрация</a>
							</div>

	<div class="basket-line-block">
					<a class="basket-line-block-icon-cart" href="/personal/cart/">Корзина</a>
			0 позиций					<br class="d-none d-block-sm"/>
					<span>
						на сумму <strong>0 &#8381;</strong>
					</span>
						</div>
</div><!--'end_frame_cache_bx_basketpCwjw4'--></div>
	</div>
</div> <!-- //bx-wrapper -->


<div id="card-popup" class="mfp-hide popup-block popup-block_big"></div>
<script type="text/javascript">if(!window.BX)window.BX={};if(!window.BX.message)window.BX.message=function(mess){if(typeof mess==='object'){for(let i in mess) {BX.message[i]=mess[i];} return true;}};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script><script type="text/javascript" src="/bitrix/js/main/core/core.js?1617219521549528"></script><script>BX.setJSList(['/bitrix/js/main/core/core_ajax.js','/bitrix/js/main/core/core_promise.js','/bitrix/js/main/polyfill/promise/js/promise.js','/bitrix/js/main/loadext/loadext.js','/bitrix/js/main/loadext/extension.js','/bitrix/js/main/polyfill/promise/js/promise.js','/bitrix/js/main/polyfill/find/js/find.js','/bitrix/js/main/polyfill/includes/js/includes.js','/bitrix/js/main/polyfill/matches/js/matches.js','/bitrix/js/ui/polyfill/closest/js/closest.js','/bitrix/js/main/polyfill/fill/main.polyfill.fill.js','/bitrix/js/main/polyfill/find/js/find.js','/bitrix/js/main/polyfill/matches/js/matches.js','/bitrix/js/main/polyfill/core/dist/polyfill.bundle.js','/bitrix/js/main/core/core.js','/bitrix/js/main/polyfill/intersectionobserver/js/intersectionobserver.js','/bitrix/js/main/lazyload/dist/lazyload.bundle.js','/bitrix/js/main/polyfill/core/dist/polyfill.bundle.js','/bitrix/js/main/parambag/dist/parambag.bundle.js']);
BX.setCSSList(['/bitrix/js/main/core/css/core.css','/bitrix/js/main/lazyload/dist/lazyload.bundle.css','/bitrix/js/main/parambag/dist/parambag.bundle.css']);</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s1','SITE_DIR':'/','USER_ID':'','SERVER_TIME':'1618225397','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'267ed9e5e952ae5d4ab61b495649c0aa'});</script><script type="text/javascript" src="/bitrix/js/main/core/core_fx.js?161721952116888"></script>
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-3.3.1.min.js?161721952186927"></script>
<script type="text/javascript" src="/bitrix/js/ui/bootstrap4/js/bootstrap.js?1617219520123765"></script>
<script type="text/javascript" src="/bitrix/js/main/popup/dist/main.popup.bundle.js?1617219521103976"></script>
<script type="text/javascript">
					(function () {
						"use strict";

						var counter = function ()
						{
							var cookie = (function (name) {
								var parts = ("; " + document.cookie).split("; " + name + "=");
								if (parts.length == 2) {
									try {return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()));}
									catch (e) {}
								}
							})("BITRIX_CONVERSION_CONTEXT_s1");

							if (cookie && cookie.EXPIRE >= BX.message("SERVER_TIME"))
								return;

							var request = new XMLHttpRequest();
							request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true);
							request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							request.send(
								"SITE_ID="+encodeURIComponent("s1")+
								"&sessid="+encodeURIComponent(BX.bitrix_sessid())+
								"&HTTP_REFERER="+encodeURIComponent(document.referrer)
							);
						};

						if (window.frameRequestStart === true)
							BX.addCustomEvent("onFrameDataReceived", counter);
						else
							BX.ready(counter);
					})();
				</script>



<script type="text/javascript" src="/local/templates/pomidor/js/jquery.matchHeight.js?161721957211778"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/jquery.magnific-popup.min.js?161721957220219"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/nouislider.min.js?161721957225247"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/select2.min.js?161721957270851"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/wNumb.min.js?16172195722235"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/aos.js?161721957214239"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/slick.js?161721957288955"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/jquery.mask.min.js?16172195728327"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/datepicker.min.js?161721957235542"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/script.js?161721957214302"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/custom.js?16182175832323"></script>
<script type="text/javascript" src="/local/templates/pomidor/components/bitrix/menu/main_menu/script.js?16172195726228"></script>
<script type="text/javascript" src="/local/templates/pomidor/components/bitrix/sale.basket.basket.line/bootstrap_v5/script.js?16172195725335"></script>
<script type="text/javascript" src="/bitrix/components/bitrix/sale.basket.basket.line/templates/bootstrap_v4/script.js?16172195535335"></script>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "5a87679950946de5e129cafc2b7136e7"]); _ba.push(["host", "seniorpomidor.digitalwf.ru"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>


<script>
    $(document).ready(function() {
        $.magnificPopup.open({
            items: {
                src: $('#popup-city')
            },
            type: 'inline'
        });
    })
    </script>
<script>
	BX.ready(function () {
		window.obj_catalog_menu_LkGdQn = new BX.Main.MenuComponent.CatalogHorizontal('catalog_menu_LkGdQn', {'1430168220':{'PICTURE':'','DESC':''},'2444424269':{'PICTURE':'','DESC':''},'2782047071':{'PICTURE':'','DESC':''},'3534752720':{'PICTURE':'','DESC':''},'4176003167':{'PICTURE':'','DESC':''},'1270155113':{'PICTURE':'','DESC':''},'36229728':{'PICTURE':'','DESC':''}});
	});
</script><script>
var bx_basketFKauiI = new BitrixSmallCart;
</script>
<script type="text/javascript">
	bx_basketFKauiI.siteId       = 's1';
	bx_basketFKauiI.cartId       = 'bx_basketFKauiI';
	bx_basketFKauiI.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketFKauiI.templateName = 'bootstrap_v5';
	bx_basketFKauiI.arParams     =  {'PATH_TO_BASKET':'/personal/cart/','PATH_TO_PERSONAL':'/personal/','SHOW_PERSONAL_LINK':'Y','SHOW_NUM_PRODUCTS':'Y','SHOW_TOTAL_PRICE':'Y','SHOW_PRODUCTS':'Y','POSITION_FIXED':'N','SHOW_AUTHOR':'Y','PATH_TO_REGISTER':'/login/','PATH_TO_PROFILE':'/personal/','COMPONENT_TEMPLATE':'bootstrap_v5','PATH_TO_ORDER':'/personal/order/make/','SHOW_EMPTY_VALUES':'Y','PATH_TO_AUTHORIZE':'/login/','SHOW_REGISTRATION':'Y','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'N','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','HIDE_ON_BASKET_PAGES':'Y','CACHE_TYPE':'A','POSITION_VERTICAL':'top','POSITION_HORIZONTAL':'right','MAX_IMAGE_SIZE':'70','AJAX':'N','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_PERSONAL':'/personal/','~SHOW_PERSONAL_LINK':'Y','~SHOW_NUM_PRODUCTS':'Y','~SHOW_TOTAL_PRICE':'Y','~SHOW_PRODUCTS':'Y','~POSITION_FIXED':'N','~SHOW_AUTHOR':'Y','~PATH_TO_REGISTER':'/login/','~PATH_TO_PROFILE':'/personal/','~COMPONENT_TEMPLATE':'bootstrap_v5','~PATH_TO_ORDER':'/personal/order/make/','~SHOW_EMPTY_VALUES':'Y','~PATH_TO_AUTHORIZE':'/login/','~SHOW_REGISTRATION':'Y','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'N','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~HIDE_ON_BASKET_PAGES':'Y','~CACHE_TYPE':'A','~POSITION_VERTICAL':'top','~POSITION_HORIZONTAL':'right','~MAX_IMAGE_SIZE':'70','~AJAX':'N','cartId':'bx_basketFKauiI'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketFKauiI.closeMessage = 'Скрыть';
	bx_basketFKauiI.openMessage  = 'Раскрыть';
	bx_basketFKauiI.activate();
</script><script>
var bx_basketT0kNhm = new BitrixSmallCart;
</script>
<script type="text/javascript">
	bx_basketT0kNhm.siteId       = 's1';
	bx_basketT0kNhm.cartId       = 'bx_basketT0kNhm';
	bx_basketT0kNhm.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketT0kNhm.templateName = 'bootstrap_v5';
	bx_basketT0kNhm.arParams     =  {'PATH_TO_BASKET':'/personal/cart/','PATH_TO_PERSONAL':'/personal/','SHOW_PERSONAL_LINK':'Y','SHOW_NUM_PRODUCTS':'Y','SHOW_TOTAL_PRICE':'Y','SHOW_PRODUCTS':'Y','POSITION_FIXED':'N','SHOW_AUTHOR':'Y','PATH_TO_REGISTER':'/login/','PATH_TO_PROFILE':'/personal/','COMPONENT_TEMPLATE':'bootstrap_v5','PATH_TO_ORDER':'/personal/order/make/','SHOW_EMPTY_VALUES':'Y','PATH_TO_AUTHORIZE':'/login/','SHOW_REGISTRATION':'Y','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'N','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','HIDE_ON_BASKET_PAGES':'Y','CACHE_TYPE':'A','POSITION_VERTICAL':'top','POSITION_HORIZONTAL':'right','MAX_IMAGE_SIZE':'70','AJAX':'N','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_PERSONAL':'/personal/','~SHOW_PERSONAL_LINK':'Y','~SHOW_NUM_PRODUCTS':'Y','~SHOW_TOTAL_PRICE':'Y','~SHOW_PRODUCTS':'Y','~POSITION_FIXED':'N','~SHOW_AUTHOR':'Y','~PATH_TO_REGISTER':'/login/','~PATH_TO_PROFILE':'/personal/','~COMPONENT_TEMPLATE':'bootstrap_v5','~PATH_TO_ORDER':'/personal/order/make/','~SHOW_EMPTY_VALUES':'Y','~PATH_TO_AUTHORIZE':'/login/','~SHOW_REGISTRATION':'Y','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'N','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~HIDE_ON_BASKET_PAGES':'Y','~CACHE_TYPE':'A','~POSITION_VERTICAL':'top','~POSITION_HORIZONTAL':'right','~MAX_IMAGE_SIZE':'70','~AJAX':'N','cartId':'bx_basketT0kNhm'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketT0kNhm.closeMessage = 'Скрыть';
	bx_basketT0kNhm.openMessage  = 'Раскрыть';
	bx_basketT0kNhm.activate();
</script>

<script>
var bx_basketpCwjw4 = new BitrixSmallCart;
</script>
<script type="text/javascript">bx_basketpCwjw4.currentUrl = '%2F';</script><script type="text/javascript">
	bx_basketpCwjw4.siteId       = 's1';
	bx_basketpCwjw4.cartId       = 'bx_basketpCwjw4';
	bx_basketpCwjw4.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketpCwjw4.templateName = 'bootstrap_v4';
	bx_basketpCwjw4.arParams     =  {'PATH_TO_BASKET':'/personal/cart/','PATH_TO_PERSONAL':'/personal/','SHOW_PERSONAL_LINK':'N','SHOW_NUM_PRODUCTS':'Y','SHOW_TOTAL_PRICE':'Y','SHOW_PRODUCTS':'N','POSITION_FIXED':'Y','POSITION_HORIZONTAL':'right','POSITION_VERTICAL':'bottom','SHOW_AUTHOR':'Y','PATH_TO_REGISTER':'/login/','PATH_TO_PROFILE':'/personal/','CACHE_TYPE':'A','PATH_TO_ORDER':'/personal/order/make/','HIDE_ON_BASKET_PAGES':'Y','SHOW_EMPTY_VALUES':'Y','SHOW_REGISTRATION':'Y','PATH_TO_AUTHORIZE':'/login/','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'Y','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','MAX_IMAGE_SIZE':'70','AJAX':'N','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_PERSONAL':'/personal/','~SHOW_PERSONAL_LINK':'N','~SHOW_NUM_PRODUCTS':'Y','~SHOW_TOTAL_PRICE':'Y','~SHOW_PRODUCTS':'N','~POSITION_FIXED':'Y','~POSITION_HORIZONTAL':'right','~POSITION_VERTICAL':'bottom','~SHOW_AUTHOR':'Y','~PATH_TO_REGISTER':'/login/','~PATH_TO_PROFILE':'/personal/','~CACHE_TYPE':'A','~PATH_TO_ORDER':'/personal/order/make/','~HIDE_ON_BASKET_PAGES':'Y','~SHOW_EMPTY_VALUES':'Y','~SHOW_REGISTRATION':'Y','~PATH_TO_AUTHORIZE':'/login/','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'Y','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~MAX_IMAGE_SIZE':'70','~AJAX':'N','cartId':'bx_basketpCwjw4'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketpCwjw4.closeMessage = 'Скрыть';
	bx_basketpCwjw4.openMessage  = 'Раскрыть';
	bx_basketpCwjw4.activate();
</script><script>
	BX.ready(function(){
		var upButton = document.querySelector('[data-role="eshopUpButton"]');
		BX.bind(upButton, "click", function(){
			var windowScroll = BX.GetWindowScrollPos();
			(new BX.easing({
				duration : 500,
				start : { scroll : windowScroll.scrollTop },
				finish : { scroll : 0 },
				transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
				step : function(state){
					window.scrollTo(0, state.scroll);
				},
				complete: function() {
				}
			})).animate();
		})
	});
</script>

<link rel="stylesheet" href="css/callbacks.css">

<div class="popup middle-popup tnx-popup" id="tnx">
    <img class="tnx-popup__img" src="images/pizza-popup.svg" alt="">
    <div class="">
        <h3 class="tnx-popup__title">Спасибо, ваша заявка принята!</h3>
        <div class="tnx-popup__desc">
            <p>
               Бронь действительна только после подтверждения гостевым менеджером 
            </p>
        </div>
        <a href="#" class="btn btn_green tnx-popup__btn">
            ok
        </a>
    </div>
</div>
<script>
    $(document).ready(function(){
        $.magnificPopup.open({
            items: {
                src: $('#tnx')
            },
            type: 'inline'
        });
    })
</script>
</body>
</html>