
<!DOCTYPE html>
<html xml:lang="ru" lang="ru">

<head>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;600;700;800;900&amp;display=swap"
        rel="stylesheet">
    <title>
        Меню    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="cmsmagazine" content="cd4d311df3276962226d4c5d3b0c994d" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="index, follow" />
<meta name="keywords" content="Синьор Помидор" />
<meta name="description" content="Синьор Помидор" />
<link href="/bitrix/js/main/core/css/core.css?16172195213963" type="text/css" rel="stylesheet" />

<script type="text/javascript" data-skip-moving="true">(function(w, d, n) {var cl = "bx-core";var ht = d.documentElement;var htc = ht ? ht.className : undefined;if (htc === undefined || htc.indexOf(cl) !== -1){return;}var ua = n.userAgent;if (/(iPad;)|(iPhone;)/i.test(ua)){cl += " bx-ios";}else if (/Android/i.test(ua)){cl += " bx-android";}cl += (/(ipad|iphone|android|mobile|touch)/i.test(ua) ? " bx-touch" : " bx-no-touch");cl += w.devicePixelRatio && w.devicePixelRatio >= 2? " bx-retina": " bx-no-retina";var ieVersion = -1;if (/AppleWebKit/.test(ua)){cl += " bx-chrome";}else if ((ieVersion = getIeVersion()) > 0){cl += " bx-ie bx-ie" + ieVersion;if (ieVersion > 7 && ieVersion < 10 && !isDoctype()){cl += " bx-quirks";}}else if (/Opera/.test(ua)){cl += " bx-opera";}else if (/Gecko/.test(ua)){cl += " bx-firefox";}if (/Macintosh/i.test(ua)){cl += " bx-mac";}ht.className = htc ? htc + " " + cl : cl;function isDoctype(){if (d.compatMode){return d.compatMode == "CSS1Compat";}return d.documentElement && d.documentElement.clientHeight;}function getIeVersion(){if (/Opera/i.test(ua) || /Webkit/i.test(ua) || /Firefox/i.test(ua) || /Chrome/i.test(ua)){return -1;}var rv = -1;if (!!(w.MSStream) && !(w.ActiveXObject) && ("ActiveXObject" in w)){rv = 11;}else if (!!d.documentMode && d.documentMode >= 10){rv = 10;}else if (!!d.documentMode && d.documentMode >= 9){rv = 9;}else if (d.attachEvent && !/Opera/.test(ua)){rv = 8;}if (rv == -1 || rv == 8){var re;if (n.appName == "Microsoft Internet Explorer"){re = new RegExp("MSIE ([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}else if (n.appName == "Netscape"){rv = 11;re = new RegExp("Trident/.*rv:([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}}return rv;}})(window, document, navigator);</script>


<link href="/bitrix/js/ui/fonts/opensans/ui.font.opensans.css?16172195202003" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/popup/dist/main.popup.bundle.css?161721952126339" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/core/css/core_date.css?161721952110289" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/fileman/sticker.css?161721952027011" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/core/css/core_panel.css?161721952168528" type="text/css"  rel="stylesheet" />
<link href="/bitrix/js/main/sidepanel/css/sidepanel.css?16172195227218" type="text/css"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/catalog/menu-pomidor/style.css?1617219572853" type="text/css"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/catalog.item/pomidor_catalog_item/style.css?161721957226506" type="text/css"  rel="stylesheet" />
<link href="/bitrix/wizards/bitrix/eshop/css/panel.css?1617219555240" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/styles.css?1617219572175798" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/panel/main/popup.css?161721953623084" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/menu/main_menu/style.css?161721957211894" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/catalog.section.list/menu_razdel/style.css?16172195727151" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/sale.basket.basket.line/bootstrap_v5/style.css?16172195724718" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/breadcrumb/pomidor_breadcrumb/style.css?1617219572585" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/components/bitrix/menu/footer_menu/style.css?1617219572581" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/components/bitrix/sale.basket.basket.line/templates/bootstrap_v4/style.css?16172195534718" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/panel/main/admin-public.css?161721953681795" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/panel/main/hot_keys.css?16172195363558" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/bitrix/themes/.default/pubstyles.css?161721955350596" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/aos.css?161721957226053" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/datepicker.min.css?161721957212257" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/magnific-popup.css?16172195727300" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/slick.css?16172195721735" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/variables.css?16172195722763" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/stylesheet.css?161721957284463" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/variables.min.css?16172195722501" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/sweetalert2.min.css?162010111824459" type="text/css"  data-template-style="true"  rel="stylesheet" />
<link href="/local/templates/pomidor/css/custom.css?16212471992694" type="text/css"  data-template-style="true"  rel="stylesheet" />







                                        <!--    -->
                                                        
    <style>
    @font-face {
        font-family: 'rub';
        font-weight: normal;
        font-style: normal;

        src: url('/local/templates/pomidor/fonts/rouble.woff') format('woff2'), url('/local/templates/pomidor/fonts/rouble.woff') format('woff');
    }
    </style>
</head>
<link rel="stylesheet" href="css/preload.css">

<body class="bx-background-image bx-theme-blue preload"     data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
       <img class="preload-pic" src="images/preloader-pic.gif" alt="">
        <div id="panel">
        
	<!--[if lte IE 7]>
	<style type="text/css">#bx-panel {display:none !important;}</style>
	<div id="bx-panel-error">Административная панель не поддерживает Internet Explorer версии 7 и ниже. Установите современный браузер <a href="http://www.firefox.com">Firefox</a>, <a href="http://www.google.com/chrome/">Chrome</a>, <a href="http://www.opera.com">Opera</a> или <a href="http://www.microsoft.com/windows/internet-explorer/">Microsoft Edge</a>.</div><![endif]-->
		<div style="display:none; overflow:hidden;" id="bx-panel-back"></div>
	<div id="bx-panel">
		<div id="bx-panel-top">
			<div id="bx-panel-top-gutter"></div>
			<div id="bx-panel-tabs">
	
			<a id="bx-panel-menu" href="" onmouseover="BX.hint(this, 'Меню', 'Быстрый переход на любую страницу Административной части.')"><span id="bx-panel-menu-icon"></span><span id="bx-panel-menu-text">Меню</span></a><div id="bx-panel-btn-wrap"><a id="bx-panel-view-tab"><span>Сайт</span></a>
				<a id="bx-panel-admin-tab" href="/bitrix/admin/index.php?lang=ru&amp;back_url_pub=%2Fmenu%2F"><span>Администрирование</span></a></div><a class="adm-header-notif-block" id="adm-header-notif-block" onclick="return BX.adminInformer.Toggle(this);" href="" title="Просмотр уведомлений"><span class="adm-header-notif-icon"></span><span id="adm-header-notif-counter" class="adm-header-notif-counter">6</span></a><a id="bx-panel-clear-cache" href="" onclick="BX.clearCache(); return false;"><span id="bx-panel-clear-cache-icon"></span><span id="bx-panel-clear-cache-text">Сбросить кеш</span></a>
			</div>
			<div id="bx-panel-userinfo">
	<a href="/bitrix/admin/user_edit.php?lang=ru&ID=1" id="bx-panel-user" onmouseover="BX.hint(this, 'Быстрый переход на страницу редактирования параметров пользователя.')"><span id="bx-panel-user-icon"></span><span id="bx-panel-user-text">Владислав1 Воробьёв</span></a><a href="/menu/?logout=yes" id="bx-panel-logout" onmouseover="BX.hint(this, 'Выход пользователя из системы. (&amp;nbsp;Ctrl+Alt+O&amp;nbsp;) ')">Выйти</a><a href="" id="bx-panel-expander" onmouseover="BX.hint(this, 'Развернуть', 'Развернуть Панель управления. (&amp;nbsp;Ctrl+Alt+E&amp;nbsp;) ')"><span id="bx-panel-expander-text">Развернуть</span><span id="bx-panel-expander-arrow"></span></a><a id="bx-panel-hotkeys" href="javascript:void(0)" onclick="BXHotKeys.ShowSettings();" onmouseover="BX.hint(this, 'Настройки горячих клавиш (&amp;nbsp;Ctrl+Alt+P&amp;nbsp;) ')"></a><a href="javascript:void(0)" id="bx-panel-pin" onmouseover="BX.hint(this, 'Фиксация Панели управления в верхней части экрана.')"></a>			</div>
		</div>
	<div id="bx-panel-site-toolbar"><div id="bx-panel-buttons-gutter"></div><div id="bx-panel-switcher"><a href="/menu/?bitrix_include_areas=Y" id="bx-panel-toggle" class="bx-panel-toggle-off" onmouseover="BX.hint(this, 'Режим правки', 'Включение режима контекстного редактирования сайта. При включенном режиме наведите курсор на выбранный блок информации и используйте всплывающие панели инструментов. (&amp;nbsp;Ctrl+Alt+D&amp;nbsp;) ')"><span id="bx-panel-switcher-gutter-left"></span><span id="bx-panel-toggle-indicator"><span id="bx-panel-toggle-icon"></span><span id="bx-panel-toggle-icon-overlay"></span></span><span class="bx-panel-break"></span><span id="bx-panel-toggle-caption">Режим правки</span><span class="bx-panel-break"></span><span id="bx-panel-toggle-caption-mode"><span id="bx-panel-toggle-caption-mode-off">выключен</span><span id="bx-panel-toggle-caption-mode-on">включен</span></span><span id="bx-panel-switcher-gutter-right"></span></a><a href="" id="bx-panel-hider" onmouseover="BX.hint(this, 'Свернуть', 'Свернуть Панель управления. (&amp;nbsp;Ctrl+Alt+E&amp;nbsp;) ')">Свернуть<span id="bx-panel-hider-arrow"></span></a></div><div id="bx-panel-buttons"><div id="bx-panel-buttons-inner"><span class="bx-panel-button-group" data-group-id="0"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_file_new.php?lang=ru&amp;site=s1&amp;templateID=pomidor&amp;path=%2Fmenu%2F&amp;back_url=%2Fmenu%2F&amp;siteTemplateId=pomidor','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_create"><span class="bx-panel-button-icon bx-panel-create-page-icon"></span></a><a id="bx_topmenu_btn_create_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Создать<span class="bx-panel-break"></span>страницу&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_file_new.php?lang=ru&amp;site=s1&amp;templateID=pomidor&amp;newFolder=Y&amp;path=%2Fmenu%2F&amp;back_url=%2Fmenu%2F&amp;siteTemplateId=pomidor','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_create_section"><span class="bx-panel-button-icon bx-panel-create-section-icon"></span></a><a id="bx_topmenu_btn_create_section_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Создать<span class="bx-panel-break"></span>раздел&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="1"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CEditorDialog({'content_url':'/bitrix/admin/public_file_edit.php?lang=ru&amp;path=%2Fmenu%2Findex.php&amp;site=s1&amp;back_url=%2Fmenu%2F&amp;templateID=pomidor&amp;siteTemplateId=pomidor','width':'780','height':'470','min_width':'780','min_height':'400'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_edit"><span class="bx-panel-button-icon bx-panel-edit-page-icon"></span></a><a id="bx_topmenu_btn_edit_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Изменить<span class="bx-panel-break"></span>страницу&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_folder_edit.php?lang=ru&amp;site=s1&amp;path=%2Fmenu%2F&amp;back_url=%2Fmenu%2F&amp;siteTemplateId=pomidor','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_edit_section"><span class="bx-panel-button-icon bx-panel-edit-section-icon"></span></a><a id="bx_topmenu_btn_edit_section_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Изменить<span class="bx-panel-break"></span>раздел&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="2"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_menus"><span class="bx-panel-small-button-icon bx-panel-menu-icon"></span><span class="bx-panel-small-button-text">Меню</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_structure.php?lang=ru&amp;site=s1&amp;path=%2Fmenu%2Findex.php&amp;templateID=pomidor&amp;siteTemplateId=pomidor','width':'350','height':'470'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_structure"><span class="bx-panel-small-button-icon bx-panel-site-structure-icon"></span><span class="bx-panel-small-button-text">Структура</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_structure_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CAdminDialog({'content_url':'/bitrix/admin/public_seo_tools.php?lang=ru&amp;bxpublic=Y&amp;from_module=seo&amp;site=s1&amp;path=%2Fmenu%2Findex.php&amp;title_final=0JzQtdC90Y4g0LrQsNGE0LU%3D&amp;title_changer_name=&amp;title_changer_link=&amp;title_win_final=&amp;title_win_changer_name=&amp;title_win_changer_link=&amp;sessid=ca1c4050ba04585a6c287526ad531818&amp;back_url=%2Fmenu%2F&amp;siteTemplateId=pomidor','width':'920','height':'400'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-active')" id="bx_topmenu_btn_seo"><span class="bx-panel-small-button-icon bx-panel-seo-icon"></span><span class="bx-panel-small-button-text">SEO</span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="3"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="BX.clearCache();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_0"><span class="bx-panel-button-icon bx-panel-clear-cache-icon"></span></a><a id="bx_topmenu_btn_0_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Сбросить<span class="bx-panel-break"></span>кеш&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="4"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_components"><span class="bx-panel-small-button-icon bx-panel-components-icon"></span><span class="bx-panel-small-button-text">Компоненты</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_1"><span class="bx-panel-small-button-icon bx-panel-site-template-icon"></span><span class="bx-panel-small-button-text">Шаблон сайта</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="/menu/?show_page_exec_time=Y&amp;show_include_exec_time=Y&amp;show_sql_stat=Y" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_2"><span class="bx-panel-small-button-icon bx-panel-performance-icon"></span><span class="bx-panel-small-button-text">Отладка</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_2_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="5"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CAdminDialog({'content_url':'/bitrix/admin/short_uri_edit.php?lang=ru&amp;public=Y&amp;bxpublic=Y&amp;str_URI=%2Fmenu%2F&amp;site=s1&amp;back_url=%2Fmenu%2F&amp;siteTemplateId=pomidor','width':'770','height':'270'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_3"><span class="bx-panel-small-button-icon bx-panel-short-url-icon"></span><span class="bx-panel-small-button-text">Короткий URL</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_3_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="if (window.oBXSticker){window.oBXSticker.AddSticker();};BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_4"><span class="bx-panel-small-button-icon bx-panel-small-stickers-icon"></span><span class="bx-panel-small-button-text">Стикеры</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_4_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="6"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_5"><span class="bx-panel-small-button-icon bx-panel-themes-icon"></span><span class="bx-panel-small-button-text">Настройка темы</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="7"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="/bitrix/admin/wizard_install.php?lang=ru&amp;wizardName=bitrix:eshop&amp;wizardSiteID=s1&amp;sessid=ca1c4050ba04585a6c287526ad531818" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_eshop_wizard" title="Запустить мастер смены дизайна и настроек сайта"><span class="bx-panel-button-icon bx-panel-site-wizard-icon"></span></a><a id="bx_topmenu_btn_eshop_wizard_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Мастер<span class="bx-panel-break"></span>настройки&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="8"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="/?add_new_site_sol=sol&amp;sessid=ca1c4050ba04585a6c287526ad531818" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_solutions_wizard"><span class="bx-panel-button-icon bx-panel-install-solution-icon"></span></a><a id="bx_topmenu_btn_solutions_wizard_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Протестировать<span class="bx-panel-break"></span>новое решение&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span></div>
			</div>
		</div>
	</div>
	<div class="adm-informer" id="admin-informer" style="display: none; top:48px; left:316px;" onclick="return BX.adminInformer.OnInnerClick(event);">
	<div class="adm-informer-header">Новые уведомления</div>
		<div class="adm-informer-item adm-informer-item-red" style="display:block">
			<div class="adm-informer-item-title">
				Автокомпозитный сайт
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-0">
					Уникальная технология позволит ускорить скорость загрузки страниц вашего сайта.
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-0">
				<a href="/bitrix/admin/composite.php?lang=ru">Включить АвтоКомпозит</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-green" style="display:block">
			<div class="adm-informer-item-title">
				Оцените решение Маркетплейс
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-1">
					Вы используете решение <b>YALSTUDIO переводчик сайта</b> (yalstudio.translate).<br />Поделитесь своими впечатлениями.<br />
Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
												<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-1">
				<a href="javascript:void(0)" onclick="hideMpAnswer(this, 'yalstudio.translate')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/yalstudio.translate/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'yalstudio.translate')">Оставить отзыв</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-green" style="display:block">
			<div class="adm-informer-item-title">
				Оцените решение Маркетплейс
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-2">
					Вы используете решение <b>Интернет-эквайринг Сбербанк</b> (sberbank.ecom2).<br />Поделитесь своими впечатлениями.<br />
Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
												<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-2">
				<a href="javascript:void(0)" onclick="hideMpAnswer(this, 'sberbank.ecom2')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/sberbank.ecom2/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'sberbank.ecom2')">Оставить отзыв</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-green" style="display:block">
			<div class="adm-informer-item-title">
				Оцените решение Маркетплейс
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-3">
					Вы используете решение <b>Простое оформление заказа</b> (tega.ordersimple).<br />Поделитесь своими впечатлениями.<br />
Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
												<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-3">
				<a href="javascript:void(0)" onclick="hideMpAnswer(this, 'tega.ordersimple')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/tega.ordersimple/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'tega.ordersimple')">Оставить отзыв</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-peach" style="display:block">
			<div class="adm-informer-item-title">
				Резервное копирование
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-4">
					
				<div class="adm-informer-item-section">
					<span class="adm-informer-item-l">
						<span class="adm-informer-strong-text">Всего:</span> 2 ГБ
					</span>
					<span class="adm-informer-item-r">
							<span class="adm-informer-strong-text">Доступно:</span> 6 МБ
					</span>
				</div>
				<div class="adm-informer-status-bar-block" >
					<div class="adm-informer-status-bar-indicator" style="width:100%; "></div>
					<div class="adm-informer-status-bar-text">100%</div>
				</div>
			<span class="adm-informer-strong-text">Выполнялось: 181 день назад.</span>
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-4">
				<a href="/bitrix/admin/dump.php?lang=ru">Рекомендуется выполнить</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-blue" style="display:block">
			<div class="adm-informer-item-title">
				Сканер безопасности
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-5">
					
<div class="adm-informer-item-section">
	<span class="adm-informer-item-l">
		<span>Сканирование выполнялось очень давно либо не выполнялось вовсе</span>
	</span>
</div>

					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-5">
				<a href="/bitrix/admin/security_scanner.php?lang=ru">Перейти к сканированию</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-gray" style="display:none">
			<div class="adm-informer-item-title">
				Обновления
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-6">
					<span class="adm-informer-strong-text">Версия системы 20.5.393</span><br>Последнее обновление<br>04.05.2021 06:58
					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-6">
				<a href="/bitrix/admin/update_system.php?refresh=Y&lang=ru">Проверить обновления</a>
				</div>
			</div>
		</div>
		<div class="adm-informer-item adm-informer-item-blue" style="display:none">
			<div class="adm-informer-item-title">
				Проактивная защита
			</div>
			<div class="adm-informer-item-body">
				<div class="adm-informer-item-html" id="adm-informer-item-html-7">
					
<div class="adm-informer-item-section">
	<span class="adm-informer-item-l">
		<span class="adm-informer-strong-text">Защита включена.<br></span>
		<span>Попыток вторжения не обнаружено</span>
	</span>
</div>

					<span class="adm-informer-icon"></span>
				</div>
				<div class="adm-informer-item-footer" id="adm-informer-item-footer-7">
				<a href="/bitrix/admin/security_filter.php?lang=ru">Настроить защиту</a>
				</div>
			</div>
		</div>
	<a href="javascript:void(0);" class="adm-informer-footer adm-informer-footer-collapsed" hidefocus="true" id="adm-informer-footer" onclick="return BX.adminInformer.ToggleExtra();" >Показать все (8) </a>
	<span class="adm-informer-arrow"></span>
</div>

    </div>
        <div class="bx-wrapper" id="bx_eshop_wrap">
        <header class="header">
            <div class="header-des">
                <div class="header-top">
                    <div class="wrap">
                        <div class="header-top__container">
                            <div class="block-left">
                                <a class="header__logo" href="/">
                                    <img src="/upload/images/logo.svg" alt="">                                </a>
                                <div class="header__translate"><a href="index.html#">RU<span
                                            class="header__translate-arrow"></span></a></div>
                                <div class="header__delivery"><span class="header__delivery-icon">
                                        <svg class="icon icon-del">
                                            <svg viewBox="0 0 361.3 240.8" id="icon-del">
                                                <g id="Слой_2" data-name="Слой 2">
                                                    <g id="Capa_1" data-name="Capa 1">
                                                        <path
                                                            d="M80.3 186.4a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2zm189.7 0a27.2 27.2 0 1027.2 27.2 27.23 27.23 0 00-27.2-27.2z">
                                                        </path>
                                                        <path
                                                            d="M340 109.3c-13.1-3.4-22-1.4-26.1-11.5l-11.4-39.5c-2.9-10.3-15.3-23.4-36.2-23.4l-34-2.6a3 3 0 01-3.2-3.1v-8.6c0-11.3-6.4-20.6-20-20.6H28.8C9.3 0 0 9.3 0 20.6v9.3s0 5.4 5.3 5.4h107.3c10 0 18.2 4.2 18.2 14.2V51c0 10-8.2 13.2-18.2 13.2H5.3A4.89 4.89 0 000 69.5v8.3c0 5 6.9 5 6.9 5h72.3c10 0 18.2 4.2 18.2 14.2v-.5c0 10-8.2 14.2-18.2 14.2H8s-8-.1-8 6.2v78.2a20.7 20.7 0 0020.6 20.6H32c2.6 0 3-1.4 3-2.2a46.6 46.6 0 1193.2 0c0 .7-.2 2.2 1.8 2.2h91.8c1.8 0 1.7-1.5 1.7-2.2a46.6 46.6 0 1193.2 0c0 .7 0 2.2 1.1 2.2h23.1a20.49 20.49 0 0020.4-20.4v-38.7c-.1-32.4-10.4-44.5-21.3-47.3zm-55.3-1.8h-52.9a2.67 2.67 0 01-2.7-2.3V56.9S229 55 232 55h28.1c15.9 0 23.3 4.1 25.5 14.5 1.9 8.9 4.2 23.3 4.2 23.3.3 1.6.5 11 .9 12.7-.1.9-1.4 2-6 2z">
                                                        </path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </svg>
                                    </span><span>Доставка еды <a class="underline popup-call" href="#popup-city">
                                            Хабаровск

                                        </a></span></div>
                                <div class="header__phone"><span class="header__phone-icon">
                                        <span class="header__phone-icon">
                                            <svg class="icon icon-phone">
                                                <svg viewBox="0 0 480.55 480.56" id="icon-phone">
                                                    <g id="Слой_2" data-name="Слой 2">
                                                        <g id="Capa_1" data-name="Capa 1">
                                                            <path
                                                                d="M365.35 317.9c-15.7-15.5-35.3-15.5-50.9 0-11.9 11.8-23.8 23.6-35.5 35.6-3.2 3.3-5.9 4-9.8 1.8-7.7-4.2-15.9-7.6-23.3-12.2-34.5-21.7-63.4-49.6-89-81-12.7-15.6-24-32.3-31.9-51.1-1.6-3.8-1.3-6.3 1.8-9.4 11.9-11.5 23.5-23.3 35.2-35.1 16.3-16.4 16.3-35.6-.1-52.1-9.3-9.4-18.6-18.6-27.9-28-9.6-9.6-19.1-19.3-28.8-28.8-15.7-15.3-35.3-15.3-50.9.1-12 11.8-23.5 23.9-35.7 35.5-11.3 10.7-17 23.8-18.2 39.1-1.9 24.9 4.2 48.4 12.8 71.3 17.6 47.4 44.4 89.5 76.9 128.1 43.9 52.2 96.3 93.5 157.6 123.3 27.6 13.4 56.2 23.7 87.3 25.4 21.4 1.2 40-4.2 54.9-20.9 10.2-11.4 21.7-21.8 32.5-32.7 16-16.2 16.1-35.8.2-51.8q-28.5-28.65-57.2-57.1zm-19.1-79.7l36.9-6.3A165.63 165.63 0 00243.05 96l-5.2 37.1a128 128 0 01108.4 105.1z">
                                                            </path>
                                                            <path
                                                                d="M404 77.8A272.09 272.09 0 00248 0l-5.2 37.1a237.42 237.42 0 01200.9 194.7l36.9-6.3A274.08 274.08 0 00404 77.8z">
                                                            </path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </svg>
                                        </span>
                                                                                <a class="header__link" href="tel:+74212960606">+7 (4212) 96 06 06</a>

                                </div>
                            </div>
                            <div class="block-right">
                                <a class="header__link" href="/territoriya-dostavki/">Карта доставки </a><a
                                    class="btn header__btn header__btn_del btn_del" href="/menu-devilery/">Заказать
                                    доставку</a>
                                <a class="btn header__btn header__btn_log" href="/personal">

                                    <svg class="icon">
                                        <use xlink:href="http://pomidor/upload/images/icons.svg#icon-prof"></use>
                                        <svg viewBox="0 0 352.28 440.6" id="icon-prof">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <path
                                                    d="M205.6 222.4a113.1 113.1 0 10-56-.1c-60.3 5.4-111.8 54-123.4 91-1.8 5.8-13.5 37.8 1.4 41.4 13.4 3.2 55 6.1 78.4 7.7a14.31 14.31 0 0113.4 14.2v4.4a14.85 14.85 0 01-15.8 14.9c-33.5-2.3-72.2-3-87.9-9.4-4.7-1.9-9.4.2-11.9 5-2.7 5.2-3.8 7.8-3.8 18.9 0 18.4 11.9 30.2 30.3 30.2h291.6c17.5 0 31.6-14.7 30.3-32.2-6.2-83.2-38.8-172.2-146.6-186z"
                                                    id="Layer_1" data-name="Layer 1"></path>
                                            </g>
                                        </svg>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bot">
                    <div class="wrap">
                        <div class="header-bot__container"><a class="btn__reserv popup-call" href="#popup-book"> <span
                                    class="btn__reserv-text">Забронировать стол</span>
                                <div class="btn__reserv-bg">
                                    <svg class="icon reserv-icon">
                                        <use xlink:href="http://pomidor/upload/images/icons.svg#reserv"></use>
                                        <svg viewBox="0 0 341.25 55.69" id="reserv">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <path
                                                    d="M338.35 1.41c.7.6.9 1.1.5 1.8a16.67 16.67 0 01-2.5 3.6 23.29 23.29 0 00-3.4 3.7c-.6 1.8-3.3 5.9-3.7 8-1.4 3.2-1.8 6.4-3 8.8 2.3 2.9 4.6 5.9 6.9 8.9s4.6 10.1 6.8 13c1.5 2.1 1.5 2.4 1 5a3.49 3.49 0 01-2.5.9c-1.6 0-3.2.1-4.8.1-9.6.5-19.2.5-28.8.4-2.4 0 15.1-.2 12.7-.3-2.8-.1-5.5-.1-8.3-.1-3.1-.1-6.2-.1-9.3-.2-4.1-.1-100.2-.3-104.3-.3h-21.1c-4.1 0-8.1 0-12.2.1-3.1 0-6.2 0-9.3.1-3.8.1-27.7.2-31.5.3s-7.9.1-11.8.2-8 .1-12 .2H43a36.17 36.17 0 00-5.7-.1c-1.9.2-3.8 0-5.7.1q-3.15.15-6.3 0l73.7-.2H38.75c-2.2 0-4.5-.1-6.7-.1L3.85 55a31.06 31.06 0 01-3.3-.3c-.7-.7-.7-1.3-.2-1.9 1.6-1.7 3.3-3.3 4.8-5 3.2-3.6 6.8-14.1 9.7-18a8.79 8.79 0 01-.6-1c-2-3-4-10.9-6-13.9-.3-.4-.6-1.8-.9-2.2-1.9-2.5-3.8-4.9-5.7-7.4-.3-.8-.8-1.7-1.2-2.6S.35.5 2.45.3l7.6-.3c7.8-.1 15.6.4 23.5.3 4.4 0 58.7.3 63.1.3 2.9.1-74.3 0-71.4 0 5.4 0 10.9.1 16.3.1 4.4 0 58.7 0 63.1-.1 1.4 0 2.8-.1 4.1-.1 4.5-.1 8.9-.3 13.4-.4 4.9 0 29.8 0 34.7.1h2.9a48.74 48.74 0 015.4.2c2.1.2 4.2.1 6.4.1 4.1 0 8.3.1 12.4-.2 2.2-.1 4.5.2 6.7.2h13.1c5.5 0 90.7-.5 96.2.1.5.1-95.6-.1-95.1-.1 4.8.1 98.6.5 103.4.6 4.3.1 8.5.5 12.7.7 3.9.1 7.9.4 11.8-.2 1.9-.09 3.8-.09 5.6-.19z"
                                                    id="Слой_1-2" data-name="Слой 1"></path>
                                            </g>
                                        </svg>
                                    </svg>
                                </div>
                            </a>

                            	<nav class="header__nav" id="cont_catalog_menu_LkGdQn">
		<ul class="header__nav-list" id="ul_catalog_menu_LkGdQn">
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/about/"
									>
                    						О нас										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col header__nav-link-active  menu-trigger"
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/menu/"
									>
                                        <svg class="icon icon-tomat">
                        <svg viewBox="0 0 621.93 745.7" id="icon-tomat"><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z"></path><path d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z"></path></g></g></svg>
                    </svg>
                        						Меню кафе										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/restaurants/"
									>
                    						Кафе										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/loyalty-program/"
									>
                    						Программа лояльности										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/shares/"
									>
                    						Акции										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link "
					href="/rabota-u-nas/"
									>
                    						Работа у нас										</a>
							</li>
					<li
				class="header__nav-item  bx-nav-list-0-col "
				onmouseover="BX.CatalogMenu.itemOver(this);"
				onmouseout="BX.CatalogMenu.itemOut(this)"
							>
                
				<a
					class=" header__nav-link  menu-modal"
					href="#cooperation"
									>
                    						Сотрудничество										</a>
							</li>
				</ul>
	</nav>


                            <!--region menu-->

                            
<div class="menu-window ">    <div class="menu-window__wrapp">

                        <a href="/menu/hot-meals/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/4f4/4f4e71527e1182b842dad98ab86a3b7f.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Горячие блюда                       </span>
        </a>
                                              <a href="/menu/desserts/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/1ec/1ecee1f62965e83dccf58674dcd133cd.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Десерты                       </span>
        </a>
                                                                    <a href="/menu/paste/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/65a/65a506ed38e19a197cf348e824d6592b.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Паста                       </span>
        </a>
                                              <a href="/menu/pizza/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/19e/19eab2327c7f27a2cf6cb80470ee31c3.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Пицца                       </span>
        </a>
                                              <a href="/menu/rolls/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/7c1/7c13f00bfd8d29815a5f9b1b895bafb9.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Роллы                       </span>
        </a>
                                              <a href="/menu/salads/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/da0/da0ca150512fe741bd8f8c6fdbdd22f1.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Салаты                       </span>
        </a>
                                              <a href="/menu/soups-ramen/" class="menu-window__item">
                <span class="menu-window__img">
                           <img class="menu-window__ico" src="/upload/iblock/315/3158f3faf295ccbc37b9ec8ded336afa.png" alt="">
                       </span>
            <span class="menu-window__title">
                           Супы                       </span>
        </a>
                      
    </div>
</div>
                            <!--endregion-->
                            <div id="bx_basketFKauiI " class="bx-basket bx-opener d-flex align-items-center"><!--'start_frame_cache_bx_basketFKauiI'-->		                <span class="header__price page-nav">810 &#8381;                        </span>


                
			<a class="btn header__btn btn__cart popup-call" href="#popup-cart">
<!--                -->                <svg class="icon icon-cart-h">
                    <svg viewBox="0 0 69.65 73.8" id="icon-cart-h"><g id="Слой_2" data-name="Слой 2"><g id="Layer_1" data-name="Layer 1"><path d="M34.77 3.5c-21.2 0-21.7 25.3-21.7 25.3h43.4S56 3.5 34.77 3.5z" fill="none" stroke-miterlimit="10" stroke-width="7"></path><path d="M68.67 30a4.84 4.84 0 00-4.8-4H5.77A4.84 4.84 0 001 30C.2 34.5-.7 42.4 1 50.2c.1 0 .1 0 .2.1s10.7 3.3 29 4.8c1.7.1 2.9 2.6 2.8 4.2s-1.4 3.8-3 3.8h-.2C17.87 62.2 8.87 61 3.67 59 8 67.7 17 73.8 34.87 73.8c38.2 0 35.9-32.7 33.8-43.8z"></path></g></g></svg>
                </svg>
                                        <span class="cart__count">2</span>
                        
            </a>
			
<div id="popup-cart" class="mfp-hide popup-block popup-block_small">
    <div class="window__header window__header_sb">
        <h4 class="h4 window__title text-center">
            Корзина
        </h4>
                <a href="#" class="link link_decorated" onclick="clear_basket(event)">Очистить корзину</a>
    </div>
    <div class="popup-cart">
            <hr class="w-total__hr">
        <div class="comporison-list">
                                    <div class="comporison">
                <div class="product-lite">
                    <a href="#" class="product-lite__img">
                                            <img src="/upload/resize_cache/iblock/caf/152_152_2/caffac84b79200d05ff3b15b934cc6e4.jpg" alt="">
                    </a>
                    <div class="product-lite__content">
                        <a href="#" class="basket-item__remove"></a>
                        <a href="#" class="product-lite__title">Чикконе с курицей</a>
                        <span class="product-lite__weight">
                            450&nbsp;г.
                        </span>
                        <div class="order-edited">
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_plus"></i>
                                <span class="order-edited__title">
                                    маслины, помидоры
                                </span>
                            </span>
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_minus"></i>
                                <span class="order-edited__title">
                                    сыр, колбаски
                                </span>
                            </span>
                        </div>
                        <div class="product-lite__price">
                            <div class="counter">
                                <button class="counter__btn counter__btn_minus"></button>
                                <input type="text" class="counter__input" value="1">
                                <button class="counter__btn counter__btn_plus "></button>
                            </div>
                            <span class="price comporison__price medium green">
                                450 <span class="rub">i</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
                                    <div class="comporison">
                <div class="product-lite">
                    <a href="#" class="product-lite__img">
                                            <img src="/upload/resize_cache/iblock/b13/152_152_2/b13e0ee98f0af7b3e4d465f529f720f2.jpg" alt="">
                    </a>
                    <div class="product-lite__content">
                        <a href="#" class="basket-item__remove"></a>
                        <a href="#" class="product-lite__title">Пять сыров</a>
                        <span class="product-lite__weight">
                            450&nbsp;г.
                        </span>
                        <div class="order-edited">
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_plus"></i>
                                <span class="order-edited__title">
                                    маслины, помидоры
                                </span>
                            </span>
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_minus"></i>
                                <span class="order-edited__title">
                                    сыр, колбаски
                                </span>
                            </span>
                        </div>
                        <div class="product-lite__price">
                            <div class="counter">
                                <button class="counter__btn counter__btn_minus"></button>
                                <input type="text" class="counter__input" value="1">
                                <button class="counter__btn counter__btn_plus "></button>
                            </div>
                            <span class="price comporison__price medium green">
                                450 <span class="rub">i</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
                </div>
        <div class="product-lite__title">Рекомендуем</div>
        <div class="cart-slider">
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
        </div>
        <div class="grey-bg">
            <span class="notif-title">
                До бесплатной доставки осталось <b>100&nbsp;<span class="rub">i</span></b>
            </span>
            <div class="w-total__content">
                <div class="key-val w-total__line">
                    <span class="key w-total__key">
                        Товары
                    </span>
                    <span class="val w-total__val green">
                        810 <span class="rub">i</span>
                    </span>
                </div>
                <div class="key-val w-total__line">
                    <span class="key w-total__key">
                        С учетом скидки
                    </span>
                    <span class="val w-total__val green">
                        810 <span class="rub">i</span>
                    </span>
                </div>
                <div class="key-val w-total__line">
                    <span class="key w-total__key">
                        Доставка
                    </span>
                    <span class="val w-total__val green">
                        200 <span class="rub">i</span>
                    </span>
                </div>
                <div class="w-total__balls">
                    <div class="key-val w-total__line">
                        <span class="key middle w-total__key">
                            Начислится баллов
                        </span>
                        <span class="val w-total__val">
                            20
                        </span>
                    </div>
                </div>
            </div>
            <div class="w-result w-result_s">
                <span class="w-result__title">
                    Итог
                </span>
                <span class="val w-total__val">
                    1400 <span class="rub">i</span>
                </span>
            </div>
            <a href=" /personal/cart/" class="btn w-result__btn btn_default btn_yellow">
                Далее
            </a>
        </div>
    </div>


</div>
<!--'end_frame_cache_bx_basketFKauiI'--></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-mobile mobile">
                <div class="mobile-top">
                    <div class="mobile__logo">
                        <img src="/upload/images/logo.svg" alt="">                    </div>
                    <div class="mobile__buttons">
                        <a class="btn header__btn header__btn_log mobile__btn" href="index.html#">
                            <svg class="icon mobile__icon">
                                <svg viewBox="0 0 352.28 440.6" id="icon-prof">
                                    <g id="Слой_2" data-name="Слой 2">
                                        <path
                                            d="M205.6 222.4a113.1 113.1 0 10-56-.1c-60.3 5.4-111.8 54-123.4 91-1.8 5.8-13.5 37.8 1.4 41.4 13.4 3.2 55 6.1 78.4 7.7a14.31 14.31 0 0113.4 14.2v4.4a14.85 14.85 0 01-15.8 14.9c-33.5-2.3-72.2-3-87.9-9.4-4.7-1.9-9.4.2-11.9 5-2.7 5.2-3.8 7.8-3.8 18.9 0 18.4 11.9 30.2 30.3 30.2h291.6c17.5 0 31.6-14.7 30.3-32.2-6.2-83.2-38.8-172.2-146.6-186z"
                                            id="Layer_1" data-name="Layer 1"></path>
                                    </g>
                                </svg>
                            </svg></a>
                        <div id="bx_basketT0kNhm " class="bx-basket bx-opener d-flex align-items-center"><!--'start_frame_cache_bx_basketT0kNhm'-->		                <span class="header__price page-nav">810 &#8381;                        </span>


                
			<a class="btn header__btn btn__cart popup-call" href="#popup-cart">
<!--                -->                <svg class="icon icon-cart-h">
                    <svg viewBox="0 0 69.65 73.8" id="icon-cart-h"><g id="Слой_2" data-name="Слой 2"><g id="Layer_1" data-name="Layer 1"><path d="M34.77 3.5c-21.2 0-21.7 25.3-21.7 25.3h43.4S56 3.5 34.77 3.5z" fill="none" stroke-miterlimit="10" stroke-width="7"></path><path d="M68.67 30a4.84 4.84 0 00-4.8-4H5.77A4.84 4.84 0 001 30C.2 34.5-.7 42.4 1 50.2c.1 0 .1 0 .2.1s10.7 3.3 29 4.8c1.7.1 2.9 2.6 2.8 4.2s-1.4 3.8-3 3.8h-.2C17.87 62.2 8.87 61 3.67 59 8 67.7 17 73.8 34.87 73.8c38.2 0 35.9-32.7 33.8-43.8z"></path></g></g></svg>
                </svg>
                                        <span class="cart__count">2</span>
                        
            </a>
			
<div id="popup-cart" class="mfp-hide popup-block popup-block_small">
    <div class="window__header window__header_sb">
        <h4 class="h4 window__title text-center">
            Корзина
        </h4>
                <a href="#" class="link link_decorated" onclick="clear_basket(event)">Очистить корзину</a>
    </div>
    <div class="popup-cart">
            <hr class="w-total__hr">
        <div class="comporison-list">
                                    <div class="comporison">
                <div class="product-lite">
                    <a href="#" class="product-lite__img">
                                            <img src="/upload/resize_cache/iblock/caf/152_152_2/caffac84b79200d05ff3b15b934cc6e4.jpg" alt="">
                    </a>
                    <div class="product-lite__content">
                        <a href="#" class="basket-item__remove"></a>
                        <a href="#" class="product-lite__title">Чикконе с курицей</a>
                        <span class="product-lite__weight">
                            450&nbsp;г.
                        </span>
                        <div class="order-edited">
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_plus"></i>
                                <span class="order-edited__title">
                                    маслины, помидоры
                                </span>
                            </span>
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_minus"></i>
                                <span class="order-edited__title">
                                    сыр, колбаски
                                </span>
                            </span>
                        </div>
                        <div class="product-lite__price">
                            <div class="counter">
                                <button class="counter__btn counter__btn_minus"></button>
                                <input type="text" class="counter__input" value="1">
                                <button class="counter__btn counter__btn_plus "></button>
                            </div>
                            <span class="price comporison__price medium green">
                                450 <span class="rub">i</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
                                    <div class="comporison">
                <div class="product-lite">
                    <a href="#" class="product-lite__img">
                                            <img src="/upload/resize_cache/iblock/b13/152_152_2/b13e0ee98f0af7b3e4d465f529f720f2.jpg" alt="">
                    </a>
                    <div class="product-lite__content">
                        <a href="#" class="basket-item__remove"></a>
                        <a href="#" class="product-lite__title">Пять сыров</a>
                        <span class="product-lite__weight">
                            450&nbsp;г.
                        </span>
                        <div class="order-edited">
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_plus"></i>
                                <span class="order-edited__title">
                                    маслины, помидоры
                                </span>
                            </span>
                            <span class="order-edited__item order-edited__item_offset">
                                <i class="order-edited__ico order-edited__ico_minus"></i>
                                <span class="order-edited__title">
                                    сыр, колбаски
                                </span>
                            </span>
                        </div>
                        <div class="product-lite__price">
                            <div class="counter">
                                <button class="counter__btn counter__btn_minus"></button>
                                <input type="text" class="counter__input" value="1">
                                <button class="counter__btn counter__btn_plus "></button>
                            </div>
                            <span class="price comporison__price medium green">
                                450 <span class="rub">i</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
                </div>
        <div class="product-lite__title">Рекомендуем</div>
        <div class="cart-slider">
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
            <div class="cart-slider__item">
                <img src="/local/templates/pomidor/images/cart-preview.jpg" alt="">
                <div class="product-lite__title">Мясная</div>
                <span>450 <span class="rub">i</span></span>
                <a href="#" class="btn btn_default btn_yellow_bordered">В корзину</a>
            </div>
        </div>
        <div class="grey-bg">
            <span class="notif-title">
                До бесплатной доставки осталось <b>100&nbsp;<span class="rub">i</span></b>
            </span>
            <div class="w-total__content">
                <div class="key-val w-total__line">
                    <span class="key w-total__key">
                        Товары
                    </span>
                    <span class="val w-total__val green">
                        810 <span class="rub">i</span>
                    </span>
                </div>
                <div class="key-val w-total__line">
                    <span class="key w-total__key">
                        С учетом скидки
                    </span>
                    <span class="val w-total__val green">
                        810 <span class="rub">i</span>
                    </span>
                </div>
                <div class="key-val w-total__line">
                    <span class="key w-total__key">
                        Доставка
                    </span>
                    <span class="val w-total__val green">
                        200 <span class="rub">i</span>
                    </span>
                </div>
                <div class="w-total__balls">
                    <div class="key-val w-total__line">
                        <span class="key middle w-total__key">
                            Начислится баллов
                        </span>
                        <span class="val w-total__val">
                            20
                        </span>
                    </div>
                </div>
            </div>
            <div class="w-result w-result_s">
                <span class="w-result__title">
                    Итог
                </span>
                <span class="val w-total__val">
                    1400 <span class="rub">i</span>
                </span>
            </div>
            <a href=" /personal/cart/" class="btn w-result__btn btn_default btn_yellow">
                Далее
            </a>
        </div>
    </div>


</div>
<!--'end_frame_cache_bx_basketT0kNhm'--></div>
                        <div class="mobile-burger" href="#"><span></span><span></span><span></span></div>
                    </div>
                </div>
                <div class="mobile-middle">
                    <nav class="mobile__nav">
                        <ul class="mobile__nav-list">
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/about/">О нас</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link header__nav-link-active"
                                    href="/menu/">
                                    <svg class="icon icon-tomat">
                                        <svg viewBox="0 0 621.93 745.7" id="icon-tomat">
                                            <g id="Слой_2" data-name="Слой 2">
                                                <g id="Слой_1-2" data-name="Слой 1">
                                                    <path
                                                        d="M404.19 279.6s-62.1 62.1 0 155.4c0 0-155.3 0-155.3-155.4-77.7 15.5-158.5-6.2-217.5-62.1 142.9 0 155.4-52.8 248.6-62.1C289.19 18.6 360.69 0 404.19 0v62.1c-37.3 0-55.9 31.1-62.1 96.3 90.1 9.3 80.8 59 248.6 59-.1.1-93.3 124.4-186.5 62.2z">
                                                    </path>
                                                    <path
                                                        d="M82.59 581.4c28.6 7.7 87 8.5 102.9 10.3s41.8-6.4 44.4-34.7c1.7-18.6-20.2-36.5-38.8-36.8-12.7-.2-77.4-6.2-99.1-9.6s-57.1-23.3-67.5-29.7S3 462.5 3 462.5C-.8 443.1.1 441.3.1 420.8c0-59 18.6-100.8 52.8-147.4 49.7 31.1 108.7 46.6 167.8 40.4C236.2 403.9 313.9 469.2 404 466c24.9 0 40.4-28 24.9-49.7-21.7-28-28-65.2-15.5-96.3 59 21.7 118.1-6.2 161.6-40.4 31.1 46.6 49.7 99.4 46.6 155.4 0 170.9-139.8 310.7-310.7 310.7-171.9 0-248-93.8-295.8-204.1 0 0 9.3 10.1 15.6 14.8s30.3 19.2 51.89 25z">
                                                    </path>
                                                </g>
                                            </g>
                                        </svg>
                                    </svg>Меню</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/restaurants/">Рестораны</a>
                            </li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/loyalty-program/">Программа
                                    лояльности</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/shares/">Акции</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link" href="/rabota-u-nas/">Работа у
                                    нас</a></li>
                            <li class="mobile__nav-item"><a class="mobile__nav-link"
                                    href="/cooperation/">Сотрудничество</a></li>
                        </ul>
                    </nav>
                    <div class="mobile-bot">
<!--                        <div class="mobile-bot__buttons">-->
<!--                            <div class="mobile-bot__del"><a class="btn header__btn header__btn_del btn_del"-->
<!--                                    href="index.html#">Заказать доставку</a></div><a class="btn btn__reserv"-->
<!--                                href="index.html#"> <span class="btn__reserv-text popup-call"-->
<!--                                    href="#popup-book">Забронировать стол</span>-->
<!--                                <div class="btn__reserv-bg">-->
<!--                                    <svg class="icon reserv-icon">-->
<!--                                        <svg viewBox="0 0 341.25 55.69" id="reserv">-->
<!--                                            <g id="Слой_2" data-name="Слой 2">-->
<!--                                                <path-->
<!--                                                    d="M338.35 1.41c.7.6.9 1.1.5 1.8a16.67 16.67 0 01-2.5 3.6 23.29 23.29 0 00-3.4 3.7c-.6 1.8-3.3 5.9-3.7 8-1.4 3.2-1.8 6.4-3 8.8 2.3 2.9 4.6 5.9 6.9 8.9s4.6 10.1 6.8 13c1.5 2.1 1.5 2.4 1 5a3.49 3.49 0 01-2.5.9c-1.6 0-3.2.1-4.8.1-9.6.5-19.2.5-28.8.4-2.4 0 15.1-.2 12.7-.3-2.8-.1-5.5-.1-8.3-.1-3.1-.1-6.2-.1-9.3-.2-4.1-.1-100.2-.3-104.3-.3h-21.1c-4.1 0-8.1 0-12.2.1-3.1 0-6.2 0-9.3.1-3.8.1-27.7.2-31.5.3s-7.9.1-11.8.2-8 .1-12 .2H43a36.17 36.17 0 00-5.7-.1c-1.9.2-3.8 0-5.7.1q-3.15.15-6.3 0l73.7-.2H38.75c-2.2 0-4.5-.1-6.7-.1L3.85 55a31.06 31.06 0 01-3.3-.3c-.7-.7-.7-1.3-.2-1.9 1.6-1.7 3.3-3.3 4.8-5 3.2-3.6 6.8-14.1 9.7-18a8.79 8.79 0 01-.6-1c-2-3-4-10.9-6-13.9-.3-.4-.6-1.8-.9-2.2-1.9-2.5-3.8-4.9-5.7-7.4-.3-.8-.8-1.7-1.2-2.6S.35.5 2.45.3l7.6-.3c7.8-.1 15.6.4 23.5.3 4.4 0 58.7.3 63.1.3 2.9.1-74.3 0-71.4 0 5.4 0 10.9.1 16.3.1 4.4 0 58.7 0 63.1-.1 1.4 0 2.8-.1 4.1-.1 4.5-.1 8.9-.3 13.4-.4 4.9 0 29.8 0 34.7.1h2.9a48.74 48.74 0 015.4.2c2.1.2 4.2.1 6.4.1 4.1 0 8.3.1 12.4-.2 2.2-.1 4.5.2 6.7.2h13.1c5.5 0 90.7-.5 96.2.1.5.1-95.6-.1-95.1-.1 4.8.1 98.6.5 103.4.6 4.3.1 8.5.5 12.7.7 3.9.1 7.9.4 11.8-.2 1.9-.09 3.8-.09 5.6-.19z"-->
<!--                                                    id="Слой_1-2" data-name="Слой 1"></path>-->
<!--                                            </g>-->
<!--                                        </svg>-->
<!--                                    </svg>-->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </div>-->
                        <div class="mobile-bot__info">
                            <div class="mobile-bot__link">
                                <svg class="icon icon-phone">
                                    <svg viewBox="0 0 480.55 480.56" id="icon-phone">
                                        <g id="Слой_2" data-name="Слой 2">
                                            <g id="Capa_1" data-name="Capa 1">
                                                <path
                                                    d="M365.35 317.9c-15.7-15.5-35.3-15.5-50.9 0-11.9 11.8-23.8 23.6-35.5 35.6-3.2 3.3-5.9 4-9.8 1.8-7.7-4.2-15.9-7.6-23.3-12.2-34.5-21.7-63.4-49.6-89-81-12.7-15.6-24-32.3-31.9-51.1-1.6-3.8-1.3-6.3 1.8-9.4 11.9-11.5 23.5-23.3 35.2-35.1 16.3-16.4 16.3-35.6-.1-52.1-9.3-9.4-18.6-18.6-27.9-28-9.6-9.6-19.1-19.3-28.8-28.8-15.7-15.3-35.3-15.3-50.9.1-12 11.8-23.5 23.9-35.7 35.5-11.3 10.7-17 23.8-18.2 39.1-1.9 24.9 4.2 48.4 12.8 71.3 17.6 47.4 44.4 89.5 76.9 128.1 43.9 52.2 96.3 93.5 157.6 123.3 27.6 13.4 56.2 23.7 87.3 25.4 21.4 1.2 40-4.2 54.9-20.9 10.2-11.4 21.7-21.8 32.5-32.7 16-16.2 16.1-35.8.2-51.8q-28.5-28.65-57.2-57.1zm-19.1-79.7l36.9-6.3A165.63 165.63 0 00243.05 96l-5.2 37.1a128 128 0 01108.4 105.1z">
                                                </path>
                                                <path
                                                    d="M404 77.8A272.09 272.09 0 00248 0l-5.2 37.1a237.42 237.42 0 01200.9 194.7l36.9-6.3A274.08 274.08 0 00404 77.8z">
                                                </path>
                                            </g>
                                        </g>
                                    </svg>
                                </svg><a class="header__link" href="index.html#">+7 (421) 296 06 06</a>
                            </div>
                            <div class="mobile-bot__link">
                                <svg class="icon icon-del">
                                    <use xlink:href="http://pomidor/upload/images/icons.svg#icon-del"></use>
                                </svg><span>Доставка еды <a class="underline popup-call"
                                        href="index.html#popup-city">Хабаровск</a></span>
                            </div><a href="index.html#">RU<span class="header__translate-arrow"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <main class="main">
            <div class="wrap">
            </div>
            <div class="page">
                <div class="wrap">


                    <div class="row">
                                                                        <div class="bx-content col">
                            <!--region breadcrumb-->
                                                                                    <div class="wrap col--no-gutters">
                                <div class="breadcrump breadcrump_offset" id="navigation">
                                    <link href="/bitrix/css/main/font-awesome.css?161721955628777" type="text/css" rel="stylesheet" />
<div class="bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
			
				
				<a href="/" title="Главная" itemprop="item">
					<span itemprop="name">Главная</span>
				</a>
				<meta itemprop="position" content="1" />
			
			
				<span>Меню</span>
			<div style="clear:both"></div></div>                                </div>
                            </div>
                                                                                                                <h1 id="pagetitle" class="page__title">
                                Меню кафе                            </h1>
                            
                            
                                                        <!--endregion-->
<div id="comp_1ee45fd9b745486fab1f0472b72b7bf9"><div class="row">
	<div class="col bx-site">
		<div class="row mb-4 site">
	<div class="col">


		
		<div class="row">

			<div class="col">

							</div>
		</div>

		<div class="box box_b-large">

				            <div class="line-filter line-filter_offset">
                
	<ul class='menu__tabs menu__tabs_light pomidor_menu'>

    <li class="menu__tabs-item active">
        <a href="#"  >Кухня</a>
    </li>
                                            
    <li class="menu__tabs-item ">
        <a href="/upload/uf/718/71888f7fa9c31a922e17fc4bb986d63b.pdf"target='_blank' >Барная карта</a>
    </li>
            
    <li class="menu__tabs-item ">
        <a href="/upload/uf/7e0/7e0e650da147fd5c5fcc6462c0700014.pdf"target='_blank' >Винная карта</a>
    </li>
            
</ul>            </div>
            
	<div class="page-nav">

            <a href="/menu/" onclick="BX.ajax.insertToNode('/menu/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class="active" >Все</a>
                        <a href="/menu/hot-meals/" onclick="BX.ajax.insertToNode('/menu/hot-meals/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Горячие блюда</a>

                    <a href="/menu/desserts/" onclick="BX.ajax.insertToNode('/menu/desserts/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Десерты</a>

                    <a href="/menu/drinks/" onclick="BX.ajax.insertToNode('/menu/drinks/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Закуски</a>

                    <a href="/menu/paste/" onclick="BX.ajax.insertToNode('/menu/paste/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Паста</a>

                    <a href="/menu/pizza/" onclick="BX.ajax.insertToNode('/menu/pizza/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Пицца</a>

                    <a href="/menu/rolls/" onclick="BX.ajax.insertToNode('/menu/rolls/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Роллы</a>

                    <a href="/menu/salads/" onclick="BX.ajax.insertToNode('/menu/salads/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Салаты</a>

                    <a href="/menu/soups-ramen/" onclick="BX.ajax.insertToNode('/menu/soups-ramen/?bxajaxid=1ee45fd9b745486fab1f0472b72b7bf9', 'comp_1ee45fd9b745486fab1f0472b72b7bf9'); return false;"  class=" " >Супы</a>

                    </div>
<div class="custom-select nav-select">
    <select data-select2-id="1" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">
                        <option value="/menu/" selected data-select2-id="3">Все</option>
<!--                <option value="--><!--" selected="" ><a  href="--><!--" class="--><!--">--><!--</a></option>-->
                                            <option value="/menu/hot-meals/" >Горячие блюда</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/desserts/" >Десерты</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/drinks/" >Закуски</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/paste/" >Паста</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/pizza/" >Пицца</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/rolls/" >Роллы</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/salads/" >Салаты</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                        <option value="/menu/soups-ramen/" >Супы</option>
<!--                <option value="--><!--" ><a href="--><!--"  class="--><!-- ">--><!--</a></option>-->
                                                    </select>
</div>

        </div>
                <div class="row bx-blue">
        <div class="col">

        <div class="products" data-entity="container-1">
            <div class="row products__row  " data-entity="items-row">
                <!-- items-container -->
                
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-703" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/dac/285_242_2/dac09b7151724ac7073d8345fb1d7528.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/390/285_242_2/390f84d9021e7ec584645b2020a453d7.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Телятина под соусом «бальзамико»    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">220 г</span>
        

    <span class="price  products__price" id="_price">
        490 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-703" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/390/390f84d9021e7ec584645b2020a453d7.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Телятина под соусом «бальзамико»</h1>
                    <span class="card__desc">
                        220 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Телятина в насыщенном бальзамическом соусе, шампиньоны, лук, помидоры черри и зелень
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                        <div class="add-w__info match-height" style="height: 0px;">
                                <div class="add-w__txt">Каллории</div>
                                <div class="add-w__txt add-w__txt-bold">171.2 ккал</div>
                                <div class="add-w__txt add-w__txt-white">18.8 %</div>
                            </div>
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                        рестораны в хабаровске                                                                                </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                            рестораны в хабаровске                        </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                            рестораны в комсомольске                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-704" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/7c5/285_242_2/7c5f983ad412f450076c817ef58bbb64.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/d40/285_242_2/d4004c120b9aa07b03b68f5c50b76692.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Свинина с овощами    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">290 г</span>
        

    <span class="price  products__price" id="_price">
        450 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-704" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/d40/d4004c120b9aa07b03b68f5c50b76692.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Свинина с овощами</h1>
                    <span class="card__desc">
                        290 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Свинина, обжаренные овощи: грибы, цукини, цветной перец, соус «Наполитано» и зелень
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-705" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/4f2/285_242_2/4f2254558dc66e06b08ce983ac82242d.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/370/285_242_2/370d47255616e7c2293e9a19d20f38bd.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Свинина с картофелем    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">310 г</span>
        

    <span class="price  products__price" id="_price">
        430 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-705" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/370/370d47255616e7c2293e9a19d20f38bd.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Свинина с картофелем</h1>
                    <span class="card__desc">
                        310 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Свинина, обжаренный картофель в мундире, грибной соус и зелень
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-706" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/01f/285_242_2/01f65eb729d29cb1171f6d2113bdf6c3.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/cc1/285_242_2/cc1f5a392d252810e2d662c3699b2eb0.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Куриное бедро с рисом под соусом «терияки»    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">250 г</span>
        

    <span class="price  products__price" id="_price">
        260 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-706" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/cc1/cc1f5a392d252810e2d662c3699b2eb0.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Куриное бедро с рисом под соусом «терияки»</h1>
                    <span class="card__desc">
                        250 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Сочное куриное бедро, рис, соус «Терияки», кунжут и капуста нори
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-707" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/9ae/285_242_2/9ae4d41da9ca7f7bf386cac12f3df92b.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/93c/285_242_2/93cb67c1e5647b40f61a352dfd8b91be.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Вок с курицей    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">270 г</span>
        

    <span class="price  products__price" id="_price">
        210 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-707" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/93c/93cb67c1e5647b40f61a352dfd8b91be.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Вок с курицей</h1>
                    <span class="card__desc">
                        270 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Пшеничная лапша ручной работы, обжаренная в оригинальном соусе с овощами
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-708" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/b00/285_242_2/b00a1cc61a47a2f6fe5444627e9a1a96.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/143/285_242_2/1431bf3c13dd3f6f5d55a45fc3b6d77b.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Вок с говядиной    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">270 г</span>
        

    <span class="price  products__price" id="_price">
        240 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-708" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/143/1431bf3c13dd3f6f5d55a45fc3b6d77b.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Вок с говядиной</h1>
                    <span class="card__desc">
                        270 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Пшеничная лапша ручной работы, обжаренная в оригинальном соусе с овощами
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-709" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/338/285_242_2/338aaaf3f5312fd583a68cdd85271799.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/701/285_242_2/701433e74237c9dae71a148c77b5c1c8.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Чахан с курицей    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">280 г</span>
        

    <span class="price  products__price" id="_price">
        230 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-709" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/701/701433e74237c9dae71a148c77b5c1c8.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Чахан с курицей</h1>
                    <span class="card__desc">
                        280 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Рис, обжаренный с яйцом, зеленым горошком, кукурузой и луком пореем
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-710" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/154/285_242_2/154e5b08113cfee687b339dc41eb362d.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/d03/285_242_2/d03a0da47174e32cc0c4f6f2da7cc5e4.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Чахан со свининой    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">260 г</span>
        

    <span class="price  products__price" id="_price">
        240 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-710" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/d03/d03a0da47174e32cc0c4f6f2da7cc5e4.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Чахан со свининой</h1>
                    <span class="card__desc">
                        260 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Рис, обжаренный с яйцом, зеленым горошком, кукурузой и луком пореем
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-711" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/432/285_242_2/432deef2a9d1cb52bc9567890c5639e5.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/396/285_242_2/3960fd9b92b2555f32b0ead3071cc867.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Чизкейк сливочный    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">130 г</span>
        

    <span class="price  products__price" id="_price">
        190 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-711" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/396/3960fd9b92b2555f32b0ead3071cc867.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Чизкейк сливочный</h1>
                    <span class="card__desc">
                        130 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Сыр Маскарпоне, творог, сливки, клубничный джем, нежный бисквит
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-712" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/7c5/285_242_2/7c5a0a95eb10f8596a7f2d617e30dd8e.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/e7e/285_242_2/e7e0fb3b16c3937a90fc569b30aea28b.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
         Яблочный десерт     </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">170 г</span>
        

    <span class="price  products__price" id="_price">
        190 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-712" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/e7e/e7e0fb3b16c3937a90fc569b30aea28b.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title">  Яблочный десерт </h1>
                    <span class="card__desc">
                        170 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Десерт из кисло-сладких яблок, томлённый в янтарном карамельном соусе на песочном тесте
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-713" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/3c1/285_242_2/3c16f8dcecb61663570c50e2cefe93a8.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/572/285_242_2/5728c1379a0ffce25212908a29db3e74.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Картофель фри    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">100 г</span>
        

    <span class="price  products__price" id="_price">
        90 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-713" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/572/5728c1379a0ffce25212908a29db3e74.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Картофель фри</h1>
                    <span class="card__desc">
                        100 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Подаётся с кетчупом
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-714" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/691/285_242_2/691fbabec5a8f76758b9889b53aeee87.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ad6/285_242_2/ad6cfb553b6f1f5e8e5efe43a09cd628.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Наггетсы    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">150 г</span>
        

    <span class="price  products__price" id="_price">
        190 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-714" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/ad6/ad6cfb553b6f1f5e8e5efe43a09cd628.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Наггетсы</h1>
                    <span class="card__desc">
                        150 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-715" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/bee/285_242_2/beed59bc1c01d4f9b813029459d96f8a.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/d90/285_242_2/d90b411667fc3513495d2de8e4fa2aea.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Кольца кальмара    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">185 г</span>
        

    <span class="price  products__price" id="_price">
        270 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-715" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/d90/d90b411667fc3513495d2de8e4fa2aea.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Кольца кальмара</h1>
                    <span class="card__desc">
                        185 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-716" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/e1d/285_242_2/e1dd29357ab1f39e8cc0036e7cf90163.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/5f8/285_242_2/5f88a9b162b5c00a19cdeea27b06f25b.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Гренки    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">80 г</span>
        

    <span class="price  products__price" id="_price">
        90 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-716" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/5f8/5f88a9b162b5c00a19cdeea27b06f25b.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Гренки</h1>
                    <span class="card__desc">
                        80 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Подаются с соусом Цезарь
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                                <div class="col col--xl-3 col--md-4 col--sm-6 pomidor_card_item">
                    
    <div class="menu__item menu-item aos-init aos-animate "
         id="" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="2000" data-entity="item">
        



<a href="#popup-menu-717" class="menu-item__top popup-call">
    <img class="menu-item__image" src="/upload/resize_cache/iblock/1b9/285_242_2/1b90cbdb85a8c1dd99284115aa536fe7.jpg" alt="">
    <img class="menu-item__image_second" src="/upload/resize_cache/iblock/ae2/285_242_2/ae24535d768f741be1021ec90cd42224.jpg" alt="">

</a>
<div class="menu-item__content match-height" style="height: 137px;">
    <h4 class="menu-item__title">
        Ассорти закусок    </h4>

    <p class="menu-item__desc">Оригинальный белый соус, моцарелла, бекон, куриная грудка, перец маринованный, кабачки,
        твердый сыр, руккола</p>
</div>
<div class="menu-item__bottom">
    
    <span class="weight menu-item__weight">220 г</span>
        

    <span class="price  products__price" id="_price">
        290 &#8381;    </span>

    

</div>


<span class="pomidor_hidden" id="_pict_slider">
</span>
<span class="" id="_secondpict">

</span>







<div id="popup-menu-717" class="mfp-hide popup-block popup-block_big">

    <div class="row card__row mb0">
        <div class="col col--lg-6">
            <a href="#" class="card__img">
                <img class="card__pic " src="/upload/iblock/ae2/ae24535d768f741be1021ec90cd42224.jpg" alt="">
            </a>


        </div>
        <div class="col col--lg-6">
            <div class="card__content">

                <div class="card__header">
                    <h1 class="card__title"> Ассорти закусок</h1>
                    <span class="card__desc">
                        220 г                    </span>
                </div>
                <div class="box box_b-large">
                    <div class="card__desc">
                        <div class="info">
                            Кольца кальмара, гренки, наггетсы.<br />
Подаются с горчичным соусом и кетчупом
                        </div>
                    </div>
                </div>
                <div class="box box_b-large">
                    <div class="add-w">
                        <h3 class="h3 h3_b-middle add-w__title">Пищевая ценность на 100 г</h3>
                        <div class="add-w__btns mb30">
                                                    </div>

                        <h3 class="h3 h3_b-middle add-w__title">Готовящие рестораны:</h3>
                        <div class="add-w__city-wrap">
                            <a class="add-w__city add-w__habarovsk active">Хабаровск</a>
                            <a class="add-w__city add-w__komsomolsk na">Комсомольск-на-Амуре</a>
                        </div>

                        <div class="menu_cart-address-tab">
                                                                                                                                        </div>
                        <div class="menu_cart-address-habarovsk d-sm-none ">
                                                    </div>
                        <div class="menu_cart-address-komsomolsk d-sm-none">
                                                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
            </div>
                    </div>
                
                                                <!-- items-container -->
            </div>
        </div>
                <div class="btn-wrapper btn-wrapper_center" data-entity="lazy-container-1" >
            <span class="load-btn" data-use="show-more-1" id="load_more_button">
                <i></i>
                <i></i>
                <i></i>
            </span>
        </div>

                            </div>
</div>
<!-- component-end -->							</div>
		</div>

	
	</div>

	</div>
		</div>
	</div>
</div>

			</div><!--end row-->

		</div><!--end .container.bx-content-section-->
	</div><!--end .workarea-->
</main>
<footer class="footer">
    <div class="footer__container">
        <div class="wrap">
            <div class="footer-top">
                

    <div class="footer__col footer__menu">
        <a class="footer__headline" href="/menu/">Меню</a>
        <ul class="footer__list">

	    <li class="footer__item"><a class="footer__link" href="/reviews/">Отзывы</a></li>
		

	    <li class="footer__item"><a class="footer__link" href="/news/">Новости</a></li>
		

	    <li class="footer__item"><a class="footer__link" href="/rabota-u-nas/">Карьера</a></li>
		

	    <li class="footer__item"><a class="footer__link" href="/restaurants/">Рестораны</a></li>
		
        </ul>
    </div>
                <div class="footer__col footer__address">
                    <h4 class="footer__headline">Хабаровск</h4>
                    <ul class="footer__list">
                        <li class="footer__item">Доставляем еду: <span>с 8:00 до 21:00</span></li>
<li class="footer__item"><a class="footer__link footer__phone" href="tel:+74212960606">+7 (4212) 96 06 06</a></li>                    </ul>
                </div>
                <div class="footer__col footer__address">
                    <h4 class="footer__headline">Комсомольск-на-Амуре</h4>
                    <ul class="footer__list">
                        <li class="footer__item">Доставляем еду: <span>с 8:00 до 21:00</span></li>
<li class="footer__item"><a class="footer__link footer__phone" href="tel:+74217244040">+7 (4212) 24 40 40</a></li>
                    </ul>
                </div>
                <div class="footer__col footer__inst">
                    <h4 class="footer__headline">Мы в Instagram</h4><img class="inst-logo" src="/upload/images/instagram.png" alt="">
                    <div class="inst-feed">
                        <a class="inst-feed__item" href="https://www.instagram.com/spomidordv/"><img src="/upload/images/inst-1.png" alt=""></a>
<a class="inst-feed__item" href="https://www.instagram.com/spomidordv/"><img src="/upload/images/inst-2.png" alt=""></a>
<a class="inst-feed__item" href="https://www.instagram.com/spomidordv/"><img src="/upload/images/inst-3.png" alt=""></a>                    </div>
                </div>
            </div>
            <div class="footer-bot">
                <span class="footer-bot__copyright">© Синьор Помидор</span>
<span class="footer-bot__copyright">© Японский помидор</span>                <a class="footer-bot__confidenc" href="/politika-konfidentsialnosti/">Политика конфиденциальности</a>
<a class="footer-bot__wf" href="https://webformula.pro/">Разработка сайта — <span>Webformula</span></a></div>
        </div>
    </div>
</footer>


<div id="cooperation" class="mfp-hide popup-block">
    <div class="window__header">
        <h4 class="h4 window__title window__title_b-offset text-center">
            Заявка на сотрудничество
        </h4>
        <p class="block mb-2 text-center">Заполните, пожалуйста, форму заказа </p>
        <form action="#" class="form form-reset">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Номер телефона" class="input input_default input_yellow time-w__input phone-mask">
            </div>
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Название компании" class="input input_default input_yellow time-w__input phone-mask">
            </div>
            <div class="form__item form__item_s-offset">
                <label class="file-w file-w_offset" data-title="Прикрепить коммерческое предложение">
                    <input type="file" class="file-w__input">
                    <span class="file-w__label">
                        <img class="file-w__ico" src="/local/templates/pomidor/images/file-w-ico.svg" alt="">
                        <span class="file-w__title">
                            Прикрепить коммерческое предложение
                        </span>
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <label class="checkbox checkbox_yellow">
                    <input type="checkbox" class="checkbox__input">
                    <span class="checkbox__label">
                        Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
                    </span>
                </label>
            </div>
            <div class="form__item form__item_s-offset mt-1">
                <button class="btn btn_rounded btn_yellow btn_middle">
                    Откликнуться
                </button>
            </div>
        </form>
    </div>
</div>



<div id="popup-book" class="mfp-hide popup-block">

    <div class="window__header">
    <h4 class="h4 window__title text-center">
        Забронировать стол
    </h4>
</div>
<p class="block mb-2 text-center">Заполните, пожалуйста, форму заказа </p>
<form action="#" class="form form-reset" id="table_book" onsubmit="table_book_form_send(event)">
    <div class="form__item form__item_s-offset -custom-select">
        <select class="input input_default input_yellow" name="restoran">
            <option>Выберите кафе</option>
            <option value="Волочаевская, 8щ">Волочаевская, 8щ</option>
<option value="Ленина, 53">Ленина, 53</option>
<option value="Ленинградская, 28">Ленинградская, 28</option>
<option value="Муравьева-Амурского, 40">Муравьева-Амурского, 40</option>
<option value="Суворова, 32">Суворова, 32</option>
<option value="Карла Маркса, 202 (2 этаж)">Карла Маркса, 202 (2 этаж)</option>
<option value="Вахова, 2">Вахова, 2</option>
<option value="Ким Ю Чена, 44 (5 этаж)">Ким Ю Чена, 44 (5 этаж)</option>
<option value="Стрельникова, 16а">Стрельникова, 16а</option>
        </select>
    </div>
    <div class="row">
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <div class="time-w">
                    <img class="time-w__ico" src="/upload/images/i-calendar.svg" alt="">
                    <input readonly type="text" placeholder="дд.мм.гггг"
                        class="input input_default input_yellow time-w__input datepicker-here" name="date">
                </div>
            </div>
        </div>
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <div class="time-w">
                    <img class="time-w__ico" src="/upload/images/i-clock.png" alt="">
                    <input type="time" class="input input_default input_yellow time-w__input time-w__input_s" name="time">
                </div>
            </div>
        </div>
    </div>
    <p class="block mb-2 small">Для бронирования стола в выходные звоните по номеру желаемого кафе</p>
    <div class="form__item form__item_s-offset">
        <p class="block mb small">Количество человек</p>
        <div class="counter">
            <span class="counter__btn counter__btn_minus"></span>
            <input type="text" class="counter__input" value="2" name="people_count">
            <span class="counter__btn counter__btn_plus "></span>
        </div>
    </div>
    <div class="row">
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Имя" class="input input_default input_yellow" name="name">
            </div>
        </div>
        <div class="col--md-6">
            <div class="form__item form__item_s-offset">
                <input type="text" placeholder="Телефон" class="input input_default input_yellow phone-mask" name="phone">
            </div>
        </div>
    </div>
    <div class="form__item form__item_s-offset">
        <p class="block mb small">Приготовить блюда к приходу</p>
        <input type="text" placeholder="Пицца 4 сыра" class="input input_default input_yellow" name="cook_for_me">
    </div>
    <div class="form__item form__item_s-offset">
        <textarea placeholder="Комментарии" class="input input_default input_yellow" name="comment"></textarea>
    </div>
    <div class="form__item form__item_s-offset mt-1">
        <label class="checkbox checkbox_yellow">
            <input type="checkbox" class="checkbox__input" name="agree" required>
            <span class="checkbox__label">
                Я подтверждаю свое согласие на «Политику в отношении обработки персональных данных»
            </span>
        </label>
    </div>
    <div class="form__item form__item_s-offset">
        <button class="btn btn_yellow btn_middle">
            Забронировать
        </button>
    </div>
    <span id="result_success1" style="color: green;"></span>
    <span id="result_errors1" style="color: red;"></span>
</form>

  

</div>
<div id="popup-city" class="mfp-hide popup-block text-center">
    <div class="window__header">
        <h4 class="h4 window__title text-center">
            Выберите город
        </h4>
    </div>
    <div class="city-change" style="justify-content: space-between">
        <div class="city-change__item city-change__item_yellow city_select_item" data-cityname="komsomolsk" onclick="$.magnificPopup.close()">
            <img src="/upload/images/i-city1.png" height="93">
            <div class="city-change__title">Комсомольск-на-Амуре</div>
        </div>
        <div class="city-change__item city-change__item_green city_select_item" data-cityname="habarovsk" onclick="$.magnificPopup.close()">
            <img src="/upload/images/i-city2.png" height="93">
            <div class="city-change__title">Хабаровск</div>
        </div>
    </div>
</div>



	<div class="col d-sm-none">
		<div id="bx_basketpCwjw4" class="bx-basket-fixed right bottom"><!--'start_frame_cache_bx_basketpCwjw4'--><div class="basket-line">
	<div class="mb-1 basket-line-block">
					<a class="basket-line-block-icon-profile" href="/personal/">Владислав1 Воробьёв</a>
			<a style='margin-right: 0;' href="?logout=yes">Выйти</a>
			</div>

	<div class="basket-line-block">
					<a class="basket-line-block-icon-cart" href="/personal/cart/">Корзина</a>
			2 позиции					<br class="d-none d-block-sm"/>
					<span>
						на сумму <strong>810 &#8381;</strong>
					</span>
						</div>
</div><!--'end_frame_cache_bx_basketpCwjw4'--></div>
	</div>
</div> <!-- //bx-wrapper -->


<div id="card-popup" class="mfp-hide popup-block popup-block_big"></div>

<div class="popup middle-popup tnx-popup mfp-hide" id="tnx">
    <img class="tnx-popup__img" src="images/pizza-popup.svg" alt="">
    <div class="">
        <h3 class="tnx-popup__title">Спасибо, ваша заявка принята!</h3>
        <div class="tnx-popup__desc">
            <p>
               Бронь действительна только после подтверждения гостевым менеджером 
            </p>
        </div>
        <a href="#" class="btn btn_green tnx-popup__btn" onclick="$.magnificPopup.close();">
            ok
        </a>
    </div>
</div>
<script type="text/javascript">if(!window.BX)window.BX={};if(!window.BX.message)window.BX.message=function(mess){if(typeof mess==='object'){for(let i in mess) {BX.message[i]=mess[i];} return true;}};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script><script type="text/javascript" src="/bitrix/js/main/core/core.js?1617219521549528"></script><script>BX.setJSList(['/bitrix/js/main/core/core_ajax.js','/bitrix/js/main/core/core_promise.js','/bitrix/js/main/polyfill/promise/js/promise.js','/bitrix/js/main/loadext/loadext.js','/bitrix/js/main/loadext/extension.js','/bitrix/js/main/polyfill/promise/js/promise.js','/bitrix/js/main/polyfill/find/js/find.js','/bitrix/js/main/polyfill/includes/js/includes.js','/bitrix/js/main/polyfill/matches/js/matches.js','/bitrix/js/ui/polyfill/closest/js/closest.js','/bitrix/js/main/polyfill/fill/main.polyfill.fill.js','/bitrix/js/main/polyfill/find/js/find.js','/bitrix/js/main/polyfill/matches/js/matches.js','/bitrix/js/main/polyfill/core/dist/polyfill.bundle.js','/bitrix/js/main/core/core.js','/bitrix/js/main/polyfill/intersectionobserver/js/intersectionobserver.js','/bitrix/js/main/lazyload/dist/lazyload.bundle.js','/bitrix/js/main/polyfill/core/dist/polyfill.bundle.js','/bitrix/js/main/parambag/dist/parambag.bundle.js']);
BX.setCSSList(['/bitrix/js/main/core/css/core.css','/bitrix/js/main/lazyload/dist/lazyload.bundle.css','/bitrix/js/main/parambag/dist/parambag.bundle.css']);</script>
<script type="text/javascript">(window.BX||top.BX).message({'pull_server_enabled':'N','pull_config_timestamp':'0','pull_guest_mode':'N','pull_guest_user_id':'0'});(window.BX||top.BX).message({'PULL_OLD_REVISION':'Для продолжения корректной работы с сайтом необходимо перезагрузить страницу.'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'AMPM_MODE':false});(window.BX||top.BX).message({'MONTH_1':'Январь','MONTH_2':'Февраль','MONTH_3':'Март','MONTH_4':'Апрель','MONTH_5':'Май','MONTH_6':'Июнь','MONTH_7':'Июль','MONTH_8':'Август','MONTH_9':'Сентябрь','MONTH_10':'Октябрь','MONTH_11':'Ноябрь','MONTH_12':'Декабрь','MONTH_1_S':'января','MONTH_2_S':'февраля','MONTH_3_S':'марта','MONTH_4_S':'апреля','MONTH_5_S':'мая','MONTH_6_S':'июня','MONTH_7_S':'июля','MONTH_8_S':'августа','MONTH_9_S':'сентября','MONTH_10_S':'октября','MONTH_11_S':'ноября','MONTH_12_S':'декабря','MON_1':'янв','MON_2':'фев','MON_3':'мар','MON_4':'апр','MON_5':'май','MON_6':'июн','MON_7':'июл','MON_8':'авг','MON_9':'сен','MON_10':'окт','MON_11':'ноя','MON_12':'дек','DAY_OF_WEEK_0':'Воскресенье','DAY_OF_WEEK_1':'Понедельник','DAY_OF_WEEK_2':'Вторник','DAY_OF_WEEK_3':'Среда','DAY_OF_WEEK_4':'Четверг','DAY_OF_WEEK_5':'Пятница','DAY_OF_WEEK_6':'Суббота','DOW_0':'Вс','DOW_1':'Пн','DOW_2':'Вт','DOW_3':'Ср','DOW_4':'Чт','DOW_5':'Пт','DOW_6':'Сб','FD_SECOND_AGO_0':'#VALUE# секунд назад','FD_SECOND_AGO_1':'#VALUE# секунду назад','FD_SECOND_AGO_10_20':'#VALUE# секунд назад','FD_SECOND_AGO_MOD_1':'#VALUE# секунду назад','FD_SECOND_AGO_MOD_2_4':'#VALUE# секунды назад','FD_SECOND_AGO_MOD_OTHER':'#VALUE# секунд назад','FD_SECOND_DIFF_0':'#VALUE# секунд','FD_SECOND_DIFF_1':'#VALUE# секунда','FD_SECOND_DIFF_10_20':'#VALUE# секунд','FD_SECOND_DIFF_MOD_1':'#VALUE# секунда','FD_SECOND_DIFF_MOD_2_4':'#VALUE# секунды','FD_SECOND_DIFF_MOD_OTHER':'#VALUE# секунд','FD_SECOND_SHORT':'#VALUE#с','FD_MINUTE_AGO_0':'#VALUE# минут назад','FD_MINUTE_AGO_1':'#VALUE# минуту назад','FD_MINUTE_AGO_10_20':'#VALUE# минут назад','FD_MINUTE_AGO_MOD_1':'#VALUE# минуту назад','FD_MINUTE_AGO_MOD_2_4':'#VALUE# минуты назад','FD_MINUTE_AGO_MOD_OTHER':'#VALUE# минут назад','FD_MINUTE_DIFF_0':'#VALUE# минут','FD_MINUTE_DIFF_1':'#VALUE# минута','FD_MINUTE_DIFF_10_20':'#VALUE# минут','FD_MINUTE_DIFF_MOD_1':'#VALUE# минута','FD_MINUTE_DIFF_MOD_2_4':'#VALUE# минуты','FD_MINUTE_DIFF_MOD_OTHER':'#VALUE# минут','FD_MINUTE_0':'#VALUE# минут','FD_MINUTE_1':'#VALUE# минуту','FD_MINUTE_10_20':'#VALUE# минут','FD_MINUTE_MOD_1':'#VALUE# минуту','FD_MINUTE_MOD_2_4':'#VALUE# минуты','FD_MINUTE_MOD_OTHER':'#VALUE# минут','FD_MINUTE_SHORT':'#VALUE#мин','FD_HOUR_AGO_0':'#VALUE# часов назад','FD_HOUR_AGO_1':'#VALUE# час назад','FD_HOUR_AGO_10_20':'#VALUE# часов назад','FD_HOUR_AGO_MOD_1':'#VALUE# час назад','FD_HOUR_AGO_MOD_2_4':'#VALUE# часа назад','FD_HOUR_AGO_MOD_OTHER':'#VALUE# часов назад','FD_HOUR_DIFF_0':'#VALUE# часов','FD_HOUR_DIFF_1':'#VALUE# час','FD_HOUR_DIFF_10_20':'#VALUE# часов','FD_HOUR_DIFF_MOD_1':'#VALUE# час','FD_HOUR_DIFF_MOD_2_4':'#VALUE# часа','FD_HOUR_DIFF_MOD_OTHER':'#VALUE# часов','FD_HOUR_SHORT':'#VALUE#ч','FD_YESTERDAY':'вчера','FD_TODAY':'сегодня','FD_TOMORROW':'завтра','FD_DAY_AGO_0':'#VALUE# дней назад','FD_DAY_AGO_1':'#VALUE# день назад','FD_DAY_AGO_10_20':'#VALUE# дней назад','FD_DAY_AGO_MOD_1':'#VALUE# день назад','FD_DAY_AGO_MOD_2_4':'#VALUE# дня назад','FD_DAY_AGO_MOD_OTHER':'#VALUE# дней назад','FD_DAY_DIFF_0':'#VALUE# дней','FD_DAY_DIFF_1':'#VALUE# день','FD_DAY_DIFF_10_20':'#VALUE# дней','FD_DAY_DIFF_MOD_1':'#VALUE# день','FD_DAY_DIFF_MOD_2_4':'#VALUE# дня','FD_DAY_DIFF_MOD_OTHER':'#VALUE# дней','FD_DAY_AT_TIME':'#DAY# в #TIME#','FD_DAY_SHORT':'#VALUE#д','FD_MONTH_AGO_0':'#VALUE# месяцев назад','FD_MONTH_AGO_1':'#VALUE# месяц назад','FD_MONTH_AGO_10_20':'#VALUE# месяцев назад','FD_MONTH_AGO_MOD_1':'#VALUE# месяц назад','FD_MONTH_AGO_MOD_2_4':'#VALUE# месяца назад','FD_MONTH_AGO_MOD_OTHER':'#VALUE# месяцев назад','FD_MONTH_DIFF_0':'#VALUE# месяцев','FD_MONTH_DIFF_1':'#VALUE# месяц','FD_MONTH_DIFF_10_20':'#VALUE# месяцев','FD_MONTH_DIFF_MOD_1':'#VALUE# месяц','FD_MONTH_DIFF_MOD_2_4':'#VALUE# месяца','FD_MONTH_DIFF_MOD_OTHER':'#VALUE# месяцев','FD_MONTH_SHORT':'#VALUE#мес','FD_YEARS_AGO_0':'#VALUE# лет назад','FD_YEARS_AGO_1':'#VALUE# год назад','FD_YEARS_AGO_10_20':'#VALUE# лет назад','FD_YEARS_AGO_MOD_1':'#VALUE# год назад','FD_YEARS_AGO_MOD_2_4':'#VALUE# года назад','FD_YEARS_AGO_MOD_OTHER':'#VALUE# лет назад','FD_YEARS_DIFF_0':'#VALUE# лет','FD_YEARS_DIFF_1':'#VALUE# год','FD_YEARS_DIFF_10_20':'#VALUE# лет','FD_YEARS_DIFF_MOD_1':'#VALUE# год','FD_YEARS_DIFF_MOD_2_4':'#VALUE# года','FD_YEARS_DIFF_MOD_OTHER':'#VALUE# лет','FD_YEARS_SHORT_0':'#VALUE#л','FD_YEARS_SHORT_1':'#VALUE#г','FD_YEARS_SHORT_10_20':'#VALUE#л','FD_YEARS_SHORT_MOD_1':'#VALUE#г','FD_YEARS_SHORT_MOD_2_4':'#VALUE#г','FD_YEARS_SHORT_MOD_OTHER':'#VALUE#л','CAL_BUTTON':'Выбрать','CAL_TIME_SET':'Установить время','CAL_TIME':'Время','FD_LAST_SEEN_TOMORROW':'завтра в #TIME#','FD_LAST_SEEN_NOW':'только что','FD_LAST_SEEN_TODAY':'сегодня в #TIME#','FD_LAST_SEEN_YESTERDAY':'вчера в #TIME#','FD_LAST_SEEN_MORE_YEAR':'более года назад'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'WEEK_START':'1'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'ADMIN_INCLAREA_DBLCLICK':'Двойной щелчок','ADMIN_SHOW_MODE_ON':'включен','ADMIN_SHOW_MODE_OFF':'выключен','AMDIN_SHOW_MODE_TITLE':'Режим правки','ADMIN_SHOW_MODE_ON_HINT':'Режим правки включен. На странице доступны элементы управления компонентами и включаемыми областями.','ADMIN_SHOW_MODE_OFF_HINT':'Режим правки выключен'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'TITLE_PREFIX':'Синьор помидор - '});(window.BX||top.BX).message({'JSADM_FAV_ADD':'Добавить в избранное','JSADM_FAV_DEL':'Удалить из избранного','JSADM_FAV_ADD_SUC':'Ссылка успешно добавлена в Избранное','JSADM_FAV_ADD_ERR':'Ошибка добавления в Избранное','JSADM_FAV_DEL_SUC':'Ссылка успешно удалена из Избранного','JSADM_FAV_DEL_ERR':'Ошибка удаления из Избранного','JSADM_LIST_SELECTEDALL':'все','JSADM_FLT_NO_NAME':'Без названия','JSADM_FLT_NEW_NAME':'Новый фильтр','JSADM_FLT_SAVE_TITLE':'Сохранить фильтр','JSADM_FLT_SAVE':'Сохранить','JSADM_FLT_DEL_CONFIRM':'Вы уверены, что хотите удалить фильтр?','JSADM_FLT_DEL_ERROR':'Ошибка удаления фильтра','JSADM_FLT_SAVE_ERROR':'Ошибка сохранения фильтра','JSADM_FLT_SAVE_AS':'Сохранить как','JSADM_FLT_DELETE':'Удалить','JSADM_FLT_SHOW_ALL':'Показать все условия','JSADM_FLT_HIDE_ALL':'Скрыть все условия','JSADM_CALEND_PREV':'Предыдущий','JSADM_CALEND_CURR':'Текущий','JSADM_CALEND_NEXT':'Следующий','JSADM_CALEND_PREV_WEEK':'Предыдущая','JSADM_CALEND_CURR_WEEK':'Текущая','JSADM_CALEND_NEXT_WEEK':'Следующая','JSADM_TABS_EXPAND':'Развернуть все вкладки на одну страницу','JSADM_TABS_COLLAPSE':'Свернуть вкладки','JSADM_FILE':'Добавить файл','JSADM_FILE_M':'Добавить файлы','JSADM_PIN_ON':'Прикрепить панель','JSADM_PIN_OFF':'Открепить панель','JSADM_FLT_FOLD':'Свернуть фильтр','JSADM_FLT_UNFOLD':'Развернуть фильтр','JSADM_SP_ON_CLOSE_BY_ESC':'Возможно, внесенные изменения не сохранятся'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'MAIN_SIDEPANEL_CLOSE':'Закрыть','MAIN_SIDEPANEL_PRINT':'Печать'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s1','SITE_DIR':'/','USER_ID':'1','SERVER_TIME':'1621594740','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'ca1c4050ba04585a6c287526ad531818'});</script><script type="text/javascript" src="/bitrix/js/main/core/core_ls.js?161721952110430"></script>
<script type="text/javascript" src="/bitrix/js/main/session.js?16172195223250"></script>
<script type="text/javascript" src="/bitrix/js/main/core/core_fx.js?161721952116888"></script>
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-3.3.1.min.js?161721952186927"></script>
<script type="text/javascript" src="/bitrix/js/ui/bootstrap4/js/bootstrap.js?1617219520123765"></script>
<script type="text/javascript" src="/bitrix/js/main/pageobject/pageobject.js?1617219521864"></script>
<script type="text/javascript" src="/bitrix/js/main/core/core_window.js?161721952198321"></script>
<script type="text/javascript" src="/bitrix/js/main/popup/dist/main.popup.bundle.js?1617219521103976"></script>
<script type="text/javascript" src="/bitrix/js/currency/core_currency.js?16172195213027"></script>
<script type="text/javascript" src="/bitrix/js/pull/protobuf/protobuf.js?1617219521274055"></script>
<script type="text/javascript" src="/bitrix/js/pull/protobuf/model.js?161721952170928"></script>
<script type="text/javascript" src="/bitrix/js/rest/client/rest.client.js?161721952116255"></script>
<script type="text/javascript" src="/bitrix/js/pull/client/pull.client.js?161721952166315"></script>
<script type="text/javascript" src="/bitrix/js/main/date/main.date.js?161721952234530"></script>
<script type="text/javascript" src="/bitrix/js/main/core/core_date.js?161721952133995"></script>
<script type="text/javascript" src="/bitrix/js/fileman/sticker.js?161721952070699"></script>
<script type="text/javascript" src="/bitrix/js/main/core/core_admin.js?161721952128623"></script>
<script type="text/javascript" src="/bitrix/js/main/utils.js?161721952130973"></script>
<script type="text/javascript" src="/bitrix/js/main/hot_keys.js?161721952217302"></script>
<script type="text/javascript" src="/bitrix/js/main/core/core_admin_interface.js?1617219521154109"></script>
<script type="text/javascript" src="/bitrix/js/main/sidepanel/manager.js?161721952230937"></script>
<script type="text/javascript" src="/bitrix/js/main/sidepanel/slider.js?161721952243258"></script>
<script type="text/javascript" src="/bitrix/js/main/admin_sidepanel.js?1617219522340"></script>
<script type="text/javascript" src="/bitrix/js/main/admin_tools.js?161721952267959"></script>
<script type="text/javascript" src="/bitrix/js/main/popup_menu.js?161721952312913"></script>
<script type="text/javascript" src="/bitrix/js/main/admin_search.js?16172195217230"></script>
<script type="text/javascript" src="/bitrix/js/main/public_tools.js?161721952232738"></script>
<script type="text/javascript">
BX.message({"SessExpired": 'Ваш сеанс работы с сайтом завершен из-за отсутствия активности в течение 24 мин. Введенные на странице данные не будут сохранены. Скопируйте их перед тем, как закроете или обновите страницу.'});
bxSession.Expand('ca1c4050ba04585a6c287526ad531818.423b53e5100acd7bcada2dea5da18775ee6c640890b1b494bf0777cd77e40542');
</script>
<script type="text/javascript">
					(function () {
						"use strict";

						var counter = function ()
						{
							var cookie = (function (name) {
								var parts = ("; " + document.cookie).split("; " + name + "=");
								if (parts.length == 2) {
									try {return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()));}
									catch (e) {}
								}
							})("BITRIX_CONVERSION_CONTEXT_s1");

							if (cookie && cookie.EXPIRE >= BX.message("SERVER_TIME"))
								return;

							var request = new XMLHttpRequest();
							request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true);
							request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							request.send(
								"SITE_ID="+encodeURIComponent("s1")+
								"&sessid="+encodeURIComponent(BX.bitrix_sessid())+
								"&HTTP_REFERER="+encodeURIComponent(document.referrer)
							);
						};

						if (window.frameRequestStart === true)
							BX.addCustomEvent("onFrameDataReceived", counter);
						else
							BX.ready(counter);
					})();
				</script>
<script type="text/javascript">BX.bind(window, "load", function(){BX.PULL.start();});</script>

<script type="text/javascript">
var phpVars = {
	'ADMIN_THEME_ID': '.default',
	'LANGUAGE_ID': 'ru',
	'FORMAT_DATE': 'DD.MM.YYYY',
	'FORMAT_DATETIME': 'DD.MM.YYYY HH:MI:SS',
	'opt_context_ctrl': false,
	'cookiePrefix': 'BITRIX_SM',
	'titlePrefix': 'Синьор помидор - ',
	'bitrix_sessid': 'ca1c4050ba04585a6c287526ad531818',
	'messHideMenu': 'Скрыть меню',
	'messShowMenu': 'Показать меню',
	'messHideButtons': 'Уменьшить кнопки',
	'messShowButtons': 'Увеличить кнопки',
	'messFilterInactive': 'Поиск не используется - показаны все записи',
	'messFilterActive': 'Используется поиск - показаны только найденные записи',
	'messFilterLess': 'Скрыть условие поиска',
	'messLoading': 'Загрузка...',
	'messMenuLoading': 'Загрузка...',
	'messMenuLoadingTitle': 'Загружаются пункты меню...',
	'messNoData': '- Нет данных -',
	'messExpandTabs': 'Развернуть все вкладки на одну страницу',
	'messCollapseTabs': 'Свернуть вкладки',
	'messPanelFixOn': 'Зафиксировать панель на экране',
	'messPanelFixOff': 'Открепить панель',
	'messPanelCollapse': 'Скрыть панель',
	'messPanelExpand': 'Показать панель'
};
</script>




<script type="text/javascript" src="/local/templates/pomidor/js/jquery.matchHeight.js?161721957211778"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/jquery.magnific-popup.min.js?161721957220219"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/nouislider.min.js?161721957225247"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/select2.min.js?161721957270851"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/wNumb.min.js?16172195722235"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/aos.js?161721957214239"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/slick.js?161721957288955"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/jquery.mask.min.js?16172195728327"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/datepicker.min.js?161721957235542"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/script.js?161721957214302"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/sweetalert2.min.js?162010111847915"></script>
<script type="text/javascript" src="/local/templates/pomidor/js/custom.js?16187940643235"></script>
<script type="text/javascript" src="/local/templates/pomidor/components/bitrix/menu/main_menu/script.js?16172195726228"></script>
<script type="text/javascript" src="/local/templates/pomidor/components/bitrix/sale.basket.basket.line/bootstrap_v5/script.js?16172195725335"></script>
<script type="text/javascript" src="/bitrix/components/bitrix/sale.basket.basket.line/templates/bootstrap_v4/script.js?16172195535335"></script>
<script type="text/javascript" src="/local/templates/pomidor/components/bitrix/catalog.section/pomidor_catalog_section/script.js?16201011188345"></script>
<script type="text/javascript" src="/local/templates/pomidor/components/bitrix/catalog.item/pomidor_catalog_item/script.js?161957519863263"></script>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "5a87679950946de5e129cafc2b7136e7"]); _ba.push(["host", "seniorpomidor.digitalwf.ru"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>


<script type="text/javascript">BX.admin.dynamic_mode=false; BX.admin.dynamic_mode_show_borders = false;</script>
<script type="text/javascript">BX.message({MENU_ENABLE_TOOLTIP: true}); new BX.COpener({'DIV':'bx-panel-menu','ACTIVE_CLASS':'bx-pressed','MENU_URL':'/bitrix/admin/get_start_menu.php?lang=ru&back_url_pub=%2Fmenu%2F&sessid=ca1c4050ba04585a6c287526ad531818','MENU_PRELOAD':false});</script><script type="text/javascript"> BXHotKeys.Add("", "var d=BX(\"bx-panel-menu\"); if (d) d.click();", 91, 'Меню', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "var d=BX(\'bx-panel-admin-tab\'); if (d) location.href = d.href;", 92, 'Администрирование', 0); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+79", "var d=BX(\'bx-panel-logout\'); if (d) location.href = d.href;", 133, 'Выход пользователя из системы.', 19); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+68", "location.href=\"/menu/?bitrix_include_areas=Y\";", 117, 'Режим правки', 16); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+69", "var d=BX(\'bx-panel-expander\'); if (d) BX.fireEvent(d, \'click\');", 118, 'Развернуть/Свернуть', 21); </script>
<script type="text/javascript"> BXHotKeys.Add("", "", 111, '<b>Создать страницу</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=pomidor&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 60, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Создать страницу', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Создать страницу','TEXT':'Создание страниц с помощью мастера.'}, GROUP_ID : 0, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_menu', TYPE: 'BIG', MENU: [{'TEXT':'Создать страницу','TITLE':'Мастер создания новой страницы','ICON':'panel-new-file','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=pomidor&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','DEFAULT':true,'SORT':'10','HK_ID':'top_panel_create_page'},{'TEXT':'По шаблону','TITLE':'Мастер создания страницы по выбранному шаблону','ICON':'panel-new-file-template','MENU':[{'TEXT':'Шаблоны сеток страницы','TITLE':'Шаблон bootstrap\nРаздел с шаблоном сетки','ICON':'panel-new-file-template','IMAGE':'','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?wiz_template=bootstrap&lang=ru&site=s1&templateID=pomidor&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'TEXT':'Стандартная страница','TITLE':'Шаблон standard.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?page_template=standard.php&lang=ru&site=s1&templateID=pomidor&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'}],'SORT':'20'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Создать новую страницу','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_file_edit.php?lang=ru&site=s1&path=%2Fmenu%2F&new=Y&templateID=pomidor&back_url=%2Fmenu%2F\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Создать страницу','TEXT':'Нажмите на эту кнопку, чтобы запустить мастер или шаблон создания страниц или включаемых областей для страниц. С помощью этих мастеров, можно создать страницу или включаемую область, последовательно внося данные на каждом шаге мастера.'}, GROUP_ID : 0, TEXT :  "Создать страницу"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 112, '<b>Создать раздел</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=pomidor&newFolder=Y&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 62, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Создать раздел', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_section', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Создать раздел','TEXT':'Создание разделов с помощью мастера.'}, GROUP_ID : 0, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_section_menu', TYPE: 'BIG', MENU: [{'TEXT':'Создать раздел','TITLE':'Мастер создания нового раздела','ICON':'panel-new-folder','DEFAULT':true,'ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=pomidor&newFolder=Y&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'10','HK_ID':'top_panel_create_folder'},{'TEXT':'По шаблону','TITLE':'Мастер создания нового раздела с индексной страницей по шаблону','ICON':'panel-new-folder-template','MENU':[{'TEXT':'Форум (ЧПУ)','TITLE':'Шаблон forum\nСтраница с форумами в режиме ЧПУ','ICON':'','IMAGE':'/bitrix/themes/.default/start_menu/forum/forum.gif','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&wiz_template=forum&lang=ru&site=s1&templateID=pomidor&newFolder=Y&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'TEXT':'Шаблоны сеток страницы','TITLE':'Шаблон bootstrap\nРаздел с шаблоном сетки','ICON':'panel-new-file-template','IMAGE':'','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&wiz_template=bootstrap&lang=ru&site=s1&templateID=pomidor&newFolder=Y&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'SEPARATOR':true},{'TEXT':'Стандартная страница','TITLE':'Шаблон standard.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&page_template=standard.php&lang=ru&site=s1&templateID=pomidor&newFolder=Y&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'}],'SORT':'20'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Создать новый раздел','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_newfolder.php?lang=ru&site=s1&path=%2Fmenu%2F&back_url=%2Fmenu%2F\')'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Создать раздел','TEXT':'Нажмите на эту кнопку, чтобы запустить мастер или шаблон создания разделов или включаемых областей для разделов. С помощью этих мастеров, можно создать страницу или включаемую область, последовательно внося данные на каждом шаге мастера.'}, GROUP_ID : 0, TEXT :  "Создать раздел"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 107, '<b>Изменить страницу</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&path=%2Fmenu%2Findex.php&site=s1&back_url=%2Fmenu%2F&templateID=pomidor&siteTemplateId=pomidor\',\'width\':\'780\',\'height\':\'470\',\'min_width\':\'780\',\'min_height\':\'400\'})).Show()", 63, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В визуальном редакторе', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_property.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 64, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заголовок и свойства страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&noeditor=Y&path=%2Fmenu%2Findex.php&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'780\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 65, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В режиме HTML-кода', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Fmenu%2Findex.php&site=s1&back_url=%2Fmenu%2F&templateID=pomidor&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 67, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В режиме PHP-кода', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_delete.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&siteTemplateId=pomidor\',\'width\':\'440\',\'height\':\'180\',\'min_width\':\'250\',\'min_height\':\'180\'})).Show()", 68, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Удалить страницу', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Изменить страницу','TEXT':'Изменение содержимого страницы.'}, GROUP_ID : 1, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_menu', TYPE: 'BIG', MENU: [{'TEXT':'В визуальном редакторе','TITLE':'Редактировать страницу в визуальном редакторе','ICON':'panel-edit-visual','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&path=%2Fmenu%2Findex.php&site=s1&back_url=%2Fmenu%2F&templateID=pomidor&siteTemplateId=pomidor\',\'width\':\'780\',\'height\':\'470\',\'min_width\':\'780\',\'min_height\':\'400\'})).Show()','DEFAULT':true,'SORT':'10','HK_ID':'top_panel_edit_page'},{'TEXT':'Заголовок и свойства страницы','TITLE':'Изменить заголовок и другие свойства страницы','ICON':'panel-file-props','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_property.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'20','HK_ID':'top_panel_page_prop'},{'SEPARATOR':true,'SORT':'29'},{'TEXT':'Доступ к странице','TITLE':'Установить права доступа к странице','ICON':'panel-file-access','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'30','HK_ID':'top_panel_access_page_new'},{'ID':'delete','ICON':'icon-delete','ALT':'Удалить страницу','TEXT':'Удалить страницу','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_delete.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&siteTemplateId=pomidor\',\'width\':\'440\',\'height\':\'180\',\'min_width\':\'250\',\'min_height\':\'180\'})).Show()','SORT':'40','HK_ID':'top_panel_del_page'},{'SEPARATOR':true,'SORT':'49'},{'TEXT':'В режиме HTML-кода','TITLE':'Редактировать страницу в режиме HTML','ICON':'panel-edit-text','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&noeditor=Y&path=%2Fmenu%2Findex.php&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'780\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()','SORT':'50','HK_ID':'top_panel_edit_page_html'},{'TEXT':'В режиме PHP-кода','TITLE':'Редактировать полный PHP-код страницы','ICON':'panel-edit-php','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Fmenu%2Findex.php&site=s1&back_url=%2Fmenu%2F&templateID=pomidor&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()','SORT':'60','HK_ID':'top_panel_edit_page_php'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Редактировать текущую страницу','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_file_edit.php?lang=ru&site=s1&templateID=pomidor&path=%2Fmenu%2Findex.php&back_url=%2Fmenu%2F\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Изменить страницу','TEXT':'Нажмите на эту кнопку, чтобы вызвать диалог редактирования страницы, в том числе через документооборот, просмотреть историю изменений страницы, изменить свойства страницы, ограничить доступ к ней.'}, GROUP_ID : 1, TEXT :  "Изменить страницу"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 96, '<b>Изменить раздел</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "javascript:(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_folder_edit.php?lang=ru&site=s1&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 69, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Свойства раздела', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 70, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Доступ к разделу', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_section', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Изменить раздел','TEXT':'Изменение свойств раздела.'}, GROUP_ID : 1, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_section_menu', TYPE: 'BIG', MENU: [{'TEXT':'Свойства раздела','TITLE':'Изменить название и другие свойства раздела','ICON':'panel-folder-props','DEFAULT':true,'ACTION':'javascript:(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_folder_edit.php?lang=ru&site=s1&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'10','HK_ID':'top_panel_folder_prop'},{'TEXT':'Доступ к разделу','TITLE':'Установить права доступа к разделу','ICON':'panel-folder-access','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Fmenu%2F&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'30','HK_ID':'top_panel_access_folder_new'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Редактировать свойства раздела','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_folder.php?lang=ru&site=s1&path=%2Fmenu%2F&back_url=%2Fmenu%2F\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Изменить раздел','TEXT':'Нажмите на эту кнопку, чтобы изменить свойства раздела и ограничить доступ к нему.'}, GROUP_ID : 1, TEXT :  "Изменить раздел"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_menus', TYPE: 'SMALL', MENU: [{'TEXT':'Редактировать \"Верхнее меню\"','TITLE':'Редактировать пункты меню \"Верхнее меню\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Fmenu%2F&path=%2F&name=top&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"Нижнее меню\"','TITLE':'Редактировать пункты меню \"Нижнее меню\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Fmenu%2F&path=%2F&name=bottom&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'SEPARATOR':'Y','SORT':'150'},{'TEXT':'Создать \"Верхнее меню\"','TITLE':'Создать меню \"Верхнее меню\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Fmenu%2F&path=%2Fmenu&name=top&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"Нижнее меню\"','TITLE':'Создать меню \"Нижнее меню\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Fmenu%2F&path=%2Fmenu&name=bottom&siteTemplateId=pomidor\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Меню','TEXT':'Вызов формы редактирования меню. Нажмите на стрелку, чтобы отредактировать все меню данной страницы или создать новое меню.','TARGET':'parent'}, GROUP_ID : 2, TEXT :  "Меню"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 97, '<b>Структура</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=%2Fmenu%2F\')", 71, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В панели управления', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_structure', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Структура','TEXT':'Вызов диалога Структура сайта с возможностью добавления, редактирования, перемещения, удаления, изменения свойств разделов и страниц.','TARGET':'parent'}, GROUP_ID : 2, SKIP : true, LINK: "javascript:void(0)", ACTION : "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_structure.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&templateID=pomidor&siteTemplateId=pomidor\',\'width\':\'350\',\'height\':\'470\'})).Show()",TEXT :  "Структура" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_structure_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Управление структурой','TITLE':'Управление структурой сайта','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_structure.php?lang=ru&site=s1&path=%2Fmenu%2Findex.php&templateID=pomidor&siteTemplateId=pomidor\',\'width\':\'350\',\'height\':\'470\'})).Show()','DEFAULT':true,'HK_ID':'main_top_panel_struct'},{'SEPARATOR':true},{'TEXT':'В панели управления','TITLE':'Перейти в управление структурой в административном разделе','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=%2Fmenu%2F\')','HK_ID':'main_top_panel_struct_panel'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 2, TEXT :  "Структура"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_seo', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'SEO','TEXT':'Вызов инструментов оптимизации сайта с целью повышения положения сайта в выдаче поисковых машин.'}, GROUP_ID : 2, SKIP : false, LINK: "javascript:void(0)", ACTION : "(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/public_seo_tools.php?lang=ru&bxpublic=Y&from_module=seo&site=s1&path=%2Fmenu%2Findex.php&title_final=0JzQtdC90Y4g0LrQsNGE0LU%3D&title_changer_name=&title_changer_link=&title_win_final=&title_win_changer_name=&title_win_changer_link=&sessid=ca1c4050ba04585a6c287526ad531818&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'920\',\'height\':\'400\'})).Show()",TEXT :  "SEO" })</script><script type="text/javascript"> BXHotKeys.Add("", "", 98, '<b>Сбросить кеш</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "BX.clearCache()", 72, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Обновить кеш страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/menu/?clear_cache_session=Y\');", 74, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Не использовать кеш', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_0', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Сбросить кеш','TEXT':'Обновление кеша страницы.'}, GROUP_ID : 3, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_0_menu', TYPE: 'BIG', MENU: [{'TEXT':'Обновить кеш страницы','TITLE':'Обновить закешированные данные текущей страницы','ICON':'panel-page-cache','ACTION':'BX.clearCache()','DEFAULT':true,'HK_ID':'top_panel_cache_page'},{'SEPARATOR':true},{'TEXT':'Не использовать кеш','TITLE':'Отключить кеширование для текущего сеанса пользователя','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/menu/?clear_cache_session=Y\');','HK_ID':'top_panel_cache_not'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Сбросить кеш','TEXT':'Нажмите на эту кнопку, чтобы обновить кеш страницы или компонентов, или отключить кеш для данной страницы вообще.'}, GROUP_ID : 3, TEXT :  "Сбросить кеш"})</script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], BX(\'bx-panel-toggle\').href);", 75, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Перейти в режим правки', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_components', TYPE: 'SMALL', MENU: [{'TEXT':'Перейти в режим правки','TITLE':'Перейдите в режим правки для отображения списка компонентов','ACTION':'jsUtils.Redirect([], BX(\'bx-panel-toggle\').href);','HK_ID':'top_panel_edit_mode'}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Компоненты','TEXT':'Выбор компонента для редактирования его параметров.','TARGET':'parent'}, GROUP_ID : 4, TEXT :  "Компоненты"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 99, '<b>Шаблон сайта</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fpomidor%2Fstyles.css&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 76, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изменить стили сайта', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fpomidor%2Ftemplate_styles.css&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 77, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изменить стили шаблона', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/site_edit.php?lang=ru&LID=s1\')", 78, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Редактировать сайт', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_1', TYPE: 'SMALL', MENU: [{'TEXT':'Изменить стили сайта','TITLE':'Редактировать основные стили сайта (styles.css)','ICON':'panel-edit-text','HK_ID':'top_panel_templ_site_css','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fpomidor%2Fstyles.css&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()'},{'TEXT':'Изменить стили шаблона','TITLE':'Редактировать стили шаблона (template_styles.css)','ICON':'panel-edit-text','HK_ID':'top_panel_templ_templ_css','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fpomidor%2Ftemplate_styles.css&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'470\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()'},{'SEPARATOR':'Y'},{'TEXT':'В панели управления','MENU':[{'TEXT':'Редактировать шаблон','TITLE':'Редактировать шаблон сайта в панели управления','ICON':'icon-edit','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/template_edit.php?lang=ru&ID=pomidor\')','DEFAULT':false,'HK_ID':'top_panel_templ_edit'},{'TEXT':'Редактировать сайт','TITLE':'Редактировать настройки сайта в панели управления','ICON':'icon-edit','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/site_edit.php?lang=ru&LID=s1\')','DEFAULT':false,'HK_ID':'top_panel_templ_site'}]}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Шаблон сайта','TEXT':'Вызов форм редактирования стилей сайта и стилей шаблона.','TARGET':'parent'}, GROUP_ID : 4, TEXT :  "Шаблон сайта"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 100, '<b>Отладка</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/menu/?show_sql_stat=Y\');", 83, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика SQL-запросов', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/menu/?show_include_exec_time=Y\');", 82, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика включаемых областей', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/menu/?show_page_exec_time=Y\');", 81, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Время исполнения страницы', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_2', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Отладка','TEXT':'Включение режима отображения суммарной статистики. Нажмите на стрелку, чтобы Просмотреть статистику запросов базы данных, статистику включаемых областей, время исполнение страницы и статистику компрессии.','TARGET':'parent'}, GROUP_ID : 4, SKIP : true, LINK: "/menu/?show_page_exec_time=Y&show_include_exec_time=Y&show_sql_stat=Y", ACTION : "",TEXT :  "Отладка" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_2_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Суммарная статистика','TITLE':'Показывать статистику SQL-запросов, включаемых областей, времени исполнения','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/menu/?show_page_exec_time=Y&show_include_exec_time=Y&show_sql_stat=Y\');','DEFAULT':true,'HK_ID':'top_panel_debug_summ'},{'SEPARATOR':true},{'TEXT':'Статистика SQL-запросов','TITLE':'Показывать статистику SQL-запросов, исполняющихся на странице','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/menu/?show_sql_stat=Y\');','HK_ID':'top_panel_debug_sql'},{'TEXT':'Детальная статистика кеша','TITLE':'Внимание! Может сильно влиять на время исполнения страницы.','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/menu/?show_cache_stat=Y\');','HK_ID':'top_panel_debug_cache'},{'TEXT':'Статистика включаемых областей','TITLE':'Показывать статистику включаемых областей (время, запросы)','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/menu/?show_include_exec_time=Y\');','HK_ID':'top_panel_debug_incl'},{'TEXT':'Время исполнения страницы','TITLE':'Показывать время исполнения страницы','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/menu/?show_page_exec_time=Y\');','HK_ID':'top_panel_debug_time'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 4, TEXT :  "Отладка"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 101, '<b>Короткий URL</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "javascript:(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Fmenu%2F&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'270\'})).Show()", 85, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Добавить короткую ссылку', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/short_uri_admin.php?lang=ru\');", 86, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список ссылок', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_3', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Короткая ссылка','TEXT':'Настройка короткой ссылки для текущей страницы','TARGET':'parent'}, GROUP_ID : 5, SKIP : true, LINK: "javascript:void(0)", ACTION : "(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Fmenu%2F&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'270\'})).Show()",TEXT :  "Короткий URL" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_3_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Добавить короткую ссылку','TITLE':'Добавить короткую ссылку на текущую страницу','ACTION':'javascript:(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Fmenu%2F&site=s1&back_url=%2Fmenu%2F&siteTemplateId=pomidor\',\'width\':\'770\',\'height\':\'270\'})).Show()','DEFAULT':true,'HK_ID':'MTP_SHORT_URI1'},{'TEXT':'Список ссылок','TITLE':'Список коротких ссылок','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/short_uri_admin.php?lang=ru\');','HK_ID':'MTP_SHORT_URI_LIST'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 5, TEXT :  "Короткий URL"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 102, '<b>Стикеры</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+83", "if (window.oBXSticker){window.oBXSticker.AddSticker();}", 87, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Наклеить стикер', 22); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+88", "if (window.oBXSticker){window.oBXSticker.ShowAll();}", 88, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Показать стикеры (0)', 23); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+76", "if (window.oBXSticker){window.oBXSticker.ShowList(\'current\');}", 89, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список стикеров страницы', 24); </script><script type="text/javascript"> BXHotKeys.Add("", "if (window.oBXSticker){window.oBXSticker.ShowList(\'all\');}", 90, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список всех стикеров', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_4', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Стикеры','TEXT':'Наклеить новый стикер на страницу.','TARGET':'parent'}, GROUP_ID : 5, SKIP : true, LINK: "javascript:void(0)", ACTION : "if (window.oBXSticker){window.oBXSticker.AddSticker();}",TEXT :  "Стикеры" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_4_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Наклеить стикер','TITLE':'Наклеить новый стикер на страницу ( Ctrl+Shift+S ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.AddSticker();}','DEFAULT':true,'HK_ID':'FMST_PANEL_STICKER_ADD'},{'SEPARATOR':true},{'ID':'bxst-show-sticker-icon','TEXT':'Показать стикеры (0)','TITLE':'Отобразить все стикеры на странице ( Ctrl+Shift+X ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowAll();}','HK_ID':'FMST_PANEL_STICKERS_SHOW'},{'TEXT':'Список стикеров страницы','TITLE':'Показать список всех стикеров на текущей странице ( Ctrl+Shift+L ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowList(\'current\');}','HK_ID':'FMST_PANEL_CUR_STICKER_LIST'},{'TEXT':'Список всех стикеров','TITLE':'Показать список всех стикеров на сайте','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowList(\'all\');}','HK_ID':'FMST_PANEL_ALL_STICKER_LIST'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 5, TEXT :  "Стикеры"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_5', TYPE: 'SMALL', MENU: [{'TEXT':'Зелено-бирюзовая','ACTION':'jsUtils.Redirect([], \'/menu/?theme=green\')'},{'TEXT':'Синяя','ACTION':'jsUtils.Redirect([], \'/menu/?theme=blue\')'},{'TEXT':'Желтая','ACTION':'jsUtils.Redirect([], \'/menu/?theme=yellow\')'},{'TEXT':'Красная','ACTION':'jsUtils.Redirect([], \'/menu/?theme=red\')'}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Настройка темы','TEXT':'Сменить цветовую тему магазина','TARGET':'parent'}, GROUP_ID : 6, TEXT :  "Настройка темы"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_eshop_wizard', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', GROUP_ID : 7, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_eshop_wizard_menu', TYPE: 'BIG', MENU: [{'ACTION':'jsUtils.Redirect([], \'/bitrix/admin/wizard_install.php?lang=ru&wizardSiteID=s1&wizardName=bitrix:eshop&sessid=ca1c4050ba04585a6c287526ad531818\')','ICON':'bx-popup-item-wizard-icon','TITLE':'Запустить мастер смены дизайна и настройки магазина','TEXT':'Мастер настройки магазина'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', GROUP_ID : 7, TEXT :  "Мастер настройки"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_solutions_wizard', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Протестировать новое решение','TEXT':'Выбор нового решения для установки.'}, GROUP_ID : 8, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_solutions_wizard_menu', TYPE: 'BIG', MENU: [{'ACTION':'jsUtils.Redirect([], \'/?add_new_site_sol=sol&sessid=ca1c4050ba04585a6c287526ad531818\')','HTML':'<b>Протестировать новое решение<\/b>','TITLE':'Создать новый сайт и запустить мастер установки нового решения'},{'ACTION':'if(confirm(\'Внимание! После удаления кнопки вы сможете запустить мастер создания нового решения в панели управления, используя страницу с мастерами или формой добавления нового сайта. Удалить кнопку с панели?\')) jsUtils.Redirect([], \'/?delete_button_sol=sol&sessid=ca1c4050ba04585a6c287526ad531818\');','TEXT':'Удалить эту кнопку','TITLE':'Удалить эту кнопку с панели'},{'SEPARATOR':true},{'TEXT':'Перейти на сайт','MENU':[{'ACTION':'jsUtils.Redirect([], \'/\');','ICON':'checked','TEXT':'Интернет-магазин (Сайт по умолчанию)','TITLE':'Перейти на сайт Интернет-магазин (Сайт по умолчанию)'}]}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Протестировать новое решение','TEXT':'Нажмите на эту кнопку, чтобы выбрать новое решение для установки, удаление этой кнопки с Панели управления или для перехода на другой сайт.'}, GROUP_ID : 8, TEXT :  "Протестировать новое решение"})</script><script type="text/javascript">
		BX.admin.panel.state = {
			fixed: false,
			collapsed: false
		};
		BX.admin.moreButton.init({ buttonTitle : "Еще"});
		</script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+75", "location.href=\'/bitrix/admin/hot_keys_list.php?lang=ru\';", 106, 'Перейти в раздел управления горячими клавишами', 18);  BXHotKeys.Add("Ctrl+Alt+80", "BXHotKeys.ShowSettings();", 17, 'Список горячих клавиш на странице', 14);  BXHotKeys.Add("Ctrl+Alt+85", "location.href=\'/bitrix/admin/user_admin.php?lang=\'+phpVars.LANGUAGE_ID;", 139, 'Перейти в раздел управления пользователями', 13); </script><script type='text/javascript'>
			BXHotKeys.MesNotAssign = 'не назначена';
			BXHotKeys.MesClToChange = 'Кликните мышкой, чтобы изменить';
			BXHotKeys.MesClean = 'Очистить';
			BXHotKeys.MesBusy = 'Данная комбинация клавиш уже задействована. Вы уверены, что хотите её использовать повторно?';
			BXHotKeys.MesClose = 'Закрыть';
			BXHotKeys.MesSave = 'Сохранить';
			BXHotKeys.MesSettings = 'Настройка горячих клавиш';
			BXHotKeys.MesDefault = 'По умолчанию';
			BXHotKeys.MesDelAll = 'Удалить все';
			BXHotKeys.MesDelConfirm = 'Вы уверены, что хотите удалить все назначенные сочетания горячих клавиш?';
			BXHotKeys.MesDefaultConfirm = 'Вы уверены, что хотите удалить все назначенные сочетания горячих клавиш и установить сочетания горячих клавиш по умолчанию?';
			BXHotKeys.MesExport = 'Экспорт';
			BXHotKeys.MesImport = 'Импорт';
			BXHotKeys.MesExpFalse = 'Экспортировать горячие клавиши не удалось.';
			BXHotKeys.MesImpFalse = 'Импортировать горячие клавиши не удалось.';
			BXHotKeys.MesImpSuc = 'Успешно импортировано горячих клавиш: ';
			BXHotKeys.MesImpHeader = 'Импорт горячих клавиш';
			BXHotKeys.MesFileEmpty = 'Для импорта горячих клавиш необходимо указать файл.';
			BXHotKeys.MesDelete = 'Удалить';
			BXHotKeys.MesChooseFile = 'Выбрать файл';
			BXHotKeys.uid = 1;
			</script><script type="text/javascript">BX.ready(function(){var BXST_MESS =
{
	Public : "общий",
	Personal : "личный",
	Close : "Отклеить",
	Collapse : "Свернуть",
	UnCollapse : "Восстановить",
	UnCollapseTitle : "Двойной клик для восстановления стикера",
	SetMarkerArea : "Привязать к области на странице",
	SetMarkerEl : "Привязать к элементу на странице",
	Color : "цвет",
	Add : "добавить",
	PersonalTitle : "Только вы можете видеть этот стикер. Нажмите, чтобы изменить.",
	PublicTitle : "Этот стикер могут видеть другие. Нажмите, чтобы изменить.",
	CursorHint : "Выделите область",
	Yellow : "Желтый",
	Green : "Зеленый",
	Blue : "Синий",
	Red : "Красный",
	Purple : "Пурпурный",
	Gray : "Серый",
	StickerListTitle : "Список стикеров",
	CompleteLabel : "выполнено",
	DelConfirm : "Вы уверены, что хотите удалить выбранные стикеры?",
	CloseConfirm : "Этот стикер наклеил #USER_NAME# Вы уверенны, что хотите его отклеить?",
	Complete : "Пометить как завершенное",
	CloseNotify : "Вы можете вернуть отклеенные стикеры на экран из #LINK#списка стикеров#LINK#."
}; window.oBXSticker = new BXSticker({'access':'W','sessid_get':'sessid=ca1c4050ba04585a6c287526ad531818','start_width':'350','start_height':'200','min_width':'280','min_height':'160','start_color':'0','zIndex':'5000','curUserName':'Владислав1 Воробьёв','curUserId':'1','pageUrl':'/menu/','pageTitle':'Меню кафе','bShowStickers':false,'listWidth':'800','listHeight':'450','listNaviSize':'5','useHotkeys':false,'filterParams':{'type':'all','colors':'all','status':'opened','page':'all'},'bHideBottom':true,'focusOnSticker':'0','strDate':'21 мая','curPageCount':'0','site_id':'s1'}, [], BXST_MESS);});</script>
<script type="text/javascript">
							function hideMpAnswer(el, module)
							{
								if(el.parentNode.parentNode.parentNode)
									BX.hide(el.parentNode.parentNode.parentNode);
									BX.ajax({
										'method': 'POST',
										'dataType': 'html',
										'url': '/bitrix/admin/partner_modules.php',
										'data': 'module='+module+'&sessid=ca1c4050ba04585a6c287526ad531818&act=unnotify',
										'async': true,
										'processData': false

									});
							}
							</script>
<script type="text/javascript">
							function hideMpAnswer(el, module)
							{
								if(el.parentNode.parentNode.parentNode)
									BX.hide(el.parentNode.parentNode.parentNode);
									BX.ajax({
										'method': 'POST',
										'dataType': 'html',
										'url': '/bitrix/admin/partner_modules.php',
										'data': 'module='+module+'&sessid=ca1c4050ba04585a6c287526ad531818&act=unnotify',
										'async': true,
										'processData': false

									});
							}
							</script>
<script type="text/javascript">
							function hideMpAnswer(el, module)
							{
								if(el.parentNode.parentNode.parentNode)
									BX.hide(el.parentNode.parentNode.parentNode);
									BX.ajax({
										'method': 'POST',
										'dataType': 'html',
										'url': '/bitrix/admin/partner_modules.php',
										'data': 'module='+module+'&sessid=ca1c4050ba04585a6c287526ad531818&act=unnotify',
										'async': true,
										'processData': false

									});
							}
							</script>
<script type="text/javascript">
	BX.ready( function(){BX.adminInformer.Init(6); } );
</script><script>
	BX.ready(function () {
		window.obj_catalog_menu_LkGdQn = new BX.Main.MenuComponent.CatalogHorizontal('catalog_menu_LkGdQn', {'1430168220':{'PICTURE':'','DESC':''},'2444424269':{'PICTURE':'','DESC':''},'2782047071':{'PICTURE':'','DESC':''},'3534752720':{'PICTURE':'','DESC':''},'4176003167':{'PICTURE':'','DESC':''},'1270155113':{'PICTURE':'','DESC':''},'36229728':{'PICTURE':'','DESC':''}});
	});
</script><script>
var bx_basketFKauiI = new BitrixSmallCart;
</script>
<script>
        function clear_basket(event) {
            event.preventDefault();
            $.get(
                '/ajax/clear_basket.php', {},
                function(result) {

                });

        }
        </script>
<script>
BX.ready(function() {
    bx_basketFKauiI.fixCart();
});
</script>
<script type="text/javascript">
	bx_basketFKauiI.siteId       = 's1';
	bx_basketFKauiI.cartId       = 'bx_basketFKauiI';
	bx_basketFKauiI.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketFKauiI.templateName = 'bootstrap_v5';
	bx_basketFKauiI.arParams     =  {'PATH_TO_BASKET':'/personal/cart/','PATH_TO_PERSONAL':'/personal/','SHOW_PERSONAL_LINK':'Y','SHOW_NUM_PRODUCTS':'Y','SHOW_TOTAL_PRICE':'Y','SHOW_PRODUCTS':'Y','POSITION_FIXED':'N','SHOW_AUTHOR':'Y','PATH_TO_REGISTER':'/login/','PATH_TO_PROFILE':'/personal/','COMPONENT_TEMPLATE':'bootstrap_v5','PATH_TO_ORDER':'/personal/order/make/','SHOW_EMPTY_VALUES':'Y','PATH_TO_AUTHORIZE':'/login/','SHOW_REGISTRATION':'Y','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'N','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','HIDE_ON_BASKET_PAGES':'Y','CACHE_TYPE':'A','POSITION_VERTICAL':'top','POSITION_HORIZONTAL':'right','MAX_IMAGE_SIZE':'70','AJAX':'N','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_PERSONAL':'/personal/','~SHOW_PERSONAL_LINK':'Y','~SHOW_NUM_PRODUCTS':'Y','~SHOW_TOTAL_PRICE':'Y','~SHOW_PRODUCTS':'Y','~POSITION_FIXED':'N','~SHOW_AUTHOR':'Y','~PATH_TO_REGISTER':'/login/','~PATH_TO_PROFILE':'/personal/','~COMPONENT_TEMPLATE':'bootstrap_v5','~PATH_TO_ORDER':'/personal/order/make/','~SHOW_EMPTY_VALUES':'Y','~PATH_TO_AUTHORIZE':'/login/','~SHOW_REGISTRATION':'Y','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'N','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~HIDE_ON_BASKET_PAGES':'Y','~CACHE_TYPE':'A','~POSITION_VERTICAL':'top','~POSITION_HORIZONTAL':'right','~MAX_IMAGE_SIZE':'70','~AJAX':'N','cartId':'bx_basketFKauiI'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketFKauiI.closeMessage = 'Скрыть';
	bx_basketFKauiI.openMessage  = 'Раскрыть';
	bx_basketFKauiI.activate();
</script><script>
var bx_basketT0kNhm = new BitrixSmallCart;
</script>
<script>
        function clear_basket(event) {
            event.preventDefault();
            $.get(
                '/ajax/clear_basket.php', {},
                function(result) {

                });

        }
        </script>
<script>
BX.ready(function() {
    bx_basketT0kNhm.fixCart();
});
</script>
<script type="text/javascript">
	bx_basketT0kNhm.siteId       = 's1';
	bx_basketT0kNhm.cartId       = 'bx_basketT0kNhm';
	bx_basketT0kNhm.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketT0kNhm.templateName = 'bootstrap_v5';
	bx_basketT0kNhm.arParams     =  {'PATH_TO_BASKET':'/personal/cart/','PATH_TO_PERSONAL':'/personal/','SHOW_PERSONAL_LINK':'Y','SHOW_NUM_PRODUCTS':'Y','SHOW_TOTAL_PRICE':'Y','SHOW_PRODUCTS':'Y','POSITION_FIXED':'N','SHOW_AUTHOR':'Y','PATH_TO_REGISTER':'/login/','PATH_TO_PROFILE':'/personal/','COMPONENT_TEMPLATE':'bootstrap_v5','PATH_TO_ORDER':'/personal/order/make/','SHOW_EMPTY_VALUES':'Y','PATH_TO_AUTHORIZE':'/login/','SHOW_REGISTRATION':'Y','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'N','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','HIDE_ON_BASKET_PAGES':'Y','MAX_IMAGE_SIZE':'300','CACHE_TYPE':'A','POSITION_VERTICAL':'top','POSITION_HORIZONTAL':'right','AJAX':'N','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_PERSONAL':'/personal/','~SHOW_PERSONAL_LINK':'Y','~SHOW_NUM_PRODUCTS':'Y','~SHOW_TOTAL_PRICE':'Y','~SHOW_PRODUCTS':'Y','~POSITION_FIXED':'N','~SHOW_AUTHOR':'Y','~PATH_TO_REGISTER':'/login/','~PATH_TO_PROFILE':'/personal/','~COMPONENT_TEMPLATE':'bootstrap_v5','~PATH_TO_ORDER':'/personal/order/make/','~SHOW_EMPTY_VALUES':'Y','~PATH_TO_AUTHORIZE':'/login/','~SHOW_REGISTRATION':'Y','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'N','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~HIDE_ON_BASKET_PAGES':'Y','~MAX_IMAGE_SIZE':'300','~CACHE_TYPE':'A','~POSITION_VERTICAL':'top','~POSITION_HORIZONTAL':'right','~AJAX':'N','cartId':'bx_basketT0kNhm'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketT0kNhm.closeMessage = 'Скрыть';
	bx_basketT0kNhm.openMessage  = 'Раскрыть';
	bx_basketT0kNhm.activate();
</script>

<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'703','NAME':'Телятина под соусом «бальзамико»','DETAIL_PAGE_URL':'/menu/telyatina-pod-sousom-balzamiko/','PICT':{'ID':'1200','SRC':'/upload/iblock/390/390f84d9021e7ec584645b2020a453d7.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'490','UNROUND_PRICE':'490','BASE_PRICE':'490','PRICE':'490','ID':'387','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'490 &#8381;','RATIO_BASE_PRICE':'490','PRINT_RATIO_BASE_PRICE':'490 &#8381;','PRINT_PRICE':'490 &#8381;','RATIO_PRICE':'490','PRINT_RATIO_PRICE':'490 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'673':{'ID':'673','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'703'}},'ITEM_MEASURE_RATIO_SELECTED':'673','MORE_PHOTO':[{'ID':'1200','SRC':'/upload/iblock/390/390f84d9021e7ec584645b2020a453d7.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'704','NAME':'Свинина с овощами','DETAIL_PAGE_URL':'/menu/svinina-s-ovoshchami/','PICT':{'ID':'1202','SRC':'/upload/iblock/d40/d4004c120b9aa07b03b68f5c50b76692.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'450','UNROUND_PRICE':'450','BASE_PRICE':'450','PRICE':'450','ID':'388','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'450 &#8381;','RATIO_BASE_PRICE':'450','PRINT_RATIO_BASE_PRICE':'450 &#8381;','PRINT_PRICE':'450 &#8381;','RATIO_PRICE':'450','PRINT_RATIO_PRICE':'450 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'674':{'ID':'674','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'704'}},'ITEM_MEASURE_RATIO_SELECTED':'674','MORE_PHOTO':[{'ID':'1202','SRC':'/upload/iblock/d40/d4004c120b9aa07b03b68f5c50b76692.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'705','NAME':'Свинина с картофелем','DETAIL_PAGE_URL':'/menu/svinina-s-kartofelem/','PICT':{'ID':'1204','SRC':'/upload/iblock/370/370d47255616e7c2293e9a19d20f38bd.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'430','UNROUND_PRICE':'430','BASE_PRICE':'430','PRICE':'430','ID':'389','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'430 &#8381;','RATIO_BASE_PRICE':'430','PRINT_RATIO_BASE_PRICE':'430 &#8381;','PRINT_PRICE':'430 &#8381;','RATIO_PRICE':'430','PRINT_RATIO_PRICE':'430 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'675':{'ID':'675','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'705'}},'ITEM_MEASURE_RATIO_SELECTED':'675','MORE_PHOTO':[{'ID':'1204','SRC':'/upload/iblock/370/370d47255616e7c2293e9a19d20f38bd.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'706','NAME':'Куриное бедро с рисом под соусом «терияки»','DETAIL_PAGE_URL':'/menu/kurinoe-bedro-s-risom-pod-sousom-teriyaki/','PICT':{'ID':'1206','SRC':'/upload/iblock/cc1/cc1f5a392d252810e2d662c3699b2eb0.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'260','UNROUND_PRICE':'260','BASE_PRICE':'260','PRICE':'260','ID':'390','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'260 &#8381;','RATIO_BASE_PRICE':'260','PRINT_RATIO_BASE_PRICE':'260 &#8381;','PRINT_PRICE':'260 &#8381;','RATIO_PRICE':'260','PRINT_RATIO_PRICE':'260 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'676':{'ID':'676','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'706'}},'ITEM_MEASURE_RATIO_SELECTED':'676','MORE_PHOTO':[{'ID':'1206','SRC':'/upload/iblock/cc1/cc1f5a392d252810e2d662c3699b2eb0.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'707','NAME':'Вок с курицей','DETAIL_PAGE_URL':'/menu/vok-s-kuritsey/','PICT':{'ID':'1208','SRC':'/upload/iblock/93c/93cb67c1e5647b40f61a352dfd8b91be.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'210','UNROUND_PRICE':'210','BASE_PRICE':'210','PRICE':'210','ID':'391','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'210 &#8381;','RATIO_BASE_PRICE':'210','PRINT_RATIO_BASE_PRICE':'210 &#8381;','PRINT_PRICE':'210 &#8381;','RATIO_PRICE':'210','PRINT_RATIO_PRICE':'210 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'677':{'ID':'677','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'707'}},'ITEM_MEASURE_RATIO_SELECTED':'677','MORE_PHOTO':[{'ID':'1208','SRC':'/upload/iblock/93c/93cb67c1e5647b40f61a352dfd8b91be.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'708','NAME':'Вок с говядиной','DETAIL_PAGE_URL':'/menu/vok-s-govyadinoy/','PICT':{'ID':'1210','SRC':'/upload/iblock/143/1431bf3c13dd3f6f5d55a45fc3b6d77b.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'240','UNROUND_PRICE':'240','BASE_PRICE':'240','PRICE':'240','ID':'392','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'240 &#8381;','RATIO_BASE_PRICE':'240','PRINT_RATIO_BASE_PRICE':'240 &#8381;','PRINT_PRICE':'240 &#8381;','RATIO_PRICE':'240','PRINT_RATIO_PRICE':'240 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'678':{'ID':'678','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'708'}},'ITEM_MEASURE_RATIO_SELECTED':'678','MORE_PHOTO':[{'ID':'1210','SRC':'/upload/iblock/143/1431bf3c13dd3f6f5d55a45fc3b6d77b.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'709','NAME':'Чахан с курицей','DETAIL_PAGE_URL':'/menu/chakhan-s-kuritsey/','PICT':{'ID':'1212','SRC':'/upload/iblock/701/701433e74237c9dae71a148c77b5c1c8.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'230','UNROUND_PRICE':'230','BASE_PRICE':'230','PRICE':'230','ID':'393','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'230 &#8381;','RATIO_BASE_PRICE':'230','PRINT_RATIO_BASE_PRICE':'230 &#8381;','PRINT_PRICE':'230 &#8381;','RATIO_PRICE':'230','PRINT_RATIO_PRICE':'230 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'679':{'ID':'679','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'709'}},'ITEM_MEASURE_RATIO_SELECTED':'679','MORE_PHOTO':[{'ID':'1212','SRC':'/upload/iblock/701/701433e74237c9dae71a148c77b5c1c8.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'710','NAME':'Чахан со свининой','DETAIL_PAGE_URL':'/menu/chakhan-so-svininoy/','PICT':{'ID':'1214','SRC':'/upload/iblock/d03/d03a0da47174e32cc0c4f6f2da7cc5e4.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'240','UNROUND_PRICE':'240','BASE_PRICE':'240','PRICE':'240','ID':'394','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'240 &#8381;','RATIO_BASE_PRICE':'240','PRINT_RATIO_BASE_PRICE':'240 &#8381;','PRINT_PRICE':'240 &#8381;','RATIO_PRICE':'240','PRINT_RATIO_PRICE':'240 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'680':{'ID':'680','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'710'}},'ITEM_MEASURE_RATIO_SELECTED':'680','MORE_PHOTO':[{'ID':'1214','SRC':'/upload/iblock/d03/d03a0da47174e32cc0c4f6f2da7cc5e4.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'711','NAME':'Чизкейк сливочный','DETAIL_PAGE_URL':'/menu/chizkeyk-slivochnyy/','PICT':{'ID':'1216','SRC':'/upload/iblock/396/3960fd9b92b2555f32b0ead3071cc867.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'190','UNROUND_PRICE':'190','BASE_PRICE':'190','PRICE':'190','ID':'395','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'190 &#8381;','RATIO_BASE_PRICE':'190','PRINT_RATIO_BASE_PRICE':'190 &#8381;','PRINT_PRICE':'190 &#8381;','RATIO_PRICE':'190','PRINT_RATIO_PRICE':'190 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'681':{'ID':'681','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'711'}},'ITEM_MEASURE_RATIO_SELECTED':'681','MORE_PHOTO':[{'ID':'1216','SRC':'/upload/iblock/396/3960fd9b92b2555f32b0ead3071cc867.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'712','NAME':' Яблочный десерт ','DETAIL_PAGE_URL':'/menu/yablochnyy-desert-/','PICT':{'ID':'1218','SRC':'/upload/iblock/e7e/e7e0fb3b16c3937a90fc569b30aea28b.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'190','UNROUND_PRICE':'190','BASE_PRICE':'190','PRICE':'190','ID':'396','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'190 &#8381;','RATIO_BASE_PRICE':'190','PRINT_RATIO_BASE_PRICE':'190 &#8381;','PRINT_PRICE':'190 &#8381;','RATIO_PRICE':'190','PRINT_RATIO_PRICE':'190 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'682':{'ID':'682','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'712'}},'ITEM_MEASURE_RATIO_SELECTED':'682','MORE_PHOTO':[{'ID':'1218','SRC':'/upload/iblock/e7e/e7e0fb3b16c3937a90fc569b30aea28b.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'713','NAME':'Картофель фри','DETAIL_PAGE_URL':'/menu/kartofel-fri/','PICT':{'ID':'1220','SRC':'/upload/iblock/572/5728c1379a0ffce25212908a29db3e74.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'90','UNROUND_PRICE':'90','BASE_PRICE':'90','PRICE':'90','ID':'397','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'90 &#8381;','RATIO_BASE_PRICE':'90','PRINT_RATIO_BASE_PRICE':'90 &#8381;','PRINT_PRICE':'90 &#8381;','RATIO_PRICE':'90','PRINT_RATIO_PRICE':'90 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'683':{'ID':'683','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'713'}},'ITEM_MEASURE_RATIO_SELECTED':'683','MORE_PHOTO':[{'ID':'1220','SRC':'/upload/iblock/572/5728c1379a0ffce25212908a29db3e74.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'714','NAME':'Наггетсы','DETAIL_PAGE_URL':'/menu/naggetsy/','PICT':{'ID':'1222','SRC':'/upload/iblock/ad6/ad6cfb553b6f1f5e8e5efe43a09cd628.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'190','UNROUND_PRICE':'190','BASE_PRICE':'190','PRICE':'190','ID':'398','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'190 &#8381;','RATIO_BASE_PRICE':'190','PRINT_RATIO_BASE_PRICE':'190 &#8381;','PRINT_PRICE':'190 &#8381;','RATIO_PRICE':'190','PRINT_RATIO_PRICE':'190 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'684':{'ID':'684','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'714'}},'ITEM_MEASURE_RATIO_SELECTED':'684','MORE_PHOTO':[{'ID':'1222','SRC':'/upload/iblock/ad6/ad6cfb553b6f1f5e8e5efe43a09cd628.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'715','NAME':'Кольца кальмара','DETAIL_PAGE_URL':'/menu/koltsa-kalmara/','PICT':{'ID':'1225','SRC':'/upload/iblock/d90/d90b411667fc3513495d2de8e4fa2aea.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'270','UNROUND_PRICE':'270','BASE_PRICE':'270','PRICE':'270','ID':'399','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'270 &#8381;','RATIO_BASE_PRICE':'270','PRINT_RATIO_BASE_PRICE':'270 &#8381;','PRINT_PRICE':'270 &#8381;','RATIO_PRICE':'270','PRINT_RATIO_PRICE':'270 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'685':{'ID':'685','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'715'}},'ITEM_MEASURE_RATIO_SELECTED':'685','MORE_PHOTO':[{'ID':'1225','SRC':'/upload/iblock/d90/d90b411667fc3513495d2de8e4fa2aea.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'716','NAME':'Гренки','DETAIL_PAGE_URL':'/menu/grenki/','PICT':{'ID':'1227','SRC':'/upload/iblock/5f8/5f88a9b162b5c00a19cdeea27b06f25b.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'90','UNROUND_PRICE':'90','BASE_PRICE':'90','PRICE':'90','ID':'400','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'90 &#8381;','RATIO_BASE_PRICE':'90','PRINT_RATIO_BASE_PRICE':'90 &#8381;','PRINT_PRICE':'90 &#8381;','RATIO_PRICE':'90','PRINT_RATIO_PRICE':'90 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'686':{'ID':'686','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'716'}},'ITEM_MEASURE_RATIO_SELECTED':'686','MORE_PHOTO':[{'ID':'1227','SRC':'/upload/iblock/5f8/5f88a9b162b5c00a19cdeea27b06f25b.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
            var ob = new JCCatalogItem({'PRODUCT_TYPE':'1','SHOW_QUANTITY':true,'SHOW_ADD_BASKET_BTN':false,'SHOW_BUY_BTN':true,'SHOW_ABSENT':true,'SHOW_OLD_PRICE':false,'ADD_TO_BASKET_ACTION':'BUY','SHOW_CLOSE_POPUP':false,'SHOW_DISCOUNT_PERCENT':true,'DISPLAY_COMPARE':false,'BIG_DATA':false,'TEMPLATE_THEME':'blue','VIEW_MODE':'CARD','USE_SUBSCRIBE':false,'PRODUCT':{'ID':'717','NAME':'Ассорти закусок','DETAIL_PAGE_URL':'/menu/assorti-zakusok/','PICT':{'ID':'1229','SRC':'/upload/iblock/ae2/ae24535d768f741be1021ec90cd42224.jpg','WIDTH':'800','HEIGHT':'682'},'CAN_BUY':true,'CHECK_QUANTITY':false,'MAX_QUANTITY':'0','STEP_QUANTITY':'1','QUANTITY_FLOAT':false,'ITEM_PRICE_MODE':'S','ITEM_PRICES':[{'UNROUND_BASE_PRICE':'290','UNROUND_PRICE':'290','BASE_PRICE':'290','PRICE':'290','ID':'401','PRICE_TYPE_ID':'1','CURRENCY':'RUB','DISCOUNT':'0','PERCENT':'0','QUANTITY_FROM':'','QUANTITY_TO':'','QUANTITY_HASH':'ZERO-INF','MEASURE_RATIO_ID':'','PRINT_BASE_PRICE':'290 &#8381;','RATIO_BASE_PRICE':'290','PRINT_RATIO_BASE_PRICE':'290 &#8381;','PRINT_PRICE':'290 &#8381;','RATIO_PRICE':'290','PRINT_RATIO_PRICE':'290 &#8381;','PRINT_DISCOUNT':'0 &#8381;','RATIO_DISCOUNT':'0','PRINT_RATIO_DISCOUNT':'0 &#8381;','MIN_QUANTITY':'1'}],'ITEM_PRICE_SELECTED':'0','ITEM_QUANTITY_RANGES':{'ZERO-INF':{'HASH':'ZERO-INF','QUANTITY_FROM':'','QUANTITY_TO':'','SORT_FROM':'0','SORT_TO':'INF'}},'ITEM_QUANTITY_RANGE_SELECTED':'ZERO-INF','ITEM_MEASURE_RATIOS':{'687':{'ID':'687','RATIO':'1','IS_DEFAULT':'Y','PRODUCT_ID':'717'}},'ITEM_MEASURE_RATIO_SELECTED':'687','MORE_PHOTO':[{'ID':'1229','SRC':'/upload/iblock/ae2/ae24535d768f741be1021ec90cd42224.jpg','WIDTH':'800','HEIGHT':'682'}],'MORE_PHOTO_COUNT':'1'},'BASKET':{'ADD_PROPS':true,'QUANTITY':'quantity','PROPS':'prop','EMPTY_PROPS':true,'BASKET_URL':'/personal/cart/','ADD_URL_TEMPLATE':'/menu/?action=ADD2BASKET&id=#ID#','BUY_URL_TEMPLATE':'/menu/?action=BUY&id=#ID#'},'VISUAL':{'ID':'','PICT_ID':'_secondpict','PICT_SLIDER_ID':'_pict_slider','QUANTITY_ID':'_quantity','QUANTITY_UP_ID':'_quant_up','QUANTITY_DOWN_ID':'_quant_down','PRICE_ID':'_price','PRICE_OLD_ID':'_price_old','PRICE_TOTAL_ID':'_price_total','BUY_ID':'_buy_link','BASKET_PROP_DIV':'_basket_prop','BASKET_ACTIONS_ID':'_basket_actions','NOT_AVAILABLE_MESS':'_not_avail','COMPARE_LINK_ID':'_compare_link','SUBSCRIBE_ID':'_subscribe'},'PRODUCT_DISPLAY_MODE':'N','USE_ENHANCED_ECOMMERCE':'N','DATA_LAYER_NAME':'','BRAND_PROPERTY':''});
        </script>
<script>
                $('.popup-call').magnificPopup();
                $(".add-w__habarovsk").click(function() {
                    $(this).parent().find("a").removeClass("active")
                    $(this).addClass("active")
                    $(".menu_cart-address-tab").hide()
                    $(".menu_cart-address-habarovsk").removeClass("d-sm-none")
                    $(".menu_cart-address-komsomolsk").addClass("d-sm-none")
                })
                $(".add-w__komsomolsk").click(function() {
                    $(this).parent().find("a").removeClass("active")
                    $(this).addClass("active")
                    $(".menu_cart-address-tab").hide()
                    $(".menu_cart-address-komsomolsk").removeClass("d-sm-none")
                    $(".menu_cart-address-habarovsk").addClass("d-sm-none")
                })
                </script>
<script>
        $(window).scroll(function() {
            var offset_button = $("#load_more_button").offset();
            if (offset_button != undefined) {
                if ($(window).scrollTop() + 100 >= offset_button.top - $(window).height()) {
                    $("#load_more_button").click();
                }
            }

        });
        </script>
<script>
        BX.message({
            BTN_MESSAGE_BASKET_REDIRECT: 'Перейти в корзину',
            BASKET_URL: '/personal/cart/',
            ADD_TO_BASKET_OK: 'Товар добавлен в корзину',
            TITLE_ERROR: 'Ошибка',
            TITLE_BASKET_PROPS: 'Свойства товара, добавляемые в корзину',
            TITLE_SUCCESSFUL: 'Товар добавлен в корзину',
            BASKET_UNKNOWN_ERROR: 'Неизвестная ошибка при добавлении товара в корзину',
            BTN_MESSAGE_SEND_PROPS: 'Выбрать',
            BTN_MESSAGE_CLOSE: 'Закрыть',
            BTN_MESSAGE_CLOSE_POPUP: 'Продолжить покупки',
            COMPARE_MESSAGE_OK: 'Товар добавлен в список сравнения',
            COMPARE_UNKNOWN_ERROR: 'При добавлении товара в список сравнения произошла ошибка',
            COMPARE_TITLE: 'Сравнение товаров',
            PRICE_TOTAL_PREFIX: 'на сумму',
            RELATIVE_QUANTITY_MANY: '',
            RELATIVE_QUANTITY_FEW: '',
            BTN_MESSAGE_COMPARE_REDIRECT: 'Перейти в список сравнения',
            BTN_MESSAGE_LAZY_LOAD: 'Показать ещё',
            BTN_MESSAGE_LAZY_LOAD_WAITER: 'Загрузка',
            SITE_ID: 's1'
        });
        var obbx_3966226736_1 = new JCCatalogSectionComponent({
            siteId: 's1',
            componentPath: '/bitrix/components/bitrix/catalog.section',
            navParams: {'NavPageCount':'5','NavPageNomer':'1','NavNum':'1'},
            deferredLoad: false, // enable it for deferred load
            initiallyShowHeader: '1',
            bigData: {'enabled':false,'rows':[],'count':'0','rowsRange':[],'shownIds':['703','704','705','706','707','708','709','710','711','712','713','714','715','716','717'],'js':{'cookiePrefix':'BITRIX_SM','cookieDomain':'seniorpomidor.digitalwf.ru','serverTime':'1621594740'},'params':{'uid':'330951dd21d357de4f14e518048a07e7','aid':'5a87679950946de5e129cafc2b7136e7','count':'30','op':'recommend','ib':'7'}},
            lazyLoad: !!'1',
            loadOnScroll: !!'',
            template: 'pomidor_catalog_section.80f3558b6d1c3c69540045b3037168211ef25cb998520d462f158fc6a40d34b0',
            ajaxId: '',
            parameters: 'YToxMTg6e3M6MTE6IklCTE9DS19UWVBFIjtzOjc6ImNhdGFsb2ciO3M6OToiSUJMT0NLX0lEIjtzOjE6IjciO3M6MTg6IkVMRU1FTlRfU09SVF9GSUVMRCI7czo0OiJkZXNjIjtzOjE4OiJFTEVNRU5UX1NPUlRfT1JERVIiO3M6MzoiYXNjIjtzOjE5OiJFTEVNRU5UX1NPUlRfRklFTEQyIjtzOjU6InNob3dzIjtzOjE5OiJFTEVNRU5UX1NPUlRfT1JERVIyIjtzOjM6ImFzYyI7czoxMzoiUFJPUEVSVFlfQ09ERSI7YTo0OntpOjA7czoxMDoiTkVXUFJPRFVDVCI7aToxO3M6MTA6IlNBTEVMRUFERVIiO2k6MjtzOjEyOiJTUEVDSUFMT0ZGRVIiO2k6MztzOjA6IiI7fXM6MjA6IlBST1BFUlRZX0NPREVfTU9CSUxFIjthOjA6e31zOjEzOiJNRVRBX0tFWVdPUkRTIjtzOjE6Ii0iO3M6MTY6Ik1FVEFfREVTQ1JJUFRJT04iO3M6MToiLSI7czoxMzoiQlJPV1NFUl9USVRMRSI7czoxOiItIjtzOjE3OiJTRVRfTEFTVF9NT0RJRklFRCI7czoxOiJOIjtzOjE5OiJJTkNMVURFX1NVQlNFQ1RJT05TIjtzOjE6IlkiO3M6MTA6IkJBU0tFVF9VUkwiO3M6MTU6Ii9wZXJzb25hbC9jYXJ0LyI7czoxNToiQUNUSU9OX1ZBUklBQkxFIjtzOjY6ImFjdGlvbiI7czoxOToiUFJPRFVDVF9JRF9WQVJJQUJMRSI7czoyOiJpZCI7czoxOToiU0VDVElPTl9JRF9WQVJJQUJMRSI7czoxMDoiU0VDVElPTl9JRCI7czoyNToiUFJPRFVDVF9RVUFOVElUWV9WQVJJQUJMRSI7czo4OiJxdWFudGl0eSI7czoyMjoiUFJPRFVDVF9QUk9QU19WQVJJQUJMRSI7czo0OiJwcm9wIjtzOjExOiJGSUxURVJfTkFNRSI7czowOiIiO3M6MTA6IkNBQ0hFX1RZUEUiO3M6MToiQSI7czoxMDoiQ0FDSEVfVElNRSI7czo4OiIzNjAwMDAwMCI7czoxMjoiQ0FDSEVfRklMVEVSIjtzOjE6IlkiO3M6MTI6IkNBQ0hFX0dST1VQUyI7czoxOiJZIjtzOjk6IlNFVF9USVRMRSI7czoxOiJZIjtzOjExOiJNRVNTQUdFXzQwNCI7czowOiIiO3M6MTQ6IlNFVF9TVEFUVVNfNDA0IjtzOjE6IlkiO3M6ODoiU0hPV180MDQiO3M6MToiTiI7czo4OiJGSUxFXzQwNCI7TjtzOjE1OiJESVNQTEFZX0NPTVBBUkUiO3M6MToiTiI7czoxODoiUEFHRV9FTEVNRU5UX0NPVU5UIjtzOjI6IjE1IjtzOjE4OiJMSU5FX0VMRU1FTlRfQ09VTlQiO3M6MToiMyI7czoxMDoiUFJJQ0VfQ09ERSI7YToxOntpOjA7czo0OiJCQVNFIjt9czoxNToiVVNFX1BSSUNFX0NPVU5UIjtzOjE6Ik4iO3M6MTY6IlNIT1dfUFJJQ0VfQ09VTlQiO3M6MToiMSI7czoxNzoiUFJJQ0VfVkFUX0lOQ0xVREUiO3M6MToiWSI7czoyMDoiVVNFX1BST0RVQ1RfUVVBTlRJVFkiO3M6MToiWSI7czoyNDoiQUREX1BST1BFUlRJRVNfVE9fQkFTS0VUIjtzOjE6IlkiO3M6MjY6IlBBUlRJQUxfUFJPRFVDVF9QUk9QRVJUSUVTIjtzOjE6IlkiO3M6MTg6IlBST0RVQ1RfUFJPUEVSVElFUyI7czowOiIiO3M6MTc6IkRJU1BMQVlfVE9QX1BBR0VSIjtzOjE6Ik4iO3M6MjA6IkRJU1BMQVlfQk9UVE9NX1BBR0VSIjtzOjE6IlkiO3M6MTE6IlBBR0VSX1RJVExFIjtzOjEyOiLQotC+0LLQsNGA0YsiO3M6MTc6IlBBR0VSX1NIT1dfQUxXQVlTIjtzOjE6Ik4iO3M6MTQ6IlBBR0VSX1RFTVBMQVRFIjtzOjg6Ii5kZWZhdWx0IjtzOjIwOiJQQUdFUl9ERVNDX05VTUJFUklORyI7czoxOiJOIjtzOjMxOiJQQUdFUl9ERVNDX05VTUJFUklOR19DQUNIRV9USU1FIjtzOjg6IjM2MDAwMDAwIjtzOjE0OiJQQUdFUl9TSE9XX0FMTCI7czoxOiJOIjtzOjIyOiJQQUdFUl9CQVNFX0xJTktfRU5BQkxFIjtzOjE6Ik4iO3M6MTU6IlBBR0VSX0JBU0VfTElOSyI7TjtzOjE3OiJQQUdFUl9QQVJBTVNfTkFNRSI7TjtzOjk6IkxBWllfTE9BRCI7czoxOiJOIjtzOjE4OiJNRVNTX0JUTl9MQVpZX0xPQUQiO047czoxNDoiTE9BRF9PTl9TQ1JPTEwiO3M6MToiTiI7czoyMjoiT0ZGRVJTX0NBUlRfUFJPUEVSVElFUyI7YTozOntpOjA7czoxMToiU0laRVNfU0hPRVMiO2k6MTtzOjEzOiJTSVpFU19DTE9USEVTIjtpOjI7czo5OiJDT0xPUl9SRUYiO31zOjE3OiJPRkZFUlNfRklFTERfQ09ERSI7YToyOntpOjA7czowOiIiO2k6MTtzOjA6IiI7fXM6MjA6Ik9GRkVSU19QUk9QRVJUWV9DT0RFIjthOjQ6e2k6MDtzOjExOiJTSVpFU19TSE9FUyI7aTozO3M6MTA6Ik1PUkVfUEhPVE8iO2k6NDtzOjk6IkFSVE5VTUJFUiI7aTo1O3M6MDoiIjt9czoxNzoiT0ZGRVJTX1NPUlRfRklFTEQiO3M6NToic2hvd3MiO3M6MTc6Ik9GRkVSU19TT1JUX09SREVSIjtzOjM6ImFzYyI7czoxODoiT0ZGRVJTX1NPUlRfRklFTEQyIjtzOjU6InNob3dzIjtzOjE4OiJPRkZFUlNfU09SVF9PUkRFUjIiO3M6MzoiYXNjIjtzOjEyOiJPRkZFUlNfTElNSVQiO3M6MToiMCI7czoxMDoiU0VDVElPTl9JRCI7TjtzOjEyOiJTRUNUSU9OX0NPREUiO047czoxMToiU0VDVElPTl9VUkwiO3M6MjE6Ii9tZW51LyNTRUNUSU9OX0NPREUjLyI7czoxMDoiREVUQUlMX1VSTCI7czozNjoiL21lbnUvI1NFQ1RJT05fQ09ERSMvI0VMRU1FTlRfQ09ERSMvIjtzOjI0OiJVU0VfTUFJTl9FTEVNRU5UX1NFQ1RJT04iO3M6MToiTiI7czoxNjoiQ09OVkVSVF9DVVJSRU5DWSI7czoxOiJOIjtzOjExOiJDVVJSRU5DWV9JRCI7TjtzOjE4OiJISURFX05PVF9BVkFJTEFCTEUiO3M6MToiTiI7czoyNToiSElERV9OT1RfQVZBSUxBQkxFX09GRkVSUyI7czoxOiJOIjtzOjEwOiJMQUJFTF9QUk9QIjthOjM6e2k6MDtzOjM6Ik5FVyI7aToxO3M6NjoiTEVBREVSIjtpOjI7czo3OiJTUEVDSUFMIjt9czoxNzoiTEFCRUxfUFJPUF9NT0JJTEUiO2E6Mjp7aTowO3M6MzoiTkVXIjtpOjE7czo2OiJMRUFERVIiO31zOjE5OiJMQUJFTF9QUk9QX1BPU0lUSU9OIjtzOjk6InRvcC1yaWdodCI7czoxMzoiQUREX1BJQ1RfUFJPUCI7czoxOiItIjtzOjIwOiJQUk9EVUNUX0RJU1BMQVlfTU9ERSI7czoxOiJOIjtzOjIwOiJQUk9EVUNUX0JMT0NLU19PUkRFUiI7czo0NjoicHJpY2UscHJvcHMsc2t1LHF1YW50aXR5TGltaXQscXVhbnRpdHksYnV0dG9ucyI7czoyMDoiUFJPRFVDVF9ST1dfVkFSSUFOVFMiO3M6MTY2OiJbeydWQVJJQU5UJzonMicsJ0JJR19EQVRBJzpmYWxzZX0seydWQVJJQU5UJzonMicsJ0JJR19EQVRBJzpmYWxzZX0seydWQVJJQU5UJzonMicsJ0JJR19EQVRBJzpmYWxzZX0seydWQVJJQU5UJzonMicsJ0JJR19EQVRBJzpmYWxzZX0seydWQVJJQU5UJzonMicsJ0JJR19EQVRBJzpmYWxzZX1dIjtzOjE1OiJFTkxBUkdFX1BST0RVQ1QiO3M6NjoiU1RSSUNUIjtzOjEyOiJFTkxBUkdFX1BST1AiO3M6MDoiIjtzOjExOiJTSE9XX1NMSURFUiI7czoxOiJZIjtzOjE1OiJTTElERVJfSU5URVJWQUwiO3M6NDoiMzAwMCI7czoxNToiU0xJREVSX1BST0dSRVNTIjtzOjE6Ik4iO3M6MTk6Ik9GRkVSX0FERF9QSUNUX1BST1AiO3M6MToiLSI7czoxNjoiT0ZGRVJfVFJFRV9QUk9QUyI7YTo0OntpOjA7czoxMToiU0laRVNfU0hPRVMiO2k6MTtzOjEzOiJTSVpFU19DTE9USEVTIjtpOjI7czo5OiJDT0xPUl9SRUYiO2k6MztzOjA6IiI7fXM6MjA6IlBST0RVQ1RfU1VCU0NSSVBUSU9OIjtzOjE6IlkiO3M6MjE6IlNIT1dfRElTQ09VTlRfUEVSQ0VOVCI7czoxOiJZIjtzOjI1OiJESVNDT1VOVF9QRVJDRU5UX1BPU0lUSU9OIjtzOjEyOiJib3R0b20tcmlnaHQiO3M6MTQ6IlNIT1dfT0xEX1BSSUNFIjtzOjE6Ik4iO3M6MTc6IlNIT1dfTUFYX1FVQU5USVRZIjtzOjE6Ik4iO3M6MjI6Ik1FU1NfU0hPV19NQVhfUVVBTlRJVFkiO3M6MDoiIjtzOjI0OiJSRUxBVElWRV9RVUFOVElUWV9GQUNUT1IiO3M6MDoiIjtzOjI3OiJNRVNTX1JFTEFUSVZFX1FVQU5USVRZX01BTlkiO3M6MDoiIjtzOjI2OiJNRVNTX1JFTEFUSVZFX1FVQU5USVRZX0ZFVyI7czowOiIiO3M6MTI6Ik1FU1NfQlROX0JVWSI7czoxMjoi0JrRg9C/0LjRgtGMIjtzOjIyOiJNRVNTX0JUTl9BRERfVE9fQkFTS0VUIjtzOjE3OiLQkiDQutC+0YDQt9C40L3RgyI7czoxODoiTUVTU19CVE5fU1VCU0NSSUJFIjtzOjIyOiLQn9C+0LTQv9C40YHQsNGC0YzRgdGPIjtzOjE1OiJNRVNTX0JUTl9ERVRBSUwiO3M6MTg6ItCf0L7QtNGA0L7QsdC90LXQtSI7czoxODoiTUVTU19OT1RfQVZBSUxBQkxFIjtzOjI0OiLQndC10YIg0LIg0L3QsNC70LjRh9C40LgiO3M6MTY6Ik1FU1NfQlROX0NPTVBBUkUiO3M6MTg6ItCh0YDQsNCy0L3QtdC90LjQtSI7czoyMjoiVVNFX0VOSEFOQ0VEX0VDT01NRVJDRSI7czoxOiJOIjtzOjE1OiJEQVRBX0xBWUVSX05BTUUiO3M6MDoiIjtzOjE0OiJCUkFORF9QUk9QRVJUWSI7czowOiIiO3M6MTQ6IlRFTVBMQVRFX1RIRU1FIjtzOjQ6InNpdGUiO3M6MTg6IkFERF9TRUNUSU9OU19DSEFJTiI7czoxOiJOIjtzOjIwOiJBRERfVE9fQkFTS0VUX0FDVElPTiI7czozOiJCVVkiO3M6MTY6IlNIT1dfQ0xPU0VfUE9QVVAiO3M6MToiTiI7czoxMjoiQ09NUEFSRV9QQVRIIjtzOjE0OiIvbWVudS9jb21wYXJlLyI7czoxMjoiQ09NUEFSRV9OQU1FIjtOO3M6MTY6IlVTRV9DT01QQVJFX0xJU1QiO3M6MToiWSI7czoxNjoiQkFDS0dST1VORF9JTUFHRSI7czoxOiItIjtzOjE1OiJDT01QQVRJQkxFX01PREUiO3M6MToiTiI7czoyODoiRElTQUJMRV9JTklUX0pTX0lOX0NPTVBPTkVOVCI7czoxOiJOIjtzOjE3OiJDVVJSRU5UX0JBU0VfUEFHRSI7czo2OiIvbWVudS8iO3M6MTE6IlBBUkVOVF9OQU1FIjtzOjE0OiJiaXRyaXg6Y2F0YWxvZyI7czoyMDoiUEFSRU5UX1RFTVBMQVRFX05BTUUiO3M6MTI6Im1lbnUtcG9taWRvciI7czoyMDoiUEFSRU5UX1RFTVBMQVRFX1BBR0UiO3M6ODoic2VjdGlvbnMiO3M6MTM6IkdMT0JBTF9GSUxURVIiO2E6MDp7fX0=.d9ade40f3839f39d9ba8a05e2818dd3f4d7289899e784c19665882b512c8033e',
            container: 'container-1'
        });
        </script>


<script>
			BX.Currency.setCurrencies([{'CURRENCY':'BYN','FORMAT':{'FORMAT_STRING':'# руб.','DEC_POINT':'.','THOUSANDS_SEP':'&nbsp;','DECIMALS':2,'THOUSANDS_VARIANT':'B','HIDE_ZERO':'Y'}},{'CURRENCY':'EUR','FORMAT':{'FORMAT_STRING':'# &euro;','DEC_POINT':'.','THOUSANDS_SEP':'&nbsp;','DECIMALS':2,'THOUSANDS_VARIANT':'B','HIDE_ZERO':'Y'}},{'CURRENCY':'RUB','FORMAT':{'FORMAT_STRING':'# &#8381;','DEC_POINT':'.','THOUSANDS_SEP':'&nbsp;','DECIMALS':2,'THOUSANDS_VARIANT':'B','HIDE_ZERO':'Y'}},{'CURRENCY':'UAH','FORMAT':{'FORMAT_STRING':'# грн.','DEC_POINT':'.','THOUSANDS_SEP':'&nbsp;','DECIMALS':2,'THOUSANDS_VARIANT':'B','HIDE_ZERO':'Y'}},{'CURRENCY':'USD','FORMAT':{'FORMAT_STRING':'$#','DEC_POINT':'.','THOUSANDS_SEP':',','DECIMALS':2,'THOUSANDS_VARIANT':'C','HIDE_ZERO':'Y'}}]);
		</script>
<script>
function clear_message() {
    $('#result_success1').html('');
    $('#result_errors1').html('');
}

function show_message(data) {
    clear_message();
    var s = '';
    if (typeof(data) == 'string') {
        s = data;
    } else if (typeof(data) == 'object') {
        data.forEach(function(item, i, arr) {
            s += item + '<br>';
        });
    }

    $('#result_success1').html(s);
}

function show_error(data) {
    clear_message();
    var s = '';
    if (typeof(data) == 'string') {
        s = data;
    } else if (typeof(data) == 'object') {
        data.forEach(function(item, i, arr) {
            s += item + '<br>';
        });
    }
    $('#result_errors1').html(s);
}

function table_book_form_send(event) {
    event.preventDefault();

    var form = $('#table_book');

    var data = form.serialize();

    event.preventDefault();
    $.ajax({
        url: '/ajax/table_book_form.php',
        method: 'get',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.error == false) {
                $.magnificPopup.close();
                $.magnificPopup.open({
                    items: {
                        src: $('#tnx')
                    },
                    type: 'inline'
                });
            } else {


            }
        },
        error: function(data) {
            show_error('Произошла ошибка. Попробуйте еще раз позднее.');
        }
    });
}
</script><script>
var bx_basketpCwjw4 = new BitrixSmallCart;
</script>
<script type="text/javascript">
	bx_basketpCwjw4.siteId       = 's1';
	bx_basketpCwjw4.cartId       = 'bx_basketpCwjw4';
	bx_basketpCwjw4.ajaxPath     = '/bitrix/components/bitrix/sale.basket.basket.line/ajax.php';
	bx_basketpCwjw4.templateName = 'bootstrap_v4';
	bx_basketpCwjw4.arParams     =  {'PATH_TO_BASKET':'/personal/cart/','PATH_TO_PERSONAL':'/personal/','SHOW_PERSONAL_LINK':'N','SHOW_NUM_PRODUCTS':'Y','SHOW_TOTAL_PRICE':'Y','SHOW_PRODUCTS':'N','POSITION_FIXED':'Y','POSITION_HORIZONTAL':'right','POSITION_VERTICAL':'bottom','SHOW_AUTHOR':'Y','PATH_TO_REGISTER':'/login/','PATH_TO_PROFILE':'/personal/','CACHE_TYPE':'A','PATH_TO_ORDER':'/personal/order/make/','HIDE_ON_BASKET_PAGES':'Y','SHOW_EMPTY_VALUES':'Y','SHOW_REGISTRATION':'Y','PATH_TO_AUTHORIZE':'/login/','SHOW_DELAY':'Y','SHOW_NOTAVAIL':'Y','SHOW_IMAGE':'Y','SHOW_PRICE':'Y','SHOW_SUMMARY':'Y','MAX_IMAGE_SIZE':'70','AJAX':'N','~PATH_TO_BASKET':'/personal/cart/','~PATH_TO_PERSONAL':'/personal/','~SHOW_PERSONAL_LINK':'N','~SHOW_NUM_PRODUCTS':'Y','~SHOW_TOTAL_PRICE':'Y','~SHOW_PRODUCTS':'N','~POSITION_FIXED':'Y','~POSITION_HORIZONTAL':'right','~POSITION_VERTICAL':'bottom','~SHOW_AUTHOR':'Y','~PATH_TO_REGISTER':'/login/','~PATH_TO_PROFILE':'/personal/','~CACHE_TYPE':'A','~PATH_TO_ORDER':'/personal/order/make/','~HIDE_ON_BASKET_PAGES':'Y','~SHOW_EMPTY_VALUES':'Y','~SHOW_REGISTRATION':'Y','~PATH_TO_AUTHORIZE':'/login/','~SHOW_DELAY':'Y','~SHOW_NOTAVAIL':'Y','~SHOW_IMAGE':'Y','~SHOW_PRICE':'Y','~SHOW_SUMMARY':'Y','~MAX_IMAGE_SIZE':'70','~AJAX':'N','cartId':'bx_basketpCwjw4'}; // TODO \Bitrix\Main\Web\Json::encode
	bx_basketpCwjw4.closeMessage = 'Скрыть';
	bx_basketpCwjw4.openMessage  = 'Раскрыть';
	bx_basketpCwjw4.activate();
</script><script>
	BX.ready(function(){
		var upButton = document.querySelector('[data-role="eshopUpButton"]');
		BX.bind(upButton, "click", function(){
			var windowScroll = BX.GetWindowScrollPos();
			(new BX.easing({
				duration : 500,
				start : { scroll : windowScroll.scrollTop },
				finish : { scroll : 0 },
				transition : BX.easing.makeEaseOut(BX.easing.transitions.quart),
				step : function(state){
					window.scrollTo(0, state.scroll);
				},
				complete: function() {
				}
			})).animate();
		})
	});
</script>


</body>
</html>
