<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<div class="section section_t-offset">
    <div class="page-header page-header_b-offset">
        <h2 class="section__title mb-md-0">Поделитесь своим мнением с&nbsp;другими клиентами</h2>
        <a href="/menu-devilery/" class="btn btn_default btn_green_bordered">Вернуться в меню доставки</a>
    </div>
</div>
<?$APPLICATION->IncludeComponent('bitrix:news.list','reviews',Array(
	'AJAX_MODE' => 'N',
	'IBLOCK_TYPE' => 'content',
	'IBLOCK_ID' => get_iblock_id_by_code('reviews'),
	'NEWS_COUNT' => '2',
	'SORT_BY1' => 'SORT',
	'SORT_ORDER1' => 'DESC',
	'SORT_BY2' => 'ID',
	'SORT_ORDER2' => 'ASC',
	'FILTER_NAME' => '',
	'FIELD_CODE' => ['PREVIEW_TEXT', 'DETAIL_TEXT', 'PREVIEW_PICTURE'],
	'PROPERTY_CODE' => [],
	'CHECK_DATES' => 'Y',
	'DETAIL_URL' => '',
	'PREVIEW_TRUNCATE_LEN' => '',
	'ACTIVE_DATE_FORMAT' => 'd.m.Y',
	'SET_TITLE' => 'N',
	'SET_BROWSER_TITLE' => 'N',
	'SET_META_KEYWORDS' => 'N',
	'SET_META_DESCRIPTION' => 'N',
	'SET_LAST_MODIFIED' => 'Y',
	'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
	'ADD_SECTIONS_CHAIN' => 'N',
	'HIDE_LINK_WHEN_NO_DETAIL' => 'Y',
	'PARENT_SECTION' => '',
	'PARENT_SECTION_CODE' => '',
	'INCLUDE_SUBSECTIONS' => 'Y',
	'CACHE_TYPE' => 'A',
	'CACHE_TIME' => '3600',
	'CACHE_FILTER' => 'Y',
	'CACHE_GROUPS' => 'Y',
	'DISPLAY_TOP_PAGER' => 'N',
	'DISPLAY_BOTTOM_PAGER' => 'Y',
	'PAGER_TEMPLATE' => 'dots',
	'SET_STATUS_404' => 'N',
	'SHOW_404' => 'N',
	'MESSAGE_404' => '',
	'AJAX_OPTION_JUMP' => 'N',
	'AJAX_OPTION_STYLE' => 'Y',
	'AJAX_OPTION_HISTORY' => 'N',
	'AJAX_OPTION_ADDITIONAL' => ''
)
);?> 
<br><br>
<div class="line-w line-w-big line-w_noflex line-w_grey section_t-offset mb-3">
    <div href="#" class="line-w__pic window_s-offset"
        style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/bg.png'); width:100%; display: block;">
        <div class="line-w__header">
            Если вам понравилось обслуживание в&nbsp;нашем кафе, отсканируйте <nobr>qr-код,</nobr> указанный
            на&nbsp;чеке, и&nbsp;оставьте свой отзыв или чаевые официанту.
        </div>
        <a href="#popup-review" class="btn btn_default btn_green popup-call">Оставить отзыв</a>
    </div>
</div>

<div id="popup-review" class="mfp-hide popup-block">
    <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
	"AREA_FILE_SHOW" => "file",
	"PATH" => '/include/_forms/add_review_form.php',
	"EDIT_TEMPLATE" => "standard.php"
	)
	);?>  
</div>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>