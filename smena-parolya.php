<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(" ");
?>

<form id="restore_passwd_form" class="form form-middle c" onsubmit="restore_passwd_form_send(event)">
    <h2 class="h2 form-middle__title">Восстановить пароль</h2>
	<input type="hidden" name="action" value="need_restore">
    <div class="form__item form__item_s-offset">
        <input type="email" class="input input_default input_yellow " placeholder="Email" name="email" required>
    </div>
	<span id="success_message" style="color: green;"></span>
    <span id="error_message" style="color: red;"></span>
    <div class="form__item form__item_s-offset">
        <button class="btn btn_large btn_default btn_yellow">
            Продолжить
        </button>
    </div>
	
    <div class="form-nav">
        <a href="/personal/" class="form-nav__link">
            Вход
        </a>
        <a href="/registratsiya.php" class="form-nav__link">
            Регистрация
        </a>
    </div>

</form>

<script>

function restore_passwd_form_send(event) {
    event.preventDefault();

    var form = $('#restore_passwd_form');

    var data = form.serialize();

    event.preventDefault();
    $.ajax({
        url: '/ajax/forgot_password.php',
        method: 'get',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.error == false) {
                form_show_success(form, 'Ссылка для восстановления пароля отправлена на ваш Email.');
            } else {
                form_show_error(form, data.errors);

            }
        },
        error: function(data) {
            form_show_error(form, 'Произошла ошибка. Попробуйте пожалуйста позже.');
        }
    });
}

</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>