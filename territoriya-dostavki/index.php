<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Территория доставки");
?> <?if( get_city() == "khabarovsk"):?>
             <div class="box box_b-large">
              <div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps/76/khabarovsk/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Хабаровск</a><a href="https://yandex.ru/maps/76/khabarovsk/?ll=135.155051%2C48.452490&mode=usermaps&source=constructorLink&um=constructor%3A16a7af615cdd1cd70051bd49034ed64385164c74ce4e88641cb3b348a06cde00&utm_medium=mapframe&utm_source=maps&z=11" style="color:#eee;font-size:12px;position:absolute;top:14px;">Яндекс.Карты — поиск мест и адресов, городской транспорт</a><iframe src="https://yandex.ru/map-widget/v1/-/CCQ~fQuMoC" width="100%" height="500" frameborder="0" allowfullscreen="true" style="position:relative;"></iframe></div>
             </div>
            <?endif;?>
            <? if( get_city() == "komsomolsk-na-amure"):?>
              <div class="box box_b-large">
               <div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps/11453/komsomolsk-at-amur/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Комсомольск‑на‑Амуре</a><a href="https://yandex.ru/maps/11453/komsomolsk-at-amur/?ll=137.100832%2C50.577995&mode=usermaps&source=constructorLink&um=constructor%3A988eb57d919e4f18d8892d22e2aaf9b358b0a98b9522964159b84320221f5ad2&utm_medium=mapframe&utm_source=maps&z=12" style="color:#eee;font-size:12px;position:absolute;top:14px;">Яндекс.Карты — поиск мест и адресов, городской транспорт</a><iframe src="https://yandex.ru/map-widget/v1/-/CCUA74uUKB" width="100%" height="500" frameborder="0" allowfullscreen="true" style="position:relative;"></iframe></div>
            </div>
            <?endif;?>










    <script>
        AOS.init();
    </script>

    <script>
        //Переменная для включения/отключения индикатора загрузки
        var spinner = $('.ymap-container').children('.loader');
        //Переменная для определения была ли хоть раз загружена Яндекс.Карта (чтобы избежать повторной загрузки при наведении)
        var check_if_load = false;
        //Необходимые переменные для того, чтобы задать координаты на Яндекс.Карте
        var myMapTemp, myPlacemarkTemp;

        //Функция создания карты сайта и затем вставки ее в блок с идентификатором &#34;map-yandex&#34;
        function init() {
            var myMapTemp = new ymaps.Map("map", {
                center: [43.495405, 39.909962],
                zoom: 17,
                controls: ['zoomControl', 'fullscreenControl']
            });
            var myPlacemarkTemp = new ymaps.Placemark([43.495405, 39.909962], {
                balloonContent: "Отдел продаж",
            }, {
                iconLayout: 'default#imageWithContent',
                iconImageHref: '/upload/iblock/68e/68e423ce5420cbfe59550589697de353.png',
                iconImageSize: [67, 82],
                iconImageOffset: [-25, -50],
            });
            myMapTemp.geoObjects.add(myPlacemarkTemp);
            var myPlacemarkTemp = new ymaps.Placemark([43.495190208447, 39.910128296959], {
                balloonContent: "ЖК Летний",
            }, {
                iconLayout: 'default#imageWithContent',
                iconImageHref: '/upload/iblock/1f3/1f3c69370ad8e950414b413bac8dfd26.png',
                iconImageSize: [67, 82],
                iconImageOffset: [-25, -50],
            });
            myMapTemp.geoObjects.add(myPlacemarkTemp);



            var layer = myMapTemp.layers.get(0).get(0);

            // Решение по callback-у для определения полной загрузки карты
            waitForTilesLoad(layer).then(function() {
                // Скрываем индикатор загрузки после полной загрузки карты
                spinner.removeClass('is-active');
            });
        }

        // Функция для определения полной загрузки карты (на самом деле проверяется загрузка тайлов)
        function waitForTilesLoad(layer) {
            return new ymaps.vow.Promise(function(resolve, reject) {
                var tc = getTileContainer(layer),
                    readyAll = true;
                tc.tiles.each(function(tile, number) {
                    if (!tile.isReady()) {
                        readyAll = false;
                    }
                });
                if (readyAll) {
                    resolve();
                } else {
                    tc.events.once("ready", function() {
                        resolve();
                    });
                }
            });
        }

        function getTileContainer(layer) {
            for (var k in layer) {
                if (layer.hasOwnProperty(k)) {
                    if (
                        layer[k] instanceof ymaps.layer.tileContainer.CanvasContainer ||
                        layer[k] instanceof ymaps.layer.tileContainer.DomContainer
                    ) {
                        return layer[k];
                    }
                }
            }
            return null;
        }

        // Функция загрузки API Яндекс.Карт по требованию (в нашем случае при наведении)
        function loadScript(url, callback) {
            var script = document.createElement("script");

            if (script.readyState) { // IE
                script.onreadystatechange = function() {
                    if (script.readyState == "loaded" ||
                        script.readyState == "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // Другие браузеры
                script.onload = function() {
                    callback();
                };
            }
            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        }

        // Основная функция, которая проверяет когда мы навели на блок с классом &#34;ymap-container&#34;
        var ymap = function() {
            $('.ymap-container').mouseenter(function() {
                if (!check_if_load) { // проверяем первый ли раз загружается Яндекс.Карта, если да, то загружаем

                    // Чтобы не было повторной загрузки карты, мы изменяем значение переменной
                    check_if_load = true;

                    // Показываем индикатор загрузки до тех пор, пока карта не загрузится
                    spinner.addClass('is-active');

                    // Загружаем API Яндекс.Карт
                    loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1", function() {
                        // Как только API Яндекс.Карт загрузились, сразу формируем карту и помещаем в блок с идентификатором &#34;map-yandex&#34;
                        ymaps.load(init);
                    });
                }
            });
        }

        $(function() {
            ymap();
        });
    </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>