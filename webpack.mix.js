let mix = require('laravel-mix');

mix.js('personal/order/make/app/script.js', 'personal/order/make').vue();
mix.js('personal/cart/app/script.js', 'personal/cart').vue();